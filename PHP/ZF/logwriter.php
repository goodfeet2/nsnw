<?php
date_default_timezone_set('America/Los_Angeles');
ini_set("log_errors", 1);
ini_set("error_log", "/home/bitnami/logs/php-error.log");

/**
 * The LogWriter class is intended to allow creation and update of a .log file based on date
 */
class LogWriter {
	// declare log file and file pointer as private properties
	private $file_path, $file_pointer;
	function __construct() {
		$str_log_date = date("Y_m_d"); // YYYY_MM_DD ex. 2021_11_22
		$this->filename = $str_log_date."__logfile.log";
		$this->file_path = '/home/bitnami/logs/'.$this->filename;
	}
	// close log file (it's always a good idea to close a file when you're done with it)
	public function lclose() { // ---------------------------------------------------------------------------------------- commented for server maintenance
		// if(isset($this->file_pointer)) {
		// 	// close the file
		// 	$tmp = file_get_contents($this->file_path);
		// 	fclose($this->file_pointer);
		// }
	}
	// write to a local file instead of the database
	public function lwrite($_source, $_func, $_log) { // ---------------------------------------------------------------------------------------- commented for server maintenance
		// // if file pointer doesn't exist, then open log file
		// $flg_add_comma = true;
		// if (!is_resource($this->file_pointer)) {
		// 	$this->lopen();
		// 	chmod($this->file_path, 0777);
		// }
		// $str_log = '{"timestamp":"'.time().'","source":"'.$_source.'","function":"'.$_func.'","log":'.$_log.'}]}'; // .PHP_EOL
		// // write current time, script name and message to the log file
		// fwrite($this->file_pointer, $str_log);
		// $this->lclose();
	}
	// open log file (private method)
	private function lopen() { // ---------------------------------------------------------------------------------------- commented for server maintenance
		// if (!file_exists('/home/bitnami/logs')) {
		// 	mkdir('/home/bitnami/logs', 0755, true);
		// }
		// if(!file_exists($this->file_path)){
		// 	// when the logfile has not yet been created open it and add the first line to start valid JSON format
		// 	// open log file for writing only and place file pointer at the end of the file
		// 	$this->file_pointer = fopen($this->file_path, 'a') or exit("Can't open ".$this->file_path);
		// 	fwrite($this->file_pointer, '{"filename":"'.$this->filename.'","logs":[');
		// }else{
		// 	// open the file for reading and writing to over-write the last two characters
		// 	$fh = fopen($this->file_path, 'r+') or die("Can't open ".$this->file_path);
		// 	$stat = fstat($fh); // Gets information about a file using an open file pointer
		// 	ftruncate($fh, $stat['size']-2); // Truncates a file to a given length - note: opening the file with the w option will cause the entire file to truncate
		// 	fclose($fh); // close the file so that we can re-open it in append mode
		// 	// open log file for writing only and place file pointer at the end of the file
		// 	$this->file_pointer = fopen($this->file_path, 'a') or exit("Can't open ".$this->file_path);
		// 	fwrite($this->file_pointer, ',');
		// }
	}
}

?>