<?php
// ini_set('display_errors', 1);
error_reporting(E_ALL);
require_once('/home/bitnami/nsconfig.php');
header('Content-Type: application/json');

try{
	// $response = test_send_to_ns();
	// echo $response;

	$request = file_get_contents("php://input");
	$GLOBALS['log']->lwrite('pricing', 'main', '{"request":'.json_encode($request).'}');
	$data = json_decode($request);
	$response = new stdClass();
	// if($data->Header->StoreLogin == "String content"){ $data->success = "true"; } // verify login data
	
	if(isValidLogin($data->Header)){
		/*
		// send input to NS
		// build input to send to NS
		$data_to_ns = new stdClass();
		$data_to_ns->endpoint = 'pricing';
		if(property_exists($data, 'PriceItems')){ $data_to_ns->PriceItems = $data->PriceItems; }
		$ns_response = postNSRec($data_to_ns);
		if($ns_response != null){
			$response = $ns_response;
		}else{
			$response = new stdClass();
			$response->Success = false;
		}
		*/
		$response = new stdClass();
		$response->Success = true;
	}else{
		$response->message = 'Failed - Authentication Not Valid -- '.strCredentials($data->Header);
	}
	$GLOBALS['log']->lwrite('pricing', 'main', '{"response":'.json_encode($response).'}');
	header('Content-Type: application/json');
	echo json_decode(json_encode($response)); // return response for RICS

} catch (Exception $e) {
	var_dump($e);
}
?>