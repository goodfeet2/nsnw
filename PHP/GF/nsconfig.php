<?php

require_once('/home/bitnami/logwriter.php');
$log = new LogWriter();
$remote_addr = isset($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : '127.0.0.1';
$remote_port = isset($_SERVER["REMOTE_PORT"]) ? $_SERVER["REMOTE_PORT"] : '0';
$server_port = isset($_SERVER["SERVER_PORT"]) ? $_SERVER["SERVER_PORT"] : '0';
$log->lwrite('nsconfig', 'main', '{"remote_addr":"'.$remote_addr.'","remote_port":"'.$remote_port.'","server_port":"'.$server_port.'"}');

$live = true;
$script = ($live) ? '1384' : '1136';
$acct = ($live) ? '4942365' : '4942365-sb1';
$str_NS_URL = 'https://'.$acct.'.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script='.$script.'&deploy=1';

function set_NS_TBA_Headers($_ch, $_httpMethod, $_live, $_url){
	$_live = ($_live != null) ? $_live : false;
	$acctId = ($_live === true) ? '4942365' : '4942365-sb1';
	$realm = ($_live === true) ? '4942365' : '4942365_SB1'; //scompid
	$baseUrl = 'https://'.$acctId.'.restlets.api.netsuite.com/app/site/hosting/restlet.nl';
	$_httpMethod = ($_httpMethod != null) ? strtoupper($_httpMethod) : 'GET';
	$consumerKey = ($_live === true) ? '6e6ceb39bed298d5d25705b8f282b7221c6148b1622753c66795ea3a132a48f6' : '6e6ceb39bed298d5d25705b8f282b7221c6148b1622753c66795ea3a132a48f6';
	$consumerSecret = ($_live === true) ? '07338ed4f304f6244397f4562684b54816f972039689273e89faea27fd3e608b' : '07338ed4f304f6244397f4562684b54816f972039689273e89faea27fd3e608b';
	$tokenKey = ($_live === true) ? '9437ce95939e096509551e42023a57e7d31754e9a7b31c8beb3ef52714ab026a' : '72d41bafd450b9140c1b89db86cc124a042f52847896b774628d0eab3f91f19a';
	$tokenSecret = ($_live === true) ? '589ab3c94d7185a1cd57ab00e0ac2da5d3f1f95ae6bf3322846386adb9942a92' : 'ada035d814adef4130ef35ac837cba46289581345f33979475348f1a7506d390';
	$signatureMethod = 'HMAC-SHA256'; // supported HMAC-SHA1 & HMAC-SHA256
	$nonce = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 20);
	$timestamp = time();
	$version = '1.0';
	
	$baseString = restletBaseString($_httpMethod, $_url, $consumerKey, $tokenKey, $nonce, $timestamp, $version, $signatureMethod, null);
	
	$key = rawurlencode($consumerSecret) .'&'. rawurlencode($tokenSecret);
	$signature = base64_encode(hash_hmac('sha256', $baseString, $key, true));
	$oAuthHead = array( 'Authorization: OAuth '
			.'realm="'.rawurlencode($realm).'", '
			.'oauth_consumer_key="'.rawurlencode($consumerKey).'", '
			.'oauth_token="'.rawurlencode($tokenKey).'", '
			.'oauth_signature_method="'.rawurlencode($signatureMethod).'", '
			.'oauth_timestamp="'.rawurlencode($timestamp).'", '
			.'oauth_nonce="'.rawurlencode($nonce).'", '
			.'oauth_version="'.rawurlencode($version).'", '
			.'oauth_signature="'.rawurlencode($signature).'"',
			'Cache-Control: no-cache',
			'Content-Type: application/json');
			
	return $oAuthHead;
}

function restletBaseString($_httpMethod, $url, $consumerKey, $tokenKey, $nonce, $timestamp, $version, $signatureMethod, $postParams){
	//http method must be upper case
	$baseString = strtoupper($_httpMethod) .'&';
	
	//include url without parameters, schema and hostname must be lower case
	if (strpos($url, '?')){
		$baseUrl = substr($url, 0, strpos($url, '?'));
		$getParams = substr($url, strpos($url, '?') + 1);
	} else {
		$baseUrl = $url;
		$getParams = "";
	}
	$hostname = strtolower(substr($baseUrl, 0,  strpos($baseUrl, '/', 10)));
	$path = substr($baseUrl, strpos($baseUrl, '/', 10));
	$baseUrl = $hostname . $path;
	$baseString .= rawurlencode($baseUrl) .'&';
	
	//all oauth and get params. First they are decoded, next alphabetically sorted, next each key and values is encoded and finally whole parameters are encoded
	$params = array();
	$params['oauth_consumer_key'] = array($consumerKey);
	$params['oauth_token'] = array($tokenKey);
	$params['oauth_nonce'] = array($nonce);
	$params['oauth_timestamp'] = array($timestamp);
	$params['oauth_signature_method'] = array($signatureMethod);
	$params['oauth_version'] = array($version);
	
	foreach (explode('&', $getParams ."&". $postParams) as $param) {
		$parsed = explode('=', $param);
		if ($parsed[0] != "") {
			$value = isset($parsed[1]) ? urldecode($parsed[1]): "";
			if (isset($params[urldecode($parsed[0])])) {
				array_push($params[urldecode($parsed[0])], $value);
			} else {
				$params[urldecode($parsed[0])] = array($value);
			}
		}
	}
	
	//all parameters must be alphabetically sorted
	ksort($params);
	
	$paramString = "";
	foreach ($params as $key => $valueArray){
		//all values must be alphabetically sorted
		sort($valueArray);
		foreach ($valueArray as $value){
			$paramString .= rawurlencode($key) . '='. rawurlencode($value) .'&';
		}
	}
	$paramString = substr($paramString, 0, -1);
	$baseString .= rawurlencode($paramString);
	return $baseString;
}

// =======================================================================================================================================================
function test_send_to_ns(){
	$ch = curl_init();
	$url = 'https://4942365-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=1136&deploy=1';
	$ch = setHeaders($ch, "GET", $url);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	$ns_response = curl_exec($ch);
	// var_dump($ns_response);
	return $ns_response;
}

function setHeaders($_ch, $_method, $_url){
	$_oauth_header = set_NS_TBA_Headers($_ch, $_method, $GLOBALS['live'], $_url);
	$headers = $_oauth_header;
	curl_setopt($_ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($_ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
	curl_setopt($_ch, CURLOPT_SSLVERSION, 6);
	return $_ch;
}

function getNSRec($recType, $recRefType, $recRef){
	$GLOBALS['log']->lwrite('nsconfig', 'getNSRec', '{"recType":"'.$recType.'","recRefType":"'.$recRefType.'","recRef":"'.$recRef.'"}');
	$ch = curl_init();
	$ch = setHeaders($ch, "GET", $GLOBALS['str_NS_URL']."&recordtype=".$recType."&".$recRefType."=".$recRef);
	curl_setopt($ch, CURLOPT_URL, $GLOBALS['str_NS_URL']."&recordtype=".$recType."&".$recRefType."=".$recRef);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	$response = sendRequestToNS($ch, "getNSRec()");
	return $response;
}

function postNSRec($record){
	$GLOBALS['log']->lwrite('nsconfig', 'postNSRec', '{"record":'.json_encode($record).'}');
	$strObjectValues = print_r($record, true);
	$response = null;
	$ch = curl_init();
	$ch = setHeaders($ch, "POST", $GLOBALS['str_NS_URL']);
	curl_setopt($ch, CURLOPT_URL, $GLOBALS['str_NS_URL']);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($record));
	$response = sendRequestToNS($ch, "postNSRec()");
	return $response;
}

function putNSRec($record){
	$GLOBALS['log']->lwrite('nsconfig', 'putNSRec', '{"record":'.json_encode($record).'}');
	$strObjectValues = print_r($record, true);
	$response = null;
	$ch = curl_init();
	$ch = setHeaders($ch, "PUT", $GLOBALS['str_NS_URL']);
	curl_setopt($ch, CURLOPT_URL, $GLOBALS['str_NS_URL']);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($record));
	$response = sendRequestToNS($ch, "putNSRec()");
	return $response;
}

function sendRequestToNS($ch, $str_func){
	$currentAttempt = 0;
	$maxAttempts = 2;
	do{
		++$currentAttempt;
		$ns_response = curl_exec($ch);
		flush();
		sleep(10 * $currentAttempt);
		$strObjectValues = print_r($ns_response, true);
		if(isset($ns_response->error) && $ns_response->error->code == "SSS_REQUEST_LIMIT_EXCEEDED"){
			exit();
		}
	}while($currentAttempt <= $maxAttempts && isset($ns_response->error));
	return $ns_response;
}

function isValidLogin($_header){
	$result = false;
	if($_header->StoreLogin == "goodfeetnw_api" && $_header->StorePassword == "iL%d'O6a#'[ti-R(cY$}" && $_header->WebServiceKey == "C14949D4-6D72-4828-9B61-B7E77AD78352"){
		$result = true;
	}
	$log_header = $_header;
	$GLOBALS['log']->lwrite('nsconfig', 'isValidLogin', '{"log_header":'.json_encode($log_header).',"result":"'.($result ? 'TRUE' : 'FALSE').'"}');
	return $result;
}

function strCredentials($_header){
	$result = '';
	$result .= 'StoreLogin: '.$_header->StoreLogin;
	$result .= 'StorePassword: '.$_header->StorePassword;
	$result .= 'WebServiceKey: '.$_header->WebServiceKey;
	return $result;
}

?>
