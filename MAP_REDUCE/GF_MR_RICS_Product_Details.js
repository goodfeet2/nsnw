/**
*@NApiVersion 2.1
*@NScriptType MapReduceScript
*
Script Name:   GF_MR_RICS_Product_Details.js
Author:        Mark Robinson
*/

define(['N/log', 'N/search', 'N/record', 'N/https', 'N/format', 'N/runtime', '/SuiteScripts/GF_LIB/gf_nsrics'], function(log, search, record, https, format, runtime, gf_nsrics) {

    /**
     * In this get input function we make calls to the RICS API to get detail data on all products that have a SKU and UPC.
     * @param {*} context - this variable is not utilized in this function.
     * @returns - an array containing the data returned from the API calls.
     */
    function rics_getInputData(context){
        var nsrics = gf_nsrics.getIntegration();
        //Get all product details data from RICS with a POST request to: https://enterprise.ricssoftware.com/api/Product/GetProductDetails
        var scriptObj = runtime.getCurrentScript(); // scriptObj.getRemainingUsage()
        var param_rics_skip = scriptObj.getParameter({ name: 'custscript_rics_skip' });
        var num_skip = (param_rics_skip != null) ? param_rics_skip : 0; // get a parameter of num_skip to start from on next run
        var param_rics_sku = scriptObj.getParameter({ name: 'custscript_rics_pd_sku' });
        var str_sku = (param_rics_sku != null) ? param_rics_sku : '%'; // get a parameter of sku to find use % if not set
        var arr_ricsProducts = [];
        var flg_run_complete = false;
        try {
            do{
                var max_attempts = 2;
                var current_attempt = 0;
                do{
                    var getProductDetails = nsrics.baseurl + nsrics.getEndPoint('GetProductDetails');
                    var header = [];
                    header['Content-Type'] = 'application/json';
                    header['Token'] = nsrics.getActiveConnection();
                    var postData = { "UPC": "%", "SKU": str_sku, "Skip":num_skip };
                    postData = JSON.stringify(postData);
                    nsrics.sleep(1000); // let the connection to the RICS system breathe
                    var response = https.post({
                        url: getProductDetails,
                        headers:header,
                        body: postData
                    });
                    // log.debug({title: 'GF_MR_RICS_Product_Details.js:rics_getInputData', details: 'success: ' + (response['code'] == 200)} ); // did it bail?
                    // log.debug({title: 'GF_MR_RICS_Product_Details.js:rics_getInputData', details: 'body: ' + response.body.toString()} ); // what did we get?
                    var data = JSON.parse(response.body);
                    current_attempt += 1;
                }while(scriptObj.getRemainingUsage() > 50 && current_attempt < max_attempts && response['code'] != 200);
                for(var key in data.Products){
                    arr_ricsProducts.push(data.Products[key]);
                }
                // log.debug({title: 'GF_MR_RICS_Product_Details.js:rics_getInputData', details: 'DATA: ' + JSON.stringify(arr_ricsProducts)} );
                // log.debug({title: 'GF_MR_RICS_Product_Details.js:rics_getInputData', details: 'Total: ' + JSON.stringify(data.ResultStatistics)} );
                num_skip = data.ResultStatistics.EndRecord;
                if(data.ResultStatistics.EndRecord == data.ResultStatistics.TotalRecords){ flg_run_complete = true; }
            }while(scriptObj.getRemainingUsage() > 50 && response['code'] == 200 && (data.ResultStatistics.EndRecord < data.ResultStatistics.TotalRecords));
            log.audit({title: 'GF_MR_RICS_Product_Details.js:rics_getInputData', details: 'ResultStatistics.TotalRecords Count: ' + JSON.stringify(data.ResultStatistics.TotalRecords)} );
            // log.debug({title: 'GF_MR_RICS_Product_Details.js:rics_getInputData', details: 'DATA: ' + JSON.stringify(arr_ricsProducts)} );
            log.debug({title: 'GF_MR_RICS_Product_Details.js:rics_getInputData', details: 'DATA-length: ' + arr_ricsProducts.length} );
            // set a parameter of num_skip to start from on next run
            if(!flg_run_complete){
                log.debug({title: 'GF_MR_RICS_Product_Details.js:rics_getInputData', details: 'scriptObj: ' + JSON.stringify(scriptObj)} );
                var deployment_update_id = record.submitFields({
                    type: 'scriptdeployment',
                    id: '8223',
                    values: {
                        'custscript_rics_skip': num_skip
                    }
                });
            }
            return arr_ricsProducts;
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_Product_Details.js:rics_getInputData', details: 'Error: ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this mapping function we take the data that was passed in from the get input function and create or update the parent custom RICS Product Details record(s).
     * @param {*} context - this should contain the data from the API calls in the get input function.
     * @returns void (note: the const context variable is written to and used in the reduce function)
     */
    function rics_map(context){
        try{
            var data = JSON.parse(context.value);
            var str_sku = data.Sku;
            str_sku = str_sku.toUpperCase();
            var str_sku_end = str_sku.substring(str_sku.length, str_sku.length-4);
            // log.debug({title: 'GF_MR_RICS_Product_Details.js:rics_map', details: 'data: ' + JSON.stringify(data)});
            var parent_NS_ID = 0;
            parent_NS_ID = find_pd_record(data);
            // setup parent
            var productDetails_Record = create_productDetails(data);
            // log.debug({title: 'GF_MR_RICS_Product_Details.js:rics_map', details: 'productDetails_Record: ' + JSON.stringify(productDetails_Record)});
            if(parseInt(parent_NS_ID) == 0){
                // create new record
                var create_parent_item = record.create({
                    type: 'customrecord_rics_product_details'
                });
                for(var prop in productDetails_Record){
                    create_parent_item.setValue(prop,productDetails_Record[prop]);
                }
                parent_NS_ID = create_parent_item.save();
            }else if(parseInt(parent_NS_ID) != -1){
                // update existing record
                var update_parent_item = record.load({
                    type: 'customrecord_rics_product_details',
                    id: parent_NS_ID
                });
                for(var prop in productDetails_Record){
                    update_parent_item.setValue(prop,productDetails_Record[prop]);
                }
                update_parent_item.save();
            }
            data.parent_NS_ID = parent_NS_ID;
            if(data.parent_NS_ID > 0){
                context.write({ key: data.parent_NS_ID, value: data });
            }
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_Product_Details.js:rics_map', details: 'Error: ' + ex.toString() + ' : ' + ex.stack});
            // log.error({title: 'GF_MR_RICS_Product_Details.js:rics_map', details: 'Error: ' + JSON.stringify(data)});
        }
    }

    /**
     * In this reduce function we take the mapped context objects and create or update all of the child custom records that are related to the parent records that were created or updated in the mapping function.
     * @param {*} context - this should be the mapped object containing all of the fields and data that need to be on the custom RICS Product Item record.
     * @returns void
     */
    function rics_reduce(context){
        try{
            // process Product Item (pi) records
            for(var val in context.values){
                var data = JSON.parse(context.values[val]);

                if(data.hasOwnProperty('parent_NS_ID') && data.parent_NS_ID > 0){
                    for(var child in data.ProductItems){
                        var childData = data.ProductItems[child];
                        var child_NS_ID = 0;
                        child_NS_ID = find_pi_record(childData.UPC);
                        var productItem_Record = create_productItem(childData, data.parent_NS_ID);

                        if(parseInt(child_NS_ID) == 0){
                            // create new item
                            var create_child_item = record.create({
                                type: 'customrecord_rics_product_item'
                            });
                            for(var prop in productItem_Record){
                                create_child_item.setValue(prop,productItem_Record[prop]);
                            }
                            create_child_item.save();
                        }else{
                            // update existing item
                            var update_child_item = record.load({
                                type: 'customrecord_rics_product_item',
                                id: child_NS_ID
                            });
                            for(var prop in productItem_Record){
                                if(prop != 'custrecord_rics_pi_upc'){
                                    if(prop == 'custrecord_rics_pi_upcs'){ // the UPCs field is additive
                                        var current_upcs_string_length = update_child_item.getValue('custrecord_rics_pi_upcs').length;
                                        var new_upcs_string_length = productItem_Record[prop].toString().length;
                                        if(new_upcs_string_length > current_upcs_string_length){ update_child_item.setValue(prop,productItem_Record[prop]); }
                                    }else{
                                        update_child_item.setValue(prop,productItem_Record[prop]);
                                    }
                                }
                            }
                            update_child_item.save();
                        }
                    }
                }
            }

            return true;
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_Product_Details.js:rics_reduce', details: 'childData.UPC: ' + childData.UPC + ', child_NS_ID: ' + child_NS_ID + ', Error: ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * This summarize function is used to report important aspects about how the script performed.
     * In this script specifically it is also used to deactivate NetSuite inventory items, and remove the custom product details and product item records that have been deactivated.
     * @param {*} summary - this object contains data regarding script performance.
     */
    function rics_summarize(summary) {
        try{
            log.audit('Usage Consumed', summary.usage);
            log.audit('Concurrency Number ', summary.concurrency);
            log.audit('Number of Yields', summary.yields);
            log.audit({title: 'Process Completed'});
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_Product_Details.js:rics_summarize', details: 'ns_item_type: ' + ns_item_type + ', Error: ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    return {
        config:{
            retryCount: 3,
            exitOnError: true
        },

        getInputData: rics_getInputData,
        map: rics_map,
        reduce: rics_reduce,
        summarize: rics_summarize
    };

    // =========================== extra functions
    /**
     * This function takes in the response data from the API call and returns an object that is writable to the NetSuite custom RICS Product Details record.
     * @param {*} _data - object - this should be the API response data from RICS.
     * @returns - object - the data from the response converted to a NetSuite writable object.
     */
    function create_productDetails(_data){
        var result = new Object();
        result.custrecord_rics_pd_sku = (_data.Sku != null) ? _data.Sku : '';
        result.custrecord_rics_pd_suppliersku = (_data.SupplierSku != null) ? _data.SupplierSku : '';
        result.custrecord_rics_pd_style = (_data.Style != null) ? _data.Style : '';
        result.custrecord_rics_pd_summary = (_data.Summary != null) ? _data.Summary : '';
        result.custrecord_rics_pd_description = (_data.Description != null) ? _data.Description : '';
        result.custrecord_rics_pd_alert = (_data.Alert != null) ? _data.Alert : '';
        var availableToPOSOn = format.parse({ value : _data.AvailableToPOSOn, type : format.Type.DATE});
        // log.debug({title: 'GF_MR_RICS_Product_Details.js:create_productDetails', details: 'availableToPOSOn: ' + availableToPOSOn});
        var str_availableToPOSOn_month = availableToPOSOn.substring(5,7);
        var str_availableToPOSOn_day = availableToPOSOn.substring(8,10);
        var str_availableToPOSOn_year = availableToPOSOn.substring(0,4);
        var str_availableToPOSOn = str_availableToPOSOn_month + '/' + str_availableToPOSOn_day + '/' + str_availableToPOSOn_year;
        var dt_availableToPOSOn = new Date(str_availableToPOSOn);
        str_availableToPOSOn_month = dt_availableToPOSOn.getMonth() + 1;
        str_availableToPOSOn_day = dt_availableToPOSOn.getDate();
        str_availableToPOSOn_year = dt_availableToPOSOn.getFullYear();
        str_availableToPOSOn = str_availableToPOSOn_month + '/' + str_availableToPOSOn_day + '/' + str_availableToPOSOn_year;
        // log.debug({title: 'GF_MR_RICS_Product_Details.js:create_productDetails', details: 'str_availableToPOSOn: ' + str_availableToPOSOn});
        result.custrecord_rics_pd_avail_pos_on = format.parse({ value : str_availableToPOSOn, type : format.Type.DATE});
        result.custrecord_rics_pd_created_on = (_data.CreatedOn != null) ? _data.CreatedOn : '';
        result.custrecord_rics_pd_suppliercode = (_data.SupplierCode != null) ? _data.SupplierCode : '';
        result.custrecord_rics_pd_suppliername = (_data.SupplierName != null) ? _data.SupplierName : '';
        result.custrecord_rics_pd_labeltype = (_data.LabelType != null) ? _data.LabelType : '';
        result.custrecord_rics_pd_column_name = (_data.ColumnName != null) ? _data.ColumnName : '';
        result.custrecord_rics_pd_row_name = (_data.RowName != null) ? _data.RowName : '';
        result.custrecord_rics_pd_colors = (JSON.stringify(_data.Colors) != null) ? JSON.stringify(_data.Colors) : '';
        result.custrecord_rics_pd_classes = (JSON.stringify(_data.Classes) != null) ? JSON.stringify(_data.Classes) : '';
        result.custrecord_rics_pd_json = JSON.stringify(_data);
        if((result.custrecord_rics_pd_json).length > 4000){
            result.custrecord_rics_pd_json = (result.custrecord_rics_pd_json).substring(0, 3950);
        }
      	// if(result.custrecord_rics_pd_json.indexOf('"DeletedOn"') > -1){
            // result.isinactive = true;
        // }

        return result;
    }

    /**
     * This function takes in the response data from the API call and returns an object that is writable to the NetSuite custom RICS Product Item record.
     * @param {*} _data - object - this should be the API response data from RICS.
     * @param {*} _parentId - integer - this should be the internal id of the parent record that this record belongs to.
     * @returns 
     */
    function create_productItem(_data, _parentId){
        var result = new Object();
        result.custrecord_rics_pi_parent_pd = (_parentId != null) ? _parentId : '';
        result.custrecord_rics_pi_column = (_data.Column != null) ? _data.Column : '';
        result.custrecord_rics_pi_columnordinal = (_data.ColumnOrdinal != null) ? _data.ColumnOrdinal : '';
        result.custrecord_rics_pi_row = (_data.Row != null) ? _data.Row : '';
        result.custrecord_rics_pi_rowordinal = (_data.RowOrdinal != null) ? _data.RowOrdinal : '';
        result.custrecord_rics_pi_upc = (_data.UPC != null) ? _data.UPC : '';
        result.custrecord_rics_pi_integration_id = (_data.ProductItemId != null) ? _data.ProductItemId : '';
        // check to make sure the upcs list contains the upc
        if(_data.UPCs != undefined && (JSON.stringify(_data.UPCs)).indexOf('"'+_data.UPC+'"') == -1){
            var arr_upcs = _data.UPCs;
            arr_upcs.push(_data.UPC);
            result.custrecord_rics_pi_upcs = JSON.stringify(arr_upcs);
        }else{
            result.custrecord_rics_pi_upcs = (JSON.stringify(_data.UPCs) != null) ? JSON.stringify(_data.UPCs) : '';
        }
        if(result.custrecord_rics_pi_upcs == '' && _data.UPC != ''){
            var arr_upcs = [];
            arr_upcs.push(_data.UPC);
            result.custrecord_rics_pi_upcs = JSON.stringify(arr_upcs);
        }
        result.custrecord_rics_pi_json = JSON.stringify(_data);
        if((result.custrecord_rics_pi_json).length > 4000){
            result.custrecord_rics_pi_json = (result.custrecord_rics_pi_json).substring(0, 3950);
        }

        return result;
    }

    /**
     * This function usues the sku property of the data object to find the RICS Product Details custom record that matches that sku.
     * @param {*} _data - object - this should be the data object with the Sku property.
     * @returns - integer - the internal id of the RICS Product Details custom record, or zero.
     */
   function find_pd_record(_data){
       var result = 0;
       // log.debug({title: 'GF_MR_RICS_Product_Details.js:find_pd_record', details: '_data.Sku: ' + _data.Sku});
       // find the Product Details Record
       var pd_SearchResults = search.create({
            type: "customrecord_rics_product_details",
            filters: [
                ["isinactive","any",""], "AND",
                ["custrecord_rics_pd_sku","is",_data.Sku]
            ],
            columns: [
                search.createColumn({
                    name: "id",
                    sort: search.Sort.ASC,
                    label: "ID"
                }),
                search.createColumn({name: "internalid", label: "Internal ID"})
            ]
        }).run();
        var pd_SearchResultSet = pd_SearchResults.getRange({start: 0, end: 1000});
        if (pd_SearchResultSet != null && pd_SearchResultSet.length > 0){
            result = pd_SearchResultSet[0].id;
        }
        log.debug({title: 'GF_MR_RICS_Product_Details.js:find_pd_record', details: 'result: ' + result});
        return result;
   }

   /**
    * This function usues the upc, and parent id, to find the RICS Product Item custom record that matches the exact upc related to the parent item.
    * @param {*} _upc - string - this should be the unique identifer that was set in RICS for this specific item.
    * @returns - integer - the internal id of the RICS Product Details custom record, or zero.
    */
    function find_pi_record(_upc){
        var result = 0;
        // find the Product Details Record
        var pi_SearchResults = search.create({
            type: "customrecord_rics_product_item",
            filters: [
                ["isinactive","any",""], "AND",
                ["custrecord_rics_pi_upcs","contains",'"'+_upc+'"']
            ],
            columns: [
                search.createColumn({
                    name: "id",
                    sort: search.Sort.ASC,
                    label: "ID"
                }),
                search.createColumn({name: "internalid", label: "Internal ID"})
            ]
        }).run();
        var pi_SearchResultSet = pi_SearchResults.getRange({start: 0, end: 1000});
        if (pi_SearchResultSet != null && pi_SearchResultSet.length > 0){
            result = pi_SearchResultSet[0].id;
        }

        return result;
    }

});