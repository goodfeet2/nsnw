/**
 *@NApiVersion 2.1
 *@NScriptType MapReduceScript
 *
Script Name:   GF_MR_RICS_Inventory_Adjustment.js
Author:        Mark Robinson
 */

define(['N/log', 'N/search', 'N/record', 'N/runtime'], function(log, search, record, runtime) {

    /**
     * In this get input function we're using a search that has multiple formulas that will generate a resultset where the RICS on hand value is different from the NetSuite on hand value.
     * All of these differences will be passed to the mapping function (note: just passing the resultset will cause the Inventory Adjustment records to not be generated correctly)
     * @param {*} context - this variable is not utilized in this function
     * @returns - an array containing the full resultset array from the search
     */
    function rics_getInputData(context){
        try{
            var currentScript = runtime.getCurrentScript();
            var flg_onetime = true;
            flg_onetime = currentScript.getParameter({ name: 'custscript_rics_onetime_adjustment' });
            log.debug({title: 'GF_MR_RICS_Inventory_Adjustment.js:rics_getInputData', details: 'flg_onetime: ' + flg_onetime});
            var arr_search_filters = [
                ["type","anyof","InvtPart"], "AND",
                ["isinactive","is","F"], "AND",
                ["parent","noneof","@NONE@"], "AND", // must be a subitem
                [
                    ["custrecord_rics_ioh_ns_matrix_subitem.custrecord_rics_ioh_onhand","notequalto","0"],"OR",
                    ["locationquantityonhand","notequalto","0"]
                ], "AND", // must have a non-zero value to evaluate
                ["formulanumeric: NVL({custrecord_rics_ioh_ns_matrix_subitem.custrecord_rics_ioh_onhand}, 0) - NVL({locationquantityonhand}, 0)","notequalto","0"], "AND", // must have an adjustment to make
                ["formulanumeric: CASE WHEN {inventorylocation} = {custrecord_rics_ioh_ns_matrix_subitem.custrecord_rics_ioh_ns_location} THEN 1 ELSE 0 END","greaterthan","0"] //, "AND", // must match location
            ];
            if(flg_onetime == false){
                arr_search_filters.push("AND");
                arr_search_filters.push(["custrecord_rics_ioh_ns_matrix_subitem.custrecord_rics_ioh_dt_diff_detected","onorbefore","hoursago24"]); // must have a persisting difference
            }
            var inventoryitemSearchObj = search.create({
                type: "inventoryitem",
                filters: arr_search_filters,
                columns: [
                    search.createColumn({name: "itemid", label: "Name"}),
                    search.createColumn({
                        name: "vendorname",
                        sort: search.Sort.ASC,
                        label: "SUPPLIER SKU"
                    }),
                    search.createColumn({name: "upccode", label: "UPC Code"}),
                    search.createColumn({name: "salesdescription", label: "Description"}),
                    search.createColumn({name: "class", label: "Class"}),
                    search.createColumn({name: "custitem_psgss_product_color", label: "ROW / COLOR"}),
                    search.createColumn({name: "custitem_psgss_product_size", label: "COLUMN / SIZE"}),
                    search.createColumn({name: "inventorylocation", label: "Inventory Location"}),
                    search.createColumn({
                        name: "subsidiary",
                        join: "inventoryLocation",
                        label: "Subsidiary"
                    }),
                    search.createColumn({
                        name: "locationquantityonhand",
                        sort: search.Sort.DESC,
                        label: "Location On Hand"
                    }),
                    search.createColumn({
                        name: "custrecord_rics_ioh_onhand",
                        join: "CUSTRECORD_RICS_IOH_NS_MATRIX_SUBITEM",
                        label: "RICS ON HAND"
                    }),
                    search.createColumn({
                        name: "custrecord_rics_ioh_cost",
                        join: "CUSTRECORD_RICS_IOH_NS_MATRIX_SUBITEM",
                        label: "RICS COST"
                    }),
                    search.createColumn({
                        name: "custrecord_rics_ioh_retailprice",
                        join: "CUSTRECORD_RICS_IOH_NS_MATRIX_SUBITEM",
                        label: "RICS RETAIL PRICE"
                    }),
                    search.createColumn({
                        name: "custrecord_rics_ioh_activeprice",
                        join: "CUSTRECORD_RICS_IOH_NS_MATRIX_SUBITEM",
                        label: "RICS ACTIVE PRICE"
                    }),
                    search.createColumn({
                        name: "custrecord_rics_ioh_storename",
                        join: "CUSTRECORD_RICS_IOH_NS_MATRIX_SUBITEM",
                        label: "RICS STORE NAME"
                    }),
                    search.createColumn({
                        name: "custrecord_rics_ioh_ns_location",
                        join: "CUSTRECORD_RICS_IOH_NS_MATRIX_SUBITEM",
                        label: "RICS LOCATION"
                    }),
                    search.createColumn({
                        name: "formulanumeric",
                        formula: "CASE WHEN {locationquantityonhand} = {custrecord_rics_ioh_ns_matrix_subitem.custrecord_rics_ioh_onhand} THEN 1 ELSE 0 END",
                        label: "QTY_MATCH"
                    })
                ]
            });

            // plan for more than 4000
            var searchResultCount = inventoryitemSearchObj.runPaged().count;
            log.audit({title: 'GF_MR_RICS_Inventory_Adjustment.js:rics_getInputData', details: 'searchResultCount: ' + searchResultCount});

            return inventoryitemSearchObj;

        }catch(ex) {
            log.error({title: 'Error: GF_MR_RICS_Inventory_Adjustment.js:rics_getInputData', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this mapping function we take the data in the resultset and map it to a set of objects that are sorted by subsidiary and class so that when the Inventory Adjustment records
     * are created they are separated correctly for the requirements of that record type, specifically the subsidiary drives which locations can be on the record and the top level
     * class drives which account will be used for the value adjustment.
     * @param {*} context - this should contain the result set that will drive the rest of the script to create the correct Inventory Adjustment records.
     * @returns void (note: the const context variable is impacted and used in the reduce function)
     */
    function rics_map(context){
        try{
            // log.audit({title: 'GF_MR_RICS_Inventory_Adjustment.js:rics_map', details: 'data: ' + JSON.stringify(arr_data)});
            var result = JSON.parse(context.value);
            log.debug({title: 'GF_MR_RICS_Inventory_Adjustment.js:rics_map', details: 'result: ' + JSON.stringify(result)});
            var arr_subs = [];
            var class_list = getClassValues();
            var arr_sub_ids = getSubsidiaryIdsByName();
                
            var res_obj = {};

            var str_subsidiary = result.values['subsidiary.inventoryLocation'];
            var num_sub_id = (str_subsidiary != '') ? arr_sub_ids[str_subsidiary] : 0;
            var class_id = (result.values['class'] != undefined) ? result.values['class'].value : 0;
            var topLevelClass_obj = get_top_level_class(class_id, class_list);
            var item_id = result.id;
            var location_id = result.values['inventorylocation'].value;
            location_id = (location_id != undefined) ? location_id : result.values['custrecord_rics_ioh_ns_location.CUSTRECORD_RICS_IOH_NS_MATRIX_SUBITEM'].value;
            var location_name = result.values['inventorylocation'].text;
            var num_ns_qty = (result.values['locationquantityonhand'] != '') ? result.values['locationquantityonhand'] : 0;
            var num_rics_qty = result.values['custrecord_rics_ioh_onhand.CUSTRECORD_RICS_IOH_NS_MATRIX_SUBITEM'];
            var qty_diff = (num_rics_qty - num_ns_qty);
            var num_rics_cost = result.values['custrecord_rics_ioh_cost.CUSTRECORD_RICS_IOH_NS_MATRIX_SUBITEM'];
            num_rics_cost = (num_rics_cost != undefined && num_rics_cost > 0) ? num_rics_cost : 0;

            res_obj.str_subsidiary = str_subsidiary;
            res_obj.num_sub_id = num_sub_id;
            res_obj.class_id = class_id;
            res_obj.topLevelClass_obj = topLevelClass_obj;
            res_obj.item_id = item_id;
            res_obj.location_id = location_id;
            res_obj.location_name = location_name;
            res_obj.num_ns_qty = num_ns_qty;
            res_obj.num_rics_qty = num_rics_qty;
            res_obj.qty_diff = qty_diff;
            res_obj.num_rics_cost = num_rics_cost;

            // log.debug({title: 'GF_MR_RICS_Inventory_Adjustment.js:rics_map', details: 'Result Object: ' + JSON.stringify(res_obj)});
            // log.debug({title: 'GF_MR_RICS_Inventory_Adjustment.js:rics_map', details: 'arr_subs[str_subsidiary]: ' + arr_subs[str_subsidiary]});
            // log.debug({title: 'GF_MR_RICS_Inventory_Adjustment.js:rics_map', details: '[str_subsidiary]: ' + str_subsidiary + ', [location_id]:' + location_id + ', [topLevelClass_obj.id]:' + topLevelClass_obj.id});                
            if(arr_subs[str_subsidiary] == undefined){
                // add the new subsidiary, and items property and push the current item into it
                var arr_items = [res_obj];
                var arr_classes = [];
                arr_classes[topLevelClass_obj.id] = {items: arr_items};
                var arr_locations = [];
                arr_locations[location_id] = {classes: arr_classes};
                arr_subs[str_subsidiary] = {locations: arr_locations};
            }else{
                // log.debug({title: 'GF_MR_RICS_Inventory_Adjustment.js:rics_map', details: 'arr_subs[str_subsidiary]: ' + JSON.stringify(arr_subs[str_subsidiary])});
                // log.debug({title: 'GF_MR_RICS_Inventory_Adjustment.js:rics_map', details: 'arr_subs[str_subsidiary].classes[topLevelClass_obj.id]: ' + arr_subs[str_subsidiary].classes[topLevelClass_obj.id]});
                // when location and class exists add items, otherwise add class and item
                if(arr_subs[str_subsidiary].locations[location_id] == undefined){
                    // when location has not been added - add the location with its class and items as above
                    var arr_items = [res_obj];
                    var arr_classes = [];
                    arr_classes[topLevelClass_obj.id] = {items: arr_items};
                    var arr_locations = arr_subs[str_subsidiary].locations; // get current locations array
                    arr_locations[location_id] = {classes: arr_classes}; 
                    arr_subs[str_subsidiary] = {locations: arr_locations};
                }else{
                    // log.debug({title: 'GF_MR_RICS_Inventory_Adjustment.js:rics_map', details: 'arr_subs[str_subsidiary].locations[location_id]: ' + JSON.stringify(arr_subs[str_subsidiary].locations[location_id])});
                    if(arr_subs[str_subsidiary].locations[location_id].classes[topLevelClass_obj.id] == undefined){
                        // when the class has not been added - add the class and items as above
                        var arr_items = [res_obj];
                        var arr_classes = arr_subs[str_subsidiary].locations[location_id].classes; // get current class array
                        arr_classes[topLevelClass_obj.id] = {items: arr_items};
                        var arr_locations = arr_subs[str_subsidiary].locations;
                        arr_locations[location_id] = {classes: arr_classes};  // add to existing location
                        arr_subs[str_subsidiary] = {locations: arr_locations};
                    }else{
                        // log.debug({title: 'GF_MR_RICS_Inventory_Adjustment.js:rics_map', details: 'arr_subs[str_subsidiary].locations[location_id].classes[topLevelClass_obj.id]: ' + JSON.stringify(arr_subs[str_subsidiary].locations[location_id].classes[topLevelClass_obj.id])});
                        // when the sub, the location, and the class all exist - add the items to the item list
                        var arr_items = arr_subs[str_subsidiary].locations[location_id].classes[topLevelClass_obj.id].items;
                        arr_items.push(res_obj); // add to current items
                        arr_subs[str_subsidiary].locations[location_id].classes[topLevelClass_obj.id].items = arr_items;
                    }
                }
            }
            // log.debug({title: 'GF_MR_RICS_Inventory_Adjustment.js:rics_map', details: 'Full Array: ' + JSON.stringify(arr_subs)});

            // pass data to reduce for setting on hand
            for(var s in arr_subs){
                var locationlist_data = arr_subs[s].locations;
                for(var l in locationlist_data){
                    var classlist_data = locationlist_data[l].classes;
                    for(var c in classlist_data){
                        var class_items = classlist_data[c].items;
                        context.write({ key: s+'-'+l+'-'+c, value: class_items });
                        log.audit({title: 'GF_MR_RICS_Inventory_Adjustment.js:rics_map', details: 'context key: ' + s+'-'+l+'-'+c});
                    }
                }
            }
        }catch(ex) {
            log.error({title: 'Error: GF_MR_RICS_Inventory_Adjustment.js:rics_map', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this reduce function we take the mapped context objects and generate all of the Inventory Adjustment records that will impact the on hand quantity of each item at their correct location.
     * @param {*} context - this should be the mapped object containing all of the items that need to be on the Inventory Adjustment record.
     * @returns void
     */
    function rics_reduce(context){
        try{
            for(var val in context.values){
                var data = JSON.parse(context.values[val]);
                if(data == null){ continue; }
                // log.audit({title: 'GF_MR_RICS_Inventory_OnHand.js:rics_reduce', details: 'item count: ' + data.length + ', data: ' + JSON.stringify(data)});
                if(val == 0){
                    // create the Inventory adjustment record specific to the subsidiary and class combination
                    var inventory_adjustment_rec = record.create({
                        type: 'inventoryadjustment'
                    });
                    inventory_adjustment_rec.setValue('subsidiary', data[0].num_sub_id);
                    inventory_adjustment_rec.setValue('adjlocation', data[0].location_id);
                    inventory_adjustment_rec.setValue('account', get_adjustment_account_by_class_id(data[0].topLevelClass_obj.id)); // 999999999 Inventory Adjustment Account
                    if(data[0].topLevelClass_obj.id != 0){
                        inventory_adjustment_rec.setValue('class', data[0].topLevelClass_obj.id);
                    }
                    var dts = get_dts();
                    inventory_adjustment_rec.setValue('memo', data[0].str_subsidiary + ' - ' + data[0].location_name + ' - ' + data[0].topLevelClass_obj.name + ' - ' + dts);
                }

                for(var item in data){
                    var item_data = data[item];
                    log.debug({title: 'GF_MR_RICS_Inventory_OnHand.js:rics_reduce', details: 'item_data: ' + JSON.stringify(item_data)});
                    // add the line items to the Inventory adjustment record - this part can get more complicated if we need to use bins ever
                    inventory_adjustment_rec.setSublistValue({
                        sublistId: 'inventory',
                        fieldId: 'item',
                        line: val,
                        value: item_data.item_id
                    });
                    inventory_adjustment_rec.setSublistValue({
                        sublistId: 'inventory',
                        fieldId: 'location',
                        line: val,
                        value: item_data.location_id
                    });
                    inventory_adjustment_rec.setSublistValue({
                        sublistId: 'inventory',
                        fieldId: 'adjustqtyby',
                        line: val,
                        value: item_data.qty_diff
                    });
                    if(item_data.num_rics_cost != undefined && item_data.num_rics_cost > 0){
                        inventory_adjustment_rec.setSublistValue({
                            sublistId: 'inventory',
                            fieldId: 'unitcost',
                            line: val,
                            value: item_data.num_rics_cost
                        });
                    }
                }
            }

            inventory_adjustment_rec.save();

            return true;
        }catch(ex) {
            log.error({title: 'Error: GF_MR_RICS_Inventory_Adjustment.js:rics_reduce', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * This summarize function is used to report important aspects about how the script performed
     * @param {*} summary - this object contains data regarding script performance.
     */
    function rics_summarize(summary) {
        try{
            log.audit('Usage Consumed', summary.usage);
            log.audit('Concurrency Number ', summary.concurrency);
            log.audit('Number of Yields', summary.yields);
            log.audit({title: 'Process Completed'});
        }catch(ex) {
            log.error({title: 'Error: GF_MR_RICS_Inventory_Adjustment.js:rics_summarize', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    return {
        getInputData: rics_getInputData,
        map: rics_map,
        reduce: rics_reduce,
        summarize: rics_summarize
    };

    // =========================== extra functions
    /**
     * This function returns an array that allows us to find the id of a class by using the class name
     * @returns array
     */
    function getClassValues() {
        var searchColumn = search.createColumn({ name : 'name' });
        var listSearch = search.create({ type : 'classification', columns : searchColumn });
        var listArray = [];
        listSearch.run().each(function(searchResult) {
            listArray[searchResult.id] = searchResult.getValue(searchColumn);
            return true;
        });
        return listArray;
    }

    /**
     * This function returns an array that allows us to find the id of a subsidiary by using the subsidiary name
     * @returns array
     */
    function getSubsidiaryIdsByName() {
        var searchColumn = search.createColumn({ name : 'namenohierarchy' });
        var listSearch = search.create({ type : 'subsidiary', columns : searchColumn });
        var listArray = [];
        listSearch.run().each(function(searchResult) {
            listArray[searchResult.getValue(searchColumn)] = searchResult.id;
            return true;
        });
        return listArray;
    }

    /**
     * This function uses the passed in class id and class list to find to the highest parent class associated to the specified class id 
     * @param {*} _str_class_id - the id of the class that we are trying to find the top level parent class of
     * @param {*} _class_list - the full class list that will be used to find the top level parent class
     * @returns object - {name: string - name of the top level class used to set the memo field on the adjustment record, id: integer - the internal id of the top level class used to set the class field on the adjustment record}
     */
    function get_top_level_class(_str_class_id, _class_list){
        try{
            var result = {name: 'CLASS NOT SET', id: 0};
            if(_class_list[_str_class_id] != undefined){
                var str_className = _class_list[_str_class_id];
                if(str_className.indexOf(' : ') > -1){
                    // this is not a top level class
                    var str_topLevelClassName = str_className.substring(0, str_className.indexOf(' : '));
                    result = {name: str_topLevelClassName, id: _class_list.indexOf(str_topLevelClassName)};
                }else{
                    // this IS a top level class
                    result = {name: _str_class_id, id: _class_list[_str_class_id]};
                }
            }
            return result;
        }catch(ex){
            log.error({title: 'Error: GF_MR_RICS_Inventory_Adjustment.js:get_top_level_class', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * This function takes the top level class id that will be used on the adjustment record and returns the correct account id to be used for that class
     * @param {*} _class_id - Integer - the internal id of the top level class on the adjustment record
     * @returns - Integer - the internal id of the account that will be used on the adjustment record
     */
    function get_adjustment_account_by_class_id(_class_id){
        var result = 0;
        /*                      1               8           12          32
            classname_rics      Arch Supports   Accessories Footwear    Socks
            COGS Account	    623             625         624         625 --- currently using Asset Account (default 734 - 5007 - Inventory Adjustments - Cost of Goods Sold
            Asset Account	    620             622         621         622
            Income Account      54              54          54          54
            Gain/Loss Account	551             551         551         551
            Price Vari.Account	620             622         621         622
            Qty Vari. Account   620             622         621         622
        */
        _class_id = parseInt(_class_id);
        switch(_class_id){
            case 1: // 'arch supports':
                result = 623;
                break;
            case 8: // 'accessories':
                result = 625;
                break;
            case 12: // 'footwear':
                result = 624;
                break;
            case 32: // 'socks':
                result = 625;
                break;
            default: // 'class not set'
                result = 734;
                break;
        }
        return result;
    }

    /**
     * This function takes no args and produces a string with a date following the listed format
     * @returns - string - date time stamp Month-Day-Year_Hour_Minute_Milliseconds (24hr format)
     */
    function get_dts(){
        var result = '';
        var dt = new Date();
        result = (dt.getMonth()+1) + '-' + dt.getDate() + '-' + dt.getFullYear() + '_' + dt.getHours() + '_' + dt.getMinutes() + '_' + dt.getMilliseconds();
        return result;
    }

});