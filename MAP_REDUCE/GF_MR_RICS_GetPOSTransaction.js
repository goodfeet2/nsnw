/**
 *@NApiVersion 2.1
 *@NScriptType MapReduceScript
 *
Script Name:   GF_MR_RICS_GetPOSTransaction.js
Author:        Mark Robinson
 */

define(['N/log', 'N/search', 'N/record', 'N/runtime', 'N/task', 'N/file', '/SuiteScripts/GF_LIB/gf_nsrics'], function(log, search, record, runtime, task, file, gf_nsrics) {

    /**
     * In this function we send API calls to RICS to get all of the sales data that has been updated today including batches from up to 60 days ago.
     * We add all of the data to an array that it then pass it into the mapping function.
     * @param {*} context - this variable is not utilized in this function.
     * @returns - an array containing the data returned from the API calls.
     */
    function rics_getInputData(context){
        try{
            // now instead of calling the RICS API this get input function will be pulling from json files saved in the file cabinet

            // check if the processing folder is empty // load first file in GetPOSTransaction_Process
            var top_file_id = getTopFileId();
            if(top_file_id == 0){
                // no files found - copy all from GetPOSTransaction_Response
                copyFilesFromResponseFolder();
                // load first file in GetPOSTransaction_Process
                top_file_id = getTopFileId();
            }

            if(top_file_id == 0){ return []; } // when there is nothing to copy and no file to load exit gracefully
            var arr_SalesTickets = [];
            var fileObj = file.load({ id: top_file_id });
            if (fileObj.size < 10485760){ // specific NS limitation
                var str_json = fileObj.getContents();
                var data = JSON.parse(str_json);
                for(var key in data.Sales){
                    arr_SalesTickets.push(data.Sales[key]);
                }
            }

            log.debug({title: 'GF_MR_RICS_GetPOSTransaction.js:rics_getInputData', details: 'arr_SalesTickets: ' + JSON.stringify(arr_SalesTickets)});
            return arr_SalesTickets;
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_GetPOSTransaction.js:rics_getInputData', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
            if(top_file_id != undefined && top_file_id != null){
              if(ex.SyntaxError && ex.SyntaxError == 'Unexpected end of JSON input'){
                  file.delete({ id: top_file_id });
              }
            }
        }
    }

    /**
     * In this function we take the data from the API responses and map them into a set of custom record objects, the RICS Sales Ticket, RICS Sales Details, and RICS Sales Tenders.
     * @param {*} context - this should contain the data from the API calls in the get input function.
     * @returns void (note: the const context variable is written to and used in the reduce function)
     */
    function rics_map(context){
        try{
            var nsrics = gf_nsrics.getIntegration();
            var data = JSON.parse(context.value);
            if(data == null){ return true; }
            log.audit({title: 'GF_MR_RICS_GetPOSTransaction.js:rics_map', details: 'data: ' + JSON.stringify(data)});

            // update must have been in the last 2 hours - anything beyond that will not be processed
            var dt_now = new Date();
            var dt_mod = new Date(nsrics.str_reformat_date(data.BatchModifiedOn));
            if(Math.abs(dt_mod - dt_now) < 7200000){ return true; }

            var arr_header_data = data.SaleHeaders;
            var arr_st_rec_headers = []; // contains batch and ticket info

            for(var hd in arr_header_data){
                // build a NetSuite writable object - RICS Sales Ticket
                var st_rec_obj = {};
                var arr_st_rec_details = []; // contains ticket line details
                var arr_st_rec_tenders = []; // contains payment information

                var header_data = arr_header_data[hd];
                st_rec_obj.custrecord_rics_st_ticketdatetime = header_data.TicketDateTime;
                st_rec_obj.custrecord_rics_st_ticketnumber = header_data.TicketNumber;
                st_rec_obj.custrecord_rics_st_ticketcomment = (header_data.hasOwnProperty('TicketComment')) ? header_data.TicketComment : ''; //
                st_rec_obj.custrecord_rics_st_alert = (header_data.hasOwnProperty('Alert')) ? header_data.Alert : ''; //
                st_rec_obj.custrecord_rics_st_change = header_data.Change;
                st_rec_obj.custrecord_rics_st_shipping = (header_data.hasOwnProperty('Shipping')) ? header_data.Shipping : ''; //
                st_rec_obj.custrecord_rics_st_alternatechange = (header_data.hasOwnProperty('AlternateChange')) ? header_data.AlternateChange : ''; //
                st_rec_obj.custrecord_rics_st_exchangerate = (header_data.hasOwnProperty('ExchangeRate')) ? header_data.ExchangeRate : ''; //
                st_rec_obj.custrecord_rics_st_ticketvoided = header_data.TicketVoided;
                st_rec_obj.custrecord_rics_st_receiptprinted = header_data.ReceiptPrinted;
                st_rec_obj.custrecord_rics_st_ticketsuspended = header_data.TicketSuspended;
                st_rec_obj.custrecord_rics_st_receiptemailed = header_data.ReceiptEmailed;
                st_rec_obj.custrecord_rics_st_receipttext = header_data.ReceiptText;
                st_rec_obj.custrecord_rics_st_saledatetime = header_data.SaleDateTime;
                st_rec_obj.custrecord_rics_st_ticketmodifiedon = header_data.TicketModifiedOn;
                st_rec_obj.custrecord_rics_st_modifiedby = header_data.ModifiedBy;
                st_rec_obj.custrecord_rics_st_shiptozipcode = (header_data.hasOwnProperty('ShipToZipcode')) ? header_data.ShipToZipcode : ''; //
                st_rec_obj.custrecord_rics_st_ordernumber = (header_data.hasOwnProperty('OrderNumber')) ? header_data.OrderNumber : ''; //
                st_rec_obj.custrecord_rics_st_voideddate = (header_data.hasOwnProperty('VoidedDate')) ? header_data.VoidedDate : ''; //
                st_rec_obj.custrecord_rics_st_createdon = header_data.CreatedOn;
                st_rec_obj.custrecord_rics_st_promotioncode = (header_data.hasOwnProperty('PromotionCode')) ? header_data.PromotionCode : ''; //
                st_rec_obj.custrecord_rics_st_promotiondescription = (header_data.hasOwnProperty('PromotionDescription')) ? header_data.PromotionDescription : ''; //
                // get sale type list
                // if sales type not found add sale type
                st_rec_obj.custrecord_rics_st_saletype = 1; // ---------------------------------------------------------------------------------------------------------------------------- UPDATE
                st_rec_obj.custrecord_rics_st_ecommerceprovidertype = (header_data.hasOwnProperty('EcommerceProviderType')) ? header_data.EcommerceProviderType : ''; //
                if(header_data.hasOwnProperty('Cashier')){
                    st_rec_obj.custrecord_rics_st_saleusercashiercode = header_data.Cashier.UserCashierCode;
                    st_rec_obj.custrecord_rics_st_salericsuserid = header_data.Cashier.RicsUserId;
                    st_rec_obj.custrecord_rics_st_cashierfirstname = header_data.Cashier.FirstName;
                    st_rec_obj.custrecord_rics_st_cashiermiddlename = header_data.Cashier.MiddleName;
                    st_rec_obj.custrecord_rics_st_cashierlastname = header_data.Cashier.LastName;
                    st_rec_obj.custrecord_rics_st_cashierlogin = header_data.Cashier.Login;
                }
                if(header_data.hasOwnProperty('Customer')){
                    st_rec_obj.custrecord_rics_st_customerphonenumber = (header_data.Customer.hasOwnProperty('PhoneNumber')) ? header_data.Customer.PhoneNumber : ''; //
                    st_rec_obj.custrecord_rics_st_customeremail = (header_data.Customer.hasOwnProperty('Email')) ? header_data.Customer.Email : ''; //
                    st_rec_obj.custrecord_rics_st_customerid = header_data.Customer.CustomerId;
                    st_rec_obj.custrecord_rics_st_customeraccountnumber = header_data.Customer.AccountNumber;
                    st_rec_obj.custrecord_rics_st_customerfirstname = header_data.Customer.FirstName;
                    st_rec_obj.custrecord_rics_st_customerlastname = header_data.Customer.LastName;
                    st_rec_obj.custrecord_rics_st_customerbilling = (header_data.Customer.hasOwnProperty('BillingAddress')) ? JSON.stringify(header_data.Customer.BillingAddress) : ''; //
                    st_rec_obj.custrecord_rics_st_customermailing = (header_data.Customer.hasOwnProperty('MailingAddress')) ? JSON.stringify(header_data.Customer.MailingAddress) : ''; //
                }
                st_rec_obj.custrecord_rics_st_batchstartdate = data.BatchStartDate;
                st_rec_obj.custrecord_rics_st_batchenddate = data.BatchEndDate;
                st_rec_obj.custrecord_rics_st_batchovershort = data.BatchOverShort;
                st_rec_obj.custrecord_rics_st_batchstartingbalance = data.BatchStartingBalance;
                st_rec_obj.custrecord_rics_st_batchendingbalance = data.BatchEndingBalance;
                st_rec_obj.custrecord_rics_st_batchcreatedon = data.BatchCreatedOn;
                st_rec_obj.custrecord_rics_st_batchmodifiedon = data.BatchModifiedOn;
                st_rec_obj.custrecord_rics_st_batchmodifiedby = data.BatchModifiedBy;
                st_rec_obj.custrecord_rics_st_batchtotaldeposit = data.BatchTotalDeposit;
                st_rec_obj.custrecord_rics_st_terminaldescription = data.TerminalDescription;
                st_rec_obj.custrecord_rics_st_storecode = data.StoreCode;
                st_rec_obj.custrecord_rics_st_storename = data.StoreName;
                st_rec_obj.custrecord_rics_st_ticketnumber_text = header_data.TicketNumber + ' - ' + data.StoreName;
                if(data.hasOwnProperty('User')){
                    st_rec_obj.custrecord_rics_st_usercashiercode = data.User.UserCashierCode;
                    st_rec_obj.custrecord_rics_st_ricsuserid = data.User.RicsUserId;
                    st_rec_obj.custrecord_rics_st_userfirstname = data.User.FirstName;
                    st_rec_obj.custrecord_rics_st_usermiddlename = data.User.MiddleName;
                    st_rec_obj.custrecord_rics_st_userlastname = data.User.LastName;
                    st_rec_obj.custrecord_rics_st_userlogin = data.User.Login;
                }

                // build a NetSuite writable object - RICS Sales Details
                for(var det in header_data.SaleDetails){
                    var detail_data = header_data.SaleDetails[det];
                    var detail_rec_obj = {};
                    detail_rec_obj.custrecord_rics_sd_parent = ''; // use look up in reduce stage
                    detail_rec_obj.custrecord_rics_sd_ticketlinenumber = detail_data.TicketLineNumber;
                    // build GF-UID
                    var str_saledatetime = header_data.SaleDateTime;
                    str_saledatetime = str_saledatetime.substring(0,str_saledatetime.indexOf('.'));
                    var str_gf_uid = str_saledatetime + '_' + data.StoreCode + '_' + data.TerminalDescription + '_' + header_data.TicketNumber + '_' + detail_data.TicketLineNumber;
                    str_gf_uid = str_gf_uid.replace(/\s/g, ''); // remove spaces
                    str_gf_uid = str_gf_uid.replace(/\//g, '-'); // replace date slashes with dashes
                    detail_rec_obj.custrecord_rics_sd_gfuid = str_gf_uid;
                    detail_rec_obj.custrecord_rics_sd_quantity = detail_data.Quantity;
                    detail_rec_obj.custrecord_rics_sd_saleprice = detail_data.SalePrice;
                    detail_rec_obj.custrecord_rics_sd_retailprice = detail_data.RetailPrice;
                    detail_rec_obj.custrecord_rics_sd_discountrate = (detail_data.hasOwnProperty('DiscountRate')) ? detail_data.DiscountRate : ''; //
                    detail_rec_obj.custrecord_rics_sd_discountamount = (detail_data.hasOwnProperty('DiscountAmount')) ? detail_data.DiscountAmount : ''; //
                    if(detail_rec_obj.custrecord_rics_sd_discountamount == '' && detail_rec_obj.custrecord_rics_sd_discountrate != ''){
                        detail_rec_obj.custrecord_rics_sd_discountamount = parseFloat(detail_data.SalePrice) - (parseFloat(detail_data.SalePrice) * (parseInt(detail_data.DiscountRate) / 100)); // set the amount to be the percentage of the sale price
                        detail_rec_obj.custrecord_rics_sd_discountamount = ((detail_rec_obj.custrecord_rics_sd_discountamount * detail_data.Quantity).toFixed(2)).toString();
                    }
                    detail_rec_obj.custrecord_rics_sd_commissionamount = detail_data.CommissionAmount;
                    detail_rec_obj.custrecord_rics_sd_perkamount = detail_data.PerkAmount;
                    detail_rec_obj.custrecord_rics_sd_amountpaid = detail_data.AmountPaid;
                    detail_rec_obj.custrecord_rics_sd_cost = detail_data.Cost;
                    detail_rec_obj.custrecord_rics_sd_shippingcharges = (detail_data.hasOwnProperty('ShippingCharges')) ? detail_data.ShippingCharges : ''; //
                    detail_rec_obj.custrecord_rics_sd_shiptocustomer = detail_data.ShipToCustomer;
                    detail_rec_obj.custrecord_rics_sd_skucomment = (detail_data.hasOwnProperty('SKUComment')) ? detail_data.SKUComment : ''; //
                    detail_rec_obj.custrecord_rics_sd_modifiedon = detail_data.ModifiedOn;
                    detail_rec_obj.custrecord_rics_sd_modifiedby = detail_data.ModifiedBy;
                    detail_rec_obj.custrecord_rics_sd_isfbqualified = detail_data.IsFBQualified;
                    detail_rec_obj.custrecord_rics_sd_activeprice = detail_data.ActivePrice;
                    detail_rec_obj.custrecord_rics_sd_issellable = detail_data.IsSellable;
                    detail_rec_obj.custrecord_rics_sd_additionalinformation = (detail_data.hasOwnProperty('AdditionalInformation')) ? detail_data.AdditionalInformation : ''; //
                    detail_rec_obj.custrecord_rics_sd_netsales = detail_data.NetSales;
                    detail_rec_obj.custrecord_rics_sd_couponamountapplied = (detail_data.hasOwnProperty('CouponAmountApplied')) ? detail_data.CouponAmountApplied : ''; //
                    detail_rec_obj.custrecord_rics_sd_giftcardnumber = (detail_data.hasOwnProperty('GiftCardNumber')) ? detail_data.GiftCardNumber : ''; //
                    detail_rec_obj.custrecord_rics_sd_couponcode = (detail_data.hasOwnProperty('CouponCode')) ? detail_data.CouponCode : ''; //
                    detail_rec_obj.custrecord_rics_sd_coupondescription = (detail_data.hasOwnProperty('CouponDescription')) ? detail_data.CouponDescription : ''; //
                    detail_rec_obj.custrecord_rics_sd_returndescription = (detail_data.hasOwnProperty('ReturnDescription')) ? detail_data.ReturnDescription : ''; //
                    detail_rec_obj.custrecord_rics_sd_transactionsaledesc = detail_data.TransactionSaleDescription;
                    detail_rec_obj.custrecord_rics_sd_discountreasondesc = (detail_data.hasOwnProperty('DiscountReasonDescription')) ? detail_data.DiscountReasonDescription : ''; //
                    if(detail_data.hasOwnProperty('SaleTax')){
                        detail_rec_obj.custrecord_rics_sd_taxamount = detail_data.SaleTax.TaxAmount;
                        detail_rec_obj.custrecord_rics_sd_isuseroverridden = detail_data.SaleTax.IsUserOverridden;
                        detail_rec_obj.custrecord_rics_sd_taxrate = detail_data.SaleTax.TaxRate;
                        detail_rec_obj.custrecord_rics_sd_taxableamount = (detail_data.SaleTax.hasOwnProperty('TaxableAmount')) ? detail_data.SaleTax.TaxableAmount : ''; //
                        detail_rec_obj.custrecord_rics_sd_orgtaxdesc = detail_data.SaleTax.OrganizationTaxDescription;
                    }
                    if(detail_data.hasOwnProperty('SalesPerson')){
                        detail_rec_obj.custrecord_rics_sd_usercashiercode = detail_data.SalesPerson.UserCashierCode;
                        detail_rec_obj.custrecord_rics_sd_ricsuserid = detail_data.SalesPerson.RicsUserId;
                        detail_rec_obj.custrecord_rics_sd_salespersonfirstname = detail_data.SalesPerson.FirstName;
                        detail_rec_obj.custrecord_rics_sd_salespersonmiddlename = detail_data.SalesPerson.MiddleName;
                        detail_rec_obj.custrecord_rics_sd_salespersonlastname = detail_data.SalesPerson.LastName;
                        detail_rec_obj.custrecord_rics_sd_salespersonlogin = detail_data.SalesPerson.Login;
                    }
                    if(detail_data.hasOwnProperty('ProductItem')){
                        detail_rec_obj.custrecord_rics_sd_productitemsku = detail_data.ProductItem.Sku;
                        detail_rec_obj.custrecord_rics_sd_productitemupc = detail_data.ProductItem.UPC;
                        detail_rec_obj.custrecord_rics_sd_productitemrow = (detail_data.ProductItem.hasOwnProperty('Row')) ? detail_data.ProductItem.Row : ''; //
                        detail_rec_obj.custrecord_rics_sd_productitemcolumn = (detail_data.ProductItem.hasOwnProperty('Column')) ? detail_data.ProductItem.Column : ''; //
                        detail_rec_obj.custrecord_rics_sd_productitem = ''; // use look up in reduce stage
                    }

                    arr_st_rec_details.push(detail_rec_obj);
                }

                st_rec_obj.arr_details = arr_st_rec_details;

                // build a NetSuite writable object - RICS Sales Tenders
                for(var tend in header_data.Tenders){
                    var tender_data = header_data.Tenders[tend];
                    var tender_rec_obj = {};
                    tender_rec_obj.custrecord_rics_tender_parent = ''; // use look up in reduce stage
                    tender_rec_obj.custrecord_rics_tender_amounttendered = tender_data.AmountTendered;
                    tender_rec_obj.custrecord_rics_tender_alternateamount = (tender_data.hasOwnProperty('AlternateAmount')) ? tender_data.AlternateAmount : ''; //
                    tender_rec_obj.custrecord_rics_tender_isaltcurrency = tender_data.IsAlternateCurrency;
                    tender_rec_obj.custrecord_rics_tender_exchangerate = (tender_data.hasOwnProperty('ExchangeRate')) ? tender_data.ExchangeRate : ''; //
                    tender_rec_obj.custrecord_rics_tender_accounttype = (tender_data.hasOwnProperty('AccountType')) ? tender_data.AccountType : ''; //
                    if(tender_rec_obj.custrecord_rics_tender_accounttype == ''){
                        tender_rec_obj.custrecord_rics_tender_accounttype = (tender_data.TenderDescription).substring((tender_data.TenderDescription).indexOf(' ')+1);
                    }
                    tender_rec_obj.custrecord_rics_tender_isforced = tender_data.IsForced;
                    tender_rec_obj.custrecord_rics_tender_paymentcardresp = (tender_data.hasOwnProperty('PaymentCardResponse')) ? tender_data.PaymentCardResponse : ''; //
                    tender_rec_obj.custrecord_rics_tender_description = tender_data.TenderDescription;
                    tender_rec_obj.custrecord_rics_tender_giftcardnumber = (tender_data.hasOwnProperty('GiftCardNumber')) ? tender_data.GiftCardNumber : ''; //
                    if(tender_data.PaymentCardResponse != undefined && tender_data.PaymentCardResponse != ''){
                        if((tender_data.PaymentCardResponse).indexOf(',') > -1){
                            var arr_cardInfo = (tender_data.PaymentCardResponse).split(',');
                            tender_rec_obj.custrecord_rics_tender_cardnumber = arr_cardInfo[0].substring(arr_cardInfo[0].indexOf(':') +1);
                            tender_rec_obj.custrecord_rics_tender_approvalcode = arr_cardInfo[1].substring(arr_cardInfo[1].indexOf(':') +1);
                            if(arr_cardInfo.length > 2){
                                tender_rec_obj.custrecord_rics_tender_invoice = arr_cardInfo[2].substring(arr_cardInfo[2].indexOf(':') +1);
                            }
                        }else{
                            // only card number
                            tender_rec_obj.custrecord_rics_tender_cardnumber = tender_data.PaymentCardResponse.substring(tender_data.PaymentCardResponse.indexOf(':') +1);
                        }
                    }
                    // lookup the Tender Type Mapping
                    tender_rec_obj.custrecord_rics_tender_ttm_customrec = find_tender_type_mapping(tender_data.TenderDescription);

                    arr_st_rec_tenders.push(tender_rec_obj);
                }

                st_rec_obj.arr_tenders = arr_st_rec_tenders;
                arr_st_rec_headers.push(st_rec_obj);
            }

            // log.debug({title: 'GF_MR_RICS_GetPOSTransaction.js:rics_map-write context', details: 'arr_st_rec_headers.length: ' + arr_st_rec_headers.length});
            for(var head_key in arr_st_rec_headers){
                // log.debug({title: 'GF_MR_RICS_GetPOSTransaction.js:rics_map-write context', details: 'head_key: ' + head_key + ', arr_st_rec_headers[head_key]: ' + JSON.stringify(arr_st_rec_headers[head_key])});
                // pass data to reduce
                context.write({ key: head_key, value: arr_st_rec_headers[head_key] });
            }
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_GetPOSTransaction.js:rics_map', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this reduce function we take the mapped context objects and create or update all of the custom records.
     * @param {*} context - this should be the mapped object containing all of the fields and data that need to be on the RICS Sales Ticket, RICS Sales Details, and RICS Sales Tenders
     * @returns void
     */
    function rics_reduce(context){
        try{
            var nsrics = gf_nsrics.getIntegration();
            var currentScript = runtime.getCurrentScript();
            // some object properties should be processed and not written to the record they are:
            var arr_do_not_process = ['arr_details', 'arr_tenders'];
            var arr_date_fields = [
                'custrecord_rics_st_ticketdatetime',
                'custrecord_rics_st_saledatetime',
                'custrecord_rics_st_ticketmodifiedon',
                'custrecord_rics_st_voideddate',
                'custrecord_rics_st_createdon',
                'custrecord_rics_st_batchstartdate',
                'custrecord_rics_st_batchenddate',
                'custrecord_rics_st_batchcreatedon',
                'custrecord_rics_st_batchmodifiedon',
                'custrecord_rics_sd_modifiedon'
            ];

            // log.audit({title: 'GF_MR_RICS_GetPOSTransaction.js:rics_reduce-context', details: 'context: ' + JSON.stringify(context)});
            // log.audit({title: 'GF_MR_RICS_GetPOSTransaction.js:rics_reduce-context', details: 'context.key: ' + JSON.stringify(context.key)});
            // log.audit({title: 'GF_MR_RICS_GetPOSTransaction.js:rics_reduce-context', details: 'context.values.length: ' + JSON.stringify((context.values).length)});
            for(var c_val in context.values){ // go through all sales tickets
                log.audit({title: 'GF_MR_RICS_GetPOSTransaction.js:rics_reduce-c_val='+c_val, details: 'remaining usage: ' + currentScript.getRemainingUsage()});
                var data = JSON.parse(context.values[c_val]);
                if(data == null){ continue; }
                log.audit({title: 'GF_MR_RICS_GetPOSTransaction.js:rics_reduce-c_val='+c_val, details: 'data: ' + JSON.stringify(data)});

                var isNew = false;
                // find an existing Sales Ticket Record
                var st_id = find_st_record( data.custrecord_rics_st_createdon,
                                            data.custrecord_rics_st_terminaldescription,
                                            data.custrecord_rics_st_storecode,
                                            data.custrecord_rics_st_usercashiercode,
                                            data.custrecord_rics_st_ticketnumber );

                if(st_id != 0){
                    // load the record
                    var salesTicket_rec = record.load({
                        type: 'customrecord_rics_sales_ticket',
                        id: st_id
                    });
                }else{
                    // create the record
                    var salesTicket_rec = record.create({
                        type: 'customrecord_rics_sales_ticket'
                    });
                    isNew = true;
                }

                for(var prop in data){
                    if(arr_do_not_process.indexOf(prop) == -1 && data[prop] != undefined && data[prop] != ''){
                        if(arr_date_fields.indexOf(prop) == -1){
                            salesTicket_rec.setValue(prop,data[prop]);
                        }else{
                            salesTicket_rec.setValue(prop,new Date(nsrics.str_reformat_date(data[prop])));
                        }
                    }
                }
                st_id = salesTicket_rec.save();

                if(isNew){
                    log.audit({title: 'GF_MR_RICS_GetPOSTransaction.js:rics_reduce-newST', details: 'created new sales ticket rec - id: ' + st_id});
                    // create all details
                    for(var det_data in data.arr_details){
                        var detail_data = data.arr_details[det_data];
                        var salesDetails_rec = record.create({
                            type: 'customrecord_rics_sales_details'
                        });
                        for(var det_prop in detail_data){
                            if(detail_data[det_prop] != ''){
                                if(arr_date_fields.indexOf(det_prop) == -1 && detail_data[det_prop] != undefined){
                                    salesDetails_rec.setValue(det_prop,detail_data[det_prop]);
                                }else{
                                    salesDetails_rec.setValue(det_prop,new Date(nsrics.str_reformat_date(detail_data[det_prop])));
                                }
                            }
                            if(det_prop == 'custrecord_rics_sd_parent'){
                                salesDetails_rec.setValue(det_prop,st_id);
                            }
                            if(det_prop == 'custrecord_rics_sd_productitem'){
                                // find pi record
                                var pi_id = find_pi_record(detail_data.custrecord_rics_sd_productitemsku, detail_data.custrecord_rics_sd_productitemupc, detail_data.custrecord_rics_sd_transactionsaledesc);
                                if(pi_id != 0){ salesDetails_rec.setValue(det_prop,pi_id); }
                            }
                        }
                        var details_id = salesDetails_rec.save();
                        log.audit({title: 'GF_MR_RICS_GetPOSTransaction.js:rics_reduce-newSD', details: 'created new details rec - id: ' + details_id});
                    }
                    // create all tenders
                    for(var tend_data in data.arr_tenders){
                        var tender_data = data.arr_tenders[tend_data];
                        var salesTenders_rec = record.create({
                            type: 'customrecord_rics_sales_tenders'
                        });
                        for(var tend_prop in tender_data){
                            if(tender_data[tend_prop] != ''){
                                if(arr_date_fields.indexOf(tend_prop) == -1 && tender_data[tend_prop] != undefined){
                                    salesTenders_rec.setValue(tend_prop,tender_data[tend_prop]);
                                }else{
                                    salesTenders_rec.setValue(tend_prop,new Date(nsrics.str_reformat_date(tender_data[tend_prop])));
                                }

                                salesTenders_rec.setValue(tend_prop,tender_data[tend_prop]);
                            }
                            if(tend_prop == 'custrecord_rics_tender_parent'){
                                salesTenders_rec.setValue(tend_prop,st_id);
                            }
                        }
                        var tender_id = salesTenders_rec.save();
                        log.audit({title: 'GF_MR_RICS_GetPOSTransaction.js:rics_reduce-newTender', details: 'created new tender rec - id: ' + tender_id});
                    }
                }else{
                    log.audit({title: 'GF_MR_RICS_GetPOSTransaction.js:rics_reduce-updateST', details: 'updated sales ticket rec - id: ' + st_id});
                    // find existing details
                    for(var det_data in data.arr_details){
                        var detail_data = data.arr_details[det_data];
                        var sd_id = find_sales_details_record(st_id, detail_data.custrecord_rics_sd_ticketlinenumber);
                        if(sd_id != 0){
                            // update - load the record
                            var salesDetails_rec = record.load({
                                type: 'customrecord_rics_sales_details',
                                id: sd_id
                            });
                        }else{
                            // create new
                            var salesDetails_rec = record.create({
                                type: 'customrecord_rics_sales_details'
                            });
                        }
                        for(var det_prop in detail_data){
                            if(detail_data[det_prop] != ''){
                                if(arr_date_fields.indexOf(det_prop) == -1 && detail_data[det_prop] != undefined){
                                    salesDetails_rec.setValue(det_prop,detail_data[det_prop]);
                                }else{
                                    salesDetails_rec.setValue(det_prop,new Date(nsrics.str_reformat_date(detail_data[det_prop])));
                                }
                            }
                            if(det_prop == 'custrecord_rics_sd_parent'){
                                salesDetails_rec.setValue(det_prop,st_id);
                            }
                            if(det_prop == 'custrecord_rics_sd_productitem'){
                                // find pi record
                                var pi_id = find_pi_record(detail_data.custrecord_rics_sd_productitemsku, detail_data.custrecord_rics_sd_productitemupc, detail_data.custrecord_rics_sd_transactionsaledesc);
                                if(pi_id != 0){ salesDetails_rec.setValue(det_prop,pi_id); }
                            }
                        }
                        var details_id = salesDetails_rec.save();
                        log.audit({title: 'GF_MR_RICS_GetPOSTransaction.js:rics_reduce-' + ((sd_id != 0) ? 'updateDetail' : 'newDetail'), details: ((sd_id != 0) ? 'updated' : 'created new') + ' details rec - id: ' + details_id});
                    }
                    // find existing tenders
                    for(var tend_data in data.arr_tenders){
                        var tender_data = data.arr_tenders[tend_data];
                        var tend_id = find_sales_tender_record(st_id, tender_data.custrecord_rics_tender_paymentcardresp, tender_data.custrecord_rics_tender_amounttendered);
                        if(tend_id != 0){
                            // update - load the record
                            var salesTenders_rec = record.load({
                                type: 'customrecord_rics_sales_tenders',
                                id: tend_id
                            });
                        }else{
                            // create new
                            var salesTenders_rec = record.create({
                                type: 'customrecord_rics_sales_tenders'
                            });
                        }
                        for(var tend_prop in tender_data){
                            if(tender_data[tend_prop] != ''){
                                if(arr_date_fields.indexOf(tend_prop) == -1 && tender_data[tend_prop] != undefined){
                                    salesTenders_rec.setValue(tend_prop,tender_data[tend_prop]);
                                }else{
                                    salesTenders_rec.setValue(tend_prop,new Date(nsrics.str_reformat_date(tender_data[tend_prop])));
                                }
                            }
                            if(tend_prop == 'custrecord_rics_tender_parent'){
                                salesTenders_rec.setValue(tend_prop,st_id);
                            }
                        }
                        var tenders_id = salesTenders_rec.save();
                        log.audit({title: 'GF_MR_RICS_GetPOSTransaction.js:rics_reduce-' + ((tend_id != 0) ? 'updateTender' : 'newTender'), details: ((sd_id != 0) ? 'updated' : 'created new') + ' tenders rec - id: ' + tenders_id});
                    }
                }
            }

        }catch(ex) {
            log.error({title: 'GF_MR_RICS_GetPOSTransaction.js:rics_reduce', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * This summarize function is used to report important aspects about how the script performed
     * @param {*} summary - this object contains data regarding script performance.
     */
    function rics_summarize(summary) {
        try{
            var top_file_id = getTopFileId();
            log.audit('processed file', top_file_id);
            try{
            	file.delete({ id: top_file_id });
            }catch(e){
              	var fileObj = file.load({ id: top_file_id });
                fileObj.isInactive = true;
              	fileObj.isOnline = false;
                var fileId = fileObj.save(); // deactive it if it can't be removed
            }
            var currentScript = runtime.getCurrentScript();
            log.audit('currentScript', JSON.stringify(currentScript));
            log.audit('Usage Consumed', summary.usage);
            log.audit('Concurrency Number ', summary.concurrency);
            log.audit('Number of Yields', summary.yields);
            log.audit({title: 'Process Completed'});

			try{
                // keep running this script
                if(currentScript.deploymentId != "customdeploy_mr_rics_postransaction_od"){
                // this is not the ondemand script so call the on demand script
                var call_postransaction_od = task.create({
                    taskType: task.TaskType.MAP_REDUCE,
                    scriptId: 'customscript_mr_rics_getpostransaction',
                    deploymentId: 'customdeploy_mr_rics_postransaction_od'
                });
                call_postransaction_od.submit();
                }else{
                // this is the on demand script so call the on demand 2 script
                var call_postransaction_od = task.create({
                    taskType: task.TaskType.MAP_REDUCE,
                    scriptId: 'customscript_mr_rics_getpostransaction',
                    deploymentId: 'customdeploy_mr_rics_postransaction_od2'
                });
                call_postransaction_od.submit();
                }
            }catch(exception){
                log.audit({title: 'GF_MR_RICS_GetPOSTransaction.js:rics_summarize- calling on demand script', details: 'exception: ' + exception.toString() + ' : ' + exception.stack});
            }
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_GetPOSTransaction.js:rics_summarize', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    return {
        getInputData: rics_getInputData,
        map: rics_map,
        reduce: rics_reduce,
        summarize: rics_summarize
    };

    // =========================== extra functions
    /**
     * This function takes the required information to find a batch by the composite fields that uniquely identify it and performs a search then returns the results
     * @param {*} _batchStart - string/date - the date and time when the batch was started by the RICS user
     * @param {*} _terminalDesc - string - the description of the terminal use to create the batch
     * @param {*} _storeCode - integer - the number code of the store
     * @param {*} _userCashieCode - string - the users cashier identifier code
     * @param {*} _ticketNumber - integer - the indentifing number of the ticket at this store
     * @returns 
     */
    function find_st_record(_ticketCreatedOn, _terminalDesc, _storeCode, _userCashieCode, _ticketNumber){
        var nsrics = gf_nsrics.getIntegration();
        log.audit({title: 'GF_MR_RICS_GetPOSTransaction.js:find_st_record', details: '_ticketCreatedOn: ' + _ticketCreatedOn + ', _terminalDesc: ' + _terminalDesc + ', _storeCode: ' + _storeCode + ', _userCashieCode: ' + _userCashieCode + ', _ticketNumber: ' + _ticketNumber});
        var result = 0;
        try{
            var ticketSearchFilters = [];
            ticketSearchFilters.push(["custrecord_rics_st_createdon","on",nsrics.str_reformat_date(_ticketCreatedOn)]);
            ticketSearchFilters.push("AND");
            ticketSearchFilters.push(["custrecord_rics_st_terminaldescription","is",(_terminalDesc != null ? _terminalDesc : '')]);
            ticketSearchFilters.push("AND");
            ticketSearchFilters.push(["custrecord_rics_st_storecode","equalto",_storeCode]);
            ticketSearchFilters.push("AND");
            ticketSearchFilters.push(["custrecord_rics_st_usercashiercode","is",_userCashieCode]);
            ticketSearchFilters.push("AND");
            ticketSearchFilters.push(["custrecord_rics_st_ticketnumber","equalto",_ticketNumber]);
            // find the RICS Sales Ticket
            var customrecord_rics_sales_ticketSearchResults = search.create({
                type: "customrecord_rics_sales_ticket",
                filters: ticketSearchFilters,
                columns: [ ]
            }).run();
            var st_SearchResultSet = customrecord_rics_sales_ticketSearchResults.getRange({start: 0, end: 1});
            if (st_SearchResultSet != null && st_SearchResultSet.length > 0){
                result = st_SearchResultSet[0].id;
            }
        }catch(ex){
            log.error({title: 'GF_MR_RICS_GetPOSTransaction.js:find_st_record', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
        log.audit({title: 'GF_MR_RICS_GetPOSTransaction.js:find_st_record', details: 'result: ' + result});
        return result;
    }

    /**
     * This function takes the parent ticket id and upc code to find an existing sales details record
     * @param {*} _detail_parent_ticket - integer - the id of the parent record
     * @param {*} _ticket_line_num - integer - the number of the line on the details ticket
     * @returns 
     */
    function find_sales_details_record(_detail_parent_ticket, _ticket_line_num){
        var result = 0;
        // find the RICS Purchase Order Details Record
        var salesDetails_SearchResults = search.create({
            type: "customrecord_rics_sales_details",
            filters: [
                ["isinactive","is","F"], "AND",
                ["custrecord_rics_sd_parent","anyof",_detail_parent_ticket], "AND",
                ["custrecord_rics_sd_ticketlinenumber","equalto",_ticket_line_num]
            ],
            columns: [
                search.createColumn({
                    name: "internalid",
                    sort: search.Sort.DESC,
                    label: "ID"
                })
            ]
        }).run();
        var salesDetails_Results = salesDetails_SearchResults.getRange({start: 0, end: 1});
        if (salesDetails_Results != null && salesDetails_Results.length > 0){
            result = salesDetails_Results[0].id;
        }
        log.debug({title: 'GF_MR_RICS_GetPOSTransaction.js:find_sales_details_record', details: 'result: ' + result});
        return result;
    }

    /**
     * This function takes the parent ticket id and the payment card response to identify a tender record
     * @param {*} _tender_parent_ticket - integer - the id of the parent record
     * @param {*} _tender_payment_card_resp - string - the payment card response string
     * @returns 
     */
    function find_sales_tender_record(_tender_parent_ticket, _tender_payment_card_resp, _tender_amount){
        var result = 0;
        // find the RICS Sales Tender Search
        var tenders_filters = [];
        tenders_filters.push(["isinactive","is","F"]);
        tenders_filters.push("AND");
        tenders_filters.push(["custrecord_rics_tender_parent","anyof",_tender_parent_ticket]);
        if(_tender_payment_card_resp != undefined && _tender_payment_card_resp != ''){
            tenders_filters.push("AND");
            tenders_filters.push(["custrecord_rics_tender_paymentcardresp","is",_tender_payment_card_resp]);
        }
        if(_tender_payment_card_resp != undefined && _tender_payment_card_resp != ''){
            tenders_filters.push("AND");
            tenders_filters.push(["custrecord_rics_tender_amounttendered","equalto",_tender_amount]);
        }
        var salesTenders_SearchResults = search.create({
            type: "customrecord_rics_sales_tenders",
            filters: tenders_filters,
            columns: [ ]
        }).run();
        var salesTender_Results = salesTenders_SearchResults.getRange({start: 0, end: 1});
        if (salesTender_Results != null && salesTender_Results.length > 0){
            result = salesTender_Results[0].id;
        }
        log.debug({title: 'GF_MR_RICS_GetPOSTransaction.js:find_sales_tender_record', details: 'result: ' + result});
        return result;
    }


    function find_tender_type_mapping(_tenderDescription){
        log.debug({title: 'GF_MR_RICS_GetPOSTransaction.js:find_tender_type_mapping', details: '_tenderDescription: ' + _tenderDescription});
        var result_id = 0;
        // lowercase tenderdesc
        var lower = _tenderDescription.toLowerCase();
        var trimmed = (lower.indexOf('-') > -1) ? lower.substring(lower.indexOf('-') +2) : lower;
        // get tender type custom list - customlist_rics_tender_types
        var searchColumn = search.createColumn({ name : 'name' });
        var listSearch = search.create({ type : 'customlist_rics_tender_types', columns : searchColumn });
        var listArray = [];
        listSearch.run().each(function(searchResult) {
            listArray[searchResult.getValue(searchColumn)] = searchResult.id;
            return true;
        });
        // tender type list id
        var tender_type_list_id = listArray[trimmed];
        log.debug({title: 'GF_MR_RICS_GetPOSTransaction.js:find_tender_type_mapping', details: 'tender_type_list_id: ' + (tender_type_list_id != undefined ? tender_type_list_id : 'undefined') });
        if(tender_type_list_id != undefined){
            // found = get mapping id and set that record on this field
            result_id = getMappingRecordId(tender_type_list_id);
            if(result_id == 0){ result_id = createMappingRecord(trimmed, false); } // this happens when the list id exists but the mapping record hasn't been created
        }else{
            // not found = add mapping (and alert accounting)
            result_id = createMappingRecord(trimmed, true);
        }
        return result_id;
    }


    function getMappingRecordId(_tender_type_list_id){
        var result_id = 0;
        var customrecord_rics_tender_type_mappingSearchObj = search.create({
            type: "customrecord_rics_tender_type_mapping",
            filters: [
               ["custrecord_rics_ttmf_tender_type","anyof",_tender_type_list_id]
            ],
            columns: [ ]
        });
        customrecord_rics_tender_type_mappingSearchObj.run().each(function(result){
            // .run().each has a limit of 4,000 results
            if(result_id == 0){ result_id = result.id; }
            return true;
        });
        return result_id;
    }


    function createMappingRecord(_rics_tender_type, _flg_add_to_list){
        if(_flg_add_to_list == true){
            // get list internal id
            var searchColumn = search.createColumn({ name : 'internalid' });
            var listSearch = search.create({ type : 'customlist', filters: ["scriptid","is","customlist_rics_tender_types"], columns : searchColumn }).run();
            var list_search_result = listSearch.getRange({start: 0, end: 1});
            var mappingListId = list_search_result[0].getValue('internalid');
            // add tender type to tender type list
            if(mappingListId != 0){
                var customListRec = record.load({
                    type: 'customlist',
                    id: mappingListId,
                    isDynamic: true
                });
            }
            customListRec.selectNewLine({ sublistId: 'customvalue' });
            customListRec.setCurrentSublistValue({
                sublistId: 'customvalue',
                fieldId: 'value',
                value: _rics_tender_type,
                ignoreFieldChange: true
            });
            customListRec.commitLine({sublistId: 'customvalue'});

            customListRec.save();
        }
        // get the NEW id value
        var searchColumn = search.createColumn({ name : 'name' });
        var listSearch = search.create({ type : 'customlist_rics_tender_types', columns : searchColumn });
        var listArray = [];
        listSearch.run().each(function(searchResult) {
            listArray[searchResult.getValue(searchColumn)] = searchResult.id;
            return true;
        });
        var new_tt_id = listArray[_rics_tender_type];
        // add tender type to a mapping record and return the record id
        var ricsTenderTypeMappingRec = record.create({
            type: 'customrecord_rics_tender_type_mapping',
            isDynamic: true
        });
        ricsTenderTypeMappingRec.setValue({ fieldId:'custrecord_rics_ttmf_tender_type',value:new_tt_id });
        var rttm = ricsTenderTypeMappingRec.save();
        return getMappingRecordId(new_tt_id);
    }


    /**
     * This function takes a string of the upc value and tries to find the related RICS Product Item custom record
     * @param {*} _sku - string - the sku of the item the function will try to find
     * @param {*} _upc - string - the upc of the item the function will try to find
     * @returns integer - the internal id of the inventory item that was found
     */
    function find_pi_record(_sku, _upc, _transaction_desc) {
        var result = 0;
        // find the Product Details Record
        var pi_SearchResults = search.create({
            type: "customrecord_rics_product_item",
            filters: [
                ["isinactive", "is", false], "AND",
                ["custrecord_rics_pi_parent_pd.custrecord_rics_pd_sku","is",_sku], "AND",
                ["custrecord_rics_pi_upcs", "contains", '"' + _upc + '"']
            ],
            columns: [ ]
        }).run();
        var pi_SearchResultSet = pi_SearchResults.getRange({ start: 0, end: 1 });
        if (pi_SearchResultSet != null && pi_SearchResultSet.length > 0) {
            result = pi_SearchResultSet[0].id;
        }
        if(result == 0){
            /*
            switch(_transaction_desc){
                case 'Regular Sale':
                    result = 130805;
                    break;
                case 'Return':
                    result = 130804;
                    break;
                case 'Coupon':
                    result = 6728;
                    break;
            }
            */
        }
        log.audit({title: 'GF_MR_RICS_GetPOSTransaction.js:find_pi_record', details: '_sku: ' + _sku + ', _upc: ' + _upc + ', result: ' + result});
        return result;
    }

    /**
     * This function searches the GetPOSTransaction_Responses folder for any documents and returns the count
     * @returns void
     */
    function copyFilesFromResponseFolder(){
        var fileSearchObj = search.create({
            type: "file",
            filters: [
               ["folder","anyof","3550"]
            ],
            columns: [
               search.createColumn({
                  name: "created",
                  sort: search.Sort.ASC
               })
            ]
        });
        fileSearchObj.run().each(function(result){
            // .run().each has a limit of 4,000 results
            var file_id = result.id;
            log.audit({title: 'GF_MR_RICS_GetPOSTransaction.js:copyFilesFromResponseFolder', details: 'file_id: ' + file_id + ', result: ' + JSON.stringify(result)});
            var fileObj = file.copy({
                id: parseInt(file_id),
                folder: 3549,
                conflictResolution: 'OVERWRITE' // shouldn't have a conflict but if we get here we want the new data
            });
            return true;
        });
    }

    /**
     * This function Searches the processing folder for data to process
     * @returns file_id - integer - the id of the file that will be loaded for processing
     */
    function getTopFileId(){
        var file_id = 0;
        var fileSearch_SearchResults = search.create({
            type: "file",
            filters: [
               ["folder","anyof","3549"], "AND",
               ["availablewithoutlogin","is","T"]
            ],
            columns: [
               search.createColumn({
                  name: "created",
                  sort: search.Sort.ASC
               })
            ]
        }).run();
        var fileSearch_Results = fileSearch_SearchResults.getRange({start: 0, end: 1});
        if(fileSearch_Results != null && fileSearch_Results[0] != undefined){
            file_id = fileSearch_Results[0].id;
        }
        return file_id;
    }

});