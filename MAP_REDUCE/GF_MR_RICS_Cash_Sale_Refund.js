/**
 *@NApiVersion 2.1
 *@NScriptType MapReduceScript
 *
Script Name:   GF_MR_RICS_Cash_Sale_Refund.js
Author:        Mark Robinson
 */

define(['N/log', 'N/search', 'N/format', 'N/record', 'N/runtime', 'N/task', '/SuiteScripts/GF_LIB/gf_nsrics'], function(log, search, format, record, runtime, task, gf_nsrics) {

    /**
     * In this getinput function we search for all of the RICS Sales Details Custom records that are eligible to be processed into a Cash Sale or Cash Refund and build the list of them to process
     * @param {*} context - this variable is not utilized in this function.
     * @returns - an array containing the data returned from the API calls.
     */
    function rics_getInputData(context){
        try {
            // if the script was called with a ticket id param use that
            var currentScript = runtime.getCurrentScript();
            var ticket_id = currentScript.getParameter({ name: 'custscript_sales_ticket_id' });
            log.audit({title: 'GF_MR_RICS_Cash_Sale_Refund.js:rics_getInputData-param', details: 'ticket_id: ' + (ticket_id != null ? ticket_id : 'EMPTY')});
            var nsrics = gf_nsrics.getIntegration();
            var open_period_range = nsrics.getOpenPeriodRange(); // .earliest_open_date - .latest_open_date

            if(ticket_id != null){
                var searchFilters = [
                    ["custrecord_rics_sd_parent","anyof",ticket_id], "AND",
                    ["custrecord_rics_sd_parent.custrecord_rics_st_ticketvoided", "is", "F"], "AND",
                    ["custrecord_rics_sd_parent.custrecord_rics_st_ticketsuspended","is","F"], "AND",
                    ["custrecord_rics_sd_parent.custrecord_rics_st_saledatetime","within",open_period_range.earliest_open_date,open_period_range.latest_open_date], "AND",
                    [
                        [
                            ["custrecord_rics_sd_transaction_rec","anyof","@NONE@"]
                        ],"OR",
                        [
                            ["custrecord_rics_sd_transaction_rec.mainline","is","T"]
                        ]
                    ], "AND",
                    ["custrecord_rics_sd_parent.custrecord_rics_st_saledatetime","within","thismonth"]
                ];
            }else{
                var searchFilters = [
                    ["custrecord_rics_sd_parent.custrecord_rics_st_ticketvoided","is","F"], "AND",
                    ["custrecord_rics_sd_parent.custrecord_rics_st_ticketsuspended","is","F"], "AND",
                    [
                        [
                            ["custrecord_rics_sd_transaction_rec","anyof","@NONE@"]
                        ],"OR",
                        [
                            ["custrecord_rics_sd_transaction_rec.mainline","is","T"]
                        ]
                    ], "AND",
                    // ["custrecord_rics_sd_parent.custrecord_rics_st_saledatetime","within",open_period_range.earliest_open_date,open_period_range.latest_open_date], "AND",
                    ["custrecord_rics_sd_parent.custrecord_rics_st_ticketmodifiedon","onorafter","hoursago24"], "AND",
                    ["custrecord_rics_sd_parent.custrecord_rics_st_saledatetime","within","thismonth"]
                ];
            }
            var arr_results_grouped_by_ticket_number = [];
            var arr_ticket_numbers = [];
            var customrecord_rics_sales_detailsSearchObj = search.create({
                type: "customrecord_rics_sales_details",
                filters: searchFilters,
                columns: [
                   "custrecord_rics_sd_activeprice",
                   "custrecord_rics_sd_additionalinformation",
                   "custrecord_rics_sd_amountpaid",
                   "custrecord_rics_sd_commissionamount",
                   "custrecord_rics_sd_cost",
                   "custrecord_rics_sd_couponamountapplied",
                   "custrecord_rics_sd_couponcode",
                   "custrecord_rics_sd_coupondescription",
                   "created",
                   "custrecord_rics_sd_discountamount",
                   "custrecord_rics_sd_discountrate",
                   "custrecord_rics_sd_discountreasondesc",
                   "custrecord_rics_sd_giftcardnumber",
                   "id",
                   "isinactive",
                   "internalid",
                   "custrecord_rics_sd_isfbqualified",
                   "custrecord_rics_sd_issellable",
                   "custrecord_rics_sd_isuseroverridden",
                   "language",
                   search.createColumn({
                      name: "lastmodified",
                      sort: search.Sort.DESC
                   }),
                   "lastmodifiedby",
                   "custrecord_rics_sd_transaction_rec",
                   "custrecord_rics_sd_netsales",
                   "custrecord_rics_sd_orgtaxdesc",
                   "custrecord_rics_sd_perkamount",
                   "custrecord_rics_sd_productitem",
                   "custrecord_rics_sd_productitemcolumn",
                   "custrecord_rics_sd_productitemrow",
                   "custrecord_rics_sd_productitemsku",
                   "custrecord_rics_sd_productitemupc",
                   "custrecord_rics_sd_quantity",
                   "custrecord_rics_sd_modifiedby",
                   "custrecord_rics_sd_modifiedon",
                   "custrecord_rics_sd_parent",
                   "custrecord_rics_sd_ricsuserid",
                   "custrecord_rics_sd_retailprice",
                   "custrecord_rics_sd_returndescription",
                   "custrecord_rics_sd_skucomment",
                   "custrecord_rics_sd_saleprice",
                   "custrecord_rics_sd_salespersonfirstname",
                   "custrecord_rics_sd_salespersonlastname",
                   "custrecord_rics_sd_salespersonlogin",
                   "custrecord_rics_sd_salespersonmiddlename",
                   "custrecord_rics_sd_shiptocustomer",
                   "custrecord_rics_sd_shippingcharges",
                   "custrecord_rics_sd_taxamount",
                   "custrecord_rics_sd_taxrate",
                   "custrecord_rics_sd_taxableamount",
                   "custrecord_rics_sd_ticketlinenumber",
                   "custrecord_rics_sd_transactionsaledesc",
                   "custrecord_rics_sd_usercashiercode",
                   "custrecord_rics_sd_salesticketstorecode",
                   search.createColumn({
                      name: "custrecord_rics_st_saledatetime",
                      join: "CUSTRECORD_RICS_SD_PARENT"
                   }),
                   search.createColumn({
                      name: "custrecord_rics_st_customeraccountnumber",
                      join: "CUSTRECORD_RICS_SD_PARENT"
                   }),
                   search.createColumn({
                      name: "custrecord_rics_st_customerbilling",
                      join: "CUSTRECORD_RICS_SD_PARENT"
                   }),
                   search.createColumn({
                      name: "custrecord_rics_st_customeremail",
                      join: "CUSTRECORD_RICS_SD_PARENT"
                   }),
                   search.createColumn({
                      name: "custrecord_rics_st_customerfirstname",
                      join: "CUSTRECORD_RICS_SD_PARENT"
                   }),
                   search.createColumn({
                      name: "custrecord_rics_st_customerlastname",
                      join: "CUSTRECORD_RICS_SD_PARENT"
                   }),
                   search.createColumn({
                      name: "custrecord_rics_st_customermailing",
                      join: "CUSTRECORD_RICS_SD_PARENT"
                   }),
                   search.createColumn({
                      name: "custrecord_rics_st_customerphonenumber",
                      join: "CUSTRECORD_RICS_SD_PARENT"
                   }),
                   search.createColumn({
                      name: "custrecord_rics_st_customerid",
                      join: "CUSTRECORD_RICS_SD_PARENT"
                   }),
                   search.createColumn({
                      name: "custrecord_rics_st_ticketnumber",
                      join: "CUSTRECORD_RICS_SD_PARENT"
                   }),
                   search.createColumn({
                      name: "custrecord_rics_st_ticketvoided",
                      join: "CUSTRECORD_RICS_SD_PARENT"
                   }),
                   search.createColumn({
                      name: "custrecord_rics_st_ticketsuspended",
                      join: "CUSTRECORD_RICS_SD_PARENT"
                   }),
                   search.createColumn({
                      name: "custrecord_rics_st_storename",
                      join: "CUSTRECORD_RICS_SD_PARENT"
                   }),
                   search.createColumn({
                      name: "custrecord_rics_st_ticketnumber_text",
                      join: "CUSTRECORD_RICS_SD_PARENT"
                   }),
                   search.createColumn({
                      name: "custrecord_rics_st_receipt_total",
                      join: "CUSTRECORD_RICS_SD_PARENT"
                   })
                ]
            }).run();
            var customrecord_rics_sales_detailsSearchResults = customrecord_rics_sales_detailsSearchObj.getRange({start: 0, end: 1000});
            // log.debug({title: 'GF_MR_RICS_Cash_Sale_Refund.js:rics_getInputData', details: 'record count: ' + ((customrecord_rics_sales_detailsSearchResults != null) ? customrecord_rics_sales_detailsSearchResults.length : 0)});
            for(var result in customrecord_rics_sales_detailsSearchResults){
                var ticket_number = customrecord_rics_sales_detailsSearchResults[result].getValue({name: 'custrecord_rics_st_ticketnumber_text', join: 'CUSTRECORD_RICS_SD_PARENT'});
                var parent_rec_id = customrecord_rics_sales_detailsSearchResults[result].getValue({name: 'custrecord_rics_sd_parent'});
                // log.debug({title: 'GF_MR_RICS_Cash_Sale_Refund.js:rics_getInputData', details: 'ticket_number: ' + ticket_number});
                if(arr_ticket_numbers.indexOf(`${ticket_number}_${parent_rec_id}`) > -1){
                    // add result to ticket number
                    var arr_current_ticket_data = arr_results_grouped_by_ticket_number[arr_ticket_numbers.indexOf(`${ticket_number}_${parent_rec_id}`)];
                    arr_current_ticket_data.push(customrecord_rics_sales_detailsSearchResults[result]); // push new data to current data
                    arr_results_grouped_by_ticket_number[arr_ticket_numbers.indexOf(`${ticket_number}_${parent_rec_id}`)] = arr_current_ticket_data;
                }else{
                    // add new ticket number
                    arr_ticket_numbers.push(`${ticket_number}_${parent_rec_id}`);
                    arr_results_grouped_by_ticket_number[arr_ticket_numbers.indexOf(`${ticket_number}_${parent_rec_id}`)] = [customrecord_rics_sales_detailsSearchResults[result]];
                }
            }

            return arr_results_grouped_by_ticket_number;
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_Cash_Sale_Refund.js:rics_getInputData', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this mapping function we take the results from the getinput function and use them to define the objects that will create or update the NetSuite Transaction records
     * @param {*} context - this should contain the data from the get input function.
     * @returns void (note: the const context variable is written to and used in the reduce function)
     */
    function rics_map(context){
        try{
            // hardcoded values
            var gstl = findDefaultItem('General Sale Tax Line');
            var gri = findDefaultItem('General Refund Item');
            var grsi = findDefaultItem('General Regular Sale Item');
            var poscd = findDefaultItem('POS Discount');

            var data = JSON.parse(context.value);
            if(data == null){ return true; }
            log.audit({title: 'GF_MR_RICS_Cash_Sale_Refund.js:rics_map', details: 'data: ' + JSON.stringify(data)});
            var newInfo_obj = {};

            // check for customer - returns id of the customer or the id for the No-Name / Walk-In Customer record
            newInfo_obj.entity = findCustomer(data[0].values);
            setSubsidiarySublist(newInfo_obj.entity);
            newInfo_obj.trandate = data[0].values['CUSTRECORD_RICS_SD_PARENT.custrecord_rics_st_saledatetime'];
            var loc_id = findLocation(data[0].values);
            if(loc_id == undefined || loc_id == 0){
                log.audit({title: 'GF_MR_RICS_Cash_Sale_Refund.js:rics_map-loc-not-found', details: 'StoreCode: ' + data[0].values['custrecord_rics_sd_salesticketstorecode'] + ' - not found'});
                return true;
            }
            newInfo_obj.location = loc_id;
            var sub_id = findSubsidiary(loc_id);
            newInfo_obj.subsidiary = sub_id;
            newInfo_obj.salesrep = findSalesRep(data[0].values, sub_id);
            newInfo_obj.memo = ''; // not sure if we want to set the memo to something helpful

            newInfo_obj.custbody_gf_rics_sales_ticket_id = data[0].values['custrecord_rics_sd_parent'];
            newInfo_obj.custbody_gf_rics_ticket_number = data[0].values['CUSTRECORD_RICS_SD_PARENT.custrecord_rics_st_ticketnumber'];
            newInfo_obj.custbody_gf_rics_ticket_date = data[0].values['CUSTRECORD_RICS_SD_PARENT.custrecord_rics_st_saledatetime'];
            newInfo_obj.custbody_gf_rics_transaction_type = data[0].values['custrecord_rics_sd_transactionsaledesc'];
            newInfo_obj.custbody_gf_rics_store_name = data[0].values['CUSTRECORD_RICS_SD_PARENT.custrecord_rics_st_storename'];
            newInfo_obj.custbody_gf_rics_store_code = data[0].values['custrecord_rics_sd_salesticketstorecode'];
            newInfo_obj.custbody_gf_rics_return_reason = data[0].values['custrecord_rics_sd_returndescription'];
            newInfo_obj.receipt_total = parseFloat(data[0].values['CUSTRECORD_RICS_SD_PARENT.custrecord_rics_st_receipt_total']);

            var arr_sale_items = [];
            var arr_refund_items = [];
            var arr_coupon_items = [];
            var cs_tax_line = {
                sale_or_refund: 'cashsale',
                item: gstl, // General Sale Tax Line
                quantity: 1,
                rate: 0,
                amount: 0,
                taxcode_id: '-8', // '-Not Taxable-'
                sd_trans_type: '',
                sd_return_reason: ''
            }; // this object will be appended to the cash sale line array
            var cr_tax_line = {
                sale_or_refund: 'cashrefund',
                item: gstl, // General Sale Tax Line
                quantity: 1,
                rate: 0,
                amount: 0,
                taxcode_id: '-8', // '-Not Taxable-'
                sd_trans_type: '',
                sd_return_reason: ''
            }; // this object will be appended to the cash refund line array
            newInfo_obj.discountitem = '0';
            newInfo_obj.discountrate = 0;
            newInfo_obj.csdiscountitem = '0';
            newInfo_obj.csdiscountrate = 0;
            newInfo_obj.crdiscountitem = '0';
            newInfo_obj.crdiscountrate = 0;
            for(var d in data){
                var item_data = data[d].values;
                var temp_item = {};

                temp_item.sale_or_refund = (item_data.custrecord_rics_sd_transactionsaledesc == 'Return') ? 'cashrefund' : 'cashsale';
                if(item_data.custrecord_rics_sd_transactionsaledesc == 'Gift Card External'){
                    temp_item.item = 135019; // GIFT CARD EXTERNAL
                }else{
                    temp_item.item = findItem(item_data.custrecord_rics_sd_productitemupc, item_data.custrecord_rics_sd_productitemsku);
                }
                if(temp_item.item == 0){
                    log.audit({title: 'GF_MR_RICS_Cash_Sale_Refund.js:rics_map-no items', details: 'temp_item.item: ' + temp_item.item + ', ticket number: ' + newInfo_obj.custbody_gf_rics_ticket_number + ', item_data: ' + JSON.stringify(item_data)});
                    if(temp_item.sale_or_refund == 'cashrefund'){
                        // in the case of a return the item might not be found for several reasons - it might be deactivated, it could have been merged, etc. and we still need to process the refund
                        temp_item.item = gri; // General Refund Item
                    }
                    if(temp_item.sale_or_refund == 'cashsale'){
                        if(item_data.custrecord_rics_sd_transactionsaledesc != 'Coupon'){
                            temp_item.item = grsi; // General Regular Sale Item
                        }
                    }
                }
                // Coupons
                if(item_data.custrecord_rics_sd_transactionsaledesc == 'Coupon'){
                    arr_coupon_items.push(data[d].id);
                    // add the coupon to the discount total, but don't add it to the line items
                    var temp_couponamount = parseFloat(item_data.custrecord_rics_sd_amountpaid);
                    if(newInfo_obj.discountitem == '0'){ newInfo_obj.discountitem = poscd; } // POS Custom Discount
                    newInfo_obj.discountrate += (isNaN(temp_couponamount) ? 0 : temp_couponamount); // typically negative
                    temp_item.amount = (isNaN(temp_couponamount) ? 0 : temp_couponamount);
                    temp_item.coupon = true;
                    log.audit({title: 'GF_MR_RICS_Cash_Sale_Refund.js:rics_map-temp_couponamount', details: 'temp_couponamount: ' + temp_couponamount + ', newInfo_obj: ' + JSON.stringify(newInfo_obj)});
                }else{
                    temp_item.coupon = false;
                    temp_item.quantity = item_data.custrecord_rics_sd_quantity;
                    temp_item.quantity = (parseInt(temp_item.quantity) < 0) ? parseInt(temp_item.quantity) * -1 : parseInt(temp_item.quantity);
                    temp_item.rate = item_data.custrecord_rics_sd_activeprice;
                    if(item_data.custrecord_rics_sd_transactionsaledesc == 'Gift Card External' && temp_item.quantity == 1){
                        temp_item.rate = 0;
                        temp_item.amount = item_data.custrecord_rics_sd_amountpaid;
                    }else{
                        temp_item.amount = parseFloat(item_data.custrecord_rics_sd_activeprice) * temp_item.quantity;
                        if(temp_item.amount == undefined || temp_item.amount === '' || Number.isNaN(temp_item.amount)){
                            temp_item.amount = parseFloat(item_data.custrecord_rics_sd_amountpaid) / temp_item.quantity;
                            if(temp_item.amount == undefined || temp_item.amount === '' || Number.isNaN(temp_item.amount)){
                                continue; // empty amounts will cause an error
                            }
                        }else{
                            if(parseFloat(temp_item.amount) < 0.00){
                                temp_item.amount = parseFloat(temp_item.amount) * -1;
                            }
                        }
                    }
                }
                // check if the net sales is different (could be discounted need to adjust line pricing to match)
                if(item_data.custrecord_rics_sd_netsales != null && item_data.custrecord_rics_sd_netsales != temp_item.amount && item_data.custrecord_rics_sd_transactionsaledesc != 'Gift Card External'){
                    temp_item.amount = item_data.custrecord_rics_sd_netsales;
                }
                temp_item.taxcode_id = '-8'; // '-Not Taxable-'
                temp_item.sd_id = data[d].id;
                temp_item.sd_trans_type = item_data.custrecord_rics_sd_transactionsaledesc;
                temp_item.sd_return_reason = item_data.custrecord_rics_sd_returndescription;
                temp_item.sd_net_sales = item_data.custrecord_rics_sd_netsales;

                if(item_data.custrecord_rics_sd_discountamount != ''){
                    // set RICS discount amount custom column on the line item
                    temp_item.sd_discount_amount = item_data.custrecord_rics_sd_discountamount;
                    log.audit({title: 'GF_MR_RICS_Cash_Sale_Refund.js:rics_map-discountrate', details: 'item_data.custrecord_rics_sd_discountrate: ' + item_data.custrecord_rics_sd_discountrate});
                    if(item_data.custrecord_rics_sd_discountrate == '100.0%'){
                        temp_item.amount = 0.00;
                    }
                }

                if(item_data.custrecord_rics_sd_taxamount != undefined && item_data.custrecord_rics_sd_taxamount != ''){
                    var invert_cr_negative_tax = parseFloat(item_data.custrecord_rics_sd_taxamount);
                    if(invert_cr_negative_tax < 0.00){
                        invert_cr_negative_tax = invert_cr_negative_tax * -1;
                        cr_tax_line.amount += parseFloat(invert_cr_negative_tax);
                    }else{
                        cs_tax_line.amount += parseFloat(item_data.custrecord_rics_sd_taxamount);
                    }
                }

                if(temp_item.sale_or_refund == 'cashrefund'){
                    // this needs to be right in order to handle two scenarios - 1 an exchange where we have a negative value & 2 a return where the entire value should be negative
                    if(newInfo_obj.receipt_total != undefined && parseFloat(newInfo_obj.receipt_total) >= 0.00){
                        if(newInfo_obj.crdiscountitem == '0'){ newInfo_obj.crdiscountitem = poscd; }
                        var temp_returnamount = parseFloat(item_data.custrecord_rics_sd_amountpaid);
                        if(temp_returnamount > 0){ temp_returnamount = (temp_returnamount * -1); }
                        newInfo_obj.crdiscountrate += temp_returnamount;
                    }
                    arr_refund_items.push(temp_item); // --------------------------------- add to refund line items array
                }else{
                    arr_sale_items.push(temp_item); // --------------------------------- add to sales line items array
                }
            }
            // check actual item count and pull the coupons out
            if(arr_sale_items.length > 0){
                var sale_items = arr_sale_items.find(function(object){ if(!object.coupon){ return true; } });
                if(sale_items == undefined){
                    arr_sale_items = [];
                }
            }
            if(arr_refund_items.length > 0){
                var refund_items = arr_refund_items.find(function(object){ if(!object.coupon){ return true; } });
                if(refund_items == undefined){
                    arr_refund_items = [];
                }
            }
            log.audit({title: 'GF_MR_RICS_Cash_Sale_Refund.js:rics_map-array length', details: 'arr_sale_items.length: ' + arr_sale_items.length + ', arr_refund_items.length: ' + arr_refund_items.length});
            if(arr_sale_items.length > 0 && arr_refund_items.length > 0){
                // in the scenario where there is an exchange
                // if the tax total is positive it belongs on the Cash Sale else it belongs on the Cash Refund
                // these will both be represented as positive numbers in these objects, but the cr tax line is treated as negative tax
                log.audit({title: 'GF_MR_RICS_Cash_Sale_Refund.js:rics_map-array length', details: 'TAX LINES - cs_tax_line: ' + JSON.stringify(cs_tax_line)});
                log.audit({title: 'GF_MR_RICS_Cash_Sale_Refund.js:rics_map-array length', details: 'TAX LINES - cr_tax_line: ' + JSON.stringify(cr_tax_line)});
                var tax_total = (cs_tax_line.amount + (cr_tax_line.amount * -1));
                if(cs_tax_line.amount > cr_tax_line.amount){
                    cs_tax_line.amount = tax_total.toFixed(2);
                    cr_tax_line.amount = 0.00;
                }else{
                    cr_tax_line.amount = tax_total.toFixed(2);
                    cs_tax_line.amount = 0.00;
                }
                // allow coupons to affect one transaction or the other but not both
                newInfo_obj.csdiscountitem = newInfo_obj.discountitem;
                newInfo_obj.csdiscountrate = newInfo_obj.discountrate;
                newInfo_obj.crdiscountitem = '0';
                newInfo_obj.crdiscountrate = 0;
            }else{
                newInfo_obj.csdiscountitem = newInfo_obj.discountitem;
                newInfo_obj.csdiscountrate = newInfo_obj.discountrate;
                newInfo_obj.crdiscountitem = newInfo_obj.discountitem;
                newInfo_obj.crdiscountrate = newInfo_obj.discountrate;
            }
            // add the fully valued tax line
            if(arr_sale_items.length > 0){ arr_sale_items.push(cs_tax_line); }
            if(arr_refund_items.length > 0){ arr_refund_items.push(cr_tax_line); }

            newInfo_obj.saleitems = arr_sale_items;
            newInfo_obj.refunditems = arr_refund_items;
            newInfo_obj.couponitems = arr_coupon_items;

            if(arr_sale_items.length > 0 && arr_refund_items.length > 0){
                newInfo_obj.is_exchange = true;
                if(newInfo_obj.receipt_total > 0.00){
                    newInfo_obj.custbody_gf_rics_transaction_type = 'NET POSITIVE EXCHANGE';
                }else if(newInfo_obj.receipt_total < 0.00){
                    newInfo_obj.custbody_gf_rics_transaction_type = 'NET NEGATIVE EXCHANGE';
                }
                if(newInfo_obj.receipt_total == 0.00 || newInfo_obj.receipt_total == '0.00'){
                    newInfo_obj.custbody_gf_rics_transaction_type = 'FLAT EXCHANGE';
                }
            }else{
                newInfo_obj.is_exchange = false;
                if(arr_sale_items.length > 0){
                    newInfo_obj.custbody_gf_rics_transaction_type = 'SALE ONLY';
                }else{
                    newInfo_obj.custbody_gf_rics_transaction_type = 'RETURN ONLY';
                }
            }

            data[0].ticketInfo = newInfo_obj;
            log.audit({title: 'GF_MR_RICS_Cash_Sale_Refund.js:rics_map-arr_sale_items', details: 'arr_sale_items: ' + JSON.stringify(arr_sale_items)});
            log.audit({title: 'GF_MR_RICS_Cash_Sale_Refund.js:rics_map-arr_refund_items', details: 'arr_refund_items: ' + JSON.stringify(arr_refund_items)});

            if(arr_sale_items.length > 0 || arr_refund_items.length > 0){
                context.write({ key: context.key, value: data[0] });
            }else{
                log.error({title: 'GF_MR_RICS_Cash_Sale_Refund.js:rics_map-no items', details: 'No items in transaction, Ticket ID: ' + newInfo_obj.custbody_gf_rics_sales_ticket_id[0].value});
            }
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_Cash_Sale_Refund.js:rics_map', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this reduce function we take the objects generated by the mapping function and either create or update the related NetSuite Transaction records
     * @param {*} context - this should be the mapped object containing all of the fields and data that need to be on the Cash Sale
     * @returns void
     */
    function rics_reduce(context){
        try{
            for(var c_val in context.values){ // go through all sales tickets
                var data = JSON.parse(context.values[c_val]);
                if(data == null){ continue; }
                log.audit({title: 'GF_MR_RICS_Cash_Sale_Refund.js:rics_reduce-c_val='+c_val, details: 'data: ' + JSON.stringify(data)});
                log.audit({title: 'GF_MR_RICS_Cash_Sale_Refund.js:rics_reduce-c_val='+c_val, details: 'data.ticketInfo: ' + JSON.stringify(data.ticketInfo)});
                log.audit({title: 'GF_MR_RICS_Cash_Sale_Refund.js:rics_reduce-c_val='+c_val, details: 'data.ticketInfo.csdiscountrate: ' + data.ticketInfo.csdiscountrate + ', data.ticketInfo.crdiscountrate: ' + data.ticketInfo.crdiscountrate});

                // ------------------------------------------------------------ SALES

                var cs_transaction_id = ((data.ticketInfo.saleitems).length > 0) ? findCashSale(data.ticketInfo) : 0;
                log.audit({title: 'GF_MR_RICS_Cash_Sale_Refund.js:rics_reduce-SAVE-c_val='+c_val, details: 'cs_transaction_id: ' + cs_transaction_id});
                if((data.ticketInfo.saleitems).length > 0){
                    if(cs_transaction_id == 0){
                        // items exist but no transaction record was found - create a new transaction record
                        var cashSale_Rec = record.create({
                            type: 'cashsale'
                        });
                        cashSale_Rec.setValue('entity', data.ticketInfo.entity);
                        cashSale_Rec.setValue('trandate', new Date(data.ticketInfo.trandate));
                        cashSale_Rec.setValue('undepfunds', 'T');
                        cashSale_Rec.setValue('subsidiary', data.ticketInfo.subsidiary);
                        cashSale_Rec.setValue('location', data.ticketInfo.location);
                        if(data.ticketInfo.salesrep > 0){ cashSale_Rec.setValue('salesrep', data.ticketInfo.salesrep); }

                        cashSale_Rec.setValue('custbody_gf_rics_sales_ticket_id', data.ticketInfo.custbody_gf_rics_sales_ticket_id[0].value);
                        cashSale_Rec.setValue('custbody_gf_rics_ticket_number', data.ticketInfo.custbody_gf_rics_ticket_number);
                        cashSale_Rec.setValue('custbody_gf_rics_ticket_date', data.ticketInfo.custbody_gf_rics_ticket_date);
                        cashSale_Rec.setValue('custbody_gf_rics_transaction_type', data.ticketInfo.custbody_gf_rics_transaction_type);
                        cashSale_Rec.setValue('custbody_gf_rics_store_name', data.ticketInfo.custbody_gf_rics_store_name);
                        cashSale_Rec.setValue('custbody_gf_rics_store_code', data.ticketInfo.custbody_gf_rics_store_code);

                        if(data.ticketInfo.csdiscountitem != '0'){
                            cashSale_Rec.setValue('discountitem', data.ticketInfo.csdiscountitem);
                            cashSale_Rec.setValue('discountrate', data.ticketInfo.csdiscountrate);
                        }

                        // process sale items
                        var line_num = 0;
                        for(var s_item in data.ticketInfo.saleitems){
                            var sale_data = data.ticketInfo.saleitems[s_item];
                            if(sale_data.coupon){ continue; }
                            log.audit({title: 'GF_MR_RICS_Cash_Sale_Refund.js:rics_reduce-c_val='+c_val, details: 'sale_data: ' + JSON.stringify(sale_data)});
                            // if(sale_data.item == undefined || sale_data.item == ''){ continue; }
                            // update all of the fields and line items
                            setTransLineItem(cashSale_Rec, sale_data, false, line_num);
                            line_num += 1;
                        }
                    }else{
                        // the transaction record was found - load it
                        var cashSale_Rec = record.load({
                            type: 'cashsale',
                            id: cs_transaction_id,
                            isDynamic: true
                        });

                        // what header fields if any should be updated??
                        if(data.ticketInfo.csdiscountitem != '0'){
                            cashSale_Rec.setValue('discountitem', data.ticketInfo.csdiscountitem);
                            cashSale_Rec.setValue('discountrate', data.ticketInfo.csdiscountrate);
                        }else{
                            cashSale_Rec.setValue('discountitem', '');
                            cashSale_Rec.setValue('discountrate', '');
                        }

                        // process sale items
                        // - first update existing line items
                        var arr_updated_lines = [];
                        var lineCount = cashSale_Rec.getLineCount({ 'sublistId': 'item' });
                        for(var i = 0; i < lineCount; i++){
                            cashSale_Rec.selectLine({ sublistId: 'item', line: i });
                            var item_id = cashSale_Rec.getCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'item'
                            });
                            for(var s_item in data.ticketInfo.saleitems){
                                var sale_data = data.ticketInfo.saleitems[s_item];
                                if(sale_data.coupon){
                                    arr_updated_lines.push(sale_data.item);
                                    continue;
                                }
                                if(item_id == sale_data.item){
                                    log.debug({title: 'GF_MR_RICS_Cash_Sale_Refund.js:rics_reduce-s_item='+s_item, details: 'sale_data: ' + JSON.stringify(sale_data)});
                                    setTransLineItem(cashSale_Rec, sale_data, true, i);
                                    cashSale_Rec.commitLine({sublistId: 'item'});
                                    arr_updated_lines.push(sale_data.item);
                                    break;
                                }
                            }
                        }
                        var line_num = lineCount + 1;
                        // then add any line items that were not updated
                        for(var s_item in data.ticketInfo.saleitems){
                            var sale_data = data.ticketInfo.saleitems[s_item];
                            if(arr_updated_lines.indexOf(sale_data.item) == -1){
                                cashSale_Rec.selectNewLine({ sublistId: 'item' });
                                setTransLineItem(cashSale_Rec, sale_data, true, line_num);
                                cashSale_Rec.commitLine({sublistId: 'item'});
                                arr_updated_lines.push(sale_data.item);
                                line_num += 1;
                            }
                        }
                    }

                    var cs_rec_id = cashSale_Rec.save();
                    log.audit({title: 'GF_MR_RICS_Cash_Sale_Refund.js:rics_reduce-SAVE-c_val='+c_val, details: 'cs_rec_id: ' + cs_rec_id});

                    // connect to custom records
                    if(cs_rec_id > 0){
                        // set payment fields
                        var st_id = data.ticketInfo.custbody_gf_rics_sales_ticket_id[0].value;
                        setPaymentFieldsAndInfo('cashsale', cs_rec_id, st_id);
                        // connect to custom records
                        for(var s_item in data.ticketInfo.saleitems){
                            var sale_data = data.ticketInfo.saleitems[s_item];
                            if(sale_data.sd_id == undefined){ continue; }
                            // add all of the sales details connections
                            var rics_sales_details_id = record.submitFields({
                                type: 'customrecord_rics_sales_details',
                                id: sale_data.sd_id,
                                values: {
                                    'custrecord_rics_sd_transaction_rec': cs_rec_id
                                }
                            });
                        }
                        // also connect the coupons
                        for(var c_item in data.ticketInfo.couponitems){
                            var coupon_id = data.ticketInfo.couponitems[c_item];
                            var rics_sales_details_id = record.submitFields({
                                type: 'customrecord_rics_sales_details',
                                id: coupon_id,
                                values: {
                                    'custrecord_rics_sd_transaction_rec': cs_rec_id
                                }
                            });
                        }
                    }

                }

                // ------------------------------------------------------------ REFUND

                var cr_transaction_id = ((data.ticketInfo.refunditems).length > 0) ? findCashRefund(data.ticketInfo) : 0;
                log.audit({title: 'GF_MR_RICS_Cash_Sale_Refund.js:rics_reduce-SAVE-c_val='+c_val, details: 'cr_transaction_id: ' + cr_transaction_id});
                if((data.ticketInfo.refunditems).length > 0){
                    if(cr_transaction_id == 0){
                        // items exist but no transaction record was found - create a new transaction record
                        var cashRefund_Rec = record.create({
                            type: 'cashrefund'
                        });
                        cashRefund_Rec.setValue('entity', data.ticketInfo.entity);
                        cashRefund_Rec.setValue('trandate', new Date(data.ticketInfo.trandate));
                        cashRefund_Rec.setValue('account', '116');
                        cashRefund_Rec.setValue('subsidiary', data.ticketInfo.subsidiary);
                        cashRefund_Rec.setValue('location', data.ticketInfo.location);
                        if(data.ticketInfo.salesrep > 0){ cashRefund_Rec.setValue('salesrep', data.ticketInfo.salesrep); }

                        cashRefund_Rec.setValue('custbody_gf_rics_sales_ticket_id', data.ticketInfo.custbody_gf_rics_sales_ticket_id[0].value);
                        cashRefund_Rec.setValue('custbody_gf_rics_ticket_number', data.ticketInfo.custbody_gf_rics_ticket_number);
                        cashRefund_Rec.setValue('custbody_gf_rics_ticket_date', data.ticketInfo.custbody_gf_rics_ticket_date);
                        cashRefund_Rec.setValue('custbody_gf_rics_transaction_type', data.ticketInfo.custbody_gf_rics_transaction_type);
                        cashRefund_Rec.setValue('custbody_gf_rics_store_name', data.ticketInfo.custbody_gf_rics_store_name);
                        cashRefund_Rec.setValue('custbody_gf_rics_store_code', data.ticketInfo.custbody_gf_rics_store_code);

                        if(data.ticketInfo.crdiscountitem != '0'){
                            cashRefund_Rec.setValue('discountitem', data.ticketInfo.crdiscountitem);
                            var temp_crdiscountrate = 0;
                            if(parseFloat(data.ticketInfo.crdiscountrate) > 0.00){ temp_crdiscountrate = data.ticketInfo.crdiscountrate * -1; }else{ temp_crdiscountrate = data.ticketInfo.crdiscountrate; }
                            cashRefund_Rec.setValue('discountrate', temp_crdiscountrate);
                        }

                        // process refund items
                        var flg_transtype_and_returnreason_set = false;
                        var line_num = 0;
                        for(var r_item in data.ticketInfo.refunditems){
                            var refund_data = data.ticketInfo.refunditems[r_item];
                            if(refund_data.coupon){ continue; }
                            log.audit({title: 'GF_MR_RICS_Cash_Sale_Refund.js:rics_reduce-c_val='+c_val, details: 'create a new transaction record -- data: ' + JSON.stringify(data)});
                            // if(refund_data.item == undefined || refund_data.item == ''){ continue; }
                            setTransLineItem(cashRefund_Rec, refund_data, false, line_num);
                            line_num += 1;
                            if(flg_transtype_and_returnreason_set == false && refund_data.sd_trans_type != '' && refund_data.sd_return_reason != ''){
                                cashRefund_Rec.setValue('custbody_gf_rics_transaction_type', data.ticketInfo.custbody_gf_rics_transaction_type);
                                cashRefund_Rec.setValue('custbody_gf_rics_return_reason', refund_data.sd_return_reason);
                                flg_transtype_and_returnreason_set = true;
                            }
                        }
                    }else{
                        // the transaction record was found - load it
                        var cashRefund_Rec = record.load({
                            type: 'cashrefund',
                            id: cr_transaction_id,
                            isDynamic: true
                        });

                        if(data.ticketInfo.crdiscountitem != '0'){
                            cashRefund_Rec.setValue('discountitem', data.ticketInfo.crdiscountitem);
                            var temp_crdiscountrate = 0;
                            if(parseFloat(data.ticketInfo.crdiscountrate) > 0.00){ temp_crdiscountrate = data.ticketInfo.crdiscountrate * -1; }else{ temp_crdiscountrate = data.ticketInfo.crdiscountrate; }
                            cashRefund_Rec.setValue('discountrate', temp_crdiscountrate);
                        }else{
                            cashRefund_Rec.setValue('discountitem', '');
                            cashRefund_Rec.setValue('discountrate', '');
                        }

                        // process refund items
                        // - first update existing line items
                        var flg_transtype_and_returnreason_set = false;
                        var arr_updated_lines = [];
                        var lineCount = cashRefund_Rec.getLineCount({ 'sublistId': 'item' });
                        for(var i = 0; i < lineCount; i++){
                            cashRefund_Rec.selectLine({ sublistId: 'item', line: i });
                            var item_id = cashRefund_Rec.getCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'item'
                            });
                            for(var r_item in data.ticketInfo.refunditems){
                                var refund_data = data.ticketInfo.refunditems[r_item];
                                if(refund_data.coupon){
                                    arr_updated_lines.push(refund_data.item);
                                    continue;
                                }
                                if(item_id == refund_data.item){
                                    log.debug({title: 'GF_MR_RICS_Cash_Sale_Refund.js:rics_reduce-r_item='+r_item, details: 'transaction record was found -- refund_data: ' + JSON.stringify(refund_data)});
                                    setTransLineItem(cashRefund_Rec, refund_data, true, i);
                                    cashRefund_Rec.commitLine({sublistId: 'item'});
                                    arr_updated_lines.push(refund_data.item);
                                    break;
                                }
                                if(flg_transtype_and_returnreason_set == false && refund_data.sd_trans_type != '' && refund_data.sd_return_reason != ''){
                                    cashRefund_Rec.setValue('custbody_gf_rics_transaction_type', data.ticketInfo.custbody_gf_rics_transaction_type);
                                    cashRefund_Rec.setValue('custbody_gf_rics_return_reason', refund_data.sd_return_reason);
                                }
                            }
                        }
                        var line_num = lineCount + 1;
                        // then add any line items that were not updated
                        for(var r_item in data.ticketInfo.refunditems){
                            var refund_data = data.ticketInfo.refunditems[r_item];
                            // if(refund_data.coupon){ continue; }
                            if(arr_updated_lines.indexOf(refund_data.item) == -1){
                                cashRefund_Rec.selectNewLine({ sublistId: 'item' });
                                setTransLineItem(cashRefund_Rec, refund_data, true, line_num);
                                cashRefund_Rec.commitLine({sublistId: 'item'});
                                arr_updated_lines.push(refund_data.item);
                                line_num += 1;
                            }
                        }
                    }

                    var cr_rec_id = cashRefund_Rec.save();
                    log.audit({title: 'GF_MR_RICS_Cash_Sale_Refund.js:rics_reduce-SAVE-c_val='+c_val, details: 'cr_rec_id: ' + cr_rec_id});

                    if(cr_rec_id > 0){
                        // set payment fields
                        var st_id = data.ticketInfo.custbody_gf_rics_sales_ticket_id[0].value;
                        setPaymentFieldsAndInfo('cashrefund', cr_rec_id, st_id);
                        for(var r_item in data.ticketInfo.refunditems){
                            var refund_data = data.ticketInfo.refunditems[r_item];
                            if(refund_data.sd_id == undefined){ continue; }
                            // add all of the sales details connections
                            var rics_sales_details_id = record.submitFields({
                                type: 'customrecord_rics_sales_details',
                                id: refund_data.sd_id,
                                values: {
                                    'custrecord_rics_sd_transaction_rec': cr_rec_id
                                }
                            });
                        }
                        // also connect the coupons
                        for(var c_item in data.ticketInfo.couponitems){
                            var coupon_id = data.ticketInfo.couponitems[c_item];
                            var rics_sales_details_id = record.submitFields({
                                type: 'customrecord_rics_sales_details',
                                id: coupon_id,
                                values: {
                                    'custrecord_rics_sd_transaction_rec': cr_rec_id
                                }
                            });
                        }
                    }
                }

                // ------------------------------------------------------------ EXCHANGE
                if(data.ticketInfo.is_exchange == true){
                    log.debug({title: 'GF_MR_RICS_Cash_Sale_Refund.js:rics_reduce-EXCHANGE-c_val='+c_val, details: 'cr_rec_id: ' + cr_rec_id + ', cs_rec_id: ' + cs_rec_id});
                    if((cr_rec_id != null && cr_rec_id > 0) && (cs_rec_id != null && cs_rec_id > 0)){
                        // set custbody_gf_rics_exchange_record_id for each record on the other
                        // update cash sale record
                        var cashsaleupdate = record.submitFields({
                            type: 'cashsale',
                            id: cs_rec_id,
                            values: {
                                'custbody_gf_rics_exchange_record_id': cr_rec_id
                            }
                        });
                        // update cash refund record
                        var cashrefundupdate = record.submitFields({
                            type: 'cashrefund',
                            id: cr_rec_id,
                            values: {
                                'custbody_gf_rics_exchange_record_id': cs_rec_id
                            }
                        });
                    }
                }
            }
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_Cash_Sale_Refund.js:rics_reduce', details: 'Error ' + ex.toString() + ' : ' + ex.stack + ' --- data: ' + JSON.stringify(data)});
        }
    }

    /**
     * This summarize function is used to report important aspects about how the script performed
     * @param {*} summary - this object contains data regarding script performance.
     */
    function rics_summarize(summary) {
        try{
            log.audit('Usage Consumed', summary.usage);
            log.audit('Concurrency Number ', summary.concurrency);
            log.audit('Number of Yields', summary.yields);
            log.audit({title: 'Process Completed'});
            removeCompletedDeployments();
            runOnDemandScript();
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_Cash_Sale_Refund.js:rics_summarize', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    return {
        getInputData: rics_getInputData,
        map: rics_map,
        reduce: rics_reduce,
        summarize: rics_summarize
    };

    // =========================== extra functions
    /**
     * findCustomer is used to return the most closely matched customer record id based on the values from the RICS Sales Ticket record related through the RICS Sales Details record
     * if the customer name is not provided the No-Name customer record will be used, and if the customer cannot be found the Walk-In customer record will be used
     * @param {*} _values
     */
    function findCustomer(_values){
        var result_id = 0;
        // set customer values
        var firstName = (_values['CUSTRECORD_RICS_SD_PARENT.custrecord_rics_st_customerfirstname'] != undefined) ? _values['CUSTRECORD_RICS_SD_PARENT.custrecord_rics_st_customerfirstname'] : '';
        var lastName = (_values['CUSTRECORD_RICS_SD_PARENT.custrecord_rics_st_customerlastname'] != undefined) ? _values['CUSTRECORD_RICS_SD_PARENT.custrecord_rics_st_customerlastname'] : '';
        var phoneNum = (_values['CUSTRECORD_RICS_SD_PARENT.custrecord_rics_st_customerphonenumber'] != undefined) ? _values['CUSTRECORD_RICS_SD_PARENT.custrecord_rics_st_customerphonenumber'] : '';
        var email = (_values['CUSTRECORD_RICS_SD_PARENT.custrecord_rics_st_customeremail'] != undefined) ? _values['CUSTRECORD_RICS_SD_PARENT.custrecord_rics_st_customeremail'] : '';
        var rics_cust_id = (_values['CUSTRECORD_RICS_SD_PARENT.custrecord_rics_st_customerid'] != undefined) ? _values['CUSTRECORD_RICS_SD_PARENT.custrecord_rics_st_customerid'] : '';
        var rics_acct_id = (_values['CUSTRECORD_RICS_SD_PARENT.custrecord_rics_st_customeraccountnumber'] != undefined) ? _values['CUSTRECORD_RICS_SD_PARENT.custrecord_rics_st_customeraccountnumber'] : '';
        // address info
        var mailing_addr = (_values['CUSTRECORD_RICS_SD_PARENT.custrecord_rics_st_customermailing'] != undefined) ? _values['CUSTRECORD_RICS_SD_PARENT.custrecord_rics_st_customermailing'] : '';
        var billing_addr = (_values['CUSTRECORD_RICS_SD_PARENT.custrecord_rics_st_customerbilling'] != undefined) ? _values['CUSTRECORD_RICS_SD_PARENT.custrecord_rics_st_customerbilling'] : '';

        if(firstName != '' && lastName != ''){
            var arr_customer_matches = [];
            // find the Customer Record
            var customer_Search = search.create({
                type: "customer",
                filters: [
                    ["isinactive","is","F"], "AND",
                    [
                        [
                            ["firstname","startswith",firstName],"AND",
                            ["lastname","is",lastName]
                        ],"OR",
                        ["firstname","is","Walk-In"],"AND",
                        ["lastname","is","Customer"]
                    ]
                ],
                columns: [
                    'phone',
                    'altphone',
                    'mobilephone',
                    'email',
                    'altemail',
                    'custentity_gf_rics_customer_id',
                    'custentity_gf_rics_cust_acct_number'
                ]
            });
            customer_Search.run().each(function(result){
                // .run().each has a limit of 4,000 results
                var customer_match = {};
                customer_match.match_val = 0;
				customer_match.id = result.id;
                var result_phone = result.getValue('phone');
                if(result_phone == phoneNum){ customer_match.match_val += 1; };
                var result_altphone = result.getValue('altphone');
                if(result_altphone == phoneNum){ customer_match.match_val += 1; };
                var result_mobilephone = result.getValue('mobilephone');
                if(result_mobilephone == phoneNum){ customer_match.match_val += 1; };
                var result_email = result.getValue('email');
                if(result_email == email){ customer_match.match_val += 1; };
                var result_altemail = result.getValue('altemail');
                if(result_altemail == email){ customer_match.match_val += 1; };
                var result_custentity_gf_rics_customer_id = result.getValue('custentity_gf_rics_customer_id');
                if(result_custentity_gf_rics_customer_id == rics_cust_id){ customer_match.match_val += 5; };
                var result_custentity_gf_rics_cust_acct_number = result.getValue('custentity_gf_rics_cust_acct_number');
                if(result_custentity_gf_rics_cust_acct_number == rics_acct_id){ customer_match.match_val += 5; };
				arr_customer_matches.push(customer_match);
                return true;
            });
            if(arr_customer_matches != undefined){
                arr_customer_matches.sort(function(a, b) {
                    return b.match_val - a.match_val; // descending sort with biggest value on top
                });
                // when the top result has any match weight at all it is assumed to be the best match
                // result_id = (arr_customer_matches[0] > 0) ? Object.keys(arr_customer_matches)[0] : 0;
                result_id = arr_customer_matches[0].id;
                log.debug({title: 'GF_MR_RICS_Cash_Sale_Refund.js:findCustomer-arr_customer_matches', details: 'arr_customer_matches: ' + arr_customer_matches});
            }
        }else{
            var customer_Search = search.create({
                type: "customer",
                filters: [
                    ["isinactive","is","F"], "AND",
                    ["firstname","is","No-Name"],"AND",
                    ["lastname","is","Customer"]
                ],
                columns: [ ]
            });
            customer_Search.run().each(function(result){
                if(result_id == 0){
                    result_id = result.id;
                }
                return true;
            });
            // use No-Name Customer
        }
        log.debug({title: 'GF_MR_RICS_Cash_Sale_Refund.js:findCustomer', details: 'result_id: ' + result_id});
        return result_id;
    }

    /**
     * This function is designed to ensure that the selected customer has all of the subsidiaries prior to saving the transaction
     * @param {*} _cust_rec_id - integer - internal id of the customer record
     * @returns - void
     */
    function setSubsidiarySublist(_cust_rec_id){
        try{
            var rec = record.load({
                type: record.Type.CUSTOMER,
                id: _cust_rec_id,
                isDynamic: true
            });
            var currentSubsidiaryCount = rec.getLineCount({ 'sublistId': 'submachine' });

            var arr_subs = ["7","3","4","2"];
            for(var i = 0; currentSubsidiaryCount > 0 && i < currentSubsidiaryCount; i++){
                // remove the existing subs from the array to write if the already exist in the customer record
                rec.selectLine({ sublistId: 'submachine', line: i });
                var sublistValue = rec.getCurrentSublistValue({
                    sublistId: 'submachine',
                    fieldId: 'subsidiary'
                });
                if(arr_subs.indexOf(sublistValue) > -1){
                    arr_subs.splice(arr_subs.indexOf(sublistValue), 1);
                }
            }

            // write all values remaining to the subsidiary sublist
            for(var i = 0; arr_subs.length > 0 && i < arr_subs.length; i++){
                rec.selectNewLine({ sublistId: 'submachine' });

                rec.setCurrentSublistValue({
                    sublistId: 'submachine',
                    fieldId: 'subsidiary',
                    value: arr_subs[i],
                    ignoreFieldChange: true
                });
                rec.commitLine({sublistId: 'submachine'});
            }
            rec.save();

            return true;
        }catch(ex){
            log.error({title: 'Error: GF_MR_CustomersFixSubsidiaries.js:setSubsidiarySublist', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * findLocation is used to return the location id based on the storecode indicated on the RICS Sales Details record
     * @param {*} _values
     */
     function findLocation(_values){
        var result_id = 0;
        var storecode = (_values['custrecord_rics_sd_salesticketstorecode'] != undefined) ? _values['custrecord_rics_sd_salesticketstorecode'] : '';

        if(storecode != ''){
            var locationSearchObj = search.create({
                type: "location",
                filters: [
                ["externalid","anyof",storecode], "AND",
                ["isinactive","is","F"]
                ],
                columns: [ ]
            });
            locationSearchObj.run().each(function(result){
                // .run().each has a limit of 4,000 results
                if(result_id == 0){
                    result_id = result.id;
                }
                return true;
            });
        }
        return result_id;
    }

    /**
     * This function takes the location id and gets the subsidiary internal id
     * @param {*} _location_id - integer - the NetSuite internal ID of the location record
     * @returns - integer - the NetSuite internal ID of the subsidiary for the location
     */
     function findSubsidiary(_location_id){
        var result = 0;
        if(_location_id != undefined && _location_id > 0){
            // load location record
            var location_record = record.load({
                type: 'location',
                id: _location_id,
                isDynamic: true
            });
            result = location_record.getValue('subsidiary');
        }

        return result;
    }

    /**
     * This function tries to find a sales rep by their cashier code, if not found it uses the subsidiary id to set the admin rep for that subsidiary
     * @param {*} _values - object - this should contain
     * @param {*} _sub_id
     */
     function findSalesRep(_values, _sub_id){
        var result_id = 0;
        var cashier_code = _values['custrecord_rics_sd_usercashiercode'];
        var employeeSearchObj = search.create({
            type: "employee",
            filters: [
               ["custentity2","is",cashier_code], "AND", // RICS Cashier Code
               ["isinactive","is","F"]
            ],
            columns: [
                'issalesrep'
            ]
        });
        employeeSearchObj.run().each(function(result){
            // .run().each has a limit of 4,000 results
            if(result_id == 0){
                result_id = result.id;
                if(result.getValue('issalesrep') != 'T'){
                    var emp_update = record.submitFields({
                        type: 'employee',
                        id: result_id,
                        values: {
                            'issalesrep': 'T'
                        }
                    });
                }
            }
            return true;
        });

        // if not found set admin
        if(result_id == 0){
            switch(parseInt(_sub_id)){
                case 2:
                    // GFOR
                    result_id = 4313;
                    break;
                case 3:
                    // GFID
                    result_id = 4316;
                    break;
                case 4:
                    // GFNW
                    result_id = 4317;
                    break;
                case 7:
                    // GFSE
                    result_id = 4315;
                    break;
            }
        }

        return result_id;
    }

    /**
     * This function takes the sku and upc of an item and tries to find the NetSuite Inventory Item record
     * @param {*} _str_upc - string - the unique upc for the item
     * @param {*} _str_sku - string - the sku value for the item
     * @returns - integer - the internal ID of the NetSuite Inventory Item record
     */
     function findItem(_str_upc, _str_sku){
        // log.debug({title: 'GF_MR_RICS_Cash_Sale_Refund.js:findItem', details: '_str_upc: ' + _str_upc + ', _str_sku:' + _str_sku});
        var result_id = 0;
        if(_str_upc == undefined || _str_sku == undefined){ return result_id; }

        if(_str_upc !== null && _str_upc != '' && _str_sku !== null && _str_sku != ''){
            var itemSearchObj = search.create({
                type: "item",
                filters: [
                    ["upccode","is",_str_upc], "AND",
                    ["vendorname","is",_str_sku], "AND",
                    ["isinactive","is","F"]
                ],
                columns: [ ]
            });
            itemSearchObj.run().each(function(result){
                // .run().each has a limit of 4,000 results
                if(result_id == 0){
                    result_id = result.id;
                }
                return true;
            });
        }
        if(result_id == 0){
            log.debug({title: 'GF_MR_RICS_Cash_Sale_Refund.js:findItem', details: 'insufficient data - _str_upc: ' + _str_upc + ', _str_sku:' + _str_sku});
        }

        return result_id;
    }

    /**
     * This function finds item record ids by name
     * @param {*} _name - string - the name of the item to use
     * @returns - integer - the internal ID of the NetSuite Item record
     */
    function findDefaultItem(_name){
        // log.debug({title: 'GF_MR_RICS_Cash_Sale_Refund.js:findDefaultItem', details: '_name: ' + _name});
        var result_id = 0;
        var itemSearchObj = search.create({
            type: "item",
            filters: [
                ["isinactive","is","F"], "AND",
                ["name","haskeywords",_name]
            ],
            columns: [ ]
        });
        itemSearchObj.run().each(function(result){
            // .run().each has a limit of 4,000 results
                if(result_id == 0){
                    result_id = result.id;
                }
            return true;
        });
        return result_id;
    }

    /**
     * This function uses the ticket info object to find the NetSuite Transaction cash sale that has the internal id of the RICS Sales Ticket
     * @param {*} _ticketInfo - object - this object should contain the internal id of the RICS Sales Ticket
     * @returns - integer - the internal ID of the RICS Sales Ticket custom record
     */
    function findCashSale(_ticketInfo){
        try{
            var result_id = 0;

            var cashsaleSearchObj = search.create({
                type: "cashsale",
                filters: [
                    ["type","anyof","CashSale"], "AND",
                    ["custbody_gf_rics_sales_ticket_id","anyof",_ticketInfo.custbody_gf_rics_sales_ticket_id[0].value]
                ],
                columns: [ ]
            });
            cashsaleSearchObj.run().each(function(result){
                // .run().each has a limit of 4,000 results
                if(result_id == 0){
                    result_id = result.id;
                }
                return true;
            });

        }catch(ex){
            log.error({title: 'GF_MR_RICS_Cash_Sale_Refund.js:findCashSale', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
            return result_id;
        }
        return result_id;
    }

    /**
     * This function uses the ticket info object to find the NetSuite Transaction cash refund that has the internal id of the RICS Sales Ticket 
     * @param {*} _ticketInfo - object - this object should contain the internal id of the RICS Sales Ticket
     * @returns - integer - the internal ID of the RICS Sales Ticket custom record
     */
    function findCashRefund(_ticketInfo){
        try{
            var result_id = 0;

            var cashsaleSearchObj = search.create({
                type: "cashrefund",
                filters: [
                    ["type","anyof","CashRfnd"], "AND",
                    ["custbody_gf_rics_sales_ticket_id","anyof",_ticketInfo.custbody_gf_rics_sales_ticket_id[0].value]
                ],
                columns: [ ]
            });
            cashsaleSearchObj.run().each(function(result){
                // .run().each has a limit of 4,000 results
                if(result_id == 0){
                    result_id = result.id;
                }
                return true;
            });
        }catch(ex){
            log.error({title: 'GF_MR_RICS_Cash_Sale_Refund.js:findCashRefund', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
            return result_id;
        }
        return result_id;
    }

    /**
     * This function takes in the required data to set the specified line on a transaction record, when the record is an existing record
     * it is loaded dynamically and the line will be selected otherwise it mus be specified
     * @param {*} _transaction_Rec - NetSuite object - this is the record object that the line updates will affect
     * @param {*} _sale_data - object - this has the data that will go on the line
     * @param {*} _isExistingRecord - boolean - this will indicate how to work with the line
     * @param {*} _line_num - integer - this is the line number that will be affected by the update
     */
    function setTransLineItem(_transaction_Rec, _sale_data, _isExistingRecord, _line_num){
        try{
            if(parseInt(_sale_data.quantity) < 0){ _sale_data.quantity = parseInt(_sale_data.quantity) * -1; }
            if(parseFloat(_sale_data.amount) < 0){ _sale_data.amount = parseFloat(_sale_data.amount) * -1; }
            if(parseFloat(_sale_data.rate) < 0){ _sale_data.rate = parseFloat(_sale_data.rate) * -1; }
            log.audit({title: 'GF_MR_RICS_Cash_Sale_Refund.js:setCashSaleLineItem-start', details: 'params - _transaction_Rec: ' + _transaction_Rec.id + ', _sale_data: ' + JSON.stringify(_sale_data) + ', _isExistingRecord:' + _isExistingRecord + ', _line_num: ' + _line_num});
            if(_isExistingRecord != undefined && _isExistingRecord){
                // the record has been loaded so we need to go through it to update the selected line items and then add any that were not existing
                _transaction_Rec.setCurrentSublistValue({
                    sublistId: 'item',
                    fieldId: 'item',
                    value: parseInt(_sale_data.item)
                });
                _transaction_Rec.setCurrentSublistValue({
                    sublistId: 'item',
                    fieldId: 'quantity',
                    value: _sale_data.quantity
                });
                _transaction_Rec.setCurrentSublistValue({
                    sublistId: 'item',
                    fieldId: 'rate',
                    value: _sale_data.rate
                });
                _transaction_Rec.setCurrentSublistValue({
                    sublistId: 'item',
                    fieldId: 'amount',
                    value: ((_sale_data.amount != null && _sale_data.amount != '') ? _sale_data.amount : 0)
                });
                _transaction_Rec.setCurrentSublistValue({
                    sublistId: 'item',
                    fieldId: 'taxcode',
                    value: '-8'
                });
                if(_sale_data.sd_return_reason != undefined && _sale_data.sd_return_reason != ''){
                    _transaction_Rec.setCurrentSublistValue({
                        sublistId: 'item',
                        fieldId: 'custcol_gf_rics_return_desc',
                        value: _sale_data.sd_return_reason
                    });
                }
                if(_sale_data.sd_discount_amount != undefined && _sale_data.sd_discount_amount != ''){
                    _transaction_Rec.setCurrentSublistValue({
                        sublistId: 'item',
                        fieldId: 'custcol_gf_rics_discount_amount',
                        value: _sale_data.sd_discount_amount
                    });
                }
                if(_sale_data.sd_net_sales != undefined && _sale_data.sd_net_sales != ''){
                    _transaction_Rec.setCurrentSublistValue({
                        sublistId: 'item',
                        fieldId: 'custcol_gf_rics_net_sales',
                        value: _sale_data.sd_net_sales
                    });
                }
            }else{
                // the record is being newly created so we just need to add all line items
                _transaction_Rec.setSublistValue({
                    sublistId: 'item',
                    fieldId: 'item',
                    line: _line_num,
                    value: parseInt(_sale_data.item)
                });
                _transaction_Rec.setSublistValue({
                    sublistId: 'item',
                    fieldId: 'quantity',
                    line: _line_num,
                    value: _sale_data.quantity
                });
                _transaction_Rec.setSublistValue({
                    sublistId: 'item',
                    fieldId: 'rate',
                    line: _line_num,
                    value: _sale_data.rate
                });
                _transaction_Rec.setSublistValue({
                    sublistId: 'item',
                    fieldId: 'amount',
                    line: _line_num,
                    value: ((_sale_data.amount != null && _sale_data.amount != '') ? _sale_data.amount : 0)
                });
                _transaction_Rec.setSublistValue({
                    sublistId: 'item',
                    fieldId: 'taxcode',
                    line: _line_num,
                    value: '-8'
                });
                if(_sale_data.sd_return_reason != undefined && _sale_data.sd_return_reason != ''){
                    _transaction_Rec.setSublistValue({
                        sublistId: 'item',
                        fieldId: 'custcol_gf_rics_return_desc',
                        line: _line_num,
                        value: _sale_data.sd_return_reason
                    });
                }
                if(_sale_data.sd_discount_amount != undefined && _sale_data.sd_discount_amount != ''){
                    _transaction_Rec.setSublistValue({
                        sublistId: 'item',
                        fieldId: 'custcol_gf_rics_discount_amount',
                        line: _line_num,
                        value: _sale_data.sd_discount_amount
                    });
                }
                if(_sale_data.sd_net_sales != undefined && _sale_data.sd_net_sales != ''){
                    _transaction_Rec.setSublistValue({
                        sublistId: 'item',
                        fieldId: 'custcol_gf_rics_net_sales',
                        line: _line_num,
                        value: _sale_data.sd_net_sales
                    });
                }
            }
        }catch(ex){
            log.error({title: 'GF_MR_RICS_Cash_Sale_Refund.js:setTransLineItem', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * This function takes the required data to set the values on the payment fields of the transaction record, and it uses that data to find the correct values for those fields
     * @param {*} _rec_type - string - used when updating the transaction
     * @param {*} _rec_id - integer - used when updating the transaction
     * @param {*} st_id - integer - used when searching for the correct RICS Slaes Tenders records
     */
    function setPaymentFieldsAndInfo(_rec_type, _rec_id, st_id){
        try{
            log.audit({title: 'GF_MR_RICS_Cash_Sale_Refund.js:setPaymentFieldsAndInfo-start', details: '_rec_type: ' + _rec_type + ', _rec_id: ' + _rec_id + ', st_id: ' + st_id});
            var str_tender_description = '';
            var str_tender_approvalcode = '';
            var str_tender_invoice = '';
            var num_tender_transaction_id = 0;
            var arr_tender_transactions = [];
            var customrecord_rics_sales_tendersSearchObj = search.create({
                type: "customrecord_rics_sales_tenders",
                filters: [
                   ["custrecord_rics_tender_parent","anyof",st_id]
                ],
                columns: [
                   "custrecord_rics_tender_description",
                   "custrecord_rics_tender_approvalcode",
                   "custrecord_rics_tender_invoice",
                   "custrecord_rics_tenders_transaction_rec"
                ]
            });
            customrecord_rics_sales_tendersSearchObj.run().each(function(result){
                // .run().each has a limit of 4,000 results
                // if(str_tender_description == ''){
                    str_tender_description = result.getValue('custrecord_rics_tender_description');
                    str_tender_approvalcode = result.getValue('custrecord_rics_tender_approvalcode');
                    str_tender_invoice = result.getValue('custrecord_rics_tender_invoice');
                    num_tender_transaction_id = result.getValue('custrecord_rics_tenders_transaction_rec');
                    if(num_tender_transaction_id != null && num_tender_transaction_id > 0){
                        arr_tender_transactions.push(num_tender_transaction_id);
                    }else{
                        var transaction_update_id = record.submitFields({
                            type: 'customrecord_rics_sales_tenders',
                            id: result.id,
                            values: {
                                'custrecord_rics_tenders_transaction_rec': _rec_id
                            }
                        });
                    }
                // }
                if(arr_tender_transactions.indexOf(_rec_id) == -1){
                    arr_tender_transactions.push(_rec_id);
                    // update tenders connection
                    var rics_sales_tenders_id = record.submitFields({
                        type: 'customrecord_rics_sales_tenders',
                        id: result.id,
                        values: {
                            'custrecord_rics_tenders_transaction_rec': arr_tender_transactions
                        }
                    });
                }
                return true;
            });

            if(str_tender_description != ''){
                var payment_method_id = getPaymentMethodId(str_tender_description);
                if(payment_method_id > 0){
                    var transaction_update_id = record.submitFields({
                        type: _rec_type,
                        id: _rec_id,
                        values: {
                            'paymentmethod': payment_method_id,
                            'custbody_gf_rics_authcode': str_tender_approvalcode,
                            'custbody_gf_rics_invoice': str_tender_invoice
                        }
                    });
                }
            }

        }catch(ex){
            log.error({title: 'GF_MR_RICS_Cash_Sale_Refund.js:setPaymentFieldsAndInfo', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * This function takes the tender description and returns the appropriate payment method id
     * @param {*} _str_tender_description - string - the description of the tendered payment
     * @returns - integer - the id of the matching list value for the tender that was used
     */
    function getPaymentMethodId(_str_tender_description){
        log.audit({title: 'GF_MR_RICS_Cash_Sale_Refund.js:getPaymentMethodId-start', details: '_str_tender_description: ' + _str_tender_description});
        var result = 0;
        // lookup a mapping record using the tender description
        // if still no mapping record with a matching description is found create it (use eact same functions as GF_MR_RICS_GetPOSTransaction.js)
        var tender_type_mapping_id = find_tender_type_mapping(_str_tender_description);

        var fieldLookUp = search.lookupFields({
            type: 'customrecord_rics_tender_type_mapping',
            id: tender_type_mapping_id,
            columns: ['custrecord_rics_ttmf_payment_method']
        });
        if(fieldLookUp.custrecord_rics_ttmf_payment_method[0] != undefined){
            result = fieldLookUp.custrecord_rics_ttmf_payment_method[0].value;
        }else{
            // if the mapping record does not have a payment method on it yet - check for a default payment method (maybe add these to the control record)
            var tender_name = (_str_tender_description.indexOf('- ') > -1) ? _str_tender_description.substring(_str_tender_description.indexOf(' ') + 1) : _str_tender_description;
            tender_name = tender_name.toLowerCase();
            /*
            -- original defaults
                RICSPay 136 = 5- AMX, 12- Debit Card, 3- MasterCard, 4- Visa, 6- Discover
                Offline Card 137 = 14- Offline Card
                Care Credit 8 = 9- CareCredit
                ACIMA 138 = 12- ACIMA
                Cash 1 = 1- Cash
                Check 2 = 2- Check
                Gift Card 9 = Gift Card External
                House Charge 11 = 11- House Charge
                Third Party 139 = 15- City Tampa Fund
            */
            switch(tender_name){
                case 'amx': // 'RICSPay'
                case 'debit card':
                case 'mastercard':
                case 'visa':
                case 'discover':
                    result = 136;
                    break;
                case 'offline card': // Offline Card
                    result = 137;
                    break;
                case 'carecredit': // Care Credit
                    result = 8;
                    break;
                case 'acima': // ACIMA
                    result = 138;
                    break;
                case 'cash': // Cash
                    result = 1;
                    break;
                case 'check': // Check
                    result = 2;
                    break;
                case 'gift card external': // Gift Card
                    result = 9;
                    break;
                case 'house charge': // House Charge
                    result = 11;
                    break;
                case 'city tampa fund': // City Tampa Fund
                    result = 139;
                    break;
                default:
                    log.error({title: 'GF_MR_RICS_Cash_Sale_Refund.js:getPaymentMethodId', details: 'NOT FOUND - tender_name: ' + tender_name});
            }
            if(result != 0 && tender_type_mapping_id != undefined && tender_type_mapping_id != 0){
                var rics_tender_type_mapping_id = record.submitFields({
                    type: 'customrecord_rics_tender_type_mapping',
                    id: tender_type_mapping_id,
                    values: {'custrecord_rics_ttmf_payment_method': result}
                });
            }
        }
        return result;
    }

    function find_tender_type_mapping(_tenderDescription){
        log.debug({title: 'GF_MR_RICS_GetPOSTransaction.js:find_tender_type_mapping', details: '_tenderDescription: ' + _tenderDescription});
        var result_id = 0;
        // lowercase tenderdesc
        var lower = _tenderDescription.toLowerCase();
        var trimmed = (lower.indexOf('-') > -1) ? lower.substring(lower.indexOf('-') +2) : lower;
        // get tender type custom list - customlist_rics_tender_types
        var searchColumn = search.createColumn({ name : 'name' });
        var listSearch = search.create({ type : 'customlist_rics_tender_types', columns : searchColumn });
        var listArray = [];
        listSearch.run().each(function(searchResult) {
            listArray[searchResult.getValue(searchColumn)] = searchResult.id;
            return true;
        });
        // tender type list id
        var tender_type_list_id = listArray[trimmed];
        log.debug({title: 'GF_MR_RICS_GetPOSTransaction.js:find_tender_type_mapping', details: 'tender_type_list_id: ' + (tender_type_list_id != undefined ? tender_type_list_id : 'undefined') });
        if(tender_type_list_id != undefined){
            // found = get mapping id and set that record on this field
            result_id = getMappingRecordId(tender_type_list_id);
            if(result_id == 0){ result_id = createMappingRecord(trimmed, false); } // this happens when the list id exists but the mapping record hasn't been created
        }else{
            // not found = add mapping (and alert accounting)
            result_id = createMappingRecord(trimmed, true);
        }
        return result_id;
    }


    function getMappingRecordId(_tender_type_list_id){
        var result_id = 0;
        var customrecord_rics_tender_type_mappingSearchObj = search.create({
            type: "customrecord_rics_tender_type_mapping",
            filters: [
               ["custrecord_rics_ttmf_tender_type","anyof",_tender_type_list_id]
            ],
            columns: [ ]
        });
        customrecord_rics_tender_type_mappingSearchObj.run().each(function(result){
            // .run().each has a limit of 4,000 results
            if(result_id == 0){ result_id = result.id; }
            return true;
        });
        return result_id;
    }


    function createMappingRecord(_rics_tender_type, _flg_add_to_list){
        if(_flg_add_to_list == true){
            // get list internal id
            var searchColumn = search.createColumn({ name : 'internalid' });
            var listSearch = search.create({ type : 'customlist', filters: ["scriptid","is","customlist_rics_tender_types"], columns : searchColumn }).run();
            var list_search_result = listSearch.getRange({start: 0, end: 1});
            var mappingListId = list_search_result[0].getValue('internalid');
            // add tender type to tender type list
            if(mappingListId != 0){
                var customListRec = record.load({
                    type: 'customlist',
                    id: mappingListId,
                    isDynamic: true
                });
            }
            customListRec.selectNewLine({ sublistId: 'customvalue' });
            customListRec.setCurrentSublistValue({
                sublistId: 'customvalue',
                fieldId: 'value',
                value: _rics_tender_type,
                ignoreFieldChange: true
            });
            customListRec.commitLine({sublistId: 'customvalue'});

            customListRec.save();
        }
        // get the NEW id value
        var searchColumn = search.createColumn({ name : 'name' });
        var listSearch = search.create({ type : 'customlist_rics_tender_types', columns : searchColumn });
        var listArray = [];
        listSearch.run().each(function(searchResult) {
            listArray[searchResult.getValue(searchColumn)] = searchResult.id;
            return true;
        });
        var new_tt_id = listArray[_rics_tender_type];
        // add tender type to a mapping record and return the record id
        var ricsTenderTypeMappingRec = record.create({
            type: 'customrecord_rics_tender_type_mapping',
            isDynamic: true
        });
        ricsTenderTypeMappingRec.setValue({ fieldId:'custrecord_rics_ttmf_tender_type',value:new_tt_id });
        var rttm = ricsTenderTypeMappingRec.save();
        return getMappingRecordId(new_tt_id);
    }


    function runOnDemandScript(){
        var currentScript = runtime.getCurrentScript();
        var cashsaleSearchObj = search.create({
            type: "transaction",
            filters: [
                ["lastmodifieddate","onorbefore","minutesago5"], "AND",
                ["custbody_gf_rics_sales_ticket_id","noneof","@NONE@"], "AND",
                ["mainline","is","T"], "AND",
                ["custbody_gf_rics_sales_ticket_id.custrecord_rics_st_receipt_total","isnotempty",""], "AND",
                ["type","anyof","CashSale","CashRfnd"], "AND",
                [
                    ["custbody_gf_rics_exchange_record_id.mainline","is","T"],"OR",
                    ["custbody_gf_rics_exchange_record_id","anyof","@NONE@"]
                ], "AND",
                ["formulanumeric: CASE WHEN ((NVL2({custbody_gf_rics_exchange_record_id}, ({amount} + {custbody_gf_rics_exchange_record_id.amount}), {amount}) - {custbody_gf_rics_sales_ticket_id.custrecord_rics_st_receipt_total})) != 0.00 THEN 1 ELSE 0 END","greaterthan","0"], "AND",
                ["formulanumeric: CASE WHEN ({type} = 'Cash Sale' AND {status} = 'Not Deposited') OR ({type} != 'Cash Sale' AND {custbody_gf_rics_exchange_record_id.status} = 'Not Deposited') THEN 1 ELSE 0 END","greaterthan","0"], "AND",
                ["trandate","onorafter","startofthismonth"]
            ],
            columns: [
               "locationnohierarchy",
               search.createColumn({
                  name: "externalid",
                  join: "location"
               }),
               "tranid",
               search.createColumn({
                  name: "custrecord_rics_st_saledatetime",
                  join: "CUSTBODY_GF_RICS_SALES_TICKET_ID",
                  sort: search.Sort.DESC
               }),
               search.createColumn({
                  name: "custrecord_rics_st_ticketnumber_text",
                  join: "CUSTBODY_GF_RICS_SALES_TICKET_ID"
               }),
               search.createColumn({
                  name: "custrecord_rics_st_receipt_total",
                  join: "CUSTBODY_GF_RICS_SALES_TICKET_ID"
               }),
               search.createColumn({
                  name: "formulacurrency",
                  formula: "NVL2({custbody_gf_rics_exchange_record_id}, ({amount} + {custbody_gf_rics_exchange_record_id.amount}), {amount})",
                  sort: search.Sort.DESC
               }),
               "amount",
               "custbody_gf_rics_exchange_record_id",
               search.createColumn({
                  name: "amount",
                  join: "CUSTBODY_GF_RICS_EXCHANGE_RECORD_ID"
               }),
               "custbody_gf_rics_sales_ticket_id",
               search.createColumn({
                  name: "lastmodifieddate",
                  sort: search.Sort.ASC
               })
            ]
        });
        var searchResultCount = cashsaleSearchObj.runPaged().count;
        if(searchResultCount > 0){
            var cs_search_run = cashsaleSearchObj.run();
            var searchResult = cs_search_run.getRange({start: 0, end: 1});
            var top_ticket_id = searchResult[0].getValue('custbody_gf_rics_sales_ticket_id');
            if(currentScript.deploymentId != 'customdeploy_mr_rics_cash_sale_refund_od'){
                var call_csr_js_od = task.create({
                    taskType: task.TaskType.MAP_REDUCE,
                    scriptId: 'customscript_mr_rics_cash_sale_refund',
                    deploymentId: 'customdeploy_mr_rics_cash_sale_refund_od',
                    params: { custscript_sales_ticket_id: top_ticket_id }
                });
                call_csr_js_od.submit();
            }else{
                var call_csr_js_od = task.create({
                    taskType: task.TaskType.MAP_REDUCE,
                    scriptId: 'customscript_mr_rics_cash_sale_refund',
                    deploymentId: 'customdeploy_mr_rics_cash_sale_refundod2',
                    params: { custscript_sales_ticket_id: top_ticket_id }
                });
                call_csr_js_od.submit();
            }
        }
    }


    function removeCompletedDeployments(){
        var arr_deployment_ids = [];
		var scheduledscriptinstanceSearchObj = search.create({
			type: "scheduledscriptinstance",
			filters: [
				["status","anyof","COMPLETE","CANCELED"], "AND",
				["script.internalid","anyof","1604"], "AND",
				["scriptdeployment.internalid","noneof","61909","61908","61907","61910"], "AND",
				["enddate","onorbefore","hoursago1"]
			],
			columns: [
				search.createColumn({
					name: "name",
					join: "script"
				}),
				search.createColumn({
					name: "scriptid",
					join: "scriptDeployment"
				}),
				search.createColumn({
					name: "internalid",
					join: "scriptDeployment"
				}),
				search.createColumn({
					name: "timestampcreated",
					sort: search.Sort.DESC
				}),
				"mapreducestage",
				"status",
				"startdate",
				"enddate"
			]
		});
		scheduledscriptinstanceSearchObj.run().each(function(result){
			// .run().each has a limit of 4,000 results
			var this_deployment_id = result.getValue({name: 'internalid', join: 'scriptDeployment'});
			if(arr_deployment_ids.indexOf(this_deployment_id) == -1){
				arr_deployment_ids.push(this_deployment_id);
			}
			return true;
		});

		// remove the deployments
		for(var dep_id in arr_deployment_ids){
			var deployment_id = arr_deployment_ids[dep_id];
			try{
				record.delete({type: 'scriptdeployment', id: deployment_id});
			}catch(ex){
				log.error({title: 'Error: GF_MR_RICS_Cash_Sale_Refund.js:removeCompletedDeployments', details: 'deployment_id: ' + deployment_id + ', Error ' + ex.toString() + ' : ' + ex.stack});
			}
		}
    }

});