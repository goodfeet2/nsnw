/**
 *@NApiVersion 2.1
 *@NScriptType MapReduceScript
 *
Script Name:   GF_MR_RICS_Transfer_Orders.js
Author:        Mark Robinson
 */

define(['N/log', 'N/search', 'N/record', 'N/https', 'N/format', 'N/runtime', '/SuiteScripts/GF_LIB/gf_nsrics'], function(log, search, record, https, format, runtime, gf_nsrics) {

    /**
     * In this Get Input function we're using the RICS /api/Inventory/GetInventoryTransaction end point to get all of the Transfer orders that have been entered for each store
     * between yesterday and tomorrow, we then write the response data to an array that will be passed to the mapping function for further processing.
     * @param {*} context - this variable is not utilized in this function
     * @returns - an array containing the full responses from the API requests
     */
    function rics_getInputData(context){
        var nsrics = gf_nsrics.getIntegration();
        // Get all Transfer Order Data data from RICS with a POST request to: https://enterprise.ricssoftware.com/api/Inventory/GetInventoryTransaction
        // Map phase will create a NetSuite writable object
        // Reduce phase will save the NetSuite writable object
        var max_attempts = 3;
        var allStores = [];
        var currentScript = runtime.getCurrentScript();
        var to_num = currentScript.getParameter({ name: 'custscript_rics_to_number' });
        var storecode_num = currentScript.getParameter({ name: 'custscript_rics_to_storecode' });
        try {
            // get list of all stores with external ids - need to iterate over these to get all transfer orders since the last script run
            var locationSearchObj = search.create({
                type: "location",
                filters: [ ["locationtype","anyof","1"], "AND", ["externalid","noneof","@NONE@"] ], // type = store, externalid != blank
                columns: [
                    search.createColumn({name: "externalid"}),
                    search.createColumn({
                        name: "name",
                        sort: search.Sort.ASC,
                        label: "Name"
                    })
                ]
            });
            locationSearchObj.run().each(function(result){
                // .run().each has a limit of 4,000 results
                allStores[result.id] = result.getValue('externalid');
                return true;
            });
            // log.debug({title: 'GF_MR_RICS_Transfer_Orders.js:rics_getInputData', details: 'allStores count: ' + allStores.length});
            if(to_num == null){
                var dt_tomorrow = new Date();
                dt_tomorrow.setDate(dt_tomorrow.getDate() + 1);
                dt_tomorrow.setHours(0,0,0,0);
                var str_today = format.format({
                    value: dt_tomorrow,
                    type: format.Type.DATETIME
                });
                var dt_yesterday = new Date(dt_tomorrow);
                dt_yesterday.setDate(dt_yesterday.getDate() -2); //  change date
                var str_yesterday = format.format({
                    value: dt_yesterday,
                    type: format.Type.DATETIME
                });
            }else{
                var temp_ns_store_id = allStores.indexOf(storecode_num);
                var allStores = [];
                allStores[temp_ns_store_id] = storecode_num;
            }

            var arr_Transactions = [];
            var getInventoryTransaction = nsrics.baseurl + nsrics.getEndPoint('GetInventoryTransaction');
            var header = [];
            header['Content-Type'] = 'application/json';
            header['Token'] = nsrics.getActiveConnection();
            for(var ns_store_id in allStores){
                var num_skip = 0;
                var current_attempt = 0;

                do{
                    nsrics.sleep(1000);
                    if(to_num == null){
                        var postData = { "TransactionStart":str_yesterday, "TransactionEnd":str_today, "StoreCode":allStores[ns_store_id], "TransferOrderNumber": "%", "Skip":num_skip };
                    }else{
                        var postData = { "TransactionStart":"1/1/1753 12:00:00 AM", "TransactionEnd":"1/1/9999 11:59:59 PM", "StoreCode":allStores[ns_store_id], "TransferOrderNumber":to_num, "Skip":num_skip };
                    }
                    log.audit({title: 'GF_MR_RICS_Transfer_Orders.js:rics_getInputData', details: 'postData: ' + JSON.stringify(postData)});
                    postData = JSON.stringify(postData);
                    var response = https.post({
                        url: getInventoryTransaction,
                        headers:header,
                        body: postData
                    });
                    if(response['code'] != 200){
                        current_attempt += 1;
                    }else{
                        current_attempt = 0;
                        var data = JSON.parse(response.body);
                        for(var key in data.Transactions){
                            var ricsToData = {};
                            ricsToData.store_ns_id = ns_store_id;
                            ricsToData.store_ext_id = allStores[ns_store_id];
                            if(data.Transactions[key].hasOwnProperty('ProductItem')){
                                ricsToData.transaction = data.Transactions[key];
                            }
                            arr_Transactions.push(ricsToData);
                        }
                        num_skip = data.ResultStatistics.EndRecord;
                    }
                }while( current_attempt < max_attempts && ( response['code'] != 200 || ( data.hasOwnProperty('ResultStatistics') && data.ResultStatistics.EndRecord < data.ResultStatistics.TotalRecords ) ) );
            }

            log.audit({title: 'GF_MR_RICS_Transfer_Orders.js:rics_getInputData', details: 'arr_Transactions: ' + JSON.stringify(arr_Transactions)});

            return arr_Transactions;
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_Transfer_Orders.js:rics_getInputData', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this mapping function we take the response data from the Get Input function and use it to create a NetSuite writable object, by
     * performing all of the lookup functions, data formatting, and direct associations required to build out the object properties.
     * @param {*} context - this is expected to be the search results from the Get Input function
     * @returns - void (note: the const context variable is impacted and used in the reduce function)
     */
    function rics_map(context){
        try{
            var data = JSON.parse(context.value);
            if(data == null){ return true; }
            log.debug({title: 'GF_MR_RICS_Transfer_Orders.js:rics_map', details: 'data: ' + JSON.stringify(data)});
            // build a NetSuite writable object
            var ns_record_data = {};
            ns_record_data.altname = data.transaction.TransferOrderNumber + ' - ' + data.transaction.User + ' - ' + data.transaction.TransactionDate;
            ns_record_data.custrecord_rics_to_onhand = data.transaction.OnHand;
            ns_record_data.custrecord_rics_to_transaction_qty = data.transaction.TransactionQuantity;
            ns_record_data.custrecord_rics_to_cost = data.transaction.Cost;
            ns_record_data.custrecord_rics_to_avg_weighted_cost = (data.transaction.hasOwnProperty('AverageWeightedCost')) ? data.transaction.AverageWeightedCost : 0;
            ns_record_data.custrecord_rics_to_transaction_date = format_RICS_date_string(data.transaction.TransactionDate);
            ns_record_data.custrecord_rics_to_created_on = format_RICS_date_string(data.transaction.CreatedOn);
            ns_record_data.custrecord_rics_to_user = data.transaction.User;
            ns_record_data.custrecord_rics_to_transaction_type = data.transaction.TransactionType;
            var transfer_type_id = find_transfer_type(data.transaction.TransactionType);
            if(transfer_type_id == 0){ transfer_type_id = setNewListValue('customlist_rics_to_transfer_type', data.transaction.TransactionType); }
            ns_record_data.custrecord_rics_to_transfer_type = transfer_type_id;
            ns_record_data.custrecord_rics_to_xfer_to_store_code = data.transaction.TransferToStoreCode;
            ns_record_data.custrecord_rics_to_xfer_to_store_name = data.transaction.TransferToStoreName;
            ns_record_data.custrecord_rics_to_xfer_order_number = data.transaction.TransferOrderNumber;
            ns_record_data.custrecord_rics_to_receiving_log_batch = (data.transaction.hasOwnProperty('ReceivingLogBatchNumber')) ? data.transaction.ReceivingLogBatchNumber : '';
            ns_record_data.custrecord_rics_to_local_date = format_RICS_date_string(data.transaction.LocalDate);
            // lookup prodect details by SKU
            if(data.transaction.ProductItem.hasOwnProperty('Sku')){
                var pd_id = find_pd_record(data.transaction.ProductItem.Sku);
                ns_record_data.custrecord_rics_to_product_details = pd_id;
            }
            // lookup product item by UPC
            if(data.transaction.ProductItem.hasOwnProperty('UPC')){
                var pi_id = find_pi_record(data.transaction.ProductItem.UPC);
                ns_record_data.custrecord_rics_to_product_item = pi_id;
            }
            ns_record_data.custrecord_rics_to_location = data.store_ns_id;
            ns_record_data.custrecord_rics_to_json = JSON.stringify(data);

            // pass data to reduce for setting on hand
            context.write({ key: context.key, value: ns_record_data });
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_Transfer_Orders.js:rics_map', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this reduce function we take the data that was passed through the context variable from the mapping function and we try to find an exisiting
     * NetSuite Transfer Order transaction record. If we find it we update it, and if we don't then we create a new one.
     * @param {*} context - this is expected to contain the JSON object built in the mapping function above
     * @returns void
     */
    function rics_reduce(context){
        try{
            var nsrics = gf_nsrics.getIntegration();
            // update pricing and on hand quantity on all of the items by UPC and storecode
            var data = JSON.parse(context.values[0]);
            if(data == null){ return true; }
            if(data.hasOwnProperty('custrecord_rics_to_xfer_order_number')){
                log.debug({title: 'GF_MR_RICS_Transfer_Orders.js:rics_reduce', details: 'data: ' + JSON.stringify(data)});
                // var ns_to_id = find_to_record(data.custrecord_rics_to_xfer_order_number, data.custrecord_rics_to_product_item, data.custrecord_rics_to_location, data.custrecord_rics_to_user, data.custrecord_rics_to_receiving_log_batch);
                var ns_to_id = find_to_record(data.custrecord_rics_to_json);
                log.audit({title: 'GF_MR_RICS_Transfer_Orders.js:rics_reduce', details: 'ns_to_id: ' + ns_to_id});
                if(ns_to_id == 0){
                    // no tranfer order record was found we need to create one
                    var transfer_order_record = record.create({
                        type: 'customrecord_rics_transfer_order'
                    });
                }else{
                    log.debug({title: 'GF_MR_RICS_Transfer_Orders.js:rics_reduce', details: 'ns_to_id: ' + ns_to_id});
                    // a tranfer order record was found we should update it
                    var transfer_order_record = record.load({
                        type: 'customrecord_rics_transfer_order',
                        id: ns_to_id,
                        isDynamic: true
                    });
                }
                for(var prop in data){
                    if(prop == 'custrecord_rics_to_transaction_date' || prop == 'custrecord_rics_to_created_on' || prop == 'custrecord_rics_to_local_date'){
                        transfer_order_record.setValue(prop,new Date(nsrics.str_reformat_date(data[prop])));
                    }else{
                        transfer_order_record.setValue(prop,data[prop]);
                    }
                }
                var saved_to_id = transfer_order_record.save();
                log.audit({title: 'GF_MR_RICS_Transfer_Orders.js:rics_reduce', details: 'saved_to_id: ' + saved_to_id + ', data: ' + JSON.stringify(data)});
            }
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_Transfer_Orders.js:rics_reduce', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * This summarize function is used to report important aspects about how the script performed
     * @param {*} summary - this object contains data regarding script performance.
     */
    function rics_summarize(summary) {
        try{
            log.audit('Usage Consumed', summary.usage);
            log.audit('Concurrency Number ', summary.concurrency);
            log.audit('Number of Yields', summary.yields);
            log.audit({title: 'Process Completed'});
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_Transfer_Orders.js:rics_summarize', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    return {
        getInputData: rics_getInputData,
        map: rics_map,
        reduce: rics_reduce,
        summarize: rics_summarize
    };

    // =========================== extra functions
    /**
     * This function takes the RICS transfer order number, the product item internal ID, and the location internal ID, and tries to find the RICS Transfer Order custom record
     * @param {*} _data - object - the data passed into the reduce method
     * @returns - integer - the internal ID of the RICS Transfer Order custom record
     */
    function find_to_record(_data){
        var result = 0;
        // find the RICS Transfer Order custom record
        var flat_data = flatten(JSON.parse(_data));
        var arr_filters = [];
        for(var i in flat_data){
            // when the data type is a string or number check the json field for that value (flat data will contain arrays and adding those can break the filters)
            if((typeof flat_data[i] == 'string' || typeof flat_data[i] == 'number') && flat_data[i].toString() != ''){
                arr_filters.push(["custrecord_rics_to_json","contains",flat_data[i].toString()]);
                arr_filters.push("AND");
            }
        }
        var drop = arr_filters.pop(); // this removes the last "AND" from the filters array
        var to_SearchResults = search.create({
            type: "customrecord_rics_transfer_order",
            filters: arr_filters,
            columns: [
                search.createColumn({
                    name: "internalid",
                    sort: search.Sort.DESC,
                    label: "ID"
                })
            ]
        }).run();
        var to_SearchResultSet = to_SearchResults.getRange({start: 0, end: 1000});
        if (to_SearchResultSet != null && to_SearchResultSet.length > 0){
            result = to_SearchResultSet[0].id;
        }
        // log.debug({title: 'GF_MR_RICS_Transfer_Orders.js:find_to_record', details: 'result: ' + result});
        return result;
    }

    /**
     * This function is designed to flatten an object with nested objects, and return a single object with all of the properties and values within
     * @param {object} _obj - object - this is the object that is going to be flattened
     * @returns - object - this is the final version of the flattened object
     */
    function flatten(_obj){
        const flattened = {};
        Object.keys(_obj).forEach(function(key){
            const value = _obj[key];
            if (typeof value === 'object' && value !== null && !Array.isArray(value)) {
                Object.assign(flattened, flatten(value));
            } else {
                flattened[key] = value;
            }
        });
        return flattened;
    }

    /**
     * This function takes the sku and returns the internal ID of the RICS Product Details custom record
     * @param {*} _sku - string - the sku of the item
     * @returns - integer - the internal ID of the RICS Product Details custom record
     */
    function find_pd_record(_sku){
        var result = 0;
        // find the Product Details Record
        var pd_SearchResults = search.create({
            type: "customrecord_rics_product_details",
            filters: [
                ["isinactive","is","F"], "AND",
                ["custrecord_rics_pd_sku","is",_sku]
            ],
            columns: [
                search.createColumn({
                    name: "id",
                    sort: search.Sort.DESC,
                    label: "ID"
                }),
                search.createColumn({name: "internalid", label: "Internal ID"})
            ]
        }).run();
        var pd_SearchResultSet = pd_SearchResults.getRange({start: 0, end: 1000});
        if (pd_SearchResultSet != null && pd_SearchResultSet.length > 0){
            result = pd_SearchResultSet[0].id;
        }
        // log.debug({title: 'GF_MR_RICS_Transfer_Orders.js:find_pd_record', details: 'result: ' + result});
        return result;
    }

    /**
     * This function takes the upc and returns the internal ID of the RICS Product Item custom record
     * @param {*} _upc - string - the sku of the item
     * @returns - integer - the internal ID of the RICS Product Item custom record
     */
    function find_pi_record(_upc){
        var result = 0;
        // find the Product Details Record
        var pi_SearchResults = search.create({
            type: "customrecord_rics_product_item",
            filters: [
                ["isinactive","is","F"], "AND",
                ["custrecord_rics_pi_upcs","contains",'"'+_upc+'"']
            ],
            columns: [
                search.createColumn({
                    name: "id",
                    sort: search.Sort.DESC,
                    label: "ID"
                }),
                search.createColumn({name: "internalid", label: "Internal ID"})
            ]
        }).run();
        var pi_SearchResultSet = pi_SearchResults.getRange({start: 0, end: 1000});
        if (pi_SearchResultSet != null && pi_SearchResultSet.length > 0){
            result = pi_SearchResultSet[0].id;
        }

        return result;
    }

    /**
     * This function takes a RICS date string and returns a NetSuite usable date string format
     * @param {*} _rics_dt_str - string - date formatted from RICS
     * @returns - string - date formatted for NetSuite
     */
    function format_RICS_date_string(_rics_dt_str){
        // _rics_dt_str example of expected value 2021-07-12T16:04:08
        var str_result = '';

        if(_rics_dt_str.indexOf('T') == 10){
            var rics_date = format.parse({ value : _rics_dt_str, type : format.Type.DATE});

            str_result = format.format({ value : rics_date, type : format.Type.DATETIMETZ, timezone: format.Timezone.AMERICA_LOS_ANGELES});
            // log.debug({title: 'GF_MR_RICS_Transfer_Orders.js:format_RICS_date_string', details: 'str_result: ' + str_result});
        }
        return str_result;
    }

    /**
     * This function takes the RICS transfer type string and returns the internal ID of the transfer type list member
     * @param {*} _str_transfer_type - string - this is the RICS transfer type, typically Transfer In or Transfer Out
     * @returns - integer - the internal ID of the NetSuite list member
     */
    function find_transfer_type(_str_transfer_type){
        var result = 0;
        var arr_transfer_types = getListValues('customlist_rics_to_transfer_type');
        if(arr_transfer_types[_str_transfer_type] != undefined){
            result = arr_transfer_types[_str_transfer_type];
        }
        return result;
    }

    /**
     * This function takes the internal ID of a list and returns the list values in an array
     * @param {*} _listScriptId - string - the internal ID of a NetSuite list
     * @returns - Array(object) - this is an array of the list values that was found with the specified ID
     */
    function getListValues(_listScriptId) {
        var searchColumn = search.createColumn({ name : 'name' });
        var listSearch = search.create({ type : _listScriptId, columns : searchColumn });
        var listArray = [];
        listSearch.run().each(function(searchResult) {
            listArray[searchResult.getValue(searchColumn)] = searchResult.id;
            return true;
        });
        return listArray;
    }

    /**
     * This function sets a new value in a list and is typically used to dynamically add to a list when a value is missing
     * @param {*} _listScriptId - string - the internal ID of the list to be updated
     * @param {*} _newVal - string - the value that will be added to the list
     * @returns - integer - once the new value has been set, the find transfer type function is used to get the id of the new list member that has been added
     */
    function setNewListValue(_listScriptId, _newVal){
        try{
            var list_internal_id = 0;
            if(_listScriptId == 'customlist_rics_to_transfer_type'){
                list_internal_id = 763;
            }
            if(_newVal != undefined && _newVal != null && _newVal != ''){
                if(list_internal_id != 0){
                    var customListRec = record.load({
                        type: 'customlist',
                        id: list_internal_id,
                        isDynamic: true
                    });

                }
                customListRec.selectNewLine({ sublistId: 'customvalue' });

                customListRec.setCurrentSublistValue({
                    sublistId: 'customvalue',
                    fieldId: 'value',
                    value: _newVal,
                    ignoreFieldChange: true
                });
                customListRec.commitLine({sublistId: 'customvalue'});

                var recordId = customListRec.save();
            }
            return find_transfer_type(_newVal);
        }catch(ex){
            log.error({title: 'Error: GF_MR_RICS_Transfer_Orders.js:setNewListValue', details: 'Params: _listScriptId = ' + _listScriptId + ', _newVal = ' + _newVal + ', Error ' + ex.toString() + ' : ' + ex.stack});
            return getListValues(_listScriptId);
        }
    }

});