/**
 *@NApiVersion 2.1
 *@NScriptType MapReduceScript
 *
 Script Name:   GF_MR_RICS_Transfers.js
 Author:        Mark Robinson
 */

 define(['N/log', 'N/search', 'N/record', '/SuiteScripts/GF_LIB/gf_nsrics'], function(log, search, record, gf_nsrics) {

    /**
     * This function searches the active RICS Transfer Order custom records and loads them into an array for processing that will be used in the mapping function
     * @param {*} context - this variable is not utilized in this function
     * @returns - an array containing the full responses from the custom record search
     */
    function rics_getInputData(context){
        // Get all Transfer Order Data data from RICS with a POST request to: https://enterprise.ricssoftware.com/api/Inventory/GetInventoryTransaction
        // Map phase will create a NetSuite writable object
        // Reduce phase will save the NetSuite writable object
        var nsrics = gf_nsrics.getIntegration();
        var open_period_range = nsrics.getOpenPeriodRange(); // .earliest_open_date - .latest_open_date
        try {
            var arr_Transactions = [];

            var customrecord_rics_transfer_orderSearchObj = search.create({
                type: "customrecord_rics_transfer_order",
                filters: [
                    [
                        ["custrecord_rics_to_transferorder","anyof","@NONE@"], "OR",
                        ["custrecord_rics_to_transferorder.status","noneof","TrnfrOrd:G","TrnfrOrd:H","TrnfrOrd:C"] // Transfer Order:Rejected, Transfer Order:Received, Transfer Order:Closed
                    ], "AND",
                    ["custrecord_rics_to_ns_xfer_order_update","isempty",""], "AND",
                    ["custrecord_rics_to_transaction_type","is","Transfer In"], "AND",
                    ["custrecord_rics_to_transaction_date","within",open_period_range.earliest_open_date,open_period_range.latest_open_date]
                ],
                columns: [ ]
            });
            customrecord_rics_transfer_orderSearchObj.run().each(function(result){
                // .run().each has a limit of 4,000 results
                var transfer_order_record = record.load({
                    type: 'customrecord_rics_transfer_order',
                    id: result.id,
                    isDynamic: true
                });
                arr_Transactions.push(transfer_order_record);
                return true;
            });

            log.debug({title: 'GF_MR_RICS_Transfers.js:rics_getInputData', details: 'arr_Transactions: ' + JSON.stringify(arr_Transactions)});

            return arr_Transactions;
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_Transfers.js:rics_getInputData', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * This function takes the data that was collected in the get input function and builds an object that will be used to create or update the NetSuite Transfer Order transaction record
     * @param {*} context - object - this is the RICS Transfer Order custom record
     * @returns - void (note: the const context variable is impacted and used in the reduce function)
     */
    function rics_map(context){
        try{
            var data = JSON.parse(context.value);
            if(data == null){ return true; }
            if(data.fields.hasOwnProperty('custrecord_rics_to_product_item') != true){ return true; }
            log.debug({title: 'GF_MR_RICS_Transfers.js:rics_map', details: 'data: ' + JSON.stringify(data)});
            // build a NetSuite writable object
            var ns_record_data = {};
            ns_record_data.source_id = data.id;
            ns_record_data.ns_to_id = data.custrecord_rics_to_transferorder;
            // lookup subsidiary
            var from_store_loc_id = find_location(data.fields.custrecord_rics_to_xfer_order_number);
            if(from_store_loc_id == 0){
                log.audit({title: 'GF_MR_RICS_Transfers.js:rics_map-from_store_loc_id not found', details: 'from_store_loc_id: ' + from_store_loc_id + ', data.id: ' + data.id + ', data: ' + JSON.stringify(data)});
                return true;
            }
            var sub_id = find_subsidiary(from_store_loc_id);
            // lookup employee
            var emp_id = find_employee(data.fields.custrecord_rics_to_user);
            // lookup to store custrecord_rics_to_xfer_to_store_code
            var to_store_sub_id = find_subsidiary(data.fields.custrecord_rics_to_location);

            if(sub_id == to_store_sub_id){
                ns_record_data.recordtype = 'transferorder';
                ns_record_data.subsidiary = sub_id;
            }else{
                ns_record_data.recordtype = 'intercompanytransferorder';
                ns_record_data.customform = '161'; // GF - Primary Transfer Order Form
                ns_record_data.subsidiary = sub_id;
                ns_record_data.tosubsidiary = to_store_sub_id; // inter-district xfer requires top level sub
            }
            ns_record_data.location = from_store_loc_id;
            if(emp_id > 0){
                ns_record_data.employee = emp_id; // lookup employee
            }
            ns_record_data.firmed = true;
            ns_record_data.useitemcostastransfercost = true;
            if(data.fields.hasOwnProperty('custrecord_rics_to_transaction_date')){
                var str_tran_date = (data.fields.custrecord_rics_to_transaction_date).split(' ')[0];
                str_tran_date = str_reformat_date(str_tran_date);
                ns_record_data.trandate = str_tran_date;
            }
            ns_record_data.transferlocation = data.fields.custrecord_rics_to_location;
            ns_record_data.memo = data.fields.custrecord_rics_to_xfer_order_number + ' - ' + data.fields.custrecord_rics_to_user;
            ns_record_data.incoterm = '2';
            ns_record_data.custbody_gf_internal_to_number = data.fields.custrecord_rics_to_xfer_order_number;
            ns_record_data.custbody_rics_to_transfer_type = data.fields.custrecord_rics_to_transfer_type;

            var item_id = find_item(data.fields.custrecord_rics_to_json, data.fields.custrecord_rics_to_product_item);
            ns_record_data.item_id = item_id;
            ns_record_data.item_qty = (data.fields.custrecord_rics_to_transaction_qty > 0) ? data.fields.custrecord_rics_to_transaction_qty : (data.fields.custrecord_rics_to_transaction_qty * -1);
            ns_record_data.item_cost = data.fields.custrecord_rics_to_cost;
            ns_record_data.item_date = data.fields.custrecord_rics_to_transaction_date;

            ns_record_data.status = (data.fields.custrecord_rics_to_transaction_type).toLowerCase();

            // pass data to reduce for setting on hand // ns_record_data.custbody_gf_internal_to_number
            // context.write({ key: context.key, value: ns_record_data });
            context.write({ key: ns_record_data.custbody_gf_internal_to_number, value: ns_record_data });
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_Transfers.js:rics_map', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * This reduce function takes the object built in the mapping function and uses the data to create or update a NetSuite Transfer Order Transaction record
     * @param {*} context - object - this object should contain all of the properties required to build or update the NetSuite Transfer Order Transaction record
     * @returns - void
     */
    function rics_reduce(context){
        try{
            // update pricing and on hand quantity on all of the items by UPC and storecode
            // for(var c in context.values){
            //     log.audit({title: 'GF_MR_RICS_Transfers.js:rics_reduce', details: 'c: ' + c + ', data: ' + JSON.stringify(context.values[c])});
            // }
            var data = JSON.parse(context.values[0]);
            if(data == null){ return true; }
            log.audit({title: 'GF_MR_RICS_Transfers.js:rics_reduce', details: 'data: ' + JSON.stringify(data)});
            if(data.hasOwnProperty('custbody_gf_internal_to_number') && data.item_id > 0){
                var arr_not_record_prop = [ // these values are for processing, and not to be directly written on the record
                    'recordtype',
                    'item_id',
                    'item_qty',
                    'item_cost',
                    'item_date',
                    'status',
                    'source_id',
                    'ns_to_id'
                ];
                var str_item_date = (data.item_date).split(' ')[0];
                var dt_item_date = new Date(str_item_date);
                var saved_to_id = 0;
                var ns_to_id = 0;
                if(data.hasOwnProperty('ns_to_id') && data.ns_to_id > 0){
                    ns_to_id = data.ns_to_id;
                }else{
                    ns_to_id = find_to_record(data.custbody_gf_internal_to_number);
                }
                // pre-process to find matching item_ids
                var arr_items = [];
                for(var c in context.values){
                    var context_val = JSON.parse(context.values[c]);
                    var itemId = context_val.item_id;
                    // log.debug({title: 'GF_MR_RICS_Transfers.js:rics_reduce', details: 'c: ' + c + ', data: ' + JSON.stringify(itemId)});
                    if(arr_items[itemId] == undefined){
                        arr_items[itemId] = context_val;
                        // log.debug({title: 'GF_MR_RICS_Transfers.js:rics_reduce', details: 'ADD ITEM: ' + itemId});
                    }else{
                        arr_items[itemId].item_qty = parseInt(arr_items[itemId].item_qty) + parseInt(context_val.item_qty);
                        // log.debug({title: 'GF_MR_RICS_Transfers.js:rics_reduce', details: 'UPD QTY new qty: ' + arr_items[itemId].item_qty});
                    }
                }
                var arr_items = arr_items.filter(function (el){ return el != null; }); // remove null values

                if(ns_to_id == 0){
                    // NEW TRANSFER ORDER =================================================
                    // no tranfer order record was found we need to create one
                    var transfer_order_record = record.create({
                        type: data.recordtype // transaction type:transferorder
                    });
                    for(var prop in data){
                        if(arr_not_record_prop.indexOf(prop) == -1){
                            if(prop == 'trandate'){
                                transfer_order_record.setText(prop,data[prop]);
                            }else{
                                transfer_order_record.setValue(prop,data[prop]);
                            }
                        }
                    }
                    for(var item in arr_items){
                        var line_data = arr_items[item];
                        // also add the line item
                        transfer_order_record.setSublistValue({
                            sublistId: 'item',
                            fieldId: 'item',
                            line: 0,
                            value: line_data.item_id
                        });
                        transfer_order_record.setSublistValue({
                            sublistId: 'item',
                            fieldId: 'quantity',
                            line: 0,
                            value: line_data.item_qty
                        });
                        transfer_order_record.setSublistValue({
                            sublistId: 'item',
                            fieldId: 'rate',
                            line: 0,
                            value: line_data.item_cost
                        });
                        transfer_order_record.setSublistValue({
                            sublistId: 'item',
                            fieldId: 'expectedshipdate',
                            line: 0,
                            value: dt_item_date
                        });
                        transfer_order_record.setSublistValue({
                            sublistId: 'item',
                            fieldId: 'expectedreceiptdate',
                            line: 0,
                            value: dt_item_date
                        });
                    }
                    if(data.status != 'transfer out'){
                        update_status(transfer_order_record, data.status);
                        saved_to_id = transfer_order_record.save();
                    }
                    // NEW TRANSFER ORDER =================================================
                }else{
                    // EXISTING TRANSFER ORDER =================================================
                    log.debug({title: 'GF_MR_RICS_Transfers.js:rics_reduce', details: 'ns_to_id: ' + ns_to_id});
                    // a tranfer order record was found we should update it
                    var transfer_order_record = record.load({
                        type: data.recordtype,
                        id: ns_to_id,
                        isDynamic: true
                    });
                    // also update the line items
                    for(var item in arr_items){
                        var line_data = arr_items[item];
                        log.audit({title: 'GF_MR_RICS_Transfers.js:rics_reduce-update TO lines loop', details: 'line_data: ' + JSON.stringify(line_data)});
                        var flg_itemFound = false;
                        var lineCount = transfer_order_record.getLineCount({ 'sublistId': 'item' });
                        for(var i = 0; flg_itemFound == false && i < lineCount; i++){
                            transfer_order_record.selectLine({ sublistId: 'item', line: i });
                            var item_id = transfer_order_record.getCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'item'
                            });
                            if(item_id != line_data.item_id){ continue; }
                            var item_quantity_fulfilled = transfer_order_record.getCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'quantityfulfilled'
                            });
                            if(item_id == line_data.item_id && item_quantity_fulfilled == 0){
                                flg_itemFound = true;
                                var item_quantity = transfer_order_record.getCurrentSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'quantity'
                                });
                                var quantity_update = parseInt(item_quantity) + parseInt(line_data.item_qty);
                                transfer_order_record.setCurrentSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'quantity',
                                    value: quantity_update
                                });
                                transfer_order_record.setCurrentSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'rate',
                                    value: line_data.item_cost
                                });
                                transfer_order_record.setCurrentSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'rate',
                                    value: line_data.item_cost
                                });
                                transfer_order_record.setCurrentSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'expectedshipdate',
                                    value: dt_item_date
                                });
                                transfer_order_record.setCurrentSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'expectedreceiptdate',
                                    value: dt_item_date
                                });
                            }
                        }
                        if(flg_itemFound == false){
                            // select a new line and add the item
                            transfer_order_record.selectNewLine({ sublistId: 'item' });
                            transfer_order_record.setCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'item',
                                value: line_data.item_id
                            });
                            transfer_order_record.setCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'quantity',
                                value: line_data.item_qty
                            });
                            transfer_order_record.setCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'rate',
                                value: line_data.item_cost
                            });
                            transfer_order_record.setCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'expectedshipdate',
                                value: dt_item_date
                            });
                            transfer_order_record.setCurrentSublistValue({
                                sublistId: 'item',
                                fieldId: 'expectedreceiptdate',
                                value: dt_item_date
                            });
                        }
                        transfer_order_record.commitLine({sublistId: 'item'});
                    }
                    update_status(transfer_order_record, data.status);

                    saved_to_id = transfer_order_record.save();
                    // EXISTING TRANSFER ORDER =================================================
                }
                if(saved_to_id > 0){
                    for(var c in context.values){
                        var line_data = JSON.parse(context.values[c]);
                        var rics_to_Id = record.submitFields({
                            type: 'customrecord_rics_transfer_order',
                            id: line_data.source_id,
                            values: {
                                'custrecord_rics_to_transferorder': saved_to_id,
                                'custrecord_rics_to_ns_xfer_order_update': new Date()
                            }
                        });
                    }
                    var transfer_order_rec = record.load({
                        type: data.recordtype,
                        id: saved_to_id,
                        isDynamic: true
                    });

                    // ITEM FULFILLMENT =================================================
                    try{
                        // check for an item fulfillment - if one has not been created then create it, if one or more have been created then check all items to ensure that we have fulfilled everything
                        var saved_if_id = 0;
                        var itemfulfillmentSearchObj = search.create({
                            type: "itemfulfillment",
                            filters: [
                                ["type","anyof","ItemShip"], "AND", // Item Fulfillment
                                ["createdfrom","anyof",saved_to_id], "AND",
                                ["mainline","is","T"]
                            ],
                            columns: [ "status" ]
                        });
                        var searchResultCount = itemfulfillmentSearchObj.runPaged().count;
                        if(searchResultCount == 0 || arr_items.length > 0){
                            // create the fulfillment
                            var rec_type = (data.recordtype == 'transferorder') ? record.Type.TRANSFER_ORDER : record.Type.INTER_COMPANY_TRANSFER_ORDER;
                            var item_fulfillment_record = record.transform({
                                fromType : rec_type,
                                fromId : saved_to_id,
                                toType : record.Type.ITEM_FULFILLMENT
                            });
                            var to_tran_date = transfer_order_rec.getValue('trandate');
                            item_fulfillment_record.setValue('trandate',new Date(to_tran_date));
                            item_fulfillment_record.setValue('shipstatus','C');
                            var toLineCount = transfer_order_rec.getLineCount({ sublistId : "item" });
                            for(var if_line_num = 0; arr_items[if_line_num] != undefined && if_line_num < toLineCount; if_line_num++){
                                transfer_order_rec.selectLine({ sublistId: "item", line: if_line_num });
                                // item_fulfillment_record.selectNewLine({ sublistId: "item" });
                                item_fulfillment_record.setSublistValue({
                                    sublistId: "item",
                                    fieldId: "item",
                                    line: if_line_num,
                                    value: arr_items[if_line_num].item_id
                                });
                                item_fulfillment_record.setSublistValue({
                                    sublistId: "item",
                                    fieldId: "quantity",
                                    line: if_line_num,
                                    value: arr_items[if_line_num].item_qty
                                });
                                item_fulfillment_record.setSublistValue({
                                    sublistId: "item",
                                    fieldId: "rate",
                                    line: if_line_num,
                                    value: arr_items[if_line_num].item_cost
                                });
                                item_fulfillment_record.setSublistValue({
                                    sublistId: "item",
                                    fieldId: "itemreceive",
                                    line: if_line_num,
                                    value: true
                                });
                            }
                            saved_if_id = item_fulfillment_record.save();
                        }else{
                            // verify the items / can we update the fulfillment? if not create a new fulfillment for the new items
                            var arr_all_IF_results = [];
                            itemfulfillmentSearchObj.run().each(function(result){
                                // .run().each has a limit of 4,000 results
                                if(arr_all_IF_results.indexOf(result.id) == -1){
                                    arr_all_IF_results.push(result.id); // only update on records that have not been shipped (once saved are any not-shipped?)
                                }
                                log.audit({title: 'GF_MR_RICS_Transfers.js:rics_reduce', details: 'Found Saved Item Fulfillment - ID: ' + result.id});
                                return true;
                            });
                            saved_if_id = (arr_all_IF_results[0] != undefined) ? arr_all_IF_results[0] : 0;
                        }
                    }catch(ex){
                        log.audit({title: 'GF_MR_RICS_Transfers.js:rics_reduce', details: 'FAILED to save Item Fulfillment - XFER ID = ' + saved_to_id + ' - Error ' + ex.toString() + ' : ' + ex.stack});
                    }
                    // ITEM FULFILLMENT =================================================
                    // ITEM RECEIPT =================================================
                    if(saved_if_id > 0){
                        try{
                            var itemreceiptSearchObj = search.create({
                                type: "itemreceipt",
                                filters: [
                                    ["type","anyof","ItemRcpt"], "AND", // Item Fulfillment
                                    ["createdfrom","anyof",saved_if_id], "AND",
                                    ["mainline","is","T"]
                                ],
                                columns: [ ]
                            });
                            var searchResultCount = itemreceiptSearchObj.runPaged().count;
                            if(searchResultCount == 0 || arr_items.length > 0){
                                // create item receipt
                                var rec_type = (data.recordtype == 'transferorder') ? record.Type.TRANSFER_ORDER : record.Type.INTER_COMPANY_TRANSFER_ORDER;
                                var item_receipt_record = record.transform({
                                    fromType : rec_type,
                                    fromId : saved_to_id,
                                    toType : record.Type.ITEM_RECEIPT
                                });
                                var to_tran_date = transfer_order_rec.getValue('trandate');
                                item_receipt_record.setValue('trandate',new Date(to_tran_date));

                                var irLineCount = item_receipt_record.getLineCount({ sublistId : "item" });
                                // select all for Receipt
                                for (var i = 0; i < irLineCount; i++) {
                                    item_receipt_record.setSublistValue({sublistId: "item", fieldId: "itemreceive", value: true, line : i});
                                }
                                var saved_ir_id = item_receipt_record.save();
                                log.debug({title: 'GF_MR_RICS_Transfers.js:rics_reduce', details: 'saved_ir_id: ' + (saved_ir_id != undefined ? saved_ir_id : 0) });
                            }
                        }catch(ex){
                            log.audit({title: 'GF_MR_RICS_Transfers.js:rics_reduce', details: 'FAILED to save Item Receipt - XFER ID = ' + saved_to_id + ' - Error ' + ex.toString() + ' : ' + ex.stack});
                        }
                    }
                    // ITEM RECEIPT =================================================
                }
                log.audit({title: 'GF_MR_RICS_Transfers.js:rics_reduce', details: 'saved_to_id: ' + saved_to_id + ', data: ' + JSON.stringify(data)});
            }else{
                var rics_to_Id = record.submitFields({
                    type: 'customrecord_rics_transfer_order',
                    id: data.source_id,
                    values: {
                        'custrecord_rics_to_ns_xfer_order_update': new Date()
                    }
                });
            }
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_Transfers.js:rics_reduce', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * This summarize function is used to report important aspects about how the script performed
     * @param {*} summary - this object contains data regarding script performance.
     */
    function rics_summarize(summary) {
        try{
            log.audit('Usage Consumed', summary.usage);
            log.audit('Concurrency Number ', summary.concurrency);
            log.audit('Number of Yields', summary.yields);
            log.audit({title: 'Process Completed'});
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_Transfers.js:rics_summarize', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    return {
        getInputData: rics_getInputData,
        map: rics_map,
        reduce: rics_reduce,
        summarize: rics_summarize
    };

    // =========================== extra functions
    /**
     * This function takes the RICS transfer order number and tries to find the internal ID of the NetSuite Transfer Order record
     * @param {*} _to_number - string - RICS transfer order number
     * @returns - integer - internal ID of the NetSuite Transfer Order record
     */
    function find_to_record(_to_number){
        var result_id = 0;

        var transferorderSearchObj = search.create({
            type: "transferorder",
            filters: [
               ["type","anyof","TrnfrOrd"], "AND",
               ["custbody_gf_internal_to_number","is",_to_number], "AND",
               ["mainline","is","T"]
            ],
            columns: [ ]
        });
        transferorderSearchObj.run().each(function(result){
            // .run().each has a limit of 4,000 results
            if(result_id == 0){
                result_id = result.id;
            }
            return true;
        });

        return result_id;
    }

    /**
     * This function takes the location ID and returns the subsidiary ID
     * @param {*} _to_location - integer - internal ID of the location record
     * @returns - integer - internal ID of the subsidiary record
     */
    function find_subsidiary(_to_location){
        try{
            var result = 0;

            // load location record
            var location_record = record.load({
                type: 'location',
                id: _to_location,
                isDynamic: true
            });
            result = location_record.getValue('subsidiary');

            return result;
        }catch(ex){
            log.error({title: 'GF_MR_RICS_Transfers.js:find_subsidiary', details: '_to_location: ' + _to_location + ', Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * This function takes the user name as it was entered on the transfer order in RICS and tries to find the internal ID of the employee record in NetSuite
     * @param {*} _to_user - string - the name of the RICS user
     * @returns - integer - the internal ID of the NetSuite employee record
     */
    function find_employee(_to_user){
        var result_id = 0;
        var str_name_search = _to_user.split(' ')[0];
        str_name_search = str_name_search.toLowerCase();

        // log.debug({title: 'GF_MR_RICS_Transfers.js:find_employee', details: '_to_user: ' + str_name_search});
        var employeeSearchObj = search.create({
            type: "employee",
            filters: [
                ["custentity2","isnotempty",""], "AND",
                ["firstname","startswith",str_name_search], "AND",
                ["title","isnotempty",""], "AND",
                ["isinactive","is","F"]
            ],
            columns:
            [
                search.createColumn({name: "firstname", label: "First Name"}),
                search.createColumn({name: "custentity2", label: "RICS Cashier Code"}),
                search.createColumn({name: "title", label: "Job Title"})
            ]
        });
        employeeSearchObj.run().each(function(result){
            // .run().each has a limit of 4,000 results
            var str_firstname = result.getValue('firstname');
            str_firstname = str_firstname.toLowerCase();
            if(result_id == 0 && str_name_search == str_firstname){
                result_id = result.id;
            }
            return true;
        });

        return result_id;
    }

    /**
     * This function take the RICS transfer order number and tries to find the RICS Tranfer Order custom record for the store that the order was sent from, it then returns the location ID for that store
     * @param {*} _xfer_order_num - string - this is the RICS transfer order number
     * @returns - integer - the internal ID of the sending store
     */
    function find_location(_xfer_order_num){
        var result_id = 0;

        var locationSearchObj = search.create({
            type: "customrecord_rics_transfer_order",
            filters: [ 
                ["custrecord_rics_to_xfer_order_number","is",_xfer_order_num], "AND",
                ["custrecord_rics_to_transaction_type","is","Transfer Out"]
            ],
            columns: [ "custrecord_rics_to_location" ]
        });
        locationSearchObj.run().each(function(result){
            // .run().each has a limit of 4,000 results
            if(result_id == 0){
                // result_id = result.id;
                result_id = result.getValue('custrecord_rics_to_location');
            }
            return true;
        });

        return result_id;
    }

    /**
     * This function takes the json from the product item and the internal ID of the product item and tries to find the NetSuite Inventory Item internal ID
     * @param {*} _json - string - the full object string from the RICS system
     * @param {*} _rics_product_item_id - integer - this is the internal ID of the RICS product item custom record
     * @returns - integer - this is the internal ID of the NetSuite Inventory Item record
     */
    function find_item(_json, _rics_product_item_id){
        var result_id = 0;

        var data = JSON.parse(_json);
        var str_upc = data.transaction.ProductItem.UPC;
        var str_sku = data.transaction.ProductItem.Sku;
        if(str_upc == null || str_upc == ''){
            var pi_fields = search.lookupFields({
                type: 'customrecord_rics_product_item',
                id: _rics_product_item_id,
                columns: ['custrecord_rics_pi_upc']
            });
            var str_upc = (pi_fields.hasOwnProperty('custrecord_rics_pi_upc')) ? pi_fields.custrecord_rics_pi_upc : '';
        }

        var itemSearchObj = search.create({
            type: "item",
            filters: [
               ["upccode","is",str_upc], "AND", 
               ["vendorname","is",str_sku], "AND", 
               ["isinactive","is","F"]
            ],
            columns: [ ]
        });
        itemSearchObj.run().each(function(result){
            // .run().each has a limit of 4,000 results
            if(result_id == 0){
                result_id = result.id;
                var itemId = record.submitFields({
                    type: result.recordType,
                    id: result_id,
                    values: { 'cost': data.transaction.Cost }
                });
            }
            return true;
        });
        log.audit({title: 'GF_MR_RICS_Transfers.js:find_item', details: 'str_upc: ' + str_upc + ', str_sku: ' + str_sku + ', result_id: ' + result_id});
        return result_id;
    }

    /**
     * This function takes the record object and the new status to set and updates the status of the Transfer Order
     * @param {*} _rec - object - this is the NetSuite transfer order record already assumed to be loaded
     * @param {*} _str_status - string - the is the status that the record will need to change to
     */
    function update_status(_rec, _str_status){
        // ["TrnfrOrd:H","TrnfrOrd:D","TrnfrOrd:A","TrnfrOrd:B","TrnfrOrd:F","TrnfrOrd:E",           "TrnfrOrd:G","TrnfrOrd:C"]
        // ["Closed",    "PartFulfil","PendApprv", "PendFulfil","PendRecipt","PendRecipt/PendFulfil","Received",  "Rejected"]
        if(_str_status == 'transfer in'){
            _rec.setValue('status','TrnfrOrd:G'); // Received
        }
        if((_str_status).indexOf('cancel') > -1){
            _rec.setValue('status','TrnfrOrd:C'); // Rejected
        }
    }

    /**
     * This function takes a date string and reformats it into a string that can be used to update or set a NetSuite date field
     * @param {*} _str - string - the date that will be used to return the NetSuite readable date
     * @returns - string - the NetSuite readable date string
     */
    function str_reformat_date(_str){
        var str_result = '';
        if(_str != ''){
            var _arr = _str.split('/');
            var str_month = (parseInt(_arr[0]) < 10) ? '0'+_arr[0] : _arr[0];
            var str_day = (parseInt(_arr[1]) < 10) ? '0'+_arr[1] : _arr[1];
            var str_year = _arr[2];
            // var str_hour = _str.substring(11,13);
            // var str_ampm = (parseInt(str_hour) > 12) ? ' pm' : ' am';
            // str_hour = (parseInt(str_hour) > 12) ? parseInt(str_hour) - 12 : parseInt(str_hour);
            // var str_minute = _str.substring(14,16);
            // str_result = str_month + '/' + str_day + '/' + str_year + ' ' + str_hour + ':' + str_minute + str_ampm;
            str_result = str_month + '/' + str_day + '/' + str_year;
        }

        return str_result;
    }

});