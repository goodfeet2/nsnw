/**
 *@NApiVersion 2.1
 *@NScriptType MapReduceScript
 *
Script Name:   GF_MR_CleanUpCscrDeployments.js
Author:        Mark Robinson
 */

 define(['N/log', 'N/search', 'N/record'], function(log, search, record) {

    /**
     * This get input function loads a search with all scriptdeployments that are not part of the main set
     * @param {*} context  - this variable is not utilized in this function
     * @returns - an array containing the full responses from the custom record search
     */
    function getInputData(context){
        try {

            var scriptdeploymentSearchObj = search.create({
                type: "scriptdeployment",
                filters: [
                    ["script","anyof","1604"], "AND",
                    ["internalid","noneof","61909","61908","61907","61910"]
                ],
                columns: []
            });

            return scriptdeploymentSearchObj;
        }catch(ex) {
            log.error({title: 'GF_MR_CleanUpCscrDeployments.js:rics_getInputData', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this mapping function we attempt to delete the deployments if they can't be deleted then they are still queued otherwise they will be removed
     * @param {*} context - Object - This should be the RICS NonSellable Batch custom record
     * @returns void (note: the const context variable is impacted and used in the reduce function)
     */
    function map(context){
        try{
            // simple pass through
            var map_data = JSON.parse(context.value);
            log.audit({title: 'GF_MR_CleanUpCscrDeployments.js:rics_map', details: 'map_data: ' + JSON.stringify(map_data)});
			if (map_data == null) {
				return true;
			}else{
                try{
                    record.delete({type: "scriptdeployment", id: map_data.id});
                }catch(ex){
                    log.error({title: 'GF_MR_CleanUpCscrDeployments.js:rics_map-onDelete', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
                }
            }

			// context.write({ key: context.key, value: map_data }); // pass through directly to reduce

			return true;
        }catch(ex) {
            log.error({title: 'GF_MR_CleanUpCscrDeployments.js:rics_map', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this reduce function we do nothing
     * @param {*} context - Object - This is the object that was built in the mapping function, it should conatin all the data we need for processing
     * @returns - void
     */
    function reduce(context){
        return true;
    }

    /**
     * This summarize function is used to report important aspects about how the script performed
     * @param {*} summary - this object contains data regarding script performance.
     */
    function summarize(summary) {
        try{
            log.audit('Usage Consumed', summary.usage);
            log.audit('Concurrency Number ', summary.concurrency);
            log.audit('Number of Yields', summary.yields);
            log.audit({title: 'Process Completed'});
        }catch(ex) {
            log.error({title: 'GF_MR_CleanUpCscrDeployments.js:rics_summarize', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    return {
        getInputData: getInputData,
        map: map,
        reduce: reduce,
        summarize: summarize
    };

    // =========================== extra functions

 });