/**
 *@NApiVersion 2.0
 *@NScriptType MapReduceScript
 *
Script Name:   GF_MR_RICS_Inventory.js
Author:        Mark Robinson
 */

 define(['N/log', 'N/search', 'N/record', 'N/https'], function(log, search, record, https) {

    function rics_getInputData(context){
        // Get all inventory data from RICS with a POST request to: https://enterprise.ricssoftware.com/api/Inventory/GetInventoryOnHand
        // Inventory On Hand also contains pricing data that should update the parent and all children items
        // Map phase will update pricing by SKU
        // Reduce phase will update On Hand Quantity
        var flg_Sandbox = true;
        var max_attempts = 1;
        var ricsInventory = [];
        var record_limit = 1000;
        var allStores = [];
        var allSkus = [];
        try {
            var customrecord_rics_product_detailsSearchObj = search.create({
                type: "customrecord_rics_product_details",
                filters: [
                    // [
                    //     ["custrecord_rics_pd_pricing","isempty",""],"OR",
                    //     ["custrecord_rics_pd_ns_pricing_update","onorbefore","today"]
                    // ], "AND",
                    ["custrecord_rics_pd_pricing","isempty",""], "AND",
                    ["custrecord_rics_pd_avail_pos_on","onorbefore","yearsfromnow1"], "AND",
                    // ["internalid","anyof",13303], "AND",
                    ["isinactive","is","F"]
                ],
                columns: [
                    "custrecord_rics_pd_sku",
                    "custrecord_rics_pd_description",
                    "custrecord_rics_pd_pricing",
                    search.createColumn({
                        name: "custrecord_rics_pd_ns_pricing_update",
                        sort: search.Sort.ASC // this keeps the least recent updates at the top
                    }),
                    "lastmodified"
                ]
            });
            customrecord_rics_product_detailsSearchObj.run().each(function(result){
                // .run().each has a limit of 4,000 results
                var str_sku = result.getValue('custrecord_rics_pd_sku');
                if(allSkus.indexOf(str_sku) == -1){
                    allSkus.push(str_sku);
                }
                return true;
            });

            if(!flg_Sandbox){
                // get list of all stores with external ids
                var locationSearchObj = search.create({
                    type: "location",
                    filters: [ ["locationtype","anyof","1"], "AND", ["externalid","noneof","@NONE@"] ], // type = store, externalid != blank
                    columns: [
                        search.createColumn({name: "externalid"}),
                        search.createColumn({
                            name: "name",
                            sort: search.Sort.ASC,
                            label: "Name"
                        })
                    ]
                });
                locationSearchObj.run().each(function(result){
                    // .run().each has a limit of 4,000 results
                    allStores[result.id] = result.getValue('externalid');
                    return true;
                });
                // var allStores = allStores.filter(function (el) { return el != null; });
                // log.debug({title: 'DEBUG: GF_MR_RICS_Inventory.js:rics_getInputData', details: 'allStores count: ' + allStores.length});
            }else{
                var allStores = ['471', '530'];
            }

            var getInventoryOnHand = 'https://enterprise.ricssoftware.com/api/Inventory/GetInventoryOnHand';
            var header = [];
            header['Content-Type'] = 'application/json';
            if(!flg_Sandbox){
                header['Token'] = 'd490f450-5cde-48ab-a5e9-0f10273b6b23'; // production
            }else{
                header['Token'] = 'e05d29f6-b86a-4375-8531-652a1090782f'; // sandbox
            }
            for(var sku in allSkus){
                for(var id in allStores){
                    if(allSkus[sku] == null){ continue; }
                    var num_skip = 0;
                    var current_attempt = 0;
                    
                    // ===================
                    // get all values
                    do{
                        if(ricsInventory.length >= record_limit){ break; }
                        sleep(1000);
                        // var postData = { "SKU": allSkus[sku], "UPC": "%", "StoreCode": parseInt(allStores[id]), "MinOnHand": 1, "Skip":num_skip };
                        // var postData = { "SKU": "%", "UPC": "%", "StoreCode": parseInt(allStores[id]), "Skip":num_skip };
                        var postData = { "SKU": allSkus[sku], "UPC": "%", "StoreCode": parseInt(allStores[id]), "Skip":num_skip };
                        log.debug({title: 'DEBUG: GF_MR_RICS_Inventory.js:rics_getInputData', details: 'get all values - postData: ' + JSON.stringify(postData)});
                        postData = JSON.stringify(postData);
                        var response = https.post({
                            url: getInventoryOnHand,
                            headers:header,
                            body: postData
                        });
                        if(response != undefined && response.hasOwnProperty('code') && response['code'] == 200){
                            current_attempt = 0;
                            var data = JSON.parse(response.body);
                            for(var key in data.Stores){
                                var currentStoreData = data.Stores[key];
                                if(currentStoreData.hasOwnProperty('Items')){
                                    for(var item in currentStoreData.Items){
                                        var obj = {};
                                        obj.StoreCode = currentStoreData.StoreCode;
                                        obj.StoreName = currentStoreData.StoreName;
                                        obj.Item = currentStoreData.Items[item];
                                        ricsInventory.push(obj);
                                    }
                                }
                            }
                            num_skip = data.ResultStatistics.EndRecord;
                        }else{
                            current_attempt += 1;
                        }
                        log.debug({title: 'DEBUG: GF_MR_RICS_Inventory.js:rics_getInputData', details: 'get all values - current_attempt: ' + current_attempt + ', response: ' + JSON.stringify(response)});
                    }while( current_attempt < max_attempts && ( response['code'] != 200 || ( data.hasOwnProperty('ResultStatistics') && data.ResultStatistics.EndRecord < data.ResultStatistics.TotalRecords ) ) );
                    
                    
                    
                    
                    
                    
                    // // ===================
                    // // get positive values
                    // do{
                    //     if(ricsInventory.length >= record_limit){ break; }
                    //     sleep(1000);
                    //     var postData = { "SKU": allSkus[sku], "UPC": "%", "StoreCode": parseInt(allStores[id]), "MinOnHand": 1, "Skip":num_skip };
                    //     log.debug({title: 'DEBUG: GF_MR_RICS_Inventory.js:rics_getInputData', details: 'get positive values - postData: ' + JSON.stringify(postData)});
                    //     postData = JSON.stringify(postData);
                    //     var response = https.post({
                    //         url: getInventoryOnHand,
                    //         headers:header,
                    //         body: postData
                    //     });
                    //     if(response['code'] != 200){
                    //         current_attempt += 1;
                    //     }else{
                    //         current_attempt = 0;
                    //         var data = JSON.parse(response.body);
                    //         for(var key in data.Stores){
                    //             var currentStoreData = data.Stores[key];
                    //             if(currentStoreData.hasOwnProperty('Items')){
                    //                 for(var item in currentStoreData.Items){
                    //                     var obj = {};
                    //                     obj.StoreCode = currentStoreData.StoreCode;
                    //                     obj.StoreName = currentStoreData.StoreName;
                    //                     obj.Item = currentStoreData.Items[item];
                    //                     ricsInventory.push(obj);
                    //                 }
                    //             }
                    //         }
                    //         num_skip = data.ResultStatistics.EndRecord;
                    //     }
                    //     log.debug({title: 'DEBUG: GF_MR_RICS_Inventory.js:rics_getInputData', details: 'get positive values - current_attempt: ' + current_attempt + ', response: ' + JSON.stringify(response)});
                    // }while( current_attempt < max_attempts && ( response['code'] != 200 || ( data.hasOwnProperty('ResultStatistics') && data.ResultStatistics.EndRecord < data.ResultStatistics.TotalRecords ) ) );
                    // current_attempt = 0;
                    // num_skip = 0;
                    // // ===================
                    // // get negative values
                    // do{
                    //     if(ricsInventory.length >= record_limit){ break; }
                    //     sleep(1000);
                    //     var postData = { "SKU": allSkus[sku], "UPC": "%", "StoreCode": parseInt(allStores[id]), "MaxOnHand": -1, "Skip":num_skip };
                    //     log.debug({title: 'DEBUG: GF_MR_RICS_Inventory.js:rics_getInputData', details: 'get positive values - postData: ' + JSON.stringify(postData)});
                    //     postData = JSON.stringify(postData);
                    //     var response = https.post({
                    //         url: getInventoryOnHand,
                    //         headers:header,
                    //         body: postData
                    //     });
                    //     if(response['code'] != 200){
                    //         current_attempt += 1;
                    //     }else{
                    //         current_attempt = 0;
                    //         var data = JSON.parse(response.body);
                    //         for(var key in data.Stores){
                    //             var currentStoreData = data.Stores[key];
                    //             if(currentStoreData.hasOwnProperty('Items')){
                    //                 for(var item in currentStoreData.Items){
                    //                     var obj = {};
                    //                     obj.StoreCode = currentStoreData.StoreCode;
                    //                     obj.StoreName = currentStoreData.StoreName;
                    //                     obj.Item = currentStoreData.Items[item];
                    //                     ricsInventory.push(obj);
                    //                 }
                    //             }
                    //         }
                    //         num_skip = data.ResultStatistics.EndRecord;
                    //     }
                    //     log.debug({title: 'DEBUG: GF_MR_RICS_Inventory.js:rics_getInputData', details: 'get positive values - current_attempt: ' + current_attempt + ', response: ' + JSON.stringify(response)});
                    // }while( current_attempt < max_attempts && ( response['code'] != 200 || ( data.hasOwnProperty('ResultStatistics') && data.ResultStatistics.EndRecord < data.ResultStatistics.TotalRecords ) ) );
                    if(ricsInventory.length >= record_limit){ break; }
                }
                if(ricsInventory.length >= record_limit){ break; }
            }

            ricsInventory = ricsInventory.filter(function (el) { return el != null; });
            log.debug({title: 'DEBUG: GF_MR_RICS_Inventory.js:rics_getInputData', details: 'ricsInventory: ' + JSON.stringify(ricsInventory)});

            return ricsInventory;
        }catch(ex) {
            log.error({title: 'Error: GF_MR_RICS_Inventory.js:rics_getInputData', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
            log.error({title: 'Error: GF_MR_RICS_Inventory.js:rics_getInputData', details: 'postData: ' + postData})
            // log.error({title: 'Error: GF_MR_RICS_Inventory.js:rics_getInputData', details: 'Error - num_skip:' + num_skip + ', data/response: ' + (data != null ? JSON.stringify(data) : JSON.stringify(response.body) ) });
            return ricsInventory;
        }
    }
    
    function rics_map(context){
        try{
            var data = JSON.parse(context.value);
            if(data == null){ return true; }
            log.audit({title: 'DEBUG: GF_MR_RICS_Inventory.js:rics_map', details: 'data: ' + JSON.stringify(data)});
            // log.debug({title: 'DEBUG: GF_MR_RICS_Inventory.js:rics_map', details: 'item count: ' + data.Items.length});
            // log.debug({title: 'DEBUG: GF_MR_RICS_Inventory.js:rics_map', details: 'store ext id: ' + data.StoreCode});
            // update pricing by SKU
            var arr_allSKU = [];
            var itemData = data.Item;
            // log.debug({title: 'DEBUG: GF_MR_RICS_Inventory.js:rics_map', details: 'itemData: ' + JSON.stringify(itemData)});
            if(typeof arr_allSKU[itemData.Sku] == 'undefined'){
                // log.debug({title: 'DEBUG: GF_MR_RICS_Inventory.js:rics_map', details: 'itemData.Sku: ' + itemData.Sku});
                // find the current inventory item
                var runsearch_pd_records = search.create({
                    type: "customrecord_rics_product_details",
                    filters: [
                        ["custrecord_rics_pd_sku","is",itemData.Sku],"AND",
                        ["isinactive","is","F"],"AND",
                        ["custrecord_rics_pd_avail_pos_on","onorbefore","today"]
                    ],
                    columns: [
                        search.createColumn({name: "custrecord_rics_pd_ns_item_id", label: "NS Item Record"}),
                        search.createColumn({
                           name: "type",
                           join: "CUSTRECORD_RICS_PD_NS_ITEM_ID"
                        }),
                        search.createColumn({
                           name: "matrix",
                           join: "CUSTRECORD_RICS_PD_NS_ITEM_ID"
                        })
                    ]
                }).run();
                var results = runsearch_pd_records.getRange({start: 0, end: 1000});
                if (results != null || results.length === 0){
                    arr_allSKU[itemData.Sku] = { "NS_ITEM_ID": 0 };
                }
                if(results != null && results.length === 1){
                    var id_ns_item = results[0].getValue('custrecord_rics_pd_ns_item_id');
                    var id_ns_item_recType = results[0].getValue({name: 'type', join: 'CUSTRECORD_RICS_PD_NS_ITEM_ID'});
                    id_ns_item_recType = (id_ns_item_recType.toUpperCase() == 'INVTPART') ? 'inventoryitem' : 'noninventoryitem';
                    var ns_item_matrix = results[0].getValue({name: 'matrix', join: 'CUSTRECORD_RICS_PD_NS_ITEM_ID'});
                    arr_allSKU[itemData.Sku] = {
                        "NS_ITEM_ID": id_ns_item,
                        "Cost": itemData.Cost,
                        "RetailPrice": itemData.RetailPrice,
                        "ActivePrice": itemData.ActivePrice,
                        "OnHand": itemData.OnHand,
                        "UPC": itemData.UPC
                    };
                    var update_parent_item = record.load({
                        type: id_ns_item_recType,
                        id: id_ns_item,
                        isDynamic: true
                    });
                    if(itemData.hasOwnProperty('Cost')){
                        updatePricing(itemData, update_parent_item, 'rics_map');
                    }
                    // don't update onhand until reduce stage
                    // if(id_ns_item_recType == 'inventoryitem' && itemData.hasOwnProperty('OnHand') && ns_item_matrix == false){ // only run on hand update for non-matrix inventory items
                    //     updateOnHand(data.StoreCode, itemData, update_parent_item, 'rics_map');
                    // }
                    var saved_ns_id = update_parent_item.save();
                    log.debug({title: 'DEBUG: GF_MR_RICS_Inventory.js:rics_reduce', details: 'saved_ns_id: ' + saved_ns_id});
                }
            }
            // update the RICS Product Details Record
            if(itemData.hasOwnProperty('Cost')){
                var pricing_obj = {
                    "Cost": itemData.Cost,
                    "RetailPrice": itemData.RetailPrice,
                    "ActivePrice": itemData.ActivePrice
                };
                var pd_id = find_pd_record(itemData.Sku);
                if(pd_id > 0){
                    var pd_update_id = record.submitFields({
                        type: 'customrecord_rics_product_details',
                        id: pd_id,
                        values: { custrecord_rics_pd_pricing: JSON.stringify(pricing_obj), custrecord_rics_pd_ns_pricing_update: new Date() },
                        options: {
                            enableSourcing: false,
                            ignoreMandatoryFields : true
                        }
                    });
                }
            }

            // pass data to reduce for setting on hand
            context.write({ key: data.StoreCode, value: data });
        }catch(ex) {
            log.error({title: 'Error: GF_MR_RICS_Inventory.js:rics_map', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }
    
    function rics_reduce(context){
        try{
            // update pricing and on hand quantity on all of the items by UPC and storecode
            var data = JSON.parse(context.values[0]);
            if(data == null){ return true; }
            log.audit({title: 'DEBUG: GF_MR_RICS_Inventory.js:rics_reduce', details: 'data: ' + JSON.stringify(data)});
            // var store_ns_id = get_store_ns_id(data.StoreCode);
            var itemData = data.Item;
            // var runsearch_pd_records = search.create({
            //     type: "item",
            //     filters: [
            //         ["type","anyof","InvtPart","NonInvtPart"], "AND",
            //         ["subsidiary","anyof","3","4","2","7"], "AND",
            //         ["upccode","is",itemData.UPC], "AND", 
            //         ["isinactive","is","F"]
            //     ],
            //     columns: [
            //         search.createColumn({name: "internalid"}),
            //         search.createColumn({name: "type"})
            //         ]
            // }).run();
            // var results = runsearch_pd_records.getRange({start: 0, end: 1000});
            // if(results != null && results.length === 1){
            //     var id_ns_item = results[0].id;
            //     var str_rec_type = results[0].getValue('type');
            //     // log.debug({title: 'DEBUG: GF_MR_RICS_Inventory.js:rics_reduce', details: 'str_rec_type: ' + str_rec_type});
            //     str_rec_type = (str_rec_type.toUpperCase() == 'INVTPART') ? 'inventoryitem' : 'noninventoryitem';
            //     var update_child_item = record.load({
            //         type: str_rec_type,
            //         id: id_ns_item,
            //         isDynamic: true
            //     });
            //     // process child record update
            //     if(itemData.hasOwnProperty('Cost')){
            //         updatePricing(itemData, update_child_item, 'rics_reduce');
            //     }
            //     if(str_rec_type == 'inventoryitem' && itemData.hasOwnProperty('OnHand')){ // can run onhand update for child items
            //         // updateOnHand(data.StoreCode, itemData, update_child_item, 'rics_reduce');
            //     }

            //     var saved_ns_id = update_child_item.save();
            //     log.debug({title: 'DEBUG: GF_MR_RICS_Inventory.js:rics_reduce', details: 'saved_ns_id: ' + saved_ns_id});
            // }else{
            //     if(results != null && results.length > 1){
            //         log.debug({title: 'DEBUG: GF_MR_RICS_Inventory.js:rics_reduce', details: 'more than one result on upc: ' + JSON.stringify(results)});
            //     }
            // }

            // update the RICS Product Item Record
            var pi_id = find_pi_record(itemData.UPC);
            if(pi_id > 0){
                var location_obj = {
                    "store_ns_id": get_store_ns_id(data.StoreCode),
                    "StoreCode": data.StoreCode,
                    "OnHand": itemData.OnHand
                };
                var update_RICS_Product_Item = record.load({
                    type: 'customrecord_rics_product_item',
                    id: pi_id,
                    isDynamic: true
                });
                var onhand_str = update_RICS_Product_Item.getValue('custrecord_rics_pi_onhand');
                var onhand_json = (onhand_str != null && onhand_str != '') ? JSON.parse(onhand_str) : '';
                // add when empty
                if(onhand_json == ''){
                    var onhand_obj = { "locations":  [location_obj] };
                    var pi_update_id = record.submitFields({
                        type: 'customrecord_rics_product_item',
                        id: pi_id,
                        values: { custrecord_rics_pi_onhand: JSON.stringify(onhand_obj), custrecord_rics_pi_ns_onhand_update: new Date() },
                        options: {
                            enableSourcing: false,
                            ignoreMandatoryFields : true
                        }
                    });
                }else{
                    var flg_LocFound = false;
                    for(var loc in onhand_json.locations){
                        var loc_onhand = onhand_json.locations[loc];
                        if(loc_onhand.StoreCode == data.StoreCode){
                            onhand_json.locations[loc].OnHand = itemData.OnHand;
                            flg_LocFound = true;
                            break;
                        }
                    }
                    if(flg_LocFound == false){
                        onhand_json.locations.push(location_obj);
                    }
                    var onhand_obj = onhand_json;
                    var pi_update_id = record.submitFields({
                        type: 'customrecord_rics_product_item',
                        id: pi_id,
                        values: { custrecord_rics_pi_onhand: JSON.stringify(onhand_obj), custrecord_rics_pi_ns_onhand_update: new Date() },
                        options: {
                            enableSourcing: false,
                            ignoreMandatoryFields : true
                        }
                    });
                }

            }

        }catch(ex) {
            log.error({title: 'Error: GF_MR_RICS_Inventory.js:rics_reduce', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }
   
    function rics_summarize(summary) {
        try{
            log.audit('Usage Consumed', summary.usage);
            log.audit('Concurrency Number ', summary.concurrency);
            log.audit('Number of Yields', summary.yields);
            log.audit({title: 'Process Completed'});
        }catch(ex) {
            log.error({title: 'Error: GF_MR_RICS_Inventory.js:rics_summarize', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }
   
    return {
        getInputData: rics_getInputData,
        map: rics_map,
        reduce: rics_reduce,
        summarize: rics_summarize
    };

    // =========================== extra functions
    function errText(_e){
        // usage : nlapiLogExecution('ERROR', 'Error: function', 'Error ' + ex.toString() + ' : ' + ex.stack);
        var txt = '';
        if(_e instanceof nlobjError){
             //this is netsuite specific error
             txt = 'NLAPI Error: ' + _e.getCode() + ' :: ' + _e.getDetails() + ' :: ' + _e.getStackTrace().join(', ');
        }else{
             //this is generic javascript error
             txt = 'JavaScript/Other Error: ' + _e.toString();
             txt += (typeof _e.stack != 'undefined') ? ' : ' + _e.stack : '';
        }
        return txt;
    }

    function sleep(_ms){
        var start = new Date().getTime();
        var end = new Date().getTime() + _ms;
        do{
            start = new Date().getTime();
        }while((end - start) >= 0);
        return true;
    }

    function updatePricing(_itemData, _rec, _strCalledFrom){
        log.debug({title: 'DEBUG: GF_MR_RICS_Inventory.js:updatePricing', details: '_rec.id: ' + _rec.id + ', _strCalledFrom' + _strCalledFrom + ', _itemData: ' + JSON.stringify(_itemData)});
            if(_itemData.hasOwnProperty('Cost')){ _rec.setValue('cost',_itemData.Cost); }
        if(_itemData.hasOwnProperty('RetailPrice')){ _rec.setValue('custitem_gf_msrp',_itemData.RetailPrice); }
        // set pricing line items
        var currentPrice1Count = _rec.getLineCount({ 'sublistId': 'price1' });
        for(var i = 0; currentPrice1Count > 0 && i < currentPrice1Count; i++){
            _rec.selectLine({ sublistId: 'price1', line: i });
            var pricelevelname = _rec.getCurrentSublistValue({
                sublistId: 'price1',
                fieldId: 'pricelevelname'
            });
            if(pricelevelname == 'Base Price (MSRP)' && _itemData.RetailPrice > 0){
                log.debug({title: 'DEBUG: GF_MR_RICS_Inventory.js:updatePricing', details: 'pricelevelname: ' + pricelevelname + ', _itemData.RetailPrice: ' + _itemData.RetailPrice});
                _rec.setCurrentSublistValue({
                    sublistId: 'price1',
                    fieldId: 'price_1_',
                    value: _itemData.RetailPrice,
                    ignoreFieldChange: true
                });
                _rec.commitLine({sublistId: 'price1'});
            }
            if(pricelevelname == 'Sale Price' && _itemData.ActivePrice > 0){
                log.debug({title: 'DEBUG: GF_MR_RICS_Inventory.js:updatePricing', details: 'pricelevelname: ' + pricelevelname + ', _itemData.ActivePrice: ' + _itemData.ActivePrice});
                _rec.setCurrentSublistValue({
                    sublistId: 'price1',
                    fieldId: 'price_1_',
                    value: _itemData.ActivePrice,
                    ignoreFieldChange: true
                });
                _rec.commitLine({sublistId: 'price1'});
            }
        }
    }
    
    function updateOnHand(_storeCode, _itemData, _rec, _strCalledFrom){
        var store_ns_data = get_store_ns_id(_storeCode);
        // find the store line to update
        // if it doesn't exist create a new line else select the found line
        log.debug({title: 'DEBUG: GF_MR_RICS_Inventory.js:updateOnHand', details: '_rec.id: ' + _rec.id + ', store_ns_data.id: ' + store_ns_data.id + ', _strCalledFrom' + _strCalledFrom + ', _itemData: ' + JSON.stringify(_itemData)});
        var currentLocationLineCount = _rec.getLineCount({ 'sublistId': 'locations' });
        log.debug({title: 'DEBUG: GF_MR_RICS_Inventory.js:updateOnHand', details: 'currentLocationLineCount: ' + currentLocationLineCount});
        var lineFound = false;
        if(currentLocationLineCount > 0){
            for(var i = 0; lineFound == false && i < currentLocationLineCount; i++){
                _rec.selectLine({ sublistId: 'locations', line: i });
                var location_id = _rec.getCurrentSublistValue({
                    sublistId: 'locations',
                    fieldId: 'locationid'
                });
                // log.debug({title: 'DEBUG: GF_MR_RICS_Inventory.js:updateOnHand', details: 'location_id: ' + location_id});
                if(location_id != null && location_id == store_ns_data.id){
                    log.debug({title: 'DEBUG: GF_MR_RICS_Inventory.js:updateOnHand', details: 'location_id: ' + location_id + ', _itemData.OnHand: ' + _itemData.OnHand});
                    lineFound = true;
                    var current_onhand = _rec.getCurrentSublistValue({
                        sublistId: 'locations',
                        fieldId: 'quantityonhand'
                    });
                    current_onhand = (current_onhand == null || current_onhand == '') ? 0 : current_onhand;
                    if(current_onhand != _itemData.OnHand){
                        var num_OnHandDifference = getOnHandDifference(current_onhand, _itemData.OnHand);
                        if(num_OnHandDifference != 0){
                            // create "inventoryadjustment" record
                            var inventory_adjustment_rec = record.create({
                                type: 'inventoryadjustment'
                            });
                            inventory_adjustment_rec.setValue('subsidiary', store_ns_data.subsidiary);
                            inventory_adjustment_rec.setValue('account', 1110); // 999999999 Inventory Adjustment Account
                            inventory_adjustment_rec.setSublistValue({
                                sublistId: 'inventory',
                                fieldId: 'item',
                                line: 0,
                                value: _rec.id
                            });
                            inventory_adjustment_rec.setSublistValue({
                                sublistId: 'inventory',
                                fieldId: 'location',
                                line: 0,
                                value: store_ns_data.id
                            });
                            inventory_adjustment_rec.setSublistValue({
                                sublistId: 'inventory',
                                fieldId: 'adjustqtyby',
                                line: 0,
                                value: num_OnHandDifference
                            });
                            inventory_adjustment_rec.save();
                        }
                    }
                    
                    log.debug({title: 'DEBUG: GF_MR_RICS_Inventory.js:updateOnHand', details: '_rec.id: ' + _rec.id + ', store_ns_data.id: ' + store_ns_data.id + ', _itemData.OnHand: ' + _itemData.OnHand});
                }
            }
        }
    }

    function get_store_ns_id(_ext_id){
        var store_data_obj = {};

        var runsearch_storelocations_with_ext_id = search.create({
            type: "location",
            filters: [ ["locationtype","anyof","1"], "AND", ["externalid","anyof",_ext_id] ],
            columns: [ "subsidiary" ]
        }).run();
        var results = runsearch_storelocations_with_ext_id.getRange({start: 0, end: 1000});
        if(results != null && results.length === 1){
            store_data_obj.id = results[0].id;
            store_data_obj.subsidiary = results[0].getValue('subsidiary');
        }

        return store_data_obj;
    }

    function find_pd_record(_sku){
        var result = 0;
        // log.debug({title: 'GF_MR_RICS_Inventory.js:find_pd_record', details: '_sku: ' + _sku});
        // find the Product Details Record
        var pd_SearchResults = search.create({
             type: "customrecord_rics_product_details",
             filters: [
                 ["isinactive","is","F"], "AND",
                 ["custrecord_rics_pd_sku","is",_sku]
             ],
             columns: [ ]
        }).run();
        var pd_SearchResultSet = pd_SearchResults.getRange({start: 0, end: 1000});
        if (pd_SearchResultSet != null && pd_SearchResultSet.length > 0){
            result = pd_SearchResultSet[0].id;
        }
        // log.debug({title: 'GF_MR_RICS_Inventory.js:find_pd_record', details: 'result: ' + result});
        return result;
    }
    
    function find_pi_record(_upc){
        var result = 0;
        // find the Product Details Record
        var pi_SearchResults = search.create({
            type: "customrecord_rics_product_item",
            filters: [
                ["isinactive","is","F"], "AND",
                ["custrecord_rics_pi_upc","is",_upc]
            ],
            columns: [ ]
        }).run();
        var pi_SearchResultSet = pi_SearchResults.getRange({start: 0, end: 1000});
        if (pi_SearchResultSet != null && pi_SearchResultSet.length > 0){
            result = pi_SearchResultSet[0].id;
        }
    
        return result;
    }

    function getOnHandDifference(_current_onhand, _rics_onhand){
        if( (_current_onhand > 0 && _rics_onhand > 0) || (_current_onhand < 0 && _rics_onhand < 0) ){
            // both values are positive or both values are negative
            if(_current_onhand > _rics_onhand){ return (_current_onhand - _rics_onhand)*-1; } // take away from current inv
            if(_current_onhand < _rics_onhand){ return (_rics_onhand - _current_onhand); } // add to current inv
        }else{
            if(_current_onhand > _rics_onhand){ return (Math.abs(_current_onhand) + Math.abs(_rics_onhand))*-1; } // take away from current inv
            if(_current_onhand < _rics_onhand){ return (Math.abs(_current_onhand) + Math.abs(_rics_onhand)); } // add to current inv
        }
        return 0;
    }

 });


// log.debug({title: 'DEBUG: GF_MR_RICS_Inventory.js:rics_map', details: 'store id: ' + data.StoreCode + ', totalOnHandCount: ' + totalOnHandCount});
// set pricing for all parent items with the matching SKU
// for(var invItem in arr_allSKU){
//     var currentItemData = arr_allSKU[invItem];
//     if(currentItemData.NS_ITEM_ID == 0){
//         continue;
//     }
//     // log.debug({title: 'DEBUG: GF_MR_RICS_Inventory.js:rics_map', details: 'currentItemData: ' + JSON.stringify(currentItemData)});
//     var update_parent_item = record.load({
//         type: 'inventoryitem',
//         id: currentItemData.NS_ITEM_ID,
//         isDynamic: true
//     });
//     updatePricing(data, currentItemData, currentItemData.NS_ITEM_ID, update_parent_item);
//     // if(currentItemData.cost > 0){ update_parent_item.setValue('cost',currentItemData.cost); }
//     // if(currentItemData.msrp > 0){ update_parent_item.setValue('custitem_gf_msrp',currentItemData.msrp); }
//     // // set pricing line items
//     // var currentPrice1Count = update_parent_item.getLineCount({ 'sublistId': 'price1' });
//     // for(var i = 0; currentPrice1Count > 0 && i < currentPrice1Count; i++){
//     //     update_parent_item.selectLine({ sublistId: 'price1', line: i });
//     //     var pricelevelname = update_parent_item.getCurrentSublistValue({
//     //         sublistId: 'price1',
//     //         fieldId: 'pricelevelname'
//     //     });
//     //     if(pricelevelname == 'Base Price (MSRP)' && currentItemData.msrp > 0){
//     //         update_parent_item.setCurrentSublistValue({
//     //             sublistId: 'price1',
//     //             fieldId: 'price_1_',
//     //             value: currentItemData.msrp,
//     //             ignoreFieldChange: true
//     //         });
//     //         update_parent_item.commitLine({sublistId: 'price1'});
//     //     }
//     //     if(pricelevelname == 'Sale Price' && currentItemData.saleprice > 0){
//     //         update_parent_item.setCurrentSublistValue({
//     //             sublistId: 'price1',
//     //             fieldId: 'price_1_',
//     //             value: currentItemData.saleprice,
//     //             ignoreFieldChange: true
//     //         });
//     //         update_parent_item.commitLine({sublistId: 'price1'});
//     //     }
//     // }
//     update_parent_item.save();
// }