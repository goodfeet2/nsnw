/**
 *@NApiVersion 2.1
 *@NScriptType MapReduceScript
 *
Script Name:   GF_MR_RICS_Daily_Deposit.js
Author:        Mark Robinson
 */

define(['N/log', 'N/search', 'N/record'], function(log, search, record) {

    /**
     * In this get input function we match location records and their related account specific data to the transaction records that were created by the RICS Cash Sale script
     * @param {*} context  - this variable is not utilized in this function
     * @returns - an array containing the full responses from the custom record search
     */
    function rics_getInputData(context){
        try {
            // create arrays to populate
            var arr_deposits = [];
            var arr_location_ids = [];
            var arr_location_data = [];
            var arr_rics_sales_tenders = [];

            // build location data array and id list
            var locationSearchObj = search.create({
                type: "location",
                filters: [
                    ["locationtype","anyof","1"], "AND",
                    ["custrecord_gf_auto_deposit_account","noneof","@NONE@"], "AND",
                    ["isinactive","is","F"], "AND",
                    ["externalid","noneof","@NONE@"]
                 ],
                 columns: [
                    "namenohierarchy",
                    "externalid",
                    search.createColumn({
                        name: "custrecord_gf_auto_deposit_account",
                        sort: search.Sort.ASC
                    }),
                    search.createColumn({
                        name: "subsidiary",
                        join: "CUSTRECORD_GF_AUTO_DEPOSIT_ACCOUNT"
                    })
                 ]
            });
            locationSearchObj.run().each(function(result){
                // .run().each has a limit of 4,000 results
                var loc_data = {};
                loc_data.id = result.id;
                loc_data.name = result.getValue('namenohierarchy');
                loc_data.ext_id = result.getValue('externalid');
                loc_data.str_deposit_account_id = result.getText('custrecord_gf_auto_deposit_account');
                loc_data.deposit_account_id = result.getValue('custrecord_gf_auto_deposit_account');
                loc_data.subsidiary_id = result.getValue({ name: "subsidiary", join: "CUSTRECORD_GF_AUTO_DEPOSIT_ACCOUNT" });
                arr_location_data.push(loc_data);
                arr_location_ids.push(result.id);
                return true;
            });

            // get RICS Sales Tenders Records specific to the location ids
            var arr_rics_sales_tenders = [];
            // build Tenders data
            var customrecord_rics_sales_tendersSearch = search.create({
                type: "customrecord_rics_sales_tenders",
                filters: [
                    ["custrecord_rics_tenders_transaction_rec.mainline","is","T"], "AND",
                    ["created","on","yesterday"], "AND",
                    ["formulanumeric: CASE WHEN NVL2({custrecord_rics_tenders_transaction_rec.paymentmethod}, 1, 0) != 0 AND {custrecord_rics_tenders_transaction_rec.paymentmethod} != 'House Charge' AND {custrecord_rics_tenders_transaction_rec.paymentmethod} != 'Cash' AND {custrecord_rics_tenders_transaction_rec.paymentmethod} != 'Check' AND {custrecord_rics_tenders_transaction_rec.paymentmethod} != 'Third Party' THEN 1 ELSE 0 END","greaterthan","0"], "AND",
                    ["custrecord_rics_tenders_transaction_rec.account","anyof","116"], "AND", // ONLY 1770 Undeposited Funds
                    ["custrecord_rics_tenders_transaction_rec.location","anyof",arr_location_ids], "AND",
                    ["formulanumeric: CASE WHEN {custrecord_rics_tender_parent.custrecord_rics_st_receipt_total} <> {custrecord_rics_tenders_transaction_rec.amount} THEN 1 ELSE 0 END","equalto","0"], "AND", // ONLY when the amount is correct
                    [
                      [
                        ["custrecord_rics_tenders_transaction_rec.type","anyof","CashSale"],"AND",
                        ["custrecord_rics_tenders_transaction_rec.status","noneof","CashSale:C","CashSale:A"] // None of Cash Sale:Deposited, Cash Sale:Unapproved Payment
                      ],"OR",
                      ["custrecord_rics_tenders_transaction_rec.type","anyof","CashRfnd"]
                    ]
                ],
                columns: [
                    "custrecord_rics_tender_parent",
                    search.createColumn({
                        name: "custrecord_rics_st_ticketnumber",
                        join: "CUSTRECORD_RICS_TENDER_PARENT"
                    }),
                    search.createColumn({
                       name: "amount",
                       join: "CUSTRECORD_RICS_TENDERS_TRANSACTION_REC",
                       sort: search.Sort.DESC
                    }),
                    search.createColumn({
                        name: "paymentmethod",
                        join: "CUSTRECORD_RICS_TENDERS_TRANSACTION_REC",
                        sort: search.Sort.ASC
                    }),
                    "custrecord_rics_tender_amounttendered",
                    "custrecord_rics_tender_description",
                    "custrecord_rics_tenders_transaction_rec",
                    search.createColumn({
                        name: "locationnohierarchy",
                        join: "CUSTRECORD_RICS_TENDERS_TRANSACTION_REC"
                    }),
                    search.createColumn({
                        name: "subsidiarynohierarchy",
                        join: "CUSTRECORD_RICS_TENDERS_TRANSACTION_REC"
                    }),
                    search.createColumn({
                        name: "created",
                        sort: search.Sort.DESC
                    })
                ]
            });
            customrecord_rics_sales_tendersSearch.run().each(function(result){
                // .run().each has a limit of 4,000 results
                var rics_st_data = {};
                rics_st_data.id = result.id;
                rics_st_data.sales_ticket_id = result.getValue('custrecord_rics_tender_parent');
                rics_st_data.sales_ticket_number = result.getValue({ name: "custrecord_rics_st_ticketnumber", join: "CUSTRECORD_RICS_TENDER_PARENT" });
                rics_st_data.str_paymentmethod = result.getText({ name: "paymentmethod", join: "CUSTRECORD_RICS_TENDERS_TRANSACTION_REC" });
                rics_st_data.tenderamount = parseFloat(result.getValue('custrecord_rics_tender_amounttendered')).toFixed(2);
                rics_st_data.amount = parseFloat(result.getValue({ name: "amount", join: "CUSTRECORD_RICS_TENDERS_TRANSACTION_REC" })).toFixed(2);
                rics_st_data.desciption = result.getValue('custrecord_rics_tender_description');
                rics_st_data.paymentmethod = getPaymentMethod(rics_st_data.desciption); // result.getValue({ name: "paymentmethod", join: "CUSTRECORD_RICS_TENDERS_TRANSACTION_REC" });
                rics_st_data.transaction_ids = result.getValue('custrecord_rics_tenders_transaction_rec');
                rics_st_data.transaction_names = result.getText('custrecord_rics_tenders_transaction_rec');
                rics_st_data.location_id = result.getValue({ name: "locationnohierarchy", join: "CUSTRECORD_RICS_TENDERS_TRANSACTION_REC" });
                rics_st_data.location_data = arr_location_data.find(function(arr_location_data){ return arr_location_data.id == rics_st_data.location_id });
                arr_rics_sales_tenders.push(rics_st_data);
                return true;
            });

            var t_ids_applied = [];
            for(var tender in arr_rics_sales_tenders){
                var t_data = arr_rics_sales_tenders[tender];
                if(t_data.location_data != undefined && t_data.location_data.str_deposit_account_id != undefined && t_data.str_paymentmethod != undefined && t_ids_applied.indexOf(t_data.id) == -1){
                    t_ids_applied.push(t_data.id);
                    var str_account_location_and_method = t_data.location_data.str_deposit_account_id + ' - ' + t_data.location_data.ext_id + '-' + t_data.location_data.name + ' - ' + t_data.paymentmethod;
                    var index = arr_deposits.map(function(e) { return e.name; }).indexOf(str_account_location_and_method);
                    if(index > -1){
                        arr_deposits[index].data.push(t_data);
                    }else{
                        arr_deposits.push({ name: str_account_location_and_method, data: [t_data] });
                    }
                }else{
                    // if there's no account or payment method we don't process it - if this issue shows up a lot it should be its own report
                    log.error({title: 'GF_MR_RICS_Daily_Deposit.js:rics_getInputData-building_array', details: 't_data: ' + JSON.stringify(t_data)});
                    continue;
                }
            }

            return arr_deposits;
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_Daily_Deposit.js:rics_getInputData', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this mapping function we pass through the data that will be  processed in the reduce phase
     * @param {*} context - Object - This should be the RICS NonSellable Batch custom record
     * @returns void (note: the const context variable is impacted and used in the reduce function)
     */
    function rics_map(context){
        try{
            // simple pass through
            var map_data = JSON.parse(context.value);
            log.audit({title: 'GF_MR_RICS_Daily_Deposit.js:rics_map', details: 'map_data: ' + JSON.stringify(map_data)});
			if (map_data == null) {
				return true;
			}

			context.write({ key: context.key, value: map_data }); // pass through directly to reduce

			return true;
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_Daily_Deposit.js:rics_map', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this reduce function we create a deposit record and add all of the transactions that were found in our search above grouped by Account and Payment Type
     * @param {*} context - Object - This is the object that was built in the mapping function, it should conatin all the data we need for processing
     * @returns - void
     */
    function rics_reduce(context){
        try{
            for(var val in context.values){
                var reduce_data = JSON.parse(context.values[val]);
                log.audit({title: 'GF_MR_RICS_Daily_Deposit.js:rics_reduce-start', details: 'context: ' + JSON.stringify(reduce_data)});
                /** data model example
                {
                    "id": "34302",
                    "sales_ticket_id": "39702",
                    "sales_ticket_number": "2430",
                    "str_paymentmethod": "Cash",
                    "paymentmethod": "1",
                    "tenderamount": "40.00",
                    "amount": "140.00",
                    "desciption": "4- Visa",
                    "transaction_ids": "1356919",
                    "location_id": "3",
                    "location_data": {
                        "id": "3",
                        "name": "Salem",
                        "ext_id": "413",
                        "str_deposit_account_id": "1005 GFOR Chase Checking 2067",
                        "deposit_account_id": "226",
                        "subsidiary_id": "2"
                    }
                }
                 */
                var acct = reduce_data.data[0].location_data.deposit_account_id;
                var deposit_record = record.create({
                    type: record.Type.DEPOSIT,
                    defaultValues: {
                        account: acct,
                        disablepaymentfilters: true
                    }
                });
                deposit_record.setValue({ fieldId: 'account', value: acct });
                deposit_record.setValue({ fieldId: 'subsidiary', value: reduce_data.data[0].location_data.subsidiary_id });
                log.audit({title: 'GF_MR_RICS_Daily_Deposit.js:rics_reduce-acct', details: 'acct#: ' + acct});
                deposit_record.setValue({ fieldId: 'exchangerate', value: 1 });
                deposit_record.setValue({ fieldId: 'memo', value: reduce_data.name });
                var payment_line_count = deposit_record.getLineCount({ sublistId: 'payment' });
                var total_amount = 0.00;
                var arr_tender_ids = [];
                var arr_amounts = [];
                // set all transactions
                for(var dep in reduce_data.data){
                    var arr_transaction_ids = reduce_data.data[dep].transaction_ids.split(',');
                    var arr_transaction_names = reduce_data.data[dep].transaction_names.split(',');
                    for(var tr_id in arr_transaction_ids){
                        // only allow a tender to be processed once
                        var amount = parseFloat(reduce_data.data[dep].amount);
                        arr_amounts.push(amount);
                        var transaction_id = arr_transaction_ids[tr_id];
                        var flg_doc_match_trans = false;
                        log.debug({title: 'GF_MR_RICS_Daily_Deposit.js:rics_reduce-acct-line-check', details: 'transaction_name: ' + arr_transaction_names[tr_id] + ', transaction_id: ' + transaction_id});
                        if(arr_tender_ids.indexOf(transaction_id) == -1){
                            for(var payment_line_number = 0; !flg_doc_match_trans && payment_line_number < payment_line_count; payment_line_number++){
                                log.debug({title: 'GF_MR_RICS_Daily_Deposit.js:rics_reduce-acct-lines', details: 'payment_line_number: ' + payment_line_number + ', transaction_id: ' + transaction_id + ', amount: ' + amount + ', memo: ' + reduce_data.data[dep].desciption});
                                //NetSuite edit - match the transaction number, don't set the transaction number
                                var docId = deposit_record.getSublistValue({
                                    sublistId: 'payment',
                                    fieldId: 'id',
                                    line: payment_line_number
                                });
                                if(docId == transaction_id){
                                    flg_doc_match_trans = true;
                                    // if((amount > 0.00) || (amount < 0.00 && Math.abs(amount) < total_amount)){
                                        deposit_record.setSublistValue({
                                            sublistId: 'payment',
                                            fieldId: 'deposit',
                                            line: payment_line_number,
                                            value: true
                                        });
                                        deposit_record.setSublistValue({
                                            sublistId: 'payment',
                                            fieldId: 'paymentamount',
                                            line: payment_line_number,
                                            value: amount
                                        });
                                        deposit_record.setSublistValue({
                                            sublistId: 'payment',
                                            fieldId: 'memo',
                                            line: payment_line_number,
                                            value: reduce_data.data[dep].desciption
                                        });
                                        arr_tender_ids.push(transaction_id);
                                        total_amount += amount;
                                    // }else{ // ((amount > 0.00) || (amount < 0.00 && Math.abs(amount) < total_amount)){
                                        // transaction was found but the amount was off
                                        // log.error({title: 'GF_MR_RICS_Daily_Deposit.js:rics_reduce-docId found but amount is off', details: 'payment_line_number: ' + payment_line_number + ', transaction_id: ' + transaction_id + ', amount: ' + amount + ', total_amount: ' + total_amount});
                                    // }
                                } // (docId == transaction_id){
                            } // payment line loop
                            if(!flg_doc_match_trans){
                                // no payment found log the error
                                log.error({title: 'GF_MR_RICS_Daily_Deposit.js:rics_reduce-flg_doc_match_trans=false', details: 'deposit name: ' + reduce_data.name + ', transaction_id: ' + transaction_id + ', amount: ' + amount + ', memo: ' + reduce_data.data[dep].desciption});
                            }
                        } // (arr_tender_ids.indexOf(transaction_id) == -1){
                    }
                }
                if(arr_tender_ids.length > 0){
                    log.audit({title: 'GF_MR_RICS_Daily_Deposit.js:rics_reduce-total_amount', details: 'deposit name: ' + reduce_data.name + ', total_amount: ' + total_amount.toFixed(2) + ', tender count: ' + arr_tender_ids.length });
                    if(parseFloat(total_amount).toFixed(2) > 0.00){
                        try{
                            var dep_rec_id = deposit_record.save();
                        }catch(ex){
                            log.error({title: 'GF_MR_RICS_Daily_Deposit.js:rics_reduce-rec save failed', details: 'Deposit name: ' + reduce_data.name + ', Error ' + ex.toString() + ' : ' + ex.stack});
                        }
                        if(dep_rec_id > 0){
                            var created_rec = record.load({ type: record.Type.DEPOSIT, id: dep_rec_id });
                            var json_created_rec = created_rec.toJSON();
                            log.audit({title: 'GF_MR_RICS_Daily_Deposit.js:rics_reduce-created_rec', details: 'created_rec: ' + JSON.stringify(json_created_rec) });
                            var obj_log_data = {};
                            obj_log_data.cashback_total = json_created_rec.fields.cashback_total;
                            obj_log_data.origtotal = json_created_rec.fields.origtotal;
                            obj_log_data.other_total = json_created_rec.fields.other_total;
                            obj_log_data.payment_total = json_created_rec.fields.payment_total;
                            obj_log_data.total = json_created_rec.fields.total;
                            obj_log_data.lines = [];
                            for(var line in json_created_rec.sublists.payment){
                                var line_data = json_created_rec.sublists.payment[line];
                                if(arr_tender_ids.indexOf(line_data.id) == -1){ continue; }
                                obj_log_data_line = {};
                                obj_log_data_line.docnumber = line_data.docnumber;
                                obj_log_data_line.paymentamount = line_data.paymentamount;
                                obj_log_data_line.id = line_data.id;
                                obj_log_data_line.lineid = line_data.lineid;
                                obj_log_data_line.transactionamount = line_data.transactionamount;
                                obj_log_data_line.deposit = line_data.deposit;
                                (obj_log_data.lines).push(obj_log_data_line);
                            }
                            log.audit({title: 'GF_MR_RICS_Daily_Deposit.js:rics_reduce-created_rec', details: 'created_rec info: ' + JSON.stringify(obj_log_data) });
                            record.delete({ type: record.Type.DEPOSIT, id: dep_rec_id });
                        }
                    }
                }else{
                    log.error({title: 'GF_MR_RICS_Daily_Deposit.js:rics_reduce- no lines added', details: 'acct: ' + acct + ', reduce_data: ' + JSON.stringify(reduce_data)});
                }
            }
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_Daily_Deposit.js:rics_reduce', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
            log.error({title: 'GF_MR_RICS_Daily_Deposit.js:rics_reduce', details: 'Name: ' + reduce_data.name});
        }
    }

    /**
     * This summarize function is used to report important aspects about how the script performed
     * @param {*} summary - this object contains data regarding script performance.
     */
    function rics_summarize(summary) {
        try{
            log.audit('Usage Consumed', summary.usage);
            log.audit('Concurrency Number ', summary.concurrency);
            log.audit('Number of Yields', summary.yields);
            log.audit({title: 'Process Completed'});
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_Daily_Deposit.js:rics_summarize', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    return {
        getInputData: rics_getInputData,
        map: rics_map,
        reduce: rics_reduce,
        summarize: rics_summarize
    };

    // =========================== extra functions
    /**
     * This function gets the payment method based on the tender description
     * @param {string} _desciption - expects tender description
     * @returns {string} - simple payment method string
     */
    function getPaymentMethod(_desciption){
        var result = 'NA';
        try{
            switch(_desciption){
                case '5- AMX': // 'RICSPay'
                case '3- Mastercard':
                case '4- Visa':
                case '6- Discover':
                    result = 'RICSPay';
                    break;
                case '14- Offline Card': // Offline Card
                    result = 'Offline Card';
                    break;
                case '9- CareCredit': // Care Credit
                    result = 'Care Credit';
                    break;
                case '1- Cash': // Cash
                    result = 'Cash';
                    break;
                case '2- Check': // Check
                    result = 'Check';
                    break;
                case 'Gift Card External': // Gift Card
                    result = 'Gift Card';
                    break;
                case '11- House Charge': // House Charge
                    result = 'House Charge';
                    break;
                default:
                    log.error({title: 'GF_MR_RICS_Daily_Deposit.js:getPaymentMethod', details: 'NOT FOUND - _desciption: ' + _desciption});
            }
        }catch(ex){
            log.error({title: 'GF_MR_RICS_Daily_Deposit.js:getPaymentMethod', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
        return result;
    }

    /**
     * This function will find the correct values in an array that add up to the sum, if there is a combination that will add up to the sum
     * @param {array} numbers - array of numbers to filter through
     * @param {number} sum - value that the numbers should add up to
     * @returns {array} - these are the numbers that add correctly to the sum
     */
    function getSum(numbers, sum) {
        function iter(index, right, left) {
            if(!left){
                return result.push(right);
            }else{
                left = parseFloat(left).toFixed(2);
            }
            if(left < 0 || index >= numbers.length){ return; }
            iter(index + 1, [...right, numbers[index]], left - numbers[index]);
            iter(index + 1, right, left);
        }

        var result = [];
        iter(0, [], sum);
        return (result != undefined && result[0] != undefined && result[0].length > 0 ? result[0] : [] );
    }

});