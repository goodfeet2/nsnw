/**
 *@NApiVersion 2.1
 *@NScriptType MapReduceScript
 *
Script Name:   GF_MR_RICS_VRMA.js
Author:        Mark Robinson
 */

define(['N/log', 'N/search', 'N/record', 'N/format', 'N/runtime', '/SuiteScripts/GF_LIB/gf_nsrics'], function(log, search, record, format, runtime, gf_nsrics) {

    /**
     * In this Get Input function we're using a search to get all of the RICS NonSellable Batch custom records that have been completed.
     * We load these records and their child records into an array that is then passed to the mapping function for processing.
     * @param {*} context  - this variable is not utilized in this function
     * @returns - an array containing the full responses from the custom record search
     */
    function rics_getInputData(context){
        var arr_data = [];
        try {
            var currentScript = runtime.getCurrentScript();
            var nsb_id = currentScript.getParameter({ name: 'custscript_rics_nsb_id' });
            var nsrics = gf_nsrics.getIntegration();
            var open_period_range = nsrics.getOpenPeriodRange(); // .earliest_open_date - .latest_open_date
            if(nsb_id != null){
                var filters = [
                    ["custrecord_rics_nsb_completedon","within",open_period_range.earliest_open_date,open_period_range.latest_open_date], "AND",
                    ["internalid","anyof",nsb_id]
                ];
            }else{
                var filters = [
                    ["custrecord_rics_nsb_completedon","within",open_period_range.earliest_open_date,open_period_range.latest_open_date], "AND",
                    [
                        ["custrecord_rics_nsb_ns_vrma_record","anyof","@NONE@"],"OR",
                        ["custrecord_rics_nsb_ns_vrma_record.status","noneof","VendAuth:F","VendAuth:G","VendAuth:C"] // Pending Credit, Credited, Cancelled
                    ]
   
                ];
            }
            var customrecord_rics_nonsellablebatchSearchObj = search.create({
                type: "customrecord_rics_nonsellablebatch",
                filters: filters,
                columns: [ ]
            });
            customrecord_rics_nonsellablebatchSearchObj.run().each(function(result){
                // .run().each has a limit of 4,000 results
                var data_obj = new Object();
                var nsb_record = record.load({
                    type: 'customrecord_rics_nonsellablebatch',
                    id: result.id,
                    isDynamic: true
                });
                data_obj.nsb = nsb_record;
                var arr_nsbd = [];
                var customrecord_rics_nsbd_SearchObj = search.create({
                    type: "customrecord_rics_nsb_details",
                    filters: [
                       ["custrecord_rics_nsbd_parent","anyof",result.id]
                    ],
                    columns: [ ]
                });
                customrecord_rics_nsbd_SearchObj.run().each(function(result_nsbd){
                    // .run().each has a limit of 4,000 results
                    var nsbd_record = record.load({
                        type: 'customrecord_rics_nsb_details',
                        id: result_nsbd.id,
                        isDynamic: true
                    });
                    arr_nsbd.push(nsbd_record);
                    return true;
                });
                data_obj.nsbd = arr_nsbd;
                arr_data.push(data_obj);
                return true;
            });

            log.debug({title: 'GF_MR_RICS_VRMA.js:rics_getInputData', details: 'arr_data: ' + JSON.stringify(arr_data)});

            return arr_data;
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_VRMA.js:rics_getInputData', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this mapping function we take the loaded RICS NonSellable Batch custom records and build an object that will be used to update the NetSuite VRMA (Vendor Return Merchandise Authorization)
     * @param {*} context - Object - This should be the RICS NonSellable Batch custom record
     * @returns void (note: the const context variable is impacted and used in the reduce function)
     */
    function rics_map(context){
        try{
            var data = JSON.parse(context.value);
            if(data == null){ return true; }
            log.debug({title: 'GF_MR_RICS_VRMA.js:rics_map', details: 'data: ' + JSON.stringify(data)});

            var ns_data = {};
            ns_data.nsb = {};
            ns_data.nsb.source_id = data.nsb.id;
            ns_data.nsbd = data.nsbd; // all items
            if(!data.nsb.fields.hasOwnProperty('custrecord_rics_nsb_ns_vrma_record')){
                // no connected NS VRMA record - need to create one
                // required fields
                var vendor_id = find_vendor(data.nsb.fields.custrecord_rics_nsb_suppliercode);
                if(vendor_id == 0){ 
                    log.audit({title: 'GF_MR_RICS_VRMA.js:rics_map', details: 'FAILED to find vendor: ' + JSON.stringify(data)});
                    return true;
                }
                ns_data.nsb.entity = vendor_id;
                ns_data.nsb.trandate = format.format({ value : new Date(data.nsb.fields.custrecord_rics_nsb_completedon), type : format.Type.DATE});
                ns_data.nsb.currency = '1';
                ns_data.nsb.exchangerate = '1';
                var loc_id = find_location_by_ext_id(data.nsb.fields.custrecord_rics_nsb_storecode);
                // lookup subsidiary
                var sub_id = find_subsidiary(loc_id);
                if(sub_id == 0){ 
                    log.audit({title: 'GF_MR_RICS_VRMA.js:rics_map', details: 'FAILED to find subsidiary: ' + JSON.stringify(data)});
                    return true;
                }
                ns_data.nsb.subsidiary = sub_id;
                // non-required fields
                ns_data.nsb.memo = '';
                ns_data.nsb.class = '';
                // ns_data.nsb.custbody_nsts_vp_vendor_email = ''; // this automatically pulls from the vendor record
                ns_data.nsb.location = parseInt(loc_id);
                ns_data.nsb.custbody_gf_internal_po = (data.nsb.fields.custrecord_rics_nsb_rma != undefined && data.nsb.fields.custrecord_rics_nsb_rma != '') ? data.nsb.fields.custrecord_rics_nsb_rma : data.nsb.fields.custrecord_rics_nsb_batchname;
                log.audit({title: 'GF_MR_RICS_VRMA.js:rics_map', details: 'custbody_gf_internal_po: ' + ns_data.nsb.custbody_gf_internal_po + ', batch name: ' + data.nsb.fields.custrecord_rics_nsb_batchname + ', rma: ' + data.nsb.fields.custrecord_rics_nsb_rma});
                ns_data.nsb.custbody_gf_vrma_comment = data.nsb.fields.custrecord_rics_nsb_comment;
                // customlist_rics_nsb_batch_types
                var arr_batchTypes = getListValues('customlist_rics_nsb_batch_types');
                ns_data.nsb.custbody_ns_rics_nsb_batch_type = (arr_batchTypes[data.nsb.fields.custrecord_rics_nsb_nonsellablebatchtype] != undefined) ? arr_batchTypes[data.nsb.fields.custrecord_rics_nsb_nonsellablebatchtype] : '';
                if(ns_data.nsb.custbody_ns_rics_nsb_batch_type == ''){
                    // add the new batch type to the list and then add it to the form
                    setNewListValue('customlist_rics_nsb_batch_types', data.nsb.fields.custrecord_rics_nsb_nonsellablebatchtype);
                    var arr_batchTypes = getListValues('customlist_rics_nsb_batch_types');
                    ns_data.nsb.custbody_ns_rics_nsb_batch_type = (arr_batchTypes[data.nsb.fields.custrecord_rics_nsb_nonsellablebatchtype] != undefined) ? arr_batchTypes[data.nsb.fields.custrecord_rics_nsb_nonsellablebatchtype] : '';
                }
            }else{
                // get the ID and pass to reduce for item add / update
                ns_data.nsb.id = data.nsb.fields.custrecord_rics_nsb_ns_vrma_record;
                ns_data.nsb.custbody_gf_internal_po = (data.nsb.fields.custrecord_rics_nsb_rma != undefined && data.nsb.fields.custrecord_rics_nsb_rma != '') ? data.nsb.fields.custrecord_rics_nsb_rma : data.nsb.fields.custrecord_rics_nsb_batchname;
                log.audit({title: 'GF_MR_RICS_VRMA.js:rics_map', details: 'custbody_gf_internal_po: ' + ns_data.nsb.custbody_gf_internal_po + ', batch name: ' + data.nsb.fields.custrecord_rics_nsb_batchname + ', rma: ' + data.nsb.fields.custrecord_rics_nsb_rma});
                ns_data.nsb.custbody_gf_vrma_comment = data.nsb.fields.custrecord_rics_nsb_comment;
                var rics_nsb_Id = record.submitFields({
                    type: 'vendorreturnauthorization',
                    id: data.nsb.fields.custrecord_rics_nsb_ns_vrma_record,
                    values: {
                        'custbody_gf_internal_po': ns_data.nsb.custbody_gf_internal_po,
                        'custbody_gf_vrma_comment' : ns_data.nsb.custbody_gf_vrma_comment
                    }
                });

                // NS VRMA record has been built - check status (if closed do not process again)
                var vrma_fields = search.lookupFields({
                    type: 'vendorreturnauthorization',
                    id: data.nsb.fields.custrecord_rics_nsb_ns_vrma_record,
                    columns: ['status']
                });
                var vrma_status = vrma_fields.status[0].value;
                if(vrma_status == 'closed'){ return true; }
                // customlist_rics_nsb_batch_types
                var arr_batchTypes = getListValues('customlist_rics_nsb_batch_types');
                ns_data.nsb.custbody_ns_rics_nsb_batch_type = (arr_batchTypes[data.nsb.fields.custrecord_rics_nsb_nonsellablebatchtype] != undefined) ? arr_batchTypes[data.nsb.fields.custrecord_rics_nsb_nonsellablebatchtype] : '';
                if(ns_data.nsb.custbody_ns_rics_nsb_batch_type == ''){
                    // add the new batch type to the list and then add it to the form
                    setNewListValue('customlist_rics_nsb_batch_types', data.nsb.fields.custrecord_rics_nsb_nonsellablebatchtype);
                    var arr_batchTypes = getListValues('customlist_rics_nsb_batch_types');
                    ns_data.nsb.custbody_ns_rics_nsb_batch_type = (arr_batchTypes[data.nsb.fields.custrecord_rics_nsb_nonsellablebatchtype] != undefined) ? arr_batchTypes[data.nsb.fields.custrecord_rics_nsb_nonsellablebatchtype] : '';
                }
                var loc_id = find_location_by_ext_id(data.nsb.fields.custrecord_rics_nsb_storecode);
                ns_data.nsb.location = parseInt(loc_id);
            }
            log.debug({title: 'GF_MR_RICS_VRMA.js:rics_map', details: 'ns_data: ' + JSON.stringify(ns_data)});

            context.write({ key: context.key, value: ns_data });
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_VRMA.js:rics_map', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this reduce function we take the object built out from the mapping function and either create or update a NetSuite VRMA Transaction record.
     * We also use the data from the object to create or update the Item Fulfillment record. Any VRMA that is being processed should have the Item Fulfillment automatically generated.
     * @param {*} context - Object - This is the object that was built in the mapping function, it should conatin all the data we need for processing
     * @returns - void
     */
    function rics_reduce(context){
        try{
            // update pricing and on hand quantity on all of the items by UPC and storecode
            var data = JSON.parse(context.values[0]);
            if(data == null){ return true; }
            log.debug({title: 'GF_MR_RICS_VRMA.js:rics_reduce', details: 'data: ' + JSON.stringify(data)});
            log.debug({title: 'GF_MR_RICS_VRMA.js:rics_reduce', details: 'data.nsb.id: ' + (data.nsb.id ? data.nsb.id : 'NOT SET')});
            var source_id = data.nsb.source_id;
            delete data.nsb.source_id; // remove this so it isn't processed like a field property
            // log.debug({title: 'GF_MR_RICS_VRMA.js:rics_reduce', details: 'data: ' + JSON.stringify(data.nsb)});
            // for(var i in data.nsbd){
            //     log.debug({title: 'GF_MR_RICS_VRMA.js:rics_reduce', details: 'data: ' + JSON.stringify(data.nsbd[i])});
            // }

            if(data.nsb.id == undefined){
                // no purchase order record was found we need to create one
                var vrma_record = record.create({
                    type: 'vendorreturnauthorization'
                });
                var line_num = 0;
                log.debug({title: 'GF_MR_RICS_VRMA.js:rics_reduce', details: 'created vrma_record!'});
            }else{
                log.debug({title: 'GF_MR_RICS_VRMA.js:rics_reduce', details: 'data.nsb.id: ' + data.nsb.id});
                // a purchase order record was found we should update it
                var vrma_record = record.load({
                    type: 'vendorreturnauthorization',
                    id: data.nsb.id,
                    isDynamic: true
                });
                log.debug({title: 'GF_MR_RICS_VRMA.js:rics_reduce', details: 'loaded vrma_record - id:' + data.nsb.id});
            }
            var arr_date_props = [ // the values are for processing as dates
                'trandate'
            ];
            for(var prop in data.nsb){
                if(arr_date_props.indexOf(prop) > -1){
                    if(data.nsb[prop] != ''){
                        vrma_record.setValue(prop,new Date(data.nsb[prop]));
                    }
                }else{
                    vrma_record.setValue(prop,data.nsb[prop]);
                }
            }
            log.debug({title: 'GF_MR_RICS_VRMA.js:rics_reduce', details: 'set all values!'});

            var num_items_added = 0;
            // need to add items before we can save the vrma record
            for(var item in data.nsbd){
                var item_data = data.nsbd[item].fields;
                log.debug({title: 'GF_MR_RICS_VRMA.js:rics_reduce', details: 'setting line data - item_data: ' + JSON.stringify(item_data)});
                var pd_id = item_data.custrecord_rics_nsbd_product_details;
                var pi_id = (item_data.hasOwnProperty('custrecord_rics_nsbd_product_item')) ? item_data.custrecord_rics_nsbd_product_item : '';
                if(pi_id != null && pi_id != ''){ // don't attempt to put the item on the line if we don't have an item listed
                    var ns_pd_fields = search.lookupFields({
                        type: 'customrecord_rics_product_details',
                        id: pd_id,
                        columns: ['custrecord_rics_pd_avail_pos_on']
                    });
                    // log.debug({title: 'GF_MR_RICS_VRMA.js:rics_reduce', details: 'ns_pd_fields: ' + JSON.stringify(ns_pd_fields)});
                    // log.debug({title: 'GF_MR_RICS_VRMA.js:rics_reduce', details: 'dt - custrecord_rics_pd_avail_pos_on: ' + format.parse({ value: ns_pd_fields.custrecord_rics_pd_avail_pos_on, type : format.Type.DATE})});
                    var dt_available_to_pos = format.parse({ value: ns_pd_fields.custrecord_rics_pd_avail_pos_on, type : format.Type.DATE});
                    var dt_oneYearFromNow = new Date();
                    dt_oneYearFromNow.setFullYear(dt_oneYearFromNow.getFullYear() + 1);
                    // log.debug({title: 'GF_MR_RICS_VRMA.js:rics_reduce', details: 'dt_available_to_pos: ' + dt_available_to_pos + ', dt_oneYearFromNow: ' + dt_oneYearFromNow});
                    // log.debug({title: 'GF_MR_RICS_VRMA.js:rics_reduce', details: '(dt_available_to_pos >= dt_oneYearFromNow): ' + (dt_available_to_pos >= dt_oneYearFromNow)});
                    if(dt_available_to_pos >= dt_oneYearFromNow){ continue; } // prevent adding deactivated items
                    var ns_pi_fields = search.lookupFields({
                        type: 'customrecord_rics_product_item',
                        id: pi_id,
                        columns: ['custrecord_rics_pi_upc', 'isinactive']
                    });
                    if(ns_pi_fields.isinactive){
                        log.debug({title: 'GF_MR_RICS_VRMA.js:rics_reduce', details: 'PI is INACTIVE - ns_pi_fields: ' + JSON.stringify(ns_pi_fields)});
                        continue;
                    }
                    var str_upc = ns_pi_fields.custrecord_rics_pi_upc;
                    var item_id = find_item(str_upc, item_data.custrecord_rics_nsbd_sku);

                    if(item_id > 0){
                        if(data.nsb.id == undefined){
                            log.debug({title: 'GF_MR_RICS_VRMA.js:rics_reduce', details: 'set_item_data_new_vrma item_id: ' + item_id});
                            // new record
                            set_item_data_new_vrma(line_num, vrma_record, item_id, item_data.custrecord_rics_nsbd_quantity, item_data.custrecord_rics_nsbd_unitvalue, data.nsb.location);
                            num_items_added += 1;
                            line_num += 1;
                        }else{
                            log.debug({title: 'GF_MR_RICS_VRMA.js:rics_reduce', details: 'update_item_data_existing_vrma item_id: ' + item_id});
                            // existing record
                            var lineCount = vrma_record.getLineCount({ 'sublistId': 'item' });
                            for(var i = 0; i < lineCount; i++){
                                vrma_record.selectLine({ sublistId: 'item', line: i });
                                var lineitem_id = vrma_record.getCurrentSublistValue({
                                    sublistId: 'item',
                                    fieldId: 'item'
                                });
                                if(item_id === lineitem_id){
                                    update_item_data_existing_vrma(vrma_record, item_id, item_data.custrecord_rics_nsbd_quantity, item_data.custrecord_rics_nsbd_unitvalue, data.nsb.location);
                                    vrma_record.commitLine({sublistId: 'item'});
                                    num_items_added += 1;
                                }
                            }
                        }
                    }
                }
            }

            if(num_items_added > 0){
                var saved_vrma_id = vrma_record.save();
                log.debug({title: 'GF_MR_RICS_VRMA.js:rics_reduce', details: 'saved_vrma_id: ' + saved_vrma_id + ', data: ' + JSON.stringify(data)});
            }else{
                log.debug({title: 'GF_MR_RICS_VRMA.js:rics_reduce', details: 'NO ITEMS TO ADD - source_id: ' + source_id + ', data: ' + JSON.stringify(data)});
                if(data.nsb.id != undefined){ var saved_vrma_id = data.nsb.id; }
            }
            if(saved_vrma_id != null && saved_vrma_id > 0){
                // update the NSB record - source_id
                var rics_nsb_Id = record.submitFields({
                    type: 'customrecord_rics_nonsellablebatch',
                    id: source_id,
                    values: {
                        'custrecord_rics_nsb_ns_vrma_record': saved_vrma_id,
                        'custrecord_rics_nsb_ns_vrma_update': new Date()
                    }
                });

                try{
                    // check for an item fulfillment - if one has not been created then create it, if one or more have been created then check all items to ensure that we have fulfilled everything
                    var itemfulfillmentSearchObj = search.create({
                        type: "itemfulfillment",
                        filters: [
                            ["type","anyof","ItemShip"], "AND", // Item Fulfillment
                            ["createdfrom","anyof",saved_vrma_id], "AND",
                            // ["status","anyof","VendAuth:E","VendAuth:B"], "AND", // VRMA Pending Credit/Partially Returned or Pending Return // not getting all status' runs us into an issue where the closed IF's are treated as new
                            ["mainline","is","T"]
                        ],
                        columns: [ "status" ]
                    });
                    var searchResultCount = itemfulfillmentSearchObj.runPaged().count;
                    if(searchResultCount == 0){
                        // create the fulfillment
                        var item_fulfillment_record = record.transform({
                            fromType : record.Type.VENDOR_RETURN_AUTHORIZATION,
                            fromId : saved_vrma_id,
                            toType : record.Type.ITEM_FULFILLMENT
                        });

                        var vrma_rec = record.load({
                            type: "vendorreturnauthorization",
                            id: saved_vrma_id,
                            isDynamic: true
                        });
                        var vrma_tran_date = vrma_rec.getValue('trandate');
                        item_fulfillment_record.setValue('trandate',new Date(vrma_tran_date));
                        var vrmaLineCount = vrma_rec.getLineCount({ sublistId : "item" });
                        for(var if_line_num = 0; if_line_num < vrmaLineCount; if_line_num++){
                            vrma_rec.selectLine({ sublistId: "item", line: if_line_num });
                            // item_fulfillment_record.selectNewLine({ sublistId: "item" });
                            var vrma_item = vrma_rec.getCurrentSublistValue({
                                sublistId: "item",
                                fieldId: "item"
                            });
                            item_fulfillment_record.setSublistValue({
                                sublistId: "item",
                                fieldId: "item",
                                line: if_line_num,
                                value: vrma_item
                            });
                            var vrma_qty = vrma_rec.getCurrentSublistValue({
                                sublistId: "item",
                                fieldId: "quantity"
                            });
                            item_fulfillment_record.setSublistValue({
                                sublistId: "item",
                                fieldId: "quantity",
                                line: if_line_num,
                                value: vrma_qty
                            });
                            var vrma_rate = vrma_rec.getCurrentSublistValue({
                                sublistId: "item",
                                fieldId: "rate"
                            });
                            item_fulfillment_record.setSublistValue({
                                sublistId: "item",
                                fieldId: "rate",
                                line: if_line_num,
                                value: vrma_rate
                            });
                            item_fulfillment_record.setSublistValue({
                                sublistId: "item",
                                fieldId: "itemreceive",
                                line: if_line_num,
                                value: true
                            });
                        }
                        item_fulfillment_record.save();
                    }else{
                        // verify the items / can we update the fulfillment? if not create a new fulfillment for the new items
                        var arr_all_IF_results = [];
                        itemfulfillmentSearchObj.run().each(function(result){
                            // .run().each has a limit of 4,000 results
                            var vrma_status = result.getValue('status');
                            if(vrma_status != 'shipped'){
                                arr_all_IF_results.push(result.id); // only update on records that have not been shipped (once saved are any not-shipped?)
                            }
                            log.audit({title: 'GF_MR_RICS_VRMA.js:rics_reduce', details: 'Found Saved Item Fulfillment - ID: ' + result.id});
                            return true;
                        });
                    }
                }catch(ex){
                    log.audit({title: 'GF_MR_RICS_VRMA.js:rics_reduce', details: 'FAILED to save Item Fulfillment - VRMA = ' + saved_vrma_id + ' - Error ' + ex.toString() + ' : ' + ex.stack});
                }
            }

        }catch(ex) {
            log.error({title: 'GF_MR_RICS_VRMA.js:rics_reduce', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * This summarize function is used to report important aspects about how the script performed
     * @param {*} summary - this object contains data regarding script performance.
     */
    function rics_summarize(summary) {
        try{
            log.audit('Usage Consumed', summary.usage);
            log.audit('Concurrency Number ', summary.concurrency);
            log.audit('Number of Yields', summary.yields);
            log.audit({title: 'Process Completed'});
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_VRMA.js:rics_summarize', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    return {
        getInputData: rics_getInputData,
        map: rics_map,
        reduce: rics_reduce,
        summarize: rics_summarize
    };

    // =========================== extra functions
    /**
     * This function takes the supplier code and tries to find the NetSuite Vendor record
     * @param {*} _str_suppliercode - string - The supplier code should be a 4 character string that will identify the vendor
     * @returns - integer - the internal ID of the NetSuite Vendor Record
     */
    function find_vendor(_str_suppliercode){
        var result_id = 0;
        if(_str_suppliercode == undefined || _str_suppliercode == null || _str_suppliercode == ''){ return result_id; }

        var vendorSearchObj = search.create({
            type: "vendor",
            filters: [ 
                ["formulanumeric: LENGTH({entityid})","equalto","4"], "AND",
                ["entityid","haskeywords",_str_suppliercode]
            ],
            columns: [
                search.createColumn({
                    name: "entityid",
                    sort: search.Sort.ASC,
                    label: "Name"
                })
            ]
        });
        vendorSearchObj.run().each(function(result){
            // .run().each has a limit of 4,000 results
            if(result_id == 0){
                result_id = result.id;
            }
            return true;
        });

        return result_id;
    }

    /**
     * This function takes the external id of a location, what RICS calls the store code, and tries to find the NetSuite Location record
     * @param {*} _ext_id - integer - this is the RICS store code
     * @returns - integer - the internal ID of the NetSuite Location record
     */
    function find_location_by_ext_id(_ext_id){
        var result_id = 0;

        var locationSearchObj = search.create({
            type: "location",
            filters: [
               ["externalid","anyof",_ext_id], "AND",
               ["isinactive","is","F"]
            ],
            columns: [ ]
        });
        locationSearchObj.run().each(function(result){
            // .run().each has a limit of 4,000 results
            if(result_id == 0){
                result_id = result.id;
            }
            return true;
        });

        return result_id;
    }

    /**
     * This function takes the internal ID of a NetSuite location and returns the internal ID of its subsidiary
     * @param {*} _location_id - integer - the internal ID of a NetSuite location
     * @returns - integer - the internal ID of the subsidiary
     */
    function find_subsidiary(_location_id){
        var result = 0;
        try{
            // load location record
            var location_record = record.load({
                type: 'location',
                id: _location_id,
                isDynamic: true
            });
            result = location_record.getValue('subsidiary');
        }catch(ex){
            // failure to get location
        }
        return result;
    }

    /**
     * This function takes the internal ID of a list and returns that list as an array where the key is the name of the list line and the value is the value of that line
     * @param {*} _listScriptId - string - the internal ID of the NetSuite list
     * @returns - array - the full list in the form of an array
     */
    function getListValues(_listScriptId) {
        var searchColumn = search.createColumn({ name : 'name' });
        var listSearch = search.create({ type : _listScriptId, columns : searchColumn });
        var listArray = [];
        listSearch.run().each(function(searchResult) {
            listArray[searchResult.getValue(searchColumn)] = searchResult.id;
            return true;
        });
        return listArray;
    }

    /**
     * This function adds a new value to a list
     * @param {*} _listScriptId - string - the internal ID of the NetSuite list
     * @param {*} _newVal - string - the new value to be added to the list
     * @returns - array - this function uses the getListValues function to return the full array after adding the new value
     */
    function setNewListValue(_listScriptId, _newVal){
        try{
            var list_internal_id = 0;
            if(_listScriptId == 'customlist_rics_nsb_batch_types'){
                list_internal_id = 762;
            }
            if(_newVal != undefined && _newVal != null && _newVal != ''){
                if(list_internal_id != 0){
                    var customListRec = record.load({
                        type: 'customlist',
                        id: list_internal_id,
                        isDynamic: true
                    });

                }
                customListRec.selectNewLine({ sublistId: 'customvalue' });

                customListRec.setCurrentSublistValue({
                    sublistId: 'customvalue',
                    fieldId: 'value',
                    value: _newVal,
                    ignoreFieldChange: true
                });
                customListRec.commitLine({sublistId: 'customvalue'});

                var recordId = customListRec.save();
            }
            return getListValues(_listScriptId);
        }catch(ex){
            log.error({title: 'Error: GF_MR_RICS_VRMA.js:setNewListValue', details: 'Params: _listScriptId = ' + _listScriptId + ', _newVal = ' + _newVal + ', Error ' + ex.toString() + ' : ' + ex.stack});
            return getListValues(_listScriptId);
        }
    }

    /**
     * This function sets a new line on a NetSuite VRMA transaction record
     * @param {*} _line_num - integer - this is the line that will be set
     * @param {*} _vrma_rec - Object - this is the VRMA record that the line will be added to
     * @param {*} _item_id - integer - this is the internal ID of the inventory item for this line
     * @param {*} _item_qty - integer - this is the count of how many items are on this line
     * @param {*} _item_cost - currency - (type is like a float or decimal) this is the cost of the item on this line
     * @param {*} _loc_id - integer - this is the location for which the inventory changes will be made
     */
    function set_item_data_new_vrma(_line_num, _vrma_rec, _item_id, _item_qty, _item_cost, _loc_id){
        // also add the line item
        _vrma_rec.setSublistValue({
            sublistId: 'item',
            fieldId: 'item',
            line: _line_num,
            value: _item_id
        });
        if(_item_qty == 0){
            _vrma_rec.setSublistValue({
                sublistId: 'item',
                fieldId: 'isclosed',
                line: _line_num,
                value: true
            });
        }
        _vrma_rec.setSublistValue({
            sublistId: 'item',
            fieldId: 'quantity',
            line: _line_num,
            value: _item_qty
        });
        _vrma_rec.setSublistValue({
            sublistId: 'item',
            fieldId: 'rate',
            line: _line_num,
            value: _item_cost
        });
        _vrma_rec.setSublistValue({
            sublistId: 'item',
            fieldId: 'location',
            line: _line_num,
            value: _loc_id
        });
    }

    /**
     * This function updates the values on a line that exists on a NetSuite VRMA transaction record
     * @param {*} _vrma_rec - Object - this is the VRMA record that the line will be added to
     * @param {*} _item_id - integer - this is the internal ID of the inventory item for this line
     * @param {*} _item_qty - integer - this is the count of how many items are on this line
     * @param {*} _item_cost - currency - (type is like a float or decimal) this is the cost of the item on this line
     * @param {*} _loc_id - integer - this is the location for which the inventory changes will be made
     */
    function update_item_data_existing_vrma(_vrma_rec, _item_id, _item_qty, _item_cost, _loc_id){
        log.debug({title: 'GF_MR_RICS_VRMA.js:update_item_data_existing_po', details: '_vrma_rec id: ' + _vrma_rec.id + ', _item_id:' + _item_id + ', _item_qty:' + _item_qty + ', _item_cost:' + _item_cost + ', _loc_id:' + _loc_id});
        // also add the line item
        _vrma_rec.setCurrentSublistValue({
            sublistId: 'item',
            fieldId: 'item',
            value: _item_id
        });
        if(_item_qty == 0){
            _vrma_rec.setCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'isclosed',
                value: true
            });
        }
        _vrma_rec.setCurrentSublistValue({
            sublistId: 'item',
            fieldId: 'rate',
            value: _item_cost
        });
        _vrma_rec.setCurrentSublistValue({
            sublistId: 'item',
            fieldId: 'quantity',
            value: _item_qty
        });
        _vrma_rec.setCurrentSublistValue({
            sublistId: 'item',
            fieldId: 'location',
            value: _loc_id
        });
    }

    /**
     * This function takes the sku and upc of an item and tries to find the NetSuite Inventory Item record
     * @param {*} _str_upc - string - the unique upc for the item
     * @param {*} _str_sku - string - the sku value for the item
     * @returns - integer - the internal ID of the NetSuite Inventory Item record
     */
    function find_item(_str_upc, _str_sku){
        // log.debug({title: 'GF_MR_RICS_VRMA.js:find_item', details: '_str_upc: ' + _str_upc + ', _str_sku:' + _str_sku});
        var result_id = 0;
        if(_str_upc == undefined || _str_sku == undefined){ return result_id; }

        if(_str_upc !== null && _str_upc != '' && _str_sku !== null && _str_sku != ''){
            var itemSearchObj = search.create({
                type: "item",
                filters: [
                    ["upccode","is",_str_upc], "AND",
                    ["vendorname","is",_str_sku], "AND",
                    ["isinactive","is","F"]
                ],
                columns: [ ]
            });
            itemSearchObj.run().each(function(result){
                // .run().each has a limit of 4,000 results
                if(result_id == 0){
                    result_id = result.id;
                }
                return true;
            });
        }else{
            log.debug({title: 'GF_MR_RICS_VRMA.js:find_item', details: 'insufficient data - _str_upc: ' + _str_upc + ', _str_sku:' + _str_sku});
        }
        if(result_id == 0){
            log.debug({title: 'GF_MR_RICS_VRMA.js:find_item', details: 'insufficient data - _str_upc: ' + _str_upc + ', _str_sku:' + _str_sku});
        }

        return result_id;
    }

});