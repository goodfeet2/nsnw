/**
 *@NApiVersion 2.1
 *@NScriptType MapReduceScript
 *
Script Name:   GF_MR_RICS_Rollback_Integration.js
Author:        Mark Robinson
 */

 define(['N/log', 'N/search', 'N/record', 'N/runtime'], function(log, search, record, runtime){

    /**
     * In this get input function we match location records and their related account specific data to the transaction records that were created by the RICS Cash Sale script
     * @param {*} context  - this variable is not utilized in this function
     * @returns - an array containing the full responses from the custom record search
     */
    function rics_getInputData(context){
        try {
            var currentScript = runtime.getCurrentScript();
            var str_record_type = currentScript.getParameter({ name: 'custscript_ns_rics_record_type' });
            // if no records created directly - this will be null
            var str_connected_record_field = currentScript.getParameter({ name: 'custscript_ns_rics_crec_field' });
            log.audit({title: 'GF_MR_RICS_Rollback_Integration.js:rics_getInputData', details: 'params: [str_record_type: ' + str_record_type + ', str_connected_record_field: ' + str_connected_record_field + ']'});

            // get inventory on hand records
            var search_columns = [];
            if(str_connected_record_field != null && str_connected_record_field != ''){
                search_columns.push(str_connected_record_field);
                var typeCol = search.createColumn({
                    name: "type",
                    join: str_connected_record_field
                });
                search_columns.push(typeCol);
            }
            var customrecord_rics_SearchObj = search.create({
                type: str_record_type,
                filters:[ ],
                columns:search_columns
            });

            return customrecord_rics_SearchObj;
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_Rollback_Integration.js:rics_getInputData', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this mapping function we pass through the data that will be  processed in the reduce phase
     * @param {*} context - Object - This should be the RICS NonSellable Batch custom record
     * @returns void (note: the const context variable is impacted and used in the reduce function)
     */
    function rics_map(context){
        try{
            // simple pass through
            var map_data = JSON.parse(context.value);
            log.audit({title: 'GF_MR_RICS_Rollback_Integration.js:rics_map', details: 'map_data: ' + JSON.stringify(map_data)});
			if (map_data == null) {
				return true;
			}else{
                // check for a connected record
                if(map_data.values != undefined && map_data.values != null && map_data.values[str_connected_record_field] != undefined){
                    var currentScript = runtime.getCurrentScript();
                    // if no records created directly - this will be null
                    var str_connected_record_field = currentScript.getParameter({ name: 'custscript_ns_rics_crec_field' });
                    var str_connected_record_type = currentScript.getParameter({ name: 'custscript_ns_rics_crec_type' });
                    log.audit({title: 'GF_MR_RICS_Rollback_Integration.js:rics_map', details: 'params: [str_connected_record_field: ' + str_connected_record_field + ', str_connected_record_type: ' + str_connected_record_type + ']'});
                    // get the id of the connected record
                    if(map_data.values[str_connected_record_field][0] != undefined){
                        var ns_rec_id = map_data.values[str_connected_record_field][0].value;
                        var ns_rec_type = map_data.values[str_connected_record_field + '.type'][0].value;
                        // based on the connected record type deactivate / remove the attached record as well
                        if(str_connected_record_type == 'item'){
                            // special case - the child type could be either inventoryitem or noninventoryitem
                            remove_all_attached_items(child_rec_id, ns_rec_type);
                            // remove the custom record
                            try{
                                log.debug({title: 'GF_MR_RICS_Rollback_Integration.js:rics_map-delete', details: 'Would have removed - recordType: ' + ns_rec_type + ', id: ' + ns_rec_id});
                                // var removed_record = record.delete({ type: map_data.recordType, id: map_data.id });
                            }catch(ex){
                                log.error({title: 'GF_MR_RICS_Rollback_Integration.js:rics_map-onDelete-'+str_connected_record_type, details: 'Error ' + ex.toString() + ' : ' + ex.stack});
                            }
                        }
                        if(str_connected_record_type == 'intercompanytransferorder'){
                            remove_all_attached_transferorders(child_rec_id, str_connected_record_type);
                        }
                        if(str_connected_record_type == 'cscr'){
                            // special case - the child type could be either cashsale or cashrefund
                            remove_all_attached_cashsalerefunds(child_rec_id, ns_rec_type);
                        }
                        if(str_connected_record_type == 'purchaseorder'){
                            remove_all_attached_purchaseorders(child_rec_id, str_connected_record_type);
                        }
                        if(str_connected_record_type == 'vendorreturnauthorization'){
                            remove_all_attached_vrmas(child_rec_id, str_connected_record_type);
                        }
                    }
                }

                // remove the custom record
                try{
                    log.debug({title: 'GF_MR_RICS_Rollback_Integration.js:rics_map-delete', details: 'Would have removed - recordType: ' + map_data.recordType + ', id: ' + map_data.id});
                    // var removed_record = record.delete({ type: map_data.recordType, id: map_data.id });
                }catch(ex){
                    log.error({title: 'GF_MR_RICS_Rollback_Integration.js:rics_map-onDelete', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
                }
            }

			// context.write({ key: context.key, value: map_data }); // pass through directly to reduce

			return true;
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_Rollback_Integration.js:rics_map', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this reduce function we do nothing
     * @param {*} context - Object - This is the object that was built in the mapping function, it should conatin all the data we need for processing
     * @returns - void
     */
    function rics_reduce(context){
        return true;
    }

    /**
     * This summarize function is used to report important aspects about how the script performed
     * @param {*} summary - this object contains data regarding script performance.
     */
    function rics_summarize(summary) {
        try{
            log.audit('Usage Consumed', summary.usage);
            log.audit('Concurrency Number ', summary.concurrency);
            log.audit('Number of Yields', summary.yields);
            log.audit({title: 'Process Completed'});
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_Rollback_Integration.js:rics_summarize', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    return {
        getInputData: rics_getInputData,
        map: rics_map,
        reduce: rics_reduce,
        summarize: rics_summarize
    };

    // =========================== extra functions
    /**
     * This function will perform a search to find all child items (sub-matrix items) associated to the attached record and attempt to remove them
     * @param {*} _ns_rec_id - integer - the internal id of the connected record
     * @param {*} _ns_rec_type - string - the specific record type of the connected record
     */
    function remove_all_attached_items(_ns_rec_id, _ns_rec_type){
        var item_type = (_ns_rec_type == 'InvtPart') ? 'inventoryitem' : 'noninventoryitem';
        if(item_type == 'inventoryitem'){
            // get all sub-items
            var inventoryitemSearchObj = search.create({
                type: "inventoryitem",
                filters: [
                    ["type","anyof","InvtPart"], "AND",
                    ["parent","anyof",_ns_rec_id], "AND",
                    ["isinactive","any",""]
                ],
                columns:[]
            });
            var searchResultCount = inventoryitemSearchObj.runPaged().count;
            log.debug({title: 'GF_MR_RICS_Rollback_Integration.js:remove_all_attached_items-results', details: 'searchResultCount: ' + searchResultCount});
            inventoryitemSearchObj.run().each(function(result){
                // .run().each has a limit of 4,000 results
                // remove sub-items
                var child_item_id = result.id;
                log.debug({title: 'GF_MR_RICS_Rollback_Integration.js:remove_all_attached_items-results', details: 'child_item_id: ' + child_item_id});
                try{
                    // var removed_record = record.delete({ type: item_type, id: child_item_id });
                }catch(ex){
                    log.error({title: 'GF_MR_RICS_Rollback_Integration.js:remove_all_attached_items-onDelete', details: 'item_type: ' + item_type + ', child_item_id: ' + child_item_id + ', Error ' + ex.toString() + ' : ' + ex.stack});
                }
                return true;
            });
            // remove item
            try{
                // var removed_record = record.delete({ type: item_type, id: _ns_rec_id });
            }catch(ex){
                log.error({title: 'GF_MR_RICS_Rollback_Integration.js:remove_all_attached_items-onDelete', details: 'item_type: ' + item_type + ', child_item_id: ' + child_item_id + ', Error ' + ex.toString() + ' : ' + ex.stack});
            }
        }
    }

    /**
     * This function tries to remove the connected transfer order transaction record
     * @param {*} _ns_rec_id - integer - the internal id of the connected record
     * @param {*} _ns_rec_type - string - the specific record type of the connected record
     */
    function remove_all_attached_transferorders(_ns_rec_id, _ns_rec_type){
        try{
            log.debug({title: 'GF_MR_RICS_Rollback_Integration.js:remove_all_attached_transferorders-delete', details: 'Would have removed - _ns_rec_type: ' + _ns_rec_type + ', _ns_rec_id: ' + _ns_rec_id});
            // var removed_record = record.delete({ type: _ns_rec_type, id: _ns_rec_id });
        }catch(ex){
            log.error({title: 'GF_MR_RICS_Rollback_Integration.js:remove_all_attached_transferorders-onDelete', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * This function tries to remove the connected cash sale or cash refund transaction record
     * @param {*} _ns_rec_id - integer - the internal id of the connected record
     * @param {*} _ns_rec_type - string - the specific record type of the connected record (values will vary CashSale, CashRfnd, or empty)
     */
    function remove_all_attached_cashsalerefunds(_ns_rec_id, _ns_rec_type){
        if(_ns_rec_id == '' || _ns_rec_type == ''){
            return true;
        }else{
            try{
                var transaction_type = '';
                if(_ns_rec_type == 'CashSale'){ transaction_type = 'cashsale'; }
                if(_ns_rec_type == 'CashRfnd'){ transaction_type = 'cashrefund'; }
                log.debug({title: 'GF_MR_RICS_Rollback_Integration.js:remove_all_attached_cashsalerefunds-delete', details: 'Would have removed - transaction_type: ' + transaction_type + ', _ns_rec_id: ' + _ns_rec_id});
                // var removed_record = record.delete({ type: transaction_type, id: _ns_rec_id });
            }catch(ex){
                // could have already been removed, or if it has been processed it will have other dependent records
                log.error({title: 'GF_MR_RICS_Rollback_Integration.js:remove_all_attached_cashsalerefunds-onDelete', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
            }
        }
    }

    /**
     * This function tries to remove the connected purchase order transaction record
     * @param {*} _ns_rec_id - integer - the internal id of the connected record
     * @param {*} _ns_rec_type - string - the specific record type of the connected record
     */
    function remove_all_attached_purchaseorders(_ns_rec_id, _ns_rec_type){
        try{
            log.debug({title: 'GF_MR_RICS_Rollback_Integration.js:remove_all_attached_purchaseorders-delete', details: 'Would have removed - _ns_rec_type: ' + _ns_rec_type + ', _ns_rec_id: ' + _ns_rec_id});
            // var removed_record = record.delete({ type: _ns_rec_type, id: _ns_rec_id });
        }catch(ex){
            log.error({title: 'GF_MR_RICS_Rollback_Integration.js:remove_all_attached_purchaseorders-onDelete', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * This function tries to remove the connected vendor return authorization transaction record
     * @param {*} _ns_rec_id - integer - the internal id of the connected record
     * @param {*} _ns_rec_type - string - the specific record type of the connected record
     */
    function remove_all_attached_vrmas(_ns_rec_id, _ns_rec_type){
        try{
            log.debug({title: 'GF_MR_RICS_Rollback_Integration.js:remove_all_attached_vrmas-delete', details: 'Would have removed - _ns_rec_type: ' + _ns_rec_type + ', _ns_rec_id: ' + _ns_rec_id});
            // var removed_record = record.delete({ type: _ns_rec_type, id: _ns_rec_id });
        }catch(ex){
            log.error({title: 'GF_MR_RICS_Rollback_Integration.js:remove_all_attached_vrmas-onDelete', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

});