/**
 *@NApiVersion 2.1
 *@NScriptType MapReduceScript
 *
Script Name:   GF_MR_RICS_Purchase_Orders.js
Author:        Mark Robinson
 */

define(['N/log', 'N/search', 'N/record', 'N/https', 'N/format', 'N/runtime', '/SuiteScripts/GF_LIB/gf_nsrics'], function(log, search, record, https, format, runtime, gf_nsrics) {

    /**
     * In this get input function we're making API calls to RICS to get all the purchase order data for Purchase Orders that are not closed
     * @param {*} context
     * @returns
     */
    function rics_getInputData(context){
        var nsrics = gf_nsrics.getIntegration();
        var currentScript = runtime.getCurrentScript();
        var po_num = currentScript.getParameter({ name: 'custscript_rics_po_number' });
        log.debug({title: 'GF_MR_RICS_Purchase_Orders.js:rics_getInputData', details: 'po_num: ' + (po_num != null ? po_num : 'NULL')});
        // Get all Purchase Order Data data from RICS with a POST request to: https://enterprise.ricssoftware.com/api/PurchaseOrder/GetPurchaseOrder
        // Map phase will create a NetSuite writable object
        // Reduce phase will save the NetSuite writable object
        var max_attempts = 3;
        var arr_PurchaseOrders = [];
        try {
            var getPurchaseOrder = nsrics.baseurl + nsrics.getEndPoint('GetPurchaseOrder');
            var header = [];
            header['Content-Type'] = 'application/json';
            header['Token'] = nsrics.getActiveConnection();
            var num_skip = 0;
            var current_attempt = 0;

            // // QUERY DW (VIA AWS) WITH RESPONSE CONTAINING ONLY UPDATED PO'S GET ALL DATA FROM RICS
            // if(po_num == null){
            //     // get the po list first
            //     var arr_Updated_Po_Numbers = getUpdatedRicsPoNumbers(nsrics);
            //     // for each po on the list get all po details

            //     for(var po_number in arr_Updated_Po_Numbers){
            //         num_skip = 0;
            //         do{
            //             nsrics.sleep(3000);
            //             var postData = { "PurchaseOrderNumber": arr_Updated_Po_Numbers[po_number].po, "Skip":num_skip };
            //             log.debug({title: 'GF_MR_RICS_Purchase_Orders.js:rics_getInputData', details: 'postData: ' + JSON.stringify(postData)});
            //             try{
            //                 postData = JSON.stringify(postData);
            //                 var response = https.post({
            //                     url: getPurchaseOrder,
            //                     headers:header,
            //                     body: postData
            //                 });
            //                 if(response != undefined && response['code'] != 200){
            //                     current_attempt += 1;
            //                 }else{
            //                     current_attempt = 0;
            //                     var data = JSON.parse(response.body);
            //                     if(po_num != null && data.ResultStatistics.TotalRecords == 0){ // order not found - setup for Void
            //                         var void_obj = new Object();
            //                         void_obj.po_num = po_num;
            //                         arr_PurchaseOrders.push(void_obj);
            //                     }
            //                     for(var key in data.PurchaseOrders){
            //                         // if(data.PurchaseOrders[key].hasOwnProperty('PurchaseOrderNumber')){ // update - if the PO Number starts with three zeros don't process it
            //                         if(data.PurchaseOrders[key].hasOwnProperty('PurchaseOrderNumber') && (data.PurchaseOrders[key].PurchaseOrderNumber).substring(0, 3) != '000'){
            //                             arr_PurchaseOrders.push(data.PurchaseOrders[key]);
            //                         }
            //                         if(data.PurchaseOrders[key].pk == undefined){ data.PurchaseOrders[key].pk = arr_Updated_Po_Numbers[po_number].pk; }
            //                     }
            //                     num_skip = data.ResultStatistics.EndRecord;
            //                 }
            //             }catch(err){
            //                 current_attempt += 1;
            //             }
            //         }while( current_attempt < max_attempts && ( (response == undefined || response['code'] != 200) || ( data.hasOwnProperty('ResultStatistics') && data.ResultStatistics.EndRecord < data.ResultStatistics.TotalRecords ) ) );
            //     }

            // }else{

            if(po_num == null){
                var dt_now = new Date();
                var str_currentMonth=('0'+(dt_now.getMonth()+1)).slice(-2);
                var str_currentYear=(dt_now.getFullYear().toString()).slice(-2);
                str_ponum = `${str_currentMonth}%%${str_currentYear}%`; // gets any of the normal naming convention po's for the current month
            }

            do{
                nsrics.sleep(3000);
                var postData = { "PurchaseOrderNumber": po_num, "Skip":num_skip };
                // var postData = { "PurchaseOrderNumber": "%", "Skip":num_skip };
                log.debug({title: 'GF_MR_RICS_Purchase_Orders.js:rics_getInputData', details: 'postData: ' + JSON.stringify(postData)});
                try{
                    postData = JSON.stringify(postData);
                    var response = https.post({
                        url: getPurchaseOrder,
                        headers:header,
                        body: postData
                    });
                    if(response != undefined && response['code'] != 200){
                        current_attempt += 1;
                    }else{
                        current_attempt = 0;
                        var data = JSON.parse(response.body);
                        if(po_num != null && data.ResultStatistics.TotalRecords == 0){ // order not found - setup for Void
                            var void_obj = new Object();
                            void_obj.po_num = po_num;
                            arr_PurchaseOrders.push(void_obj);
                        }
                        for(var key in data.PurchaseOrders){
                            // if(data.PurchaseOrders[key].hasOwnProperty('PurchaseOrderNumber')){ // update - if the PO Number starts with three zeros don't process it
                            if(data.PurchaseOrders[key].hasOwnProperty('PurchaseOrderNumber') && (data.PurchaseOrders[key].PurchaseOrderNumber).substring(0, 3) != '000'){
                                arr_PurchaseOrders.push(data.PurchaseOrders[key]);
                            }
                        }
                        num_skip = data.ResultStatistics.EndRecord;
                    }
                }catch(err){
                    current_attempt += 1;
                }
            }while( current_attempt < max_attempts && ( (response == undefined || response['code'] != 200) || ( data.hasOwnProperty('ResultStatistics') && data.ResultStatistics.EndRecord < data.ResultStatistics.TotalRecords ) ) );

            // }

            log.debug({title: 'GF_MR_RICS_Purchase_Orders.js:rics_getInputData', details: 'arr_PurchaseOrders: ' + JSON.stringify(arr_PurchaseOrders)});

            return arr_PurchaseOrders;
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_Purchase_Orders.js:rics_getInputData', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this mapping function we take the data from the responses in getinput and use it to find or build the custom RICS Purchase Order record that will get the updates.
     * @param {*} context
     * @returns
     */
    function rics_map(context){
        try{
            var data = JSON.parse(context.value);
            if(data == null){ return true; }
            // try{
          	//     updatedProcessedRicsPoNumber(data.PurchaseOrderNumber);
            // }catch(ex) {
            //     log.debug({title: 'GF_MR_RICS_Purchase_Orders.js:rics_map', details: 'updatedProcessedRicsPoNumber Error: ' + ex.toString() + ' : ' + ex.stack});
            // }
            // log.debug({title: 'GF_MR_RICS_Purchase_Orders.js:rics_map', details: 'data: ' + JSON.stringify(data)});
            if(data.hasOwnProperty('po_num')){ // searched for specific po number and it wasn't found - these need to be voided
                var ns_custom_record_id = find_po_record(data.po_num);
                // log.debug({title: 'GF_MR_RICS_Purchase_Orders.js:rics_map', details: 'ns_custom_record_id: ' + ns_custom_record_id});
                var custom_po_data = search.lookupFields({
                    type: 'customrecord_rics_purchase_order',
                    id: ns_custom_record_id,
                    columns: ['custrecord_rics_po_purchaseorder']
                });
                // log.debug({title: 'GF_MR_RICS_Purchase_Orders.js:rics_map', details: 'custom_po_data: ' + JSON.stringify(custom_po_data)});
                var purchOrd = record.load({
                    type: 'purchaseorder',
                    id: custom_po_data.custrecord_rics_po_purchaseorder[0].value,
                    isDynamic: true
                });
                purchOrd.setValue('memo', 'VOID');
                purchOrd.setValue('voided', true); // currently not working as expected
                var currentLineCount = purchOrd.getLineCount({ 'sublistId': 'item' });
                for(var i = 0; currentLineCount > 0 && i < currentLineCount; i++){
                    purchOrd.selectLine({ sublistId: 'item', line: i });
                    purchOrd.setCurrentSublistValue({
                        sublistId: 'item',
                        fieldId: 'isclosed',
                        value: true,
                        ignoreFieldChange: true
                    });
                    purchOrd.setCurrentSublistValue({
                        sublistId: 'item',
                        fieldId: 'quantity',
                        value: 0,
                        ignoreFieldChange: true
                    });
                    purchOrd.commitLine({sublistId: 'item'});
                }
                purchOrd.save();
                var custom_po_upd_id = record.submitFields({
                    type: 'customrecord_rics_purchase_order',
                    id: ns_custom_record_id,
                    values: { custrecord_rics_po_cancelon: new Date() }
                });
                return true;
            }
            var currentScript = runtime.getCurrentScript();
            var po_num = currentScript.getParameter({ name: 'custscript_rics_po_number' });
            var ns_custom_record_id = find_po_record(data.PurchaseOrderNumber);
            if(po_num == null && ns_custom_record_id > 0){ // we're not getting a specific PO and we've already built this PO in the system
                log.audit({title: 'GF_MR_RICS_Purchase_Orders.js:rics_map- po_num not set and id found', details: 'data.PurchaseOrderNumber: ' + data.PurchaseOrderNumber + ', ns_custom_record_id: ' + ns_custom_record_id});
                // var custom_po_upd_id = record.submitFields({
                //     type: 'customrecord_rics_purchase_order',
                //     id: ns_custom_record_id,
                //     values: { custrecord_rics_po_ns_purch_order_update: '', custrecord_rics_po_confirmationnumber: data.pk }
                // });
                // return true;
            }else{
                if(po_num == null){
                    log.audit({title: 'GF_MR_RICS_Purchase_Orders.js:rics_map- po_num not set and id NOT found', details: 'ADDING data.PurchaseOrderNumber: ' + data.PurchaseOrderNumber});
                }
            }
            if(ns_custom_record_id > 0){
                var ns_po_id = 0;
                var custom_po_data = search.lookupFields({
                    type: 'customrecord_rics_purchase_order',
                    id: ns_custom_record_id,
                    columns: ['custrecord_rics_po_purchaseorder']
                });
                if(custom_po_data.custrecord_rics_po_purchaseorder != undefined && custom_po_data.custrecord_rics_po_purchaseorder[0].value != undefined){
                    ns_po_id = custom_po_data.custrecord_rics_po_purchaseorder[0].value;
                }
            }

            // build a NetSuite writable object
            var ns_data = {};
            var ns_po_rec = {};
            var arr_ns_po_details = [];

            ns_po_rec.altname = data.PurchaseOrderNumber;
            ns_po_rec.custrecord_rics_po_purchaseordernumber = data.PurchaseOrderNumber;
            ns_po_rec.custrecord_rics_po_confirmationnumber = (data.hasOwnProperty('pk')) ? data.pk : data.ConfirmationNumber;
            ns_po_rec.custrecord_rics_po_terms = data.Terms;
            ns_po_rec.custrecord_rics_po_shipvia = data.ShipVia;
            ns_po_rec.custrecord_rics_po_allowbackordered = data.AllowBackOrdered;
            ns_po_rec.custrecord_rics_po_allowsplitshipment = data.AllowSplitShipment;
            ns_po_rec.custrecord_rics_po_isspecialorder = data.IsSpecialOrder;
            ns_po_rec.custrecord_rics_po_salesperiodstart = (data.hasOwnProperty('SalesPeriodStart')) ? format_RICS_date_string(data.SalesPeriodStart) : '';
            ns_po_rec.custrecord_rics_po_salesperiodend = (data.hasOwnProperty('SalesPeriodEnd')) ? format_RICS_date_string(data.SalesPeriodEnd) : '';
            ns_po_rec.custrecord_rics_po_programcode = data.ProgramCode;
            ns_po_rec.custrecord_rics_po_discountpercent = data.DiscountPercent;
            ns_po_rec.custrecord_rics_po_orderedon = (data.hasOwnProperty('OrderedOn')) ? format_RICS_date_string(data.OrderedOn) : '';
            ns_po_rec.custrecord_rics_po_shipon = (data.hasOwnProperty('ShipOn')) ? format_RICS_date_string(data.ShipOn) : '';
            ns_po_rec.custrecord_rics_po_closedon = (data.hasOwnProperty('ClosedOn')) ? format_RICS_date_string(data.ClosedOn) : '';
            ns_po_rec.custrecord_rics_po_cancelon = (data.hasOwnProperty('CancelOn')) ? format_RICS_date_string(data.CancelOn) : '';
            ns_po_rec.custrecord_rics_po_originalcancelon = (data.hasOwnProperty('OriginalCancelOn')) ? format_RICS_date_string(data.OriginalCancelOn) : '';
            ns_po_rec.custrecord_rics_po_createdon = (data.hasOwnProperty('CreatedOn')) ? format_RICS_date_string(data.CreatedOn) : '';
            ns_po_rec.custrecord_rics_po_purchaseordertype = data.PurchaseOrderType;
            ns_po_rec.custrecord_rics_po_billtostorecode = data.BillToStoreCode;
            ns_po_rec.custrecord_rics_po_billtostorename = data.BillToStoreName;
            ns_po_rec.custrecord_rics_po_shiptostorecode = data.ShipToStoreCode;
            ns_po_rec.custrecord_rics_po_shiptostorename = data.ShipToStoreName;
            ns_po_rec.custrecord_rics_po_suppliername = data.SupplierName;
            ns_po_rec.custrecord_rics_po_suppliercode = data.SupplierCode;
            ns_po_rec.custrecord_rics_po_json = JSON.stringify(data);

            // log.debug({title: 'GF_MR_RICS_Purchase_Orders.js:rics_map', details: 'data.Details: ' + JSON.stringify(data.Details)});
            for(var detail in data.Details){
                var detail_Data = data.Details[detail];
                // log.debug({title: 'GF_MR_RICS_Purchase_Orders.js:rics_map', details: 'detail_Data: ' + JSON.stringify(detail_Data)});

                var ns_po_details_rec = {};

                ns_po_details_rec.name = data.PurchaseOrderNumber;
                ns_po_details_rec.name += (detail_Data.ProductItem.hasOwnProperty('Sku')) ? '-' + detail_Data.ProductItem.Sku : '-SKU_MISSING';
                ns_po_details_rec.name += (detail_Data.ProductItem.hasOwnProperty('UPC')) ? '-' + detail_Data.ProductItem.UPC : '-UPC_MISSING';

                ns_po_details_rec.custrecord_rics_pod_orderquantity = (!detail_Data.hasOwnProperty('DeletedOn')) ? detail_Data.OrderQuantity : 0;
                ns_po_details_rec.custrecord_rics_pod_receivedquantity = (!detail_Data.hasOwnProperty('DeletedOn')) ? detail_Data.ReceivedQuantity : 0;
                ns_po_details_rec.custrecord_rics_pod_cost = detail_Data.Cost;
                ns_po_details_rec.custrecord_rics_pod_receiveatcost = detail_Data.ReceiveAtCost;
                ns_po_details_rec.custrecord_rics_pod_retailprice = detail_Data.RetailPrice;
                ns_po_details_rec.custrecord_rics_pod_createdon = (detail_Data.hasOwnProperty('CreatedOn')) ? format_RICS_date_string(detail_Data.CreatedOn) : '';
                ns_po_details_rec.custrecord_rics_pod_modifiedon = (detail_Data.hasOwnProperty('ModifiedOn')) ? format_RICS_date_string(detail_Data.ModifiedOn) : '';
                ns_po_details_rec.custrecord_rics_pod_parent_po = '';
                ns_po_details_rec.custrecord_rics_pod_product_details = 0;
                ns_po_details_rec.custrecord_rics_pod_product_item = 0;
                if(detail_Data.hasOwnProperty('DeletedOn')){
                    // need to change the detail data Order Quantity and Received Quantity so the JSON read matches the record
                    detail_Data.OrderQuantity = 0;
                    detail_Data.ReceivedQuantity = 0;
                }
                ns_po_details_rec.custrecord_rics_pod_json = JSON.stringify(detail_Data);
                if(ns_po_id != undefined){
                    ns_po_details_rec.custrecord_rics_pod_ns_po = ns_po_id;
                }

                // check for a more recently modified same item by name
                var flg_found_same_name = false;
                for(var det in arr_ns_po_details){
                    var pod_data = arr_ns_po_details[det];
                    if(pod_data.name == ns_po_details_rec.name){
                        flg_found_same_name = true;
                        var dt_pod_modifiedon = new Date(pod_data.custrecord_rics_pod_modifiedon);
                        var dt_current_modifiedon = new Date(ns_po_details_rec.custrecord_rics_pod_modifiedon);
                        if(dt_pod_modifiedon.valueOf() < dt_current_modifiedon.valueOf()){
                            arr_ns_po_details[det] = ns_po_details_rec;
                        }
                        break;
                    }
                }
                if(!flg_found_same_name){
                	arr_ns_po_details.push(ns_po_details_rec);
                }
            }

            // pass data to reduce
            ns_data.ns_po_rec = ns_po_rec;
            ns_data.ns_po_details_recs = arr_ns_po_details;
            // log.debug({title: 'GF_MR_RICS_Purchase_Orders.js:rics_map', details: 'ns_data: ' + JSON.stringify(ns_data)});

            if(po_num == null && ns_custom_record_id == 0){
                log.audit({title: 'GF_MR_RICS_Purchase_Orders.js:rics_map- po_num not set and id NOT found -ns_data', details: 'ns_data: ' + JSON.stringify(ns_data)});
            }
            context.write({ key: context.key, value: ns_data });
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_Purchase_Orders.js:rics_map', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this reduce function we take the objects that were built in the mapping function and apply them as updates to the custom records.
     * @param {*} context
     * @returns void
     */
    function rics_reduce(context){
        try{
            var nsrics = gf_nsrics.getIntegration();
            // update pricing and on hand quantity on all of the items by UPC and storecode
            var data = JSON.parse(context.values[0]);
            if(data == null){ return true; }

            if(data.hasOwnProperty('ns_po_rec')){
                log.debug({title: 'GF_MR_RICS_Purchase_Orders.js:rics_reduce', details: 'data: ' + JSON.stringify(data)});
                var arr_date_props = [ // the values are for processing, and not to be directly written on the record
                    'custrecord_rics_po_salesperiodstart',
                    'custrecord_rics_po_salesperiodend',
                    'custrecord_rics_po_orderedon',
                    'custrecord_rics_po_shipon',
                    'custrecord_rics_po_closedon',
                    'custrecord_rics_po_cancelon',
                    'custrecord_rics_po_originalcancelon',
                    'custrecord_rics_po_createdon',
                    'custrecord_rics_pod_createdon',
                    'custrecord_rics_pod_modifiedon'
                ];
                var ns_po_id = find_po_record(data.ns_po_rec.custrecord_rics_po_purchaseordernumber);

                if(ns_po_id == 0){
                    // no purchase order record was found we need to create one
                    var purchase_order_record = record.create({
                        type: 'customrecord_rics_purchase_order'
                    });
                }else{
                    log.debug({title: 'GF_MR_RICS_Purchase_Orders.js:rics_reduce', details: 'ns_po_id: ' + ns_po_id});
                    // a purchase order record was found we should update it
                    var purchase_order_record = record.load({
                        type: 'customrecord_rics_purchase_order',
                        id: ns_po_id,
                        isDynamic: true
                    });
                    // when this update changes locations remove the pod record line item connections
                    var current_storecode = purchase_order_record.getValue('custrecord_rics_po_shiptostorecode');
                    if(current_storecode != data.ns_po_rec['custrecord_rics_po_shiptostorecode']){
                        reset_pod_connection_fields(ns_po_id);
                    }
                }
                purchase_order_record.setValue('custrecord_rics_po_ns_purch_order_update',''); // new Date()
                for(var prop in data.ns_po_rec){
                    if(arr_date_props.indexOf(prop) > -1){
                        if(data.ns_po_rec[prop] != ''){
                            purchase_order_record.setValue(prop,new Date(nsrics.str_reformat_date(data.ns_po_rec[prop])));
                        }
                    }else{
                        purchase_order_record.setValue(prop,data.ns_po_rec[prop]);
                    }
                }
                var saved_po_id = purchase_order_record.save();
                log.audit({title: 'GF_MR_RICS_Purchase_Orders.js:rics_reduce', details: 'saved_po_id: ' + saved_po_id + ', data: ' + JSON.stringify(data)});
            }

            // add or update all child records
            if(data.hasOwnProperty('ns_po_details_recs') && typeof saved_po_id != 'undefined' && saved_po_id > 0){
                var arr_all_po_details = data.ns_po_details_recs;
                for(var detail in arr_all_po_details){
                    var detailData = arr_all_po_details[detail];
                    // log.debug({title: 'GF_MR_RICS_Purchase_Orders.js:rics_reduce', details: 'detailData: ' + JSON.stringify(detailData)});

                    var ns_pod_id = find_pod_record(saved_po_id, detailData.name);
                    if(ns_pod_id == 0){
                        // no tranfer order record was found we need to create one
                        var purchase_order_details_record = record.create({
                            type: 'customrecord_rics_po_details'
                        });
                        var pod_json = JSON.parse(detailData.custrecord_rics_pod_json);
                        // log.debug({title: 'GF_MR_RICS_Purchase_Orders.js:rics_reduce', details: 'pod_json: ' + JSON.stringify(pod_json)});
                        detailData.custrecord_rics_pod_product_details = find_pd_record(pod_json.ProductItem.Sku);
                        detailData.custrecord_rics_pod_product_item = find_pi_record(pod_json.ProductItem.UPC);
                    }else{
                        log.debug({title: 'GF_MR_RICS_Purchase_Orders.js:rics_reduce', details: 'ns_pod_id: ' + ns_pod_id});
                        // a tranfer order record was found we should update it
                        var purchase_order_details_record = record.load({
                            type: 'customrecord_rics_po_details',
                            id: ns_pod_id,
                            isDynamic: true
                        });
                    }
                    for(var prop in detailData){
                        if(arr_date_props.indexOf(prop) > -1){
                            if(detailData[prop] != ''){
                                purchase_order_details_record.setValue(prop,new Date(nsrics.str_reformat_date(detailData[prop])));
                            }
                        }else{
                            if((prop != 'custrecord_rics_pod_product_details' && prop != 'custrecord_rics_pod_product_item') || detailData[prop] > 0){
                                purchase_order_details_record.setValue(prop,detailData[prop]);
                            }
                        }
                    }
                    purchase_order_details_record.setValue('custrecord_rics_pod_parent_po',saved_po_id);
                    var saved_pod_id = purchase_order_details_record.save();
                    log.audit({title: 'GF_MR_RICS_Purchase_Orders.js:rics_reduce', details: 'saved_pod_id: ' + saved_pod_id + ', data: ' + JSON.stringify(detailData)});
                }
            }

        }catch(ex) {
            log.error({title: 'GF_MR_RICS_Purchase_Orders.js:rics_reduce', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * This summarize function is used to report important aspects about how the script performed
     * @param {*} summary - this object contains data regarding script performance.
     */
    function rics_summarize(summary) {
        try{
            log.audit('Usage Consumed', summary.usage);
            log.audit('Concurrency Number ', summary.concurrency);
            log.audit('Number of Yields', summary.yields);
            log.audit({title: 'Process Completed'});
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_Purchase_Orders.js:rics_summarize', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    return {
        getInputData: rics_getInputData,
        map: rics_map,
        reduce: rics_reduce,
        summarize: rics_summarize
    };

    // =========================== extra functions
    /**
     * This function uses the RICS PO Number to find the custom RICS Purchase Order record
     * @param {*} _po_order_number - string - the PO Number set by RICS or by the RICS user who created the PO
     * @returns - integer - the internal id of the custom record
     */
    function find_po_record(_po_order_number){
        var result = 0;
        // find the RICS Purchase Order Record
        var po_SearchResults_filters = [
            ["isinactive","is","F"], "AND",
            ["custrecord_rics_po_purchaseordernumber","is",_po_order_number]
        ];
        var po_SearchResults = search.create({
            type: "customrecord_rics_purchase_order",
            filters: po_SearchResults_filters,
            columns: [
                search.createColumn({
                    name: "internalid",
                    sort: search.Sort.DESC,
                    label: "ID"
                })
            ]
        }).run();
        try{
            var po_SearchResultSet = po_SearchResults.getRange({start: 0, end: 1});
            if (po_SearchResultSet != null && po_SearchResultSet.length > 0){
                result = po_SearchResultSet[0].id;
            }
        }catch(ex){
            log.error({title: 'GF_MR_RICS_Purchase_Orders.js:find_po_record', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
        // log.debug({title: 'GF_MR_RICS_Purchase_Orders.js:find_po_record', details: 'result: ' + result});
        return result;
    }

    /**
     * This function uses the internal id of the parent record and the name of the sub-record to find the custom RICS Purchase Order Detail record
     * @param {*} _po_parent - integer - the internal id of the parent record
     * @param {*} _name - string - the po details name is the combination of the RICS PO Number-SKU-UPC (i.e. 071221 GF 2416-CLASSIC-031461)
     * @returns - integer - the internal id of the custom record
     */
    function find_pod_record(_po_parent, _name){
        var result = 0;
        // find the RICS Purchase Order Details Record
        var pod_SearchResults = search.create({
            type: "customrecord_rics_po_details",
            filters: [
                ["isinactive","is","F"], "AND",
                ["custrecord_rics_pod_parent_po","is",_po_parent], "AND",
                ["name","is",_name]
            ],
            columns: [
                search.createColumn({
                    name: "internalid",
                    sort: search.Sort.DESC,
                    label: "ID"
                })
            ]
        }).run();
        var pod_SearchResults = pod_SearchResults.getRange({start: 0, end: 1});
        if (pod_SearchResults != null && pod_SearchResults.length > 0){
            result = pod_SearchResults[0].id;
        }
        // log.debug({title: 'GF_MR_RICS_Purchase_Orders.js:find_po_record', details: 'result: ' + result});
        return result;
    }

    /**
     * This function uses the sku string to locate and return the internal id of the RICS Product Details custom record.
     * @param {*} _sku - string - the suppliers sku for the product
     * @returns - integer - the internal id of the custom record
     */
    function find_pd_record(_sku){
        var result = 0;
        // find the Product Details Record
        var pd_SearchResults = search.create({
            type: "customrecord_rics_product_details",
            filters: [
                ["isinactive","is","F"], "AND",
                ["custrecord_rics_pd_sku","is",_sku]
            ],
            columns: [
                search.createColumn({
                    name: "id",
                    sort: search.Sort.DESC,
                    label: "ID"
                }),
                search.createColumn({name: "internalid", label: "Internal ID"})
            ]
        }).run();
        var pd_SearchResultSet = pd_SearchResults.getRange({start: 0, end: 1000});
        if (pd_SearchResultSet != null && pd_SearchResultSet.length > 0){
            result = pd_SearchResultSet[0].id;
        }
        // log.debug({title: 'GF_MR_RICS_Purchase_Orders.js:find_pd_record', details: 'result: ' + result});
        return result;
    }

    /**
     * This function uses the upc string to locate and return the internal id of the RICS Product Item custom record.
     * @param {*} _upc - string - the upc code for the item
     * @returns - integer - the internal id of the custom record
     */
    function find_pi_record(_upc){
        var result = 0;
        if(_upc != null && _upc != ''){
            // find the Product Details Record
            var pi_SearchResults = search.create({
                type: "customrecord_rics_product_item",
                filters: [
                    ["isinactive","is","F"], "AND",
                    ["custrecord_rics_pi_upcs","contains",'"'+_upc+'"']
                ],
                columns: [
                    search.createColumn({
                        name: "id",
                        sort: search.Sort.DESC,
                        label: "ID"
                    }),
                    search.createColumn({name: "internalid", label: "Internal ID"})
                ]
            }).run();
            var pi_SearchResultSet = pi_SearchResults.getRange({start: 0, end: 1000});
            if (pi_SearchResultSet != null && pi_SearchResultSet.length > 0){
                result = pi_SearchResultSet[0].id;
            }
        }

        return result;
    }

    /**
     * This function takes a date string and formats the date into an ISO standard date string
     * @param {*} _rics_dt_str - string - expected format is ISO standard
     * @returns - string - format.format when using the DATETIMETZ type will bring any non-ISO dates back to the expected ISO format
     */
    function format_RICS_date_string(_rics_dt_str){
        // _rics_dt_str example of expected value 2021-07-12T16:04:08
        var str_result = _rics_dt_str;

        if(_rics_dt_str.indexOf('T') == 10){
            var rics_date = format.parse({ value : _rics_dt_str, type : format.Type.DATE});
            str_result = format.format({ value : rics_date, type : format.Type.DATETIMETZ, timezone: format.Timezone.AMERICA_LOS_ANGELES});
            // log.debug({title: 'GF_MR_RICS_Purchase_Orders.js:format_RICS_date_string', details: 'str_result: ' + str_result});
        }
        return str_result;
    }

    /**
     * This function takes a RICS Purchase Order custom record id, finds all RICS Purchase Order Detail custom records generated from the PO and blanks out their PO Line Unique ID and NetSuite Purchase Order fields
     * @param {*} _source_id - integer - this is the internal ID of the RICS Purchase Order custom record
     */
     function reset_pod_connection_fields(_source_id){
        var customrecord_rics_po_detailsSearchObj = search.create({
            type: "customrecord_rics_po_details",
            filters: [
                ["custrecord_rics_pod_parent_po","anyof",_source_id]
            ],
            columns: []
        });
        customrecord_rics_po_detailsSearchObj.run().each(function(result){
            // .run().each has a limit of 4,000 results
            var rics_pod_Id = record.submitFields({
                type: 'customrecord_rics_po_details',
                id: result.id,
                values: {
                    'custrecord_rics_pod_line_unique_id': '',
                    'custrecord_rics_pod_ns_po': ''
                }
            });
            return true;
        });
    }

    /**
     * This function calls out to the data warehouse to get a list of the po updates
     */
    function getUpdatedRicsPoNumbers(){
      	var aws_max_attempt = 3;
        var aws_current_attempt = 0;
        var arr_result = [];
        // Call to AWS to get list of updated PO's
        var aws_header = [];
        aws_header['Content-Type'] = 'application/json';
        aws_header['x-api-key'] = 'A6xeIKveD1nBRIuKQ75g7IjxLsEFh4t73Dszaa5b';
        var aws_postData = { "acct_num": "", "query": "2" };
        // log.debug({title: 'GF_MR_RICS_Purchase_Orders.js:getUpdatedRicsPoNumbers', details: 'aws_postData: ' + JSON.stringify(aws_postData)});
        aws_postData = JSON.stringify(aws_postData);
        var endpoint = 'https://hg06i49148.execute-api.us-west-2.amazonaws.com/test/getAccount';
		do{
            var aws_response = https.post({
                url: endpoint,
                body: aws_postData,
                headers: aws_header
            });
            if(aws_response != undefined && aws_response['code'] != 200){
                aws_current_attempt += 1;
            }else{
                if (aws_response != undefined && aws_response['code'] == 200) {
                    log.debug({title: 'GF_MR_RICS_Purchase_Orders.js:getUpdatedRicsPoNumbers', details: 'aws_response: ' + JSON.stringify(aws_response)});
                    var res_body = JSON.parse(aws_response.body);
                    for(var rows in res_body){
                        var po_num = res_body[rows].RICS_PO_PONumber;
                        var pk_num = res_body[rows].pos_rics_pk;
                        arr_result.push({"po": po_num, "pk": pk_num});
                    }
                }
            }
        }while(aws_current_attempt < aws_max_attempt && (aws_response == undefined || aws_response['code'] != 200) );
        log.debug({title: 'GF_MR_RICS_Purchase_Orders.js:getUpdatedRicsPoNumbers', details: 'arr_result: ' + JSON.stringify(arr_result)});

        return arr_result;
    }

    /**
     * This function calls out to the data warehouse to set the po as processed
     */
    function updatedProcessedRicsPoNumber(_po_num){
      	var aws_max_attempt = 3;
        var aws_current_attempt = 0;
        var result = 0;
        // Call to AWS to get list of updated PO's
        var aws_header = [];
        aws_header['Content-Type'] = 'application/json';
        aws_header['x-api-key'] = 'A6xeIKveD1nBRIuKQ75g7IjxLsEFh4t73Dszaa5b';
        var aws_postData = { "acct_num": "", "query": "3", "po_num": _po_num };
        // log.debug({title: 'GF_MR_RICS_Purchase_Orders.js:getUpdatedRicsPoNumbers', details: 'aws_postData: ' + JSON.stringify(aws_postData)});
        aws_postData = JSON.stringify(aws_postData);
        var endpoint = 'https://hg06i49148.execute-api.us-west-2.amazonaws.com/test/getAccount';
      	var aws_response = https.post({
        	url: endpoint,
        	body: aws_postData,
        	headers: aws_header
      	});
        log.debug({title: 'GF_MR_RICS_Purchase_Orders.js:updatedProcessedRicsPoNumber', details: '_po_num: ' + _po_num});

        return arr_result;
    }

});