/**
 *@NApiVersion 2.1
 *@NScriptType MapReduceScript
 *
Script Name:   GF_MR_CustomerSetEmployeeLink.js
Author:        Mark Robinson
 */

define(['N/log', 'N/search', 'N/record'], function(log, search, record) {

    /**
     * This get input function loads a search with all customer records where the employee record link is empty, the customer has not yet been marked as not an employee, and the last name is not empty
     * @param {*} context  - this variable is not utilized in this function
     * @returns - an array containing the full responses from the custom record search
     */
    function gf_getInputData(context){
        try {
            var employeeSearchObj = search.create({
                type: "employee",
                filters: [
                    ["isinactive","is","F"]
                ],
                columns: [
                    search.createColumn({
                        name: "entityid",
                        sort: search.Sort.ASC
                    }),
                    "firstname",
                    "lastname",
                    "mobilephone",
                    "email"
                ]
            });
            employeeSearchObj.run().each(function(result){
                // .run().each has a limit of 4,000 results
                return true;
            });

            return employeeSearchObj;
        }catch(ex) {
            log.error({title: 'GF_MR_CustomerSetEmployeeLink.js:gf_getInputData', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this mapping function we search for the employee record where 
     * @param {*} context - Object - This should be the RICS NonSellable Batch custom record
     * @returns void (note: the const context variable is impacted and used in the reduce function)
     */
    function gf_map(context){
        try{
            // simple pass through
            var map_data = JSON.parse(context.value);
            log.audit({title: 'GF_MR_CustomerSetEmployeeLink.js:gf_map', details: 'map_data: ' + JSON.stringify(map_data)});
			if (map_data == null) {
				return true;
			}else{
                try{
                    // read map data and match last name and first initial
                    var count_customer = 0;
                    var str_firstname = map_data.values.firstname; // result.getValue({ name: "firstname" });
                    var str_firstinitial = str_firstname.substring(0,1);
                    var str_lastname = map_data.values.lastname; // result.getValue({ name: "lastname" });
                    log.debug({title: 'GF_MR_CustomerSetEmployeeLink.js:gf_map', details: 'str_firstname: ' + str_firstname + ', str_firstinitial: ' + str_firstinitial + ', str_lastname: ' + str_lastname});
                    var str_phone = map_data.values.mobilephone;
                    str_phone = str_phone.replace(/\D/g,'');
                    var first_three = str_phone.substring(0,3);
                    var last_four = str_phone.substring(0,-4);
                    var str_email = map_data.values.email;

                    var customerSearchObj = search.create({
                        type: "customer",
                        filters: [
                            ["custentity_gf_employee_link","anyof","@NONE@"], "AND",
                            ["custentity_gf_notanemployee","is","F"], "AND",
                            ["lastname","is",str_lastname], "AND",
                            ["firstname","startsWith",str_firstinitial]
                        ],
                        columns: [
                            search.createColumn({
                                name: "entityid",
                                sort: search.Sort.ASC
                            }),
                            "altname",
                            "firstname",
                            "lastname",
                            "email",
                            "phone"
                        ]
                    });
                    customerSearchObj.run().each(function(result){
                        // .run().each has a limit of 4,000 results
                        var match_weight = 0;
                        var customer_phone = result.getValue('phone');
                        if(str_firstname == result.getValue('firstname')){
                            // first name full match
                            match_weight += 1;
                            log.audit({title: 'GF_MR_CustomerSetEmployeeLink.js:gf_map-match_weight', details: 'FIRST NAME match_weight: ' + match_weight + ', str_firstname: ' + str_firstname + ', cust_firstname: ' + result.getValue('firstname')});
                        }
                        if(customer_phone.indexOf(first_three) > -1 && customer_phone.indexOf(last_four) > -1){
                            // phone number digits match
                            match_weight += 1;
                            log.audit({title: 'GF_MR_CustomerSetEmployeeLink.js:gf_map-match_weight', details: 'PHONE match_weight: ' + match_weight + ', str_phone: ' + str_phone + ', customer_phone: ' + customer_phone});
                        }
                        if(str_email == result.getValue('email')){
                            // email match
                            match_weight += 1;
                            log.audit({title: 'GF_MR_CustomerSetEmployeeLink.js:gf_map-match_weight', details: 'EMAIL match_weight: ' + match_weight + ', str_email: ' + str_email + ', cust_email: ' + result.getValue('email')});
                        }

                        if(match_weight > 1){
                            // the last name matches, the first name starts with the same letter, and we have at least one other data point that matches the employee record
                            // set the customer employee record link
                            var customer_update_id = record.submitFields({
                                type: 'customer',
                                id: result.id,
                                values: { custentity_gf_employee_link: map_data.id },
                                options: { enableSourcing: false, ignoreMandatoryFields: true }
                            });
                        }else{
                            // the last name matches but nothing else - we don't want to compare this again
                            // set the not an employee flag
                            var customer_update_id = record.submitFields({
                                type: 'customer',
                                id: result.id,
                                values: { custentity_gf_notanemployee: true },
                                options: { enableSourcing: false, ignoreMandatoryFields: true }
                            });
                        }

                        return true;
                    });
                }catch(ex){
                    log.error({title: 'GF_MR_CustomerSetEmployeeLink.js:gf_map-onSave', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
                }
            }

			return true;
        }catch(ex) {
            log.error({title: 'GF_MR_CustomerSetEmployeeLink.js:gf_map', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this reduce function we do nothing
     * @param {*} context - Object - This is the object that was built in the mapping function, it should conatin all the data we need for processing
     * @returns - void
     */
    function gf_reduce(context){
        return true;
    }

    /**
     * This summarize function is used to report important aspects about how the script performed
     * @param {*} summary - this object contains data regarding script performance.
     */
    function gf_summarize(summary) {
        try{
            log.audit('Usage Consumed', summary.usage);
            log.audit('Concurrency Number ', summary.concurrency);
            log.audit('Number of Yields', summary.yields);
            log.audit({title: 'Process Completed'});
        }catch(ex) {
            log.error({title: 'GF_MR_CustomerSetEmployeeLink.js:gf_summarize', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    return {
        getInputData: gf_getInputData,
        map: gf_map,
        reduce: gf_reduce,
        summarize: gf_summarize
    };

    // =========================== extra functions

});