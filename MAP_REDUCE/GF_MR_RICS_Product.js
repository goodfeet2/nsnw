/**
 *@NApiVersion 2.1
 *@NScriptType MapReduceScript
 *
Script Name:   GF_MR_RICS_Product.js
Author:        Mark Robinson
 */

define(['N/log', 'N/search', 'N/record', 'N/format', 'N/runtime'], function(log, search, record, format, runtime) {

    /**
     * In this get input function we use a search for either the passed in script parameter which should be the id of a RICS Product Details record, or
     * we use a search for RICS Product Details records, and the results are set to objects that are all added to an array that is passed to the mapping function.
     * @param {*} context 
     * @returns 
     */
    function rics_getInputData(context){
        // set ns item id by supplier SKU (where the inventory item has no parent)
        matchExistingItemsToRICS_ProductDetails();

        //Get all product details data from the NS Product Details records
        try {
            var currentScript = runtime.getCurrentScript();
            var pd_id = currentScript.getParameter({ name: 'custscript_product_details_id' });
            if(pd_id != null){
                // process against a single product details record
                var pd_record_filters = [
                    ["internalid","anyof",pd_id], "AND",
                    ["isinactive","is","F"], "AND",
                    ["custrecord_rics_pd_json","doesnotcontain","\"DeletedOn\":\"2"], "AND",
                    [
                        ["custrecord_rics_pd_avail_pos_on","onorbefore","today"],"OR",
                        ["custrecord_rics_pd_avail_pos_on","isempty",""]
                    ]
                ];
            }else{
                // get as many as possible
                var pd_record_filters = [
                    [
                        ["custrecord_rics_pd_ns_item_id","anyof","@NONE@"],"OR",
                        [
                            ["custrecord_rics_pd_ns_item_update","isempty",""],"OR",
                            ["custrecord_rics_pd_ns_item_update","onorbefore","hoursago3"]
                        ]
                    ], "AND",
                    ["isinactive","is","F"], "AND",
                    ["custrecord_rics_pd_json","doesnotcontain","\"DeletedOn\":\"2"], "AND",
                    [
                        ["custrecord_rics_pd_avail_pos_on","onorbefore","yearsfromnow1"],"OR",
                        ["custrecord_rics_pd_avail_pos_on","isempty",""]
                    ]
                ];
            }

            var arr_all_data = [];
            var runsearch_pd_records = search.create({
                type: "customrecord_rics_product_details",
                filters: pd_record_filters,
                columns: [
                    search.createColumn({name: "custrecord_rics_pd_alert", label: "Alert"}),
                    search.createColumn({name: "custrecord_rics_pd_avail_pos_on", label: "Available To POS On"}),
                    search.createColumn({name: "custrecord_rics_pd_classes", label: "Classes"}),
                    search.createColumn({name: "custrecord_rics_pd_colors", label: "Colors"}),
                    search.createColumn({name: "custrecord_rics_pd_column_name", label: "Column Name"}),
                    search.createColumn({name: "custrecord_rics_pd_created_on", label: "Created On"}),
                    search.createColumn({name: "custrecord_rics_pd_description", label: "Description"}),
                    search.createColumn({
                        name: "internalid",
                        sort: search.Sort.DESC,
                        label: "Internal ID"
                    }),
                    search.createColumn({name: "custrecord_rics_pd_labeltype", label: "Label Type"}),
                    search.createColumn({name: "custrecord_rics_pd_ns_item_id", label: "NS Item Record"}),
                    search.createColumn({name: "custrecord_rics_pd_row_name", label: "Row Name"}),
                    search.createColumn({name: "custrecord_rics_pd_sku", label: "SKU"}),
                    search.createColumn({name: "custrecord_rics_pd_style", label: "Style"}),
                    search.createColumn({name: "custrecord_rics_pd_summary", label: "Summary"}),
                    search.createColumn({name: "custrecord_rics_pd_suppliercode", label: "Supplier Code"}),
                    search.createColumn({name: "custrecord_rics_pd_suppliername", label: "Supplier Name"}),
                    search.createColumn({name: "custrecord_rics_pd_suppliersku", label: "Supplier SKU"}),
                    search.createColumn({
                        name: "type",
                        join: "CUSTRECORD_RICS_PD_NS_ITEM_ID",
                        label: "Type"
                    }),
                    search.createColumn({name: "custrecord_rics_pd_pricing", label: "Pricing"})
                ]
            }).run();
            for (var k = 0; k < 2; k++) { // max pages 2, up to 2,000 records total
                var results = runsearch_pd_records.getRange({start: k * 1000, end: (k * 1000) + 1000});
                if (results.length === 0){ break; } // if the results of the page are 0 then exit
                for (var i = 0; i < results.length; i++) {
                    var rec_obj = {
                        "NS_PD_ID": results[i].id,
                        "NS_ITEM_TYPE": results[i].getValue({name: 'type', join: 'CUSTRECORD_RICS_PD_NS_ITEM_ID'}),
                        "NS_ITEM_ID": results[i].getValue('custrecord_rics_pd_ns_item_id'),
                        "Sku": results[i].getValue('custrecord_rics_pd_sku'),
                        "Style": results[i].getValue('custrecord_rics_pd_style'),
                        "Summary": results[i].getValue('custrecord_rics_pd_summary'),
                        "Description": results[i].getValue('custrecord_rics_pd_description'),
                        "Alert": results[i].getValue('custrecord_rics_pd_alert'),
                        "SupplierSku": results[i].getValue('custrecord_rics_pd_suppliersku'),
                        "AvailableToPOSOn": results[i].getValue('custrecord_rics_pd_avail_pos_on'),
                        "CreatedOn": results[i].getValue('custrecord_rics_pd_created_on'),
                        "SupplierCode": results[i].getValue('custrecord_rics_pd_suppliercode'),
                        "SupplierName": results[i].getValue('custrecord_rics_pd_suppliername'),
                        "LabelType": results[i].getValue('custrecord_rics_pd_labeltype'),
                        "ColumnName": results[i].getValue('custrecord_rics_pd_column_name'),
                        "RowName": results[i].getValue('custrecord_rics_pd_row_name'),
                        "Colors": results[i].getValue('custrecord_rics_pd_colors'),
                        "Classes": results[i].getValue('custrecord_rics_pd_classes'),
                        "Pricing": results[i].getValue('custrecord_rics_pd_pricing')
                    };
                    // search for all related product items
                    var runsearch_pi_records = search.create({
                        type: "customrecord_rics_product_item",
                        filters: [
                            ["custrecord_rics_pi_parent_pd","is",results[i].id], "AND",
                            ["isinactive","is","F"]
                        ],
                        columns: [
                            search.createColumn({name: "custrecord_rics_pi_json", label: "JSON"})
                        ]
                    }).run();
                    var arr_all_product_items = [];
                    var pi_results = runsearch_pi_records.getRange({start: k * 1000, end: (k * 1000) + 1000});
                    for (var pi = 0; pi_results != null && pi < pi_results.length; pi++) {
                        var str_json = pi_results[pi].getValue('custrecord_rics_pi_json');
                        arr_all_product_items.push(str_json);
                    }
                    rec_obj.product_items = arr_all_product_items;
                    arr_all_data.push(rec_obj);
                }
            }
            return arr_all_data;
        }catch(ex) {
            log.error({title: 'Error: GF_MR_RICS_Product.js:rics_getInputData', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this mapping function we take the data passed in from the get input function and determine the kind of item we have to work with, services (like the bag fee), non-matrix items, or matrix items.
     * Services do not have any child records and can be built without adding them to the reduce process, while the matrix items will have a child for each combination of grid row and column, as long as
     * a UPC exists in RICS, when there is no UPC the item is not added by the GF_MR_RICS_Product_Details.js script.
     * This function detects if the NetSuite inventory item related to the RICS Product Details custom record exists, when it does the item record is updated, otherwise the new inventory item is built.
     * If the pricing information exists on the RICS Product Details custom record the pricing is also set on the inventory item record.
     * @param {*} context - object - this should contain all of the data set by the RICS Product Details custom record search done in the get input data function.
     * @returns void (note: the const context variable is written to and used in the reduce function)
     */
    function rics_map(context){
        try{
            var data = JSON.parse(context.value);
            // log.debug({title: 'GF_MR_RICS_Product.js:rics_map', details: 'item-id: ' + data.NS_ITEM_ID + ', pd-id: ' + data.NS_PD_ID});
            log.debug({title: 'GF_MR_RICS_Product.js:rics_map', details: 'data: ' + JSON.stringify(data)});

            // NonInventory + NonMatrix services will be identified by a special character(s)
            // if(data.ColumnName == '' && data.RowName == '' && data.Sku == 'BAG FEE'){
            if((data.Summary).indexOf('**') > -1 || data.NS_ITEM_TYPE == 'NonInvtPart'){
                log.debug({title: 'GF_MR_RICS_Product.js:rics_map', details: 'Non-Inventory-Item item-id: ' + data.NS_ITEM_ID + ', pd-id: ' + data.NS_PD_ID});
                var nonInv_NS_ID = processNonInventoryItem(data);
                if(nonInv_NS_ID > 0){
                    val_obj = { 'custrecord_rics_pd_ns_item_id': nonInv_NS_ID, 'custrecord_rics_pd_ns_item_update': dt_now };
                }else{
                    val_obj = { 'custrecord_rics_pd_ns_item_update': dt_now };
                }
                // update the PD record
                var dt_now = format.format({ value: new Date(), type: format.Type.DATETIME });
                var pd_update_id = record.submitFields({
                    type: 'customrecord_rics_product_details',
                    id: data.NS_PD_ID,
                    values: val_obj
                });
                // context.write({ key: context.key, value: data }); // do not process this through the reduce as it will not have any child records
                return true;
            }
            if(data.ColumnName == '' || !data.hasOwnProperty('ColumnName')){
                data.ColumnName = 'NA';
            }
            if(data.RowName == '' || !data.hasOwnProperty('RowName')){
                data.RowName = 'NA';
            }

            var ns_item_class_data = get_netsuite_item_class(data.Classes);
            // log.debug({title: 'GF_MR_RICS_Product.js:rics_map', details: 'ns_item_class_data: ' + JSON.stringify(ns_item_class_data)});

            if(data.NS_ITEM_ID != ''){
                // update existing inventory item =================================================================================================== update existing inventory item
                var parent_Item = create_item_obj('', ns_item_class_data.classname_rics, data, null, ns_item_class_data.classid_ns);
                var update_parent_item = record.load({
                    type: 'inventoryitem',
                    id: data.NS_ITEM_ID,
                    isDynamic: true
                });
                for(var prop in parent_Item){
                    update_parent_item.setValue(prop,parent_Item[prop]);
                }
                update_parent_item.setValue('subsidiary',["3","4","2","7"]);
                update_parent_item.setValue('itemid', data.Sku);

                // use the UPC codes from RICS to get the pricing data
                var pricing_data = (data.hasOwnProperty('Pricing') && data.Pricing != '') ? JSON.parse(data.Pricing) : {};
                var arr_row = [];
                var arr_col = [];
                var arr_prod_colors = getListValues('customlist_psgss_product_color'); // get internal id of each product color
                var arr_prod_sizes = getListValues('customlist_psgss_product_size'); // get internal id of each product size
                for(var pi in data.product_items){
                    var prod_item_data = JSON.parse(data.product_items[pi]);
                    if(prod_item_data.Row == '' && prod_item_data.Column == ''){ continue; }
                    // log.debug({title: 'GF_MR_RICS_Product.js:rics_map-update_existing', details: 'prod_item_data: ' + JSON.stringify(prod_item_data)} );
                    if(prod_item_data.hasOwnProperty('UPC') && typeof prod_item_data.UPC != 'undefined' && prod_item_data.UPC != ''){
                        // log.debug({title: 'GF_MR_RICS_Product.js:rics_map-update_existing', details: 'prod_item_data.Row: ' + prod_item_data.Row + ', NS_id: ' + arr_prod_colors[prod_item_data.Row] + ', arr_row index: ' + arr_row.indexOf(arr_prod_colors[prod_item_data.Row]) } );
                        if(arr_prod_colors[prod_item_data.Row] != undefined && arr_row.indexOf(arr_prod_colors[prod_item_data.Row]) == -1){
                            arr_row.push(arr_prod_colors[prod_item_data.Row]); 
                        }else{
                            if(arr_row.indexOf(arr_prod_colors[prod_item_data.Row]) == -1 && prod_item_data.Row != null && arr_prod_colors[prod_item_data.Row] == undefined){
                                // add the color/row to the custom list
                                arr_prod_colors = setNewListValue('customlist_psgss_product_color', prod_item_data.Row);
                                // add the color/row to the parent record
                                arr_row.push(arr_prod_colors[prod_item_data.Row]);
                            }
                        }
                        // log.debug({title: 'GF_MR_RICS_Product.js:rics_map-update_existing', details: 'prod_item_data.Column: ' + prod_item_data.Column + ', NS_id: ' + arr_prod_sizes[prod_item_data.Column] + ', arr_col index: ' + arr_col.indexOf(arr_prod_sizes[prod_item_data.Column])} );
                        if(arr_prod_sizes[prod_item_data.Column] != undefined && arr_col.indexOf(arr_prod_sizes[prod_item_data.Column]) == -1){
                            arr_col.push(arr_prod_sizes[prod_item_data.Column]);
                        }else{
                            if(arr_col.indexOf(arr_prod_sizes[prod_item_data.Column]) == -1 && prod_item_data.Column != null && arr_prod_sizes[prod_item_data.Column] == undefined){
                                // add the color/row to the custom list
                                arr_prod_sizes = setNewListValue('customlist_psgss_product_size', prod_item_data.Column);
                                // add the color/row to the parent record
                                arr_col.push(arr_prod_sizes[prod_item_data.Column]);
                            }
                        }
                    }
                }
                // get all of the currently existing values (if there are any)
                var arr_colors_orig = update_parent_item.getValue('custitem_psgss_product_color');
                for(var color in arr_colors_orig){ arr_row.push(arr_colors_orig[color]); }
                var arr_sizes_orig = update_parent_item.getValue('custitem_psgss_product_size');
                for(var size in arr_sizes_orig){ arr_col.push(arr_sizes_orig[size]); }
                if(arr_row.length == 0){ arr_row.push("126"); }
                // set row values - color
                update_parent_item.setValue('custitem_psgss_product_color', arr_row);
                if(arr_col.length == 0){ arr_col.push("378"); }
                // set column values - size
                update_parent_item.setValue('custitem_psgss_product_size', arr_col);

                try{
                    setVendor(data.SupplierCode, update_parent_item);
                }catch(ex){
                    // error saving vendor
                }
                if(pricing_data.hasOwnProperty('Cost')){
                    update_parent_item.setValue('cost', pricing_data.Cost);
                    update_parent_item.setValue('custitem_gf_msrp', pricing_data.RetailPrice);
                    setPricingLines(pricing_data, update_parent_item);
                }

                update_parent_item.save();
            }else{
                // create parent and all children records =================================================================================================== create parent and all children records
                var parent_Item = create_item_obj('YES', ns_item_class_data.classname_rics, data, null, ns_item_class_data.classid_ns);
                var create_parent_item = record.create({
                    type: 'inventoryitem',
                    isDynamic: true
                });
                for(var prop in parent_Item){
                    create_parent_item.setValue(prop,parent_Item[prop]);
                }
                create_parent_item.setValue('subsidiary',["3","4","2","7"]);
                create_parent_item.setValue('itemid', data.Sku);

                // use the UPC codes from RICS to get the pricing data
                var pricing_data = (data.hasOwnProperty('Pricing') && data.Pricing != '') ? JSON.parse(data.Pricing) : {};
                var arr_row = [];
                var arr_col = [];
                var arr_prod_colors = getListValues('customlist_psgss_product_color'); // get internal id of each product color
                var arr_prod_sizes = getListValues('customlist_psgss_product_size'); // get internal id of each product size
                // create a list of missing ListValues
                var arr_add_these_rows = [];
                var arr_add_these_cols = [];
                for(var pi in data.product_items){
                    var prod_item_data = JSON.parse(data.product_items[pi]);
                    if(prod_item_data.hasOwnProperty('Row') && (prod_item_data.Row).length > 0 && arr_prod_colors[prod_item_data.Row] == undefined){
                        if(arr_add_these_rows.indexOf(prod_item_data.Row) == -1){
                            arr_add_these_rows.push(prod_item_data.Row);
                        }
                    }
                    if(prod_item_data.hasOwnProperty('Column') && (prod_item_data.Column).length > 0 && arr_prod_sizes[prod_item_data.Column] == undefined){
                        if(arr_add_these_cols.indexOf(prod_item_data.Column) == -1){
                            arr_add_these_cols.push(prod_item_data.Column);
                        }
                    }
                }
                // add the values to the lists
                var arr_add_these_rows = arr_add_these_rows.filter(function (el) { return el != null; });
                var arr_add_these_cols = arr_add_these_cols.filter(function (el) { return el != null; });
                log.debug({title: 'GF_MR_RICS_Product.js:rics_map-create_new', details: 'new rows arr_add_these_rows: ' + arr_add_these_rows} );
                log.debug({title: 'GF_MR_RICS_Product.js:rics_map-create_new', details: 'new cols arr_add_these_cols: ' + arr_add_these_cols} );

                if(arr_add_these_rows.length > 0){
                    setNewListValues('customlist_psgss_product_color', arr_add_these_rows);
                }
                if(arr_add_these_cols.length > 0){
                    setNewListValues('customlist_psgss_product_size', arr_add_these_cols);
                }
                // get the lists again
                arr_prod_colors = getListValues('customlist_psgss_product_color'); // get internal id of each product color
                arr_prod_sizes = getListValues('customlist_psgss_product_size'); // get internal id of each product size

                for(var pi in data.product_items){
                    var prod_item_data = JSON.parse(data.product_items[pi]);
                    if(prod_item_data.Row == '' && prod_item_data.Column == ''){ continue; }
                    log.debug({title: 'GF_MR_RICS_Product.js:rics_map-create_new', details: 'prod_item_data: ' + JSON.stringify(prod_item_data)} );
                    if(prod_item_data.hasOwnProperty('UPC') && typeof prod_item_data.UPC != 'undefined' && prod_item_data.UPC != ''){
                        // log.debug({title: 'GF_MR_RICS_Product.js:rics_map-create_new', details: 'prod_item_data.Row: ' + prod_item_data.Row + ', NS_id: ' + arr_prod_colors[prod_item_data.Row] + ', arr_row index: ' + arr_row.indexOf(arr_prod_colors[prod_item_data.Row]) } );
                        if(arr_prod_colors[prod_item_data.Row] != undefined && arr_row.indexOf(arr_prod_colors[prod_item_data.Row]) != -1 && arr_prod_colors[prod_item_data.Row] != null){
                            arr_row.push(arr_prod_colors[prod_item_data.Row]);
                        }else{
                            if(arr_row.indexOf(arr_prod_colors[prod_item_data.Row]) == -1 && prod_item_data.Row != null && arr_prod_colors[prod_item_data.Row] == undefined && arr_prod_colors[prod_item_data.Row] != null){
                                // add the color/row to the custom list
                                arr_prod_colors = setNewListValue('customlist_psgss_product_color', prod_item_data.Row);
                                // add the color/row to the parent record
                                arr_row.push(arr_prod_colors[prod_item_data.Row]);
                            }
                        }
                        // log.debug({title: 'GF_MR_RICS_Product.js:rics_map-create_new', details: 'prod_item_data.Column: ' + prod_item_data.Column + ', NS_id: ' + arr_prod_sizes[prod_item_data.Column] + ', arr_col index: ' + arr_col.indexOf(arr_prod_sizes[prod_item_data.Column])} );
                        if(arr_prod_sizes[prod_item_data.Column] != undefined && arr_col.indexOf(arr_prod_sizes[prod_item_data.Column]) != -1 && arr_prod_sizes[prod_item_data.Column] != null){
                            arr_col.push(arr_prod_sizes[prod_item_data.Column]);
                        }else{
                            if(arr_col.indexOf(arr_prod_sizes[prod_item_data.Column]) == -1 && prod_item_data.Column != null && arr_prod_sizes[prod_item_data.Column] == undefined && arr_prod_sizes[prod_item_data.Column] != null){
                                // add the color/row to the custom list
                                arr_prod_sizes = setNewListValue('customlist_psgss_product_size', prod_item_data.Column);
                                // add the color/row to the parent record
                                arr_col.push(arr_prod_sizes[prod_item_data.Column]);
                            }
                        }
                    }
                }
                if(arr_row.length == 0){ arr_row.push("126"); }
                // set row values - color
                log.debug({title: 'GF_MR_RICS_Product.js:rics_map-product_color', details: 'set row: ' + JSON.stringify(arr_row)} );
                create_parent_item.setValue('custitem_psgss_product_color', arr_row);
                if(arr_col.length == 0){ arr_col.push("378"); }
                // set column values - size
                log.debug({title: 'GF_MR_RICS_Product.js:rics_map-product_size', details: 'set col: ' + JSON.stringify(arr_col)} );
                create_parent_item.setValue('custitem_psgss_product_size', arr_col);

                var id_new_NS_item = create_parent_item.save();
                data.NS_ITEM_ID = id_new_NS_item;
                // update the NS Product Details Record
                var pd_update_id = record.submitFields({
                    type: 'customrecord_rics_product_details',
                    id: data.NS_PD_ID,
                    values: { 'custrecord_rics_pd_ns_item_id': id_new_NS_item }
                });


                var update_parent_item = record.load({
                    type: 'inventoryitem',
                    id: id_new_NS_item,
                    isDynamic: true
                });

                // get vendor id
                setVendor(data.SupplierCode, update_parent_item);

                if(pricing_data.hasOwnProperty('Cost')){
                    update_parent_item.setValue('cost', pricing_data.Cost);
                    update_parent_item.setValue('custitem_gf_msrp', pricing_data.RetailPrice);
                    setPricingLines(pricing_data, update_parent_item);
                }

                update_parent_item.save();

            }

            data.PricingData = pricing_data;

            var dt_now = format.format({ value: new Date(), type: format.Type.DATETIME });
            var pd_update_id = record.submitFields({
                type: 'customrecord_rics_product_details',
                id: data.NS_PD_ID,
                values: { 'custrecord_rics_pd_ns_item_update': dt_now }
            });

            data.childItems = {};
            var runsearch_inventoryItem_records = search.create({
                type: "inventoryitem",
                filters: [
                    ["type","anyof","InvtPart"], "AND", 
                    // ["isinactive","is","F"], "AND", 
                    ["parent","anyof",data.NS_ITEM_ID]
                ],
                columns: [
                    search.createColumn({name: "upccode"})
                ]
            });
            runsearch_inventoryItem_records.run().each(function(result){
                // - save them to an array of objects containing only { "ID": ns_id, "UPC": upc code }
                var str_item_upc = result.getValue('upccode');
                data.childItems[str_item_upc] = result.id;
                // log.debug({title: 'GF_MR_RICS_Product.js:rics_map', details: 'add upc: ' + num_item_upc + ', id: ' + result.id});
                return true; // this is a required line for the anonymous function
            });
            // log.debug({title: 'GF_MR_RICS_Product.js:rics_map', details: 'data.childItems: ' + JSON.stringify(data.childItems)});

            context.write({ key: context.key, value: data });

        }catch(ex) {
            log.error({title: 'Error: GF_MR_RICS_Product.js:rics_map', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
            // log.error({title: 'Error: GF_MR_RICS_Product.js:rics_map', details: 'Error ' + JSON.stringify(data)});
            if((ex.toString() + ' : ' + ex.stack).indexOf('"DUP_ITEM"') > -1){
                log.debug({title: 'Error: GF_MR_RICS_Product.js:rics_map', details: 'Sku: ' + data.Sku + ', Summary: ' + data.Summary + ', SupplierSku: ' + data.SupplierSku + ', NS_ITEM_ID: ' + data.NS_ITEM_ID});
            }
        }
    }

    /**
     * In this reduce function we take the mapped context objects and create or update all of the child NetSuite Inventory Item records.
     * @param {*} context - object - this should be the same data passed through the mapping function.
     */
    function rics_reduce(context){
        //Process the sub-matrix items
        try{
            // log.debug({title: 'GF_MR_RICS_Product.js:rics_reduce', details: 'context: ' + context + ', json: ' + JSON.stringify(context)});
            var data = JSON.parse(context.values[0]);
            log.debug({title: 'GF_MR_RICS_Product.js:rics_reduce', details: 'data: ' + JSON.stringify(data)});
            // log.debug({title: 'GF_MR_RICS_Product.js:rics_reduce', details: 'NS_PD_ID: ' + data.NS_PD_ID});

            var ns_item_class_data = get_netsuite_item_class(data.Classes);
            // log.debug({title: 'GF_MR_RICS_Product.js:rics_reduce', details: 'parent PD-ID: ' + data.NS_PD_ID + ', parent ITEM-ID: ' + data.NS_ITEM_ID});
            // log.debug({title: 'GF_MR_RICS_Product.js:rics_reduce', details: 'childItems: ' + JSON.stringify(data.childItems)});

            for(var item in data.product_items){
                var data_product_item = JSON.parse(data.product_items[item]); // after saving the parent this is how to loop through the children
                log.debug({title: 'GF_MR_RICS_Product.js:rics_reduce', details: 'parent PD-ID: ' + data.NS_PD_ID + ', data_product_item: ' + JSON.stringify(data_product_item)});
                // (even if the parent was created that does not mean that we have all of the child records created -- determine if they exist by checking the childItems list property)
                var num_matching_upc_ns_id = (data.childItems.hasOwnProperty(data_product_item.UPC)) ? data.childItems[data_product_item.UPC.toString()] : -1;
                log.debug({title: 'GF_MR_RICS_Product.js:rics_reduce', details: 'num_matching_upc_ns_id: ' + num_matching_upc_ns_id + ', UPC: ' + data_product_item.UPC + ', childItems: ' + JSON.stringify(data.childItems)});
                if(num_matching_upc_ns_id == -1){
                    var arr_prod_colors = getListValues('customlist_psgss_product_color'); // get internal id of each product color
                    var arr_prod_sizes = getListValues('customlist_psgss_product_size'); // get internal id of each product size
                    var id_color_row = '126';
                    var id_size_column = '378';
                    if(data_product_item.hasOwnProperty('Row') && arr_prod_colors[data_product_item.Row] != undefined){
                        id_color_row = arr_prod_colors[data_product_item.Row];
                    }
                    if(data_product_item.hasOwnProperty('Column') && arr_prod_sizes[data_product_item.Column] != undefined){
                        id_size_column = arr_prod_sizes[data_product_item.Column];
                    }
                    // the UPC might not be the same, find the ns id using the item options
                    num_matching_upc_ns_id = findSubItemByMatrixOptions(data.Sku, id_color_row, id_size_column);
                    log.audit({title: 'GF_MR_RICS_Product.js:rics_reduce', details: 'num_matching_upc_ns_id: ' + num_matching_upc_ns_id + ', id_color_row: ' + id_color_row + ', id_size_column: ' + id_size_column});
                }
                if(num_matching_upc_ns_id != -1){
                    // update
                    var child_Item = create_item_obj('NO', ns_item_class_data.classname_rics, data, data_product_item.UPC, ns_item_class_data.classid_ns);
                    var update_child_item = record.load({
                        type: 'inventoryitem',
                        id: num_matching_upc_ns_id,
                        isDynamic: true
                    });
                    for(var prop in child_Item){
                        update_child_item.setValue(prop,child_Item[prop]);
                    }
                    var str_name = data.Sku;
                    str_name += (data_product_item.hasOwnProperty('Row')) ? '-' + data_product_item.Row : '';
                    str_name += (data_product_item.hasOwnProperty('Column')) ? '-' + data_product_item.Column : '';
                    update_child_item.setValue('itemid', str_name);
                    update_child_item.setValue('subsidiary',["3","4","2","7"]);
                    // update_child_item.setValue('usebins',true);

                    setVendor(data.SupplierCode, update_child_item);

                    update_child_item.save();

                }else{
                    // create
                    var child_Item = create_item_obj('NO', ns_item_class_data.classname_rics, data, data_product_item.UPC, ns_item_class_data.classid_ns);
                    var create_child_item = record.create({
                        type: 'inventoryitem',
                        isDynamic: true
                    });
                    for(var prop in child_Item){
                        create_child_item.setValue(prop,child_Item[prop]);
                    }
                    create_child_item.setValue('subsidiary',["3","4","2","7"]);
                    // create_child_item.setValue('usebins',true);
                    var str_name = data.Sku;
                    str_name += (data_product_item.hasOwnProperty('Row')) ? '-' + data_product_item.Row : '';
                    str_name += (data_product_item.hasOwnProperty('Column')) ? '-' + data_product_item.Column : '';
                    create_child_item.setValue('itemid', str_name);
                    create_child_item.setValue('displayname', str_name);
                    create_child_item.setValue('parent', data.NS_ITEM_ID);
                    if(data_product_item.Column == '' || !data_product_item.hasOwnProperty('Column')){
                        data_product_item.Column = 'NA';
                    }
                    if(data_product_item.Row == '' || !data_product_item.hasOwnProperty('Row')){
                        data_product_item.Row = 'NA';
                    }
                    // SET OPTIONS ============================================================================================================================================
                    var arr_prod_colors = getListValues('customlist_psgss_product_color'); // get internal id of each product color
                    var arr_prod_sizes = getListValues('customlist_psgss_product_size'); // get internal id of each product size
                    if(data_product_item.hasOwnProperty('Row') && arr_prod_colors[data_product_item.Row] != undefined){
                        create_child_item.setValue('matrixoptioncustitem_psgss_product_color', arr_prod_colors[data_product_item.Row]);
                        // log.audit({title: 'GF_MR_RICS_Product.js:rics_reduce-set-options', details: 'color: ' + (data_product_item.hasOwnProperty('Row') ? arr_prod_colors[data_product_item.Row] : 'no color data')});
                    }
                    if(data_product_item.hasOwnProperty('Column') && arr_prod_sizes[data_product_item.Column] != undefined){
                        create_child_item.setValue('matrixoptioncustitem_psgss_product_size', arr_prod_sizes[data_product_item.Column]);
                        // log.audit({title: 'GF_MR_RICS_Product.js:rics_reduce-set-options', details: 'size: ' + (data_product_item.hasOwnProperty('Column') ? arr_prod_sizes[data_product_item.Column] : 'no size data')});
                    }
                    log.audit({title: 'GF_MR_RICS_Product.js:rics_reduce-set-options', details: 'name: ' + str_name + ', color: ' + (data_product_item.hasOwnProperty('Row') ? arr_prod_colors[data_product_item.Row] : 'no color data') + ', size: ' + (data_product_item.hasOwnProperty('Column') ? arr_prod_sizes[data_product_item.Column] : 'no size data')});
                    // SET OPTIONS ============================================================================================================================================

                    try{
                        var num_new_child_id = create_child_item.save(); // unexpected error -- DUP_ITEM

                        var update_child_item = record.load({
                            type: 'inventoryitem',
                            id: num_new_child_id,
                            isDynamic: true
                        });

                        setVendor(data.SupplierCode, update_child_item);

                        update_child_item.save();
                    }catch(ex) {
                        log.error({title: 'Error: GF_MR_RICS_Product.js:rics_reduce', details: 'create_child_item.save-and-update :: Error ' + ex.toString() + ' : ' + ex.stack});
                    }

                }
            }


        }catch(ex) {
            log.error({title: 'Error: GF_MR_RICS_Product.js:rics_reduce', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * This summarize function is used to report important aspects about how the script performed.
     * @param {*} summary - this object contains data regarding script performance.
     */
    function rics_summarize(summary) {
        try{
            log.audit('Usage Consumed', summary.usage);
            log.audit('Concurrency Number ', summary.concurrency);
            log.audit('Number of Yields', summary.yields);
            log.audit({title: 'Process Completed'});
        }catch(ex) {
            log.error({title: 'Error: GF_MR_RICS_Product.js:rics_summarize', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    return {
        config:{
            retryCount: 3,
            exitOnError: true
        },

        getInputData: rics_getInputData,
        map: rics_map,
        reduce: rics_reduce,
        summarize: rics_summarize
    };

    // =========================== extra functions
    /**
     * This function is used to get an array containing the ids and name values of a list in NetSuite.
     * @param {*} _listScriptId - string - the internal id of the list to return.
     * @returns - array - containing all of the ids and values.
     */
    function getListValues(_listScriptId) {
        var searchColumn = search.createColumn({ name : 'name' });
        var listSearch = search.create({ type : _listScriptId, columns : searchColumn });
        var listArray = [];
        listSearch.run().each(function(searchResult) {
            listArray[searchResult.getValue(searchColumn)] = searchResult.id;
            return true;
        });
        return listArray;
    }

    /**
     * This function is used to add a value to either the color or size lists that are uses as the inventory item matrix options.
     * @param {*} _listScriptId - string - the internal id of the list to update.
     * @param {*} _newVal - string - the desired name value to add to the list.
     * @returns - array - uses getListValues to return the array including the newly added value.
     */
    function setNewListValue(_listScriptId, _newVal){
        try{
            var list_internal_id = 0;
            if(_listScriptId == 'customlist_psgss_product_color'){
                list_internal_id = 157;
            }
            if(_listScriptId == 'customlist_psgss_product_size'){
                list_internal_id = 147;
            }
            if(_newVal != undefined && _newVal != null && _newVal != ''){
                _newVal = _newVal.trim();
                if(list_internal_id != 0){
                    var customListRec = record.load({
                        type: 'customlist',
                        id: list_internal_id,
                        isDynamic: true
                    });

                }
                customListRec.selectNewLine({ sublistId: 'customvalue' });

                customListRec.setCurrentSublistValue({
                    sublistId: 'customvalue',
                    fieldId: 'value',
                    value: _newVal,
                    ignoreFieldChange: true
                });
                customListRec.setCurrentSublistValue({
                    sublistId: 'customvalue',
                    fieldId: 'abbreviation',
                    value: _newVal,
                    ignoreFieldChange: true
                });
                customListRec.commitLine({sublistId: 'customvalue'});

                var recordId = customListRec.save();
            }
            return getListValues(_listScriptId);
        }catch(ex){
            log.error({title: 'Error: GF_MR_RICS_Product.js:setNewListValue', details: 'Params: _listScriptId = ' + _listScriptId + ', _newVal = ' + _newVal + ', Error ' + ex.toString() + ' : ' + ex.stack});
            return getListValues(_listScriptId);
        }
    }

    /**
     * This function is similar to the setNewListValue function above, except it sets multiple values at a time.
     * @param {*} _listScriptId  - string - the internal id of the list to update.
     * @param {*} _arr_newVals - array - the list of desired name values to add to the list.
     * @returns - array - uses getListValues to return the array including the newly added value.
     */
    function setNewListValues(_listScriptId, _arr_newVals){
        try{
            log.audit({title: 'Error: GF_MR_RICS_Product.js:setNewListValues', details: 'Params: _listScriptId = ' + _listScriptId + ', _arr_newVals = ' + _arr_newVals});
            var list_internal_id = 0;
            if(_listScriptId == 'customlist_psgss_product_color'){
                list_internal_id = 157;
            }
            if(_listScriptId == 'customlist_psgss_product_size'){
                list_internal_id = 147;
            }
            if(list_internal_id != 0){
                var customListRec = record.load({
                    type: 'customlist',
                    id: list_internal_id,
                    isDynamic: true
                });

            }
            for(var val in _arr_newVals){
                _arr_newVals[val] = _arr_newVals[val].trim();

                customListRec.selectNewLine({ sublistId: 'customvalue' });

                customListRec.setCurrentSublistValue({
                    sublistId: 'customvalue',
                    fieldId: 'value',
                    value: _arr_newVals[val],
                    ignoreFieldChange: true                
                });
                customListRec.setCurrentSublistValue({
                    sublistId: 'customvalue',
                    fieldId: 'abbreviation',
                    value: _arr_newVals[val],
                    ignoreFieldChange: true                
                });
                customListRec.commitLine({sublistId: 'customvalue'});
            }

            var recordId = customListRec.save();

            return getListValues(_listScriptId);
        }catch(ex){
            log.error({title: 'Error: GF_MR_RICS_Product.js:setNewListValues', details: 'Params: _listScriptId = ' + _listScriptId + ', _arr_newVals = ' + _arr_newVals + ', Error ' + ex.toString() + ' : ' + ex.stack});
            return getListValues(_listScriptId);
        }
    }

    /**
     * This function will find the netsuite vendor with the specified supplier code, and add them to the passed in record.
     * @param {*} _supplierCode - string - typically a four character alpha-numeric code that is unique to a vendor from RICS.
     * @param {*} _rec - NetSuite Record Object - this is the record that will get updated with the vendor.
     */
    function setVendor(_supplierCode, _rec){
        var runsearch_vendor_records = search.create({
            type: "vendor",
            filters: [
                ["entityid","is",_supplierCode]
            ],
            columns: []
        }).run();
        var vendor_results = runsearch_vendor_records.getRange(0, 1000);
        if(vendor_results != null && vendor_results.length == 1){
            var vendor_id = vendor_results[0].id;
            var currentVendorCount = _rec.getLineCount({ 'sublistId': 'itemvendor' });
            if (currentVendorCount === 0){
                // set vendor
                _rec.selectNewLine({ sublistId: 'itemvendor' });
            }else{
                // update the existing top line
                _rec.selectLine({ sublistId: 'itemvendor', line: 0 });
            }
            _rec.setCurrentSublistValue({
                sublistId: 'itemvendor',
                fieldId: 'vendor',
                value: vendor_id
            });
            _rec.setCurrentSublistValue({
                sublistId: 'itemvendor',
                fieldId: 'preferredvendor',
                value: true
            });
            _rec.commitLine({sublistId: 'itemvendor'});
        }
        _rec.setValue('custitem_gf_vendor_code', _supplierCode);
    }

    /**
     * This function gets an inventory item id by using a search that looks for an item with the specified sku, color, and size options.
     * @param {*} _sku - string - the unique alphanumeric degignation for a model of items.
     * @param {*} _prod_color - string - the specific color or row value of the item.
     * @param {*} _prod_size - string - the specific size or column value of the item.
     * @returns - integer - the internal id of the item, or -1 if not found.
     */
    function findSubItemByMatrixOptions(_sku, _prod_color, _prod_size){
        var result = -1;
        // log.debug({title: 'GF_MR_RICS_Inventory.js:find_pd_record', details: '_sku: ' + _sku});
        // find the Product Details Record
        var subItem_SearchResults = search.create({
             type: "inventoryitem",
             filters: [
                ["type","anyof","InvtPart"], "AND",
                ["vendorname","is",_sku], "AND",
                ["custitem_psgss_product_color","anyof",_prod_color], "AND",
                ["custitem_psgss_product_size","anyof",_prod_size], "AND",
                ["parent","noneof","@NONE@"]
             ],
             columns: [ ]
        }).run();
        var subItem_SearchResultSet = subItem_SearchResults.getRange({start: 0, end: 1});
        if (subItem_SearchResultSet != null && subItem_SearchResultSet.length > 0){
            result = subItem_SearchResultSet[0].id;
        }
        // log.debug({title: 'GF_MR_RICS_Inventory.js:find_pd_record', details: 'result: ' + result});
        return result;
    }

    /**
     * This function uses a search to find any items that don't have a RICS Product Detail record matched to them and then for each of the results
     * another search is run to attempt to match them up. When a match is found the RICS Product Detail custom record is updated to link the records.
     */
    function matchExistingItemsToRICS_ProductDetails(){
        try{
            // find inventory items that are parent level items and have no ns item id
            var inventoryitemSearchObj = search.create({
                type: "item",
                filters: [
                    ["type","anyof","InvtPart","NonInvtPart"], "AND",
                    // ["isinactive","is","F"], "AND",
                    ["parent","anyof","@NONE@"], "AND",
                    ["custrecord_rics_pd_ns_item_id.custrecord_rics_pd_ns_item_id","anyof","@NONE@"], "AND",
                    ["systemnotes.type","is","T"], "AND",
                    ["systemnotes.name","anyof","-4"]
                ],
                columns: [
                    search.createColumn({
                     name: "itemid",
                     sort: search.Sort.ASC,
                     label: "Name"
                    }),
                    search.createColumn({name: "vendorname", label: "Vendor Name"}),
                    search.createColumn({name: "displayname", label: "Display Name"}),
                    search.createColumn({name: "salesdescription", label: "Description"}),
                    search.createColumn({name: "type", label: "Type"}),
                    search.createColumn({name: "baseprice", label: "Base Price"}),
                    search.createColumn({name: "isinactive"})
                ]
            });
            inventoryitemSearchObj.run().each(function(result){
                // ======================================== for each result try to find and map the NS item record field where the SKU matches
                // .run().each has a limit of 4,000 results
                var sku = result.getValue('vendorname');
                var flg_inactive = result.getValue('isinactive');
                if(!flg_inactive){
                    var customrecord_rics_product_detailsSearchObj = search.create({
                        type: "customrecord_rics_product_details",
                        filters: [
                            ["custrecord_rics_pd_sku","is",sku]
                        ],
                        columns: [
                            search.createColumn({name: "custrecord_rics_pd_suppliersku", label: "Supplier SKU"}),
                            search.createColumn({name: "custrecord_rics_pd_ns_item_id", label: "NS Item Record"}),
                            search.createColumn({name: "custrecord_rics_pd_sku", label: "SKU"}),
                            search.createColumn({name: "custrecord_rics_pd_classes", label: "Classes"}),
                            search.createColumn({name: "custrecord_rics_pd_colors", label: "Colors"}),
                            search.createColumn({name: "custrecord_rics_pd_column_name", label: "Column Name"}),
                            search.createColumn({name: "custrecord_rics_pd_row_name", label: "Row Name"}),
                            search.createColumn({name: "custrecord_rics_pd_suppliername", label: "Supplier Name"}),
                            search.createColumn({name: "custrecord_rics_pd_suppliercode", label: "Supplier Code"}),
                            search.createColumn({name: "custrecord_rics_pd_summary", label: "Summary"}),
                            search.createColumn({name: "internalid", label: "Internal ID"})
                        ]
                    });
                    var pd_result = customrecord_rics_product_detailsSearchObj.run().getRange(0,1);
                    if(pd_result != null && pd_result.length == 1){
                        var current_item_id = pd_result[0].getValue('custrecord_rics_pd_ns_item_id');
                        if(current_item_id == ''){
                            // when the match is found set the NS Item ID
                            var pd_update_id = record.submitFields({
                                type: 'customrecord_rics_product_details',
                                id: pd_result[0].id,
                                values: { 'custrecord_rics_pd_ns_item_id': result.id }
                            });
                            log.debug({title: 'GF_MR_RICS_Product.js:matchExistingItemsToRICS_ProductDetails', details: 'pd_update_id: ' + pd_update_id});
                        }
                    }
                }
                return true;
            });
        }catch(ex){
            log.error({title: 'GF_MR_RICS_Product.js:matchExistingItemsToRICS_ProductDetails', details: 'Error: ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * This function uses the specified RICS classes array to return an object regarding the class heirarchy, NetSuite ids, and names.
     * When the class can't be found it is created as long as a valid top level class can be found.
     * @param {*} _arrClasses - array - this should be a string based array of "TagTree" objects from RICS.
     * (example: [{"TagTree":"Women's Casual"},{"TagTree":"Women's Footwear|Women's Casual"},{"TagTree":"Footwear|Women's Footwear|Women's Casual"}])
     * @returns - object - {"index":string, "strlength":integer, "string":string, "classname_ns":string, "classid_ns":integer, "classname_rics":string}
     */
    function get_netsuite_item_class(_arrClasses){
        try{
            var arr_class_tags = JSON.parse(_arrClasses);
            // log.debug({title: 'GF_MR_RICS_Product.js:get_netsuite_item_class', details: 'arr_class_tags: ' + JSON.stringify(arr_class_tags)});
            // check tags so we can later assign the account fields based on what the class
            // find and get the base values from the longest tag on the item
            var index_long_tag = null;
            for(var tag in arr_class_tags){
                var str_orig = (arr_class_tags[tag].TagTree);
                var str_orig_class = str_orig;
                while(str_orig_class.indexOf('|') > -1){
                    str_orig_class = str_orig_class.substr(str_orig_class.indexOf('|') + 1);
                }
                var str_tag = (arr_class_tags[tag].TagTree).toLowerCase();
                var str_class = str_tag;
                while(str_class.indexOf('|') > -1){
                    str_class = str_class.substr(str_class.indexOf('|') + 1);
                }
                var classid_ns = "";
                var str_class_rics = str_tag.substring(0, str_tag.indexOf('|'));

                if(index_long_tag == null || str_tag.length > index_long_tag.strlength){
                    index_long_tag = {
                        "index":tag,
                        "strlength":str_tag.length,
                        "string":str_tag,
                        "classname_ns":str_class,
                        "classid_ns":classid_ns,
                        "classname_rics":str_class_rics
                    };
                }
            }

            if(index_long_tag != null){
                // use the values from the longest tag to set the values for this item
                // check that the class exists
                var classificationSearchObj = search.create({
                    type: "classification",
                    filters: [
                    ["isinactive","is","F"]
                    ],
                    columns: [
                        search.createColumn({
                            name: "name",
                            sort: search.Sort.ASC
                        }),
                        "internalid",
                        search.createColumn({
                            name: "formulatext",
                            formula: "REGEXP_REPLACE({name}, '( : )', '|')"
                        })
                    ]
                });
                var classid_ns = "";
                var classificationSearchObj_results = classificationSearchObj.run();
                classificationSearchObj_results.each(function(result){
                    // .run().each has a limit of 4,000 results
                    var str_formulatext = result.getValue({name: 'formulatext'}).toLowerCase();
                    if(classid_ns == "" && str_formulatext == index_long_tag.string){
                        classid_ns = result.id;
                    }
                    return true;
                });

                if(classid_ns != ""){
                    index_long_tag.classid_ns = classid_ns;
                }else{
                    // class was not found -- add the class to the class list by it's top level hierarchy
                    var parent_classid_ns = "";
                    classificationSearchObj_results.each(function(result){
                        // .run().each has a limit of 4,000 results
                        var str_formulatext = result.getValue({name: 'formulatext'}).toLowerCase();
                        if(parent_classid_ns == "" && str_formulatext == index_long_tag.classname_rics){
                            parent_classid_ns = result.id;
                        }
                        return true;
                    });
                    if(parent_classid_ns != ""){ // found a parent class to create the new class under
                        // check for sub-parent class
                        var arr_class_strings = str_tag.split('|');
                        if(arr_class_strings.length > 2){
                            var str_subparent = arr_class_strings[0] + '|' + arr_class_strings[1];
                            var subparent_classid_ns = "";
                            classificationSearchObj_results.each(function(result){
                                // .run().each has a limit of 4,000 results
                                var str_formulatext = result.getValue({name: 'formulatext'}).toLowerCase();
                                if(subparent_classid_ns == "" && str_formulatext == str_subparent){
                                    subparent_classid_ns = result.id;
                                }
                                return true;
                            });
                            if(subparent_classid_ns != ""){
                                parent_classid_ns = subparent_classid_ns; // this will nest the next part into the subparent - otherwise it will run as normal
                            }
                        }
                        // create new classification record
                        var create_classification = record.create({
                            type: 'classification',
                            isDynamic: true
                        });
                        create_classification.setValue('parent', parent_classid_ns);
                        create_classification.setValue('subsidiary', 1);
                        create_classification.setValue('includechildren', true);
                        create_classification.setValue('name', str_orig_class);
                        // create_classification.setValue('externalid', str_orig_class);
                        var classid_ns = create_classification.save();
                        if(classid_ns != null){
                            index_long_tag.classid_ns = classid_ns;
                        }
                    }
                }
            }
        }catch(ex){
            log.error({title: 'GF_MR_RICS_Product.js:get_netsuite_item_class', details: 'Error: ' + ex.toString() + ' : ' + ex.stack});
        }
        // log.debug({title: 'GF_MR_RICS_Product.js:get_netsuite_item_class', details: 'index_long_tag: ' + JSON.stringify(index_long_tag)} );
        return index_long_tag;
    }

    /**
     * This function is designed to take in the required information to set up the properties for new item object, and it returns that object for processing.
     * @param {string} _isMatrixParent - string - 'YES' - process as a newly create parent inv item, 'NO' - process as a newly create parent inv item, '' - Leave blank to not set the matrixtype property which is ONLY set during creation.
     * @param {*} _classname - string - used to select the correct accounts for the item.
     * @param {*} _rics_data - string - used to set verious properties of the item object.
     * @param {*} _upc - string - used only to set the upccode property (set null if not a child record).
     * @param {*} _classid_ns - integer - used to set the class property of the item object.
     * @returns - object - 
     */
    function create_item_obj(_isMatrixParent, _classname, _rics_data, _upc, _classid_ns){
        var result = new Object();
        // set all field defaults
        if(_isMatrixParent == 'YES'){
            result.matrixtype = 'PARENT'; // might only need this field when building a NEW item
            result.department = '10'; // inventory
        }
        if(_isMatrixParent == 'NO'){
            result.matrixtype = 'CHILD';
            if(_upc != null && _upc != ''){
                result.upccode = _upc;
            }
        }

        // result.itemid = _rics_data.Sku+'-'+_classname;
        // result.isinactive = false;
        result.matrixitemnametemplate = '{vendorname}-{custitem_psgss_product_color}-{custitem_psgss_product_size}';
        result.displayname = (_rics_data.Summary != '') ? _rics_data.Summary : '';
        if(result.displayname == ''){
            var column_size_data = (_rics_data.ColumnName != undefined && _rics_data.ColumnName != '') ? _rics_data.ColumnName : 'No_Column';
            var row_color_data = (_rics_data.RowName != undefined && _rics_data.RowName != '') ? _rics_data.RowName : 'No_Row';
            result.displayname = column_size_data+' - '+row_color_data+' - '+_rics_data.SupplierSku;
        }
        result.purchasedescription = (_rics_data.Description != '') ? _rics_data.Description : _rics_data.Summary;
        result.salesdescription = (_rics_data.Description != '') ? _rics_data.Description : _rics_data.Summary;
        result.vendorname = _rics_data.Sku;
        result.itemoptions = ['CUSTCOL1', 'CUSTCOL2'];
        result.costestimatetype = 'AVGCOST';
        result.costcategory = '1';
        result.matchbilltoreceipt = true;
        result.includechildren = false;
        result.atpmethod = 'CUMULATIVE_ATP_WITH_LOOK_AHEAD';
        result.taxschedule = '1';
        if(_classid_ns != null){
            result.class = _classid_ns;
        }

        /*
            classname_rics      Arch Supports   Accessories Footwear    Socks   Bags
            COGS Account	    623             625         624         625     626
            Asset Account	    620             622         621         622     626
            Income Account      54              54          54          54      54
            Gain/Loss Account	551             551         551         551     551
            Price Vari.Account	620             622         621         622     626
            Qty Vari. Account   620             622         621         622     626
        */
        switch(_classname){
            case 'arch supports':
                result.cogsaccount = '623';
                result.assetaccount = '620';
                result.incomeaccount = '54';
                result.gainlossaccount = '551';
                result.billpricevarianceacct = '620';
                result.billqtyvarianceacct = '620';
                break;
            case 'accessories':
                result.cogsaccount = '625';
                result.assetaccount = '622';
                result.incomeaccount = '54';
                result.gainlossaccount = '551';
                result.billpricevarianceacct = '622';
                result.billqtyvarianceacct = '622';
                break;
            case 'footwear':
                result.cogsaccount = '624';
                result.assetaccount = '621';
                result.incomeaccount = '54';
                result.gainlossaccount = '551';
                result.billpricevarianceacct = '621';
                result.billqtyvarianceacct = '621';
                break;
            case 'socks':
                result.cogsaccount = '625';
                result.assetaccount = '622';
                result.incomeaccount = '54';
                result.gainlossaccount = '551';
                result.billpricevarianceacct = '622';
                result.billqtyvarianceacct = '622';
                break;
            case 'bags':
                // result.cogsaccount = '626';
                // result.assetaccount = '626';
                // result.incomeaccount = '54';
                // result.expenseaccount = '54';
                // result.gainlossaccount = '551';
                // result.billpricevarianceacct = '626';
                // result.billqtyvarianceacct = '626';
                break;
        }

        return result;
    }

    /**
     * This function is used to set the pricing lines on an inventory item record.
     * @param {*} _pricing_data - object - this should contain data for the line items like retail price or active price or both.
     * @param {*} _item_record - object - this should be the record object that the new line data will be added to.
     */
    function setPricingLines(_pricing_data, _item_record){
        try{
            var currentPrice1Count = _item_record.getLineCount({ 'sublistId': 'price1' });
            for(var i = 0; currentPrice1Count > 0 && i < currentPrice1Count; i++){
                _item_record.selectLine({ sublistId: 'price1', line: i });
                var pricelevelname = _item_record.getCurrentSublistValue({
                    sublistId: 'price1',
                    fieldId: 'pricelevelname'
                });

                if(pricelevelname == 'Base Price (MSRP)' && _pricing_data.hasOwnProperty('RetailPrice')){
                    _item_record.setCurrentSublistValue({
                        sublistId: 'price1',
                        fieldId: 'price_1_',
                        value: _pricing_data.RetailPrice,
                        ignoreFieldChange: true
                    });
                    _item_record.commitLine({sublistId: 'price1'});
                }
                if(pricelevelname == 'Sale Price' && _pricing_data.hasOwnProperty('ActivePrice')){
                    _item_record.setCurrentSublistValue({
                        sublistId: 'price1',
                        fieldId: 'price_1_',
                        value: _pricing_data.ActivePrice,
                        ignoreFieldChange: true
                    });
                    _item_record.commitLine({sublistId: 'price1'});
                }
            }
        }catch(ex){
            log.error({title: 'GF_MR_RICS_Product.js:setPricingLines', details: 'Error: ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * This function processes creation or updates of the non-inventory items also called services (like the bag fee)
     * @param {*} _data - object - this should be the data passed in from the mapping function.
     * @returns - integer - this is the internal id of the created or updated record.
     */
    function processNonInventoryItem(_data){
        // get class
        var ns_item_class_data = get_netsuite_item_class(_data.Classes);
        var data_product_item = JSON.parse(_data.product_items[0]);
        var upcCode = data_product_item.UPC;
        if(_data.NS_ITEM_ID != ''){
            // update existing non-inventory item =================================================================================================== update existing non-inventory item
            var nonInv_Item = create_item_obj('', ns_item_class_data.classname_rics, _data, null, null);
            log.debug({title: 'GF_MR_RICS_Product.js:processNonInventoryItem', details: '_data.NS_ITEM_ID: ' + _data.NS_ITEM_ID});
            var update_nonInv_item = record.load({
                type: 'noninventoryitem',
                id: _data.NS_ITEM_ID,
                isDynamic: true
            });
            for(var prop in nonInv_Item){
                if(prop == 'costestimatetype'){
                    update_nonInv_item.setValue(prop,'LASTPURCHPRICE');
                }else{
                    update_nonInv_item.setValue(prop,nonInv_Item[prop]);
                }
            }
            update_nonInv_item.setValue('itemid', _data.Sku + '-' + ns_item_class_data.classname_rics);
            update_nonInv_item.setValue('subsidiary',["3","4","2","7"]);
            update_nonInv_item.setValue('upccode',upcCode);
        }else{
            // create parent =================================================================================================== create parent
            var nonInv_Item = create_item_obj('', ns_item_class_data.classname_rics, _data, null, ns_item_class_data.classid_ns);
            var create_nonInv_item = record.create({
                type: 'noninventoryitem',
                isDynamic: true
            });
            for(var prop in nonInv_Item){
                if(prop == 'costestimatetype'){
                    create_nonInv_item.setValue(prop,'LASTPURCHPRICE');
                }else{
                    create_nonInv_item.setValue(prop,nonInv_Item[prop]);
                }
            }
            create_nonInv_item.setValue('itemid', _data.Sku + '-' + ns_item_class_data.classname_rics);
            create_nonInv_item.setValue('subsidiary',["3","4","2","7"]);
            create_nonInv_item.setValue('upccode',upcCode);

            var id_new_NS_item = create_nonInv_item.save();

            var update_nonInv_item = record.load({
                type: 'noninventoryitem',
                id: id_new_NS_item,
                isDynamic: true
            });
        }

        try{
            setVendor(_data.SupplierCode, update_nonInv_item);
        }catch(ex){
            // error saving vendor
        }

        var rec_ID = update_nonInv_item.save();

        return rec_ID;
    }

 });