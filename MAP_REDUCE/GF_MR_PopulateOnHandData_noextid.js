/**
 *@NApiVersion 2.1
 *@NScriptType MapReduceScript
 *
Script Name:   GF_MR_PopulateOnHandData_noextid.js
Author:        Mark Robinson
 */

define(['N/log', 'N/search', 'N/record'], function(log, search, record) {

    /**
     * This get input function loads a search with all RICS On Hand Custom records that have not yet been populated with data
     * @param {*} context  - this variable is not utilized in this function
     * @returns - an array containing the full responses from the custom record search
     */
    function gf_getInputData(context){
        try {
            var customrecord_rics_inventory_on_handSearchObj = search.create({
                type: "customrecord_rics_inventory_on_hand",
                filters: [
                    ["externalid","anyof","@NONE@"], "AND",
                    ["custrecord_rics_ioh_storecode","isempty",""]
                ], // must have blank External ID and the storecode is empty
                columns: [
                    "custrecord_rics_ioh_ns_location"
                ]
            });

            return customrecord_rics_inventory_on_handSearchObj;
        }catch(ex) {
            log.error({title: 'GF_MR_PopulateOnHandData_noextid.js:rics_getInputData', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this mapping function we get the item and location data for the on hand record based on the external id
     * @param {*} context - Object - This should be the search results from the gf_getInputData function
     * @returns void (note: the const context variable is impacted and used in the reduce function)
     */
    function gf_map(context){
        try{
            // simple pass through
            var map_data = JSON.parse(context.value);
            // log.audit({title: 'GF_MR_PopulateOnHandData_noextid.js:rics_map', details: 'map_data: ' + JSON.stringify(map_data)});
			if (map_data == null) {
				return true;
			}else{
                try{
                    let loc_id = map_data.values.custrecord_rics_ioh_ns_location.value;
                    let fieldLookUp = search.lookupFields({
                        type: 'location',
                        id: loc_id,
                        columns: ['name', 'externalid']
                    });
                    if(fieldLookUp.name != ''){
                        let rics_ioh_update_id = record.submitFields({
                            type: 'customrecord_rics_inventory_on_hand',
                            id: result.id,
                            values: {
                                'custrecord_rics_ioh_storecode': fieldLookUp.externalid[0].text,
                                'custrecord_rics_ioh_storename': fieldLookUp.name
                            }
                        });
                    }
                }catch(ex){
                    log.error({title: 'GF_MR_PopulateOnHandData_noextid.js:rics_map-onSave', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
                }
            }

			return true;
        }catch(ex) {
            log.error({title: 'GF_MR_PopulateOnHandData_noextid.js:rics_map', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this reduce function we do nothing
     * @param {*} context - Object - This is the object that was built in the mapping function, it should conatin all the data we need for processing
     * @returns - void
     */
    function gf_reduce(context){
    }

    /**
     * This summarize function is used to report important aspects about how the script performed
     * @param {*} summary - this object contains data regarding script performance.
     */
    function gf_summarize(summary) {
        try{
            log.audit('Usage Consumed', summary.usage);
            log.audit('Concurrency Number ', summary.concurrency);
            log.audit('Number of Yields', summary.yields);
            log.audit({title: 'Process Completed'});
        }catch(ex) {
            log.error({title: 'GF_MR_PopulateOnHandData_noextid.js:rics_summarize', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    return {
        getInputData: gf_getInputData,
        map: gf_map,
        reduce: gf_reduce,
        summarize: gf_summarize
    };


});