/**
 *@NApiVersion 2.1
 *@NScriptType MapReduceScript
 *
Script Name:   GF_MR_RICS_NonSellableBatch.js
Author:        Mark Robinson
 */

define(['N/log', 'N/search', 'N/record', 'N/https', 'N/format', '/SuiteScripts/GF_LIB/gf_nsrics'], function(log, search, record, https, format, gf_nsrics) {

    /**
     * In this get input function we're making API calls to RICS to get all of the non-sellable batches that have been created in the last 7 days and all of the deatil records for those batches.
     * Many non-sellable batch details data objects can relate to one non-sellable batch data object.
     * We add all of the data to an array that it then passed into the mapping function.
     * @param {*} context - this variable is not utilized in this function.
     * @returns - an array containing the data returned from the API calls.
     */
    function rics_getInputData(context){
        var nsrics = gf_nsrics.getIntegration();
        // Get all NonSellableBatch Data data from RICS with a POST request to: https://enterprise.ricssoftware.com/api/NonSellable/GetNonSellableBatch
        // Map phase will create a NetSuite writable object
        // Reduce phase will save the NetSuite writable object
        var max_attempts = 3;
        var arr_data = [];
        var arr_NonSellableBatches = [];
        try {
            var getNonSellableBatch = nsrics.baseurl + nsrics.getEndPoint('GetNonSellableBatch');
            var getNonSellableBatchDetails = nsrics.baseurl + nsrics.getEndPoint('GetNonSellableBatchDetails');
            var header = [];
            header['Content-Type'] = 'application/json';
            header['Token'] = nsrics.getActiveConnection();
            var num_skip = 0;
            var current_attempt = 0;
            // var dt_today = new Date();
            // dt_today.setDate(dt_today.getDate() + 1);
            // var str_end_date = format.format({ value : dt_today, type : format.Type.DATE});
            // dt_today.setDate(dt_today.getDate() - 8); // get 7 days ago - 8
            // var str_start_date = format.format({ value : dt_today, type : format.Type.DATE});
            var dt_today = new Date(new Date().getFullYear(), new Date().getMonth(), 1); // begining of current month
            var str_start_date = format.format({ value : dt_today, type : format.Type.DATE});
            var str_end_date = '1/1/9999 11:59:59 PM';

            do{
                nsrics.sleep(1000);
                var postData = { "CompletedOnStart": str_start_date, "CompletedOnEnd": str_end_date, "Skip":num_skip };
                log.debug({title: 'GF_MR_RICS_NonSellableBatch.js:rics_getInputData', details: 'nsb postData: ' + JSON.stringify(postData)});
                try{
                    postData = JSON.stringify(postData);
                    var response = https.post({
                        url: getNonSellableBatch,
                        headers:header,
                        body: postData
                    });
                    if(response != undefined && response['code'] != 200){
                        current_attempt += 1;
                    }else{
                        current_attempt = 0;
                        var data = JSON.parse(response.body);
                        for(var key in data.NonSellableBatches){
                            if(data.NonSellableBatches[key].hasOwnProperty('NonSellableBatchId')){
                                arr_NonSellableBatches.push(data.NonSellableBatches[key]);
                            }
                        }
                        num_skip = data.ResultStatistics.EndRecord;
                    }
                }catch(err){
                    current_attempt += 1;
                }
            }while( current_attempt < max_attempts && ( response['code'] != 200 || ( data.hasOwnProperty('ResultStatistics') && data.ResultStatistics.EndRecord < data.ResultStatistics.TotalRecords ) ) );

            for(var nsb in arr_NonSellableBatches){
                var nsb_obj = new Object();
                nsb_obj.batchData = arr_NonSellableBatches[nsb];

                var arr_NonSellableBatchDetails = [];
                var num_skip = 0;
                var current_attempt = 0;
                // get details records
                do{
                    nsrics.sleep(1000);
                    var postData = { "NonSellableBatchId": arr_NonSellableBatches[nsb].NonSellableBatchId, "Skip":num_skip };
                    log.debug({title: 'GF_MR_RICS_NonSellableBatch.js:rics_getInputData', details: 'nsbd postData: ' + JSON.stringify(postData)});
                    try{
                        postData = JSON.stringify(postData);
                        var response = https.post({
                            url: getNonSellableBatchDetails,
                            headers:header,
                            body: postData
                        });
                        if(response != undefined && response['code'] != 200){
                            current_attempt += 1;
                        }else{
                            current_attempt = 0;
                            var data = JSON.parse(response.body);
                            for(var key in data.NonSellableDetails){
                                if(data.NonSellableDetails[key].hasOwnProperty('NonSellableBatchId')){
                                    arr_NonSellableBatchDetails.push(data.NonSellableDetails[key]);
                                }
                            }
                            num_skip = data.ResultStatistics.EndRecord;
                        }
                    }catch(err){
                        current_attempt += 1;
                    }
                }while( current_attempt < max_attempts && ( response['code'] != 200 || ( data.hasOwnProperty('ResultStatistics') && data.ResultStatistics.EndRecord < data.ResultStatistics.TotalRecords ) ) );
                nsb_obj.batchDetails = arr_NonSellableBatchDetails;
                arr_data.push(nsb_obj);
            }

            log.debug({title: 'GF_MR_RICS_NonSellableBatch.js:rics_getInputData', details: 'arr_data: ' + JSON.stringify(arr_data)});

            return arr_data;
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_NonSellableBatch.js:rics_getInputData', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this mapping function we take the data that was passed in from the get input function and map it out as a set of NetSuite writable objects.
     * The parent object is the RICS NonSellable Batch custom record, and the child objcts are the related RICS NonSellable Batch Details custom records.
     * @param {*} context - this should contain the data from the API calls in the get input function.
     * @returns void (note: the const context variable is written to and used in the reduce function)
     */
    function rics_map(context){
        try{
            var data = JSON.parse(context.value);
            if(data == null){ return true; }
            if(!data.batchData.hasOwnProperty('CompletedOn')){ return true; } // ONLY create Completed Returns
            log.debug({title: 'GF_MR_RICS_NonSellableBatch.js:rics_map', details: 'data: ' + JSON.stringify(data)});

            // build a NetSuite writable object
            var ns_data = {};
            var ns_nsb_rec = {};
            var arr_nsbd_details = [];

            ns_nsb_rec.custrecord_rics_nsb_json = JSON.stringify(data);
            ns_nsb_rec.custrecord_rics_nsb_nonsellablebatchid = data.batchData.NonSellableBatchId;
            ns_nsb_rec.custrecord_rics_nsb_batchname = data.batchData.BatchName;
            ns_nsb_rec.custrecord_rics_nsb_rma = (data.batchData.hasOwnProperty('RMA')) ? data.batchData.RMA : '';
            ns_nsb_rec.custrecord_rics_nsb_comment = data.batchData.Comment;
            ns_nsb_rec.custrecord_rics_nsb_totalvalue = data.batchData.TotalValue;
            ns_nsb_rec.custrecord_rics_nsb_createdon = data.batchData.CreatedOn;
            ns_nsb_rec.custrecord_rics_nsb_completedon = (data.batchData.hasOwnProperty('CompletedOn')) ? data.batchData.CompletedOn : '';
            ns_nsb_rec.custrecord_rics_nsb_storecode = data.batchData.StoreCode;
            ns_nsb_rec.custrecord_rics_nsb_nonsellablebatchtype = data.batchData.NonSellableBatchType;
            ns_nsb_rec.custrecord_rics_nsb_suppliercode = (data.batchData.hasOwnProperty('SupplierCode')) ? data.batchData.SupplierCode : '';
            ns_nsb_rec.custrecord_rics_nsb_suppliername = (data.batchData.hasOwnProperty('SupplierName')) ? data.batchData.SupplierName : '';

            // log.debug({title: 'GF_MR_RICS_NonSellableBatch.js:rics_map', details: 'data.batchDetails: ' + JSON.stringify(data.batchDetails)});
            for(var detail in data.batchDetails){
                var detail_Data = data.batchDetails[detail];
                // log.debug({title: 'GF_MR_RICS_NonSellableBatch.js:rics_map', details: 'detail_Data: ' + JSON.stringify(detail_Data)});

                var ns_nsbd_details_rec = {};

                ns_nsbd_details_rec.custrecord_rics_nsbd_json = JSON.stringify(detail_Data);
                ns_nsbd_details_rec.custrecord_rics_nsbd_dateadded = detail_Data.DateAdded;
                ns_nsbd_details_rec.custrecord_rics_nsbd_nonsellablebatchid = detail_Data.NonSellableBatchId;
                ns_nsbd_details_rec.custrecord_rics_nsbd_memo = detail_Data.Memo;
                ns_nsbd_details_rec.custrecord_rics_nsbd_quantity = detail_Data.Quantity;
                ns_nsbd_details_rec.custrecord_rics_nsbd_unitvalue = detail_Data.UnitValue;
                ns_nsbd_details_rec.custrecord_rics_nsbd_column = (detail_Data.hasOwnProperty('Column')) ? detail_Data.Column : 'NA';
                ns_nsbd_details_rec.custrecord_rics_nsbd_row = (detail_Data.hasOwnProperty('Row')) ? detail_Data.Row : 'NA';
                ns_nsbd_details_rec.custrecord_rics_nsbd_storecode = detail_Data.StoreCode;
                ns_nsbd_details_rec.custrecord_rics_nsbd_sku = detail_Data.SKU;
                ns_nsbd_details_rec.custrecord_rics_nsbd_returndescription = detail_Data.ReturnDescription;
                ns_nsbd_details_rec.custrecord_rics_nsbd_suppliercode = (detail_Data.hasOwnProperty('SupplierCode')) ? detail_Data.SupplierCode : '';
                ns_nsbd_details_rec.custrecord_rics_nsbd_suppliername = (detail_Data.hasOwnProperty('SupplierName')) ? detail_Data.SupplierName : '';
                if(ns_nsb_rec.custrecord_rics_nsb_suppliercode == '' && ns_nsbd_details_rec.custrecord_rics_nsbd_suppliercode != ''){
                    ns_nsb_rec.custrecord_rics_nsb_suppliercode = ns_nsbd_details_rec.custrecord_rics_nsbd_suppliercode;
                    if(ns_nsb_rec.custrecord_rics_nsb_suppliername == '' && ns_nsbd_details_rec.custrecord_rics_nsbd_suppliername != ''){
                        ns_nsb_rec.custrecord_rics_nsb_suppliername = ns_nsbd_details_rec.custrecord_rics_nsbd_suppliername;
                    } // if we don't nest this we risk getting different supplier data
                }

                arr_nsbd_details.push(ns_nsbd_details_rec);
            }

            // pass data to reduce 
            ns_data.ns_nsb_rec = ns_nsb_rec;
            ns_data.ns_nsbd_records = arr_nsbd_details;
            // log.debug({title: 'GF_MR_RICS_NonSellableBatch.js:rics_map', details: 'ns_data: ' + JSON.stringify(ns_data)});

            context.write({ key: context.key, value: ns_data });
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_NonSellableBatch.js:rics_map', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this reduce function we take the mapped context objects and create or update all of the custom records.
     * @param {*} context - this should be the mapped object containing all of the fields and data that need to be on the RICS NonSellable Batch custom record,
     * and the related RICS NonSellable Batch Details custom records.
     * @returns void
     */
    function rics_reduce(context){
        try{
            var nsrics = gf_nsrics.getIntegration();
            var data = JSON.parse(context.values[0]);
            if(data == null){ return true; }

            var str_supplier_code = '';
            if(data.hasOwnProperty('ns_nsb_rec')){
                log.debug({title: 'GF_MR_RICS_NonSellableBatch.js:rics_reduce', details: 'data: ' + JSON.stringify(data)});
                var arr_date_props = [ // the values are for processing, and not to be directly written on the record
                    'custrecord_rics_nsb_createdon',
                    'custrecord_rics_nsb_completedon',
                    'custrecord_rics_nsbd_dateadded'
                ];
                var ns_nsb_id = find_nsb_record(data.ns_nsb_rec.custrecord_rics_nsb_nonsellablebatchid);

                if(ns_nsb_id == 0){
                    // no purchase order record was found we need to create one
                    var nonsellablebatch_record = record.create({
                        type: 'customrecord_rics_nonsellablebatch'
                    });
                }else{
                    log.debug({title: 'GF_MR_RICS_NonSellableBatch.js:rics_reduce', details: 'ns_nsb_id: ' + ns_nsb_id});
                    // a purchase order record was found we should update it
                    var nonsellablebatch_record = record.load({
                        type: 'customrecord_rics_nonsellablebatch',
                        id: ns_nsb_id,
                        isDynamic: true
                    });
                }
                for(var prop in data.ns_nsb_rec){
                    if(prop == 'custrecord_rics_nsb_suppliercode' && str_supplier_code == ''){ str_supplier_code = data.ns_nsb_rec[prop]; }
                    if(arr_date_props.indexOf(prop) > -1){
                        if(data.ns_nsb_rec[prop] != ''){
                            nonsellablebatch_record.setValue(prop,new Date(nsrics.str_reformat_date(data.ns_nsb_rec[prop])));
                        }
                    }else{
                        nonsellablebatch_record.setValue(prop,data.ns_nsb_rec[prop]);
                    }
                }
                var saved_nsb_id = nonsellablebatch_record.save();
                log.audit({title: 'GF_MR_RICS_NonSellableBatch.js:rics_reduce', details: 'saved_nsb_id: ' + saved_nsb_id + ', data: ' + JSON.stringify(data)});
            }

            // before processing all child records - loop through them and add together the quantities on matching items
            var arr_detail_data = [];
            var arr_all_nsbd_recs = data.ns_nsbd_records;
            for(var detail in arr_all_nsbd_recs){
                var detailData = arr_all_nsbd_recs[detail];
                log.debug({title: 'GF_MR_RICS_NonSellableBatch.js:rics_reduce', details: 'detailData: ' + JSON.stringify(detailData)});
                var vrma_key = detailData.custrecord_rics_nsbd_sku + '-' + detailData.custrecord_rics_nsbd_column + '-' + detailData.custrecord_rics_nsbd_row;
                if(arr_detail_data[vrma_key] == undefined){
                    arr_detail_data[vrma_key] = detailData;
                }else{
                    var vrma_qty = arr_detail_data[vrma_key].custrecord_rics_nsbd_quantity + detailData.custrecord_rics_nsbd_quantity;
		            arr_detail_data[vrma_key].custrecord_rics_nsbd_quantity = vrma_qty;
                }
                log.debug({title: 'GF_MR_RICS_NonSellableBatch.js:rics_reduce', details: 'vrma_key: ' + vrma_key + ', arr_detail_data[vrma_key]: ' + arr_detail_data[vrma_key]});
            }

            // add or update all child records
            if(data.hasOwnProperty('ns_nsbd_records') && typeof saved_nsb_id != 'undefined' && saved_nsb_id > 0){
                // var arr_all_nsbd_recs = data.ns_nsbd_records;
                for(var detail in arr_detail_data){
                    var str_nsbd_supplier_code = '';
                    var detailData = arr_detail_data[detail];
                    // log.debug({title: 'GF_MR_RICS_NonSellableBatch.js:rics_reduce', details: 'detailData: ' + JSON.stringify(detailData)});

                    var ns_nsbd_id = find_nsbd_record(saved_nsb_id, detailData.custrecord_rics_nsbd_sku, detailData.custrecord_rics_nsbd_column, detailData.custrecord_rics_nsbd_row);
                    if(ns_nsbd_id == 0){
                        // no tranfer order record was found we need to create one
                        var nsb_details_record = record.create({
                            type: 'customrecord_rics_nsb_details'
                        });
                        detailData.custrecord_rics_nsbd_product_details = find_pd_record(detailData.custrecord_rics_nsbd_sku);
                        detailData.custrecord_rics_nsbd_product_item = find_pi_record(detailData.custrecord_rics_nsbd_sku, detailData.custrecord_rics_nsbd_column, detailData.custrecord_rics_nsbd_row);
                    }else{
                        log.debug({title: 'GF_MR_RICS_NonSellableBatch.js:rics_reduce', details: 'ns_nsbd_id: ' + ns_nsbd_id});
                        // a tranfer order record was found we should update it
                        var nsb_details_record = record.load({
                            type: 'customrecord_rics_nsb_details',
                            id: ns_nsbd_id,
                            isDynamic: true
                        });
                    }
                    for(var prop in detailData){
                        if(prop == 'custrecord_rics_nsbd_suppliercode' && str_nsbd_supplier_code == ''){ str_nsbd_supplier_code = detailData[prop]; }
                        if(arr_date_props.indexOf(prop) > -1){
                            if(detailData[prop] != ''){
                                nsb_details_record.setValue(prop,new Date(nsrics.str_reformat_date(detailData[prop])));
                            }
                        }else{
                            if((prop != 'custrecord_rics_nsbd_product_details' && prop != 'custrecord_rics_nsbd_product_item') || detailData[prop] > 0){
                                nsb_details_record.setValue(prop,detailData[prop]);
                            }
                        }
                    }
                    // NSRICS-23 - also need to set the parent for each child record to tie them together
                    nsb_details_record.setValue('custrecord_rics_nsbd_parent',saved_nsb_id);

                    if(str_supplier_code == '' && str_nsbd_supplier_code != ''){
                        str_supplier_code = str_nsbd_supplier_code;
                        var custom_nsb_upd_id = record.submitFields({
                            type: 'customrecord_rics_nonsellablebatch',
                            id: saved_nsb_id,
                            values: { custrecord_rics_nsb_suppliercode: str_supplier_code }
                        });
                    }

                    var saved_nsbd_id = nsb_details_record.save();
                    log.audit({title: 'GF_MR_RICS_NonSellableBatch.js:rics_reduce', details: 'saved_nsbd_id: ' + saved_nsbd_id + ', data: ' + JSON.stringify(detailData)});
                }
            }

            // verify parent has a supplier code
            if(str_supplier_code == ''){
                var customrecord_rics_nsb_detailsSearchObj = search.create({
                    type: "customrecord_rics_nsb_details",
                    filters: [
                       ["custrecord_rics_nsbd_parent","anyof",saved_nsb_id]
                    ],
                    columns: [
                       search.createColumn({
                          name: "custrecord_rics_pd_suppliercode",
                          join: "CUSTRECORD_RICS_NSBD_PRODUCT_DETAILS"
                       })
                    ]
                 });
                 var flg_written = false;
                 customrecord_rics_nsb_detailsSearchObj.run().each(function(result){
                    // .run().each has a limit of 4,000 results
                    var str_pd_supplierCode = result.getValue({
                        name: "custrecord_rics_pd_suppliercode",
                        join: "CUSTRECORD_RICS_NSBD_PRODUCT_DETAILS"
                    });
                    if(str_pd_supplierCode != '' && flg_written == false){
                        var custom_nsb_upd_id = record.submitFields({
                            type: 'customrecord_rics_nonsellablebatch',
                            id: saved_nsb_id,
                            values: { custrecord_rics_nsb_suppliercode: str_pd_supplierCode }
                        });
                        flg_written = true;
                    }
                    return true;
                 });
            }

        }catch(ex) {
            log.error({title: 'GF_MR_RICS_NonSellableBatch.js:rics_reduce', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * This summarize function is used to report important aspects about how the script performed
     * @param {*} summary - this object contains data regarding script performance.
     */
    function rics_summarize(summary) {
        try{
            log.audit('Usage Consumed', summary.usage);
            log.audit('Concurrency Number ', summary.concurrency);
            log.audit('Number of Yields', summary.yields);
            log.audit({title: 'Process Completed'});
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_NonSellableBatch.js:rics_summarize', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    return {
        getInputData: rics_getInputData,
        map: rics_map,
        reduce: rics_reduce,
        summarize: rics_summarize
    };

    // =========================== extra functions
    /**
     * This function takes a string of the unique batch id value from RICS and runs a search in NetSuite to find the RICS NonSellable Batch custom record.
     * @param {*} _strBatchId - string - this is the unique batch id created by RICS for this NonSellable Batch.
     * @returns - integer - the internal id of the RICS NonSellable Batch custom record.
     */
    function find_nsb_record(_strBatchId){
        var result = 0;
        // find the RICS Purchase Order Record
        var nsb_SearchResults_filters = [
            ["isinactive","is","F"], "AND",
            ["custrecord_rics_nsb_nonsellablebatchid","is",_strBatchId]
        ];
        var nsb_SearchResults = search.create({
            type: "customrecord_rics_nonsellablebatch",
            filters: nsb_SearchResults_filters,
            columns: [
                search.createColumn({
                    name: "internalid",
                    sort: search.Sort.DESC,
                    label: "ID"
                })
            ]
        }).run();
        var nsb_SearchResultSet = nsb_SearchResults.getRange({start: 0, end: 1});
        if (nsb_SearchResultSet != null && nsb_SearchResultSet.length > 0){
            result = nsb_SearchResultSet[0].id;
        }
        // log.debug({title: 'GF_MR_RICS_NonSellableBatch.js:find_nsb_record', details: 'result: ' + result});
        return result;
    }

    /**
     * This function takes a set of values and runs a search in NetSuite to find the RICS NonSellable Batch Details custom record.
     * @param {*} _nsb_parent - integer - this should be the ID of the parent record
     * @param {*} _nsbd_sku - string - this should be the SKU of the item
     * @param {*} _nsbd_column - string - this should be the column value of the item
     * @param {*} _nsbd_row - string - this should be the row value of the item
     * @returns - integer - the internal id of the RICS NonSellable Batch Details custom record.
     */
    function find_nsbd_record(_nsb_parent, _nsbd_sku, _nsbd_column, _nsbd_row){
        var result = 0;
        // find the RICS Purchase Order Details Record
        var nsbd_SearchResults = search.create({
            type: "customrecord_rics_nsb_details",
            filters: [
                ["isinactive","is","F"], "AND",
                ["custrecord_rics_nsbd_parent","is",_nsb_parent], "AND",
                ["custrecord_rics_nsbd_sku","is",_nsbd_sku], "AND",
                ["custrecord_rics_nsbd_column","is",_nsbd_column], "AND",
                ["custrecord_rics_nsbd_row","is",_nsbd_row]
            ],
            columns: [
                search.createColumn({
                    name: "internalid",
                    sort: search.Sort.DESC,
                    label: "ID"
                })
            ]
        }).run();
        var nsbd_SearchResults = nsbd_SearchResults.getRange({start: 0, end: 1});
        if (nsbd_SearchResults != null && nsbd_SearchResults.length > 0){
            result = nsbd_SearchResults[0].id;
        }
        // log.debug({title: 'GF_MR_RICS_NonSellableBatch.js:find_nsbd_record', details: 'result: ' + result});
        return result;
    }

    /**
     * This function takes a string of the unique SKU value of the item and runs a search in NetSuite to find the RICS Product Details custom record.
     * @param {*} _sku - string - this should be the SKU of the item.
     * @returns - integer - the internal id of the RICS Product Details custom record.
     */
    function find_pd_record(_sku){
        log.audit({title: 'GF_MR_RICS_NonSellableBatch.js:find_pd_record', details: '_sku: ' + _sku});
        var result = 0;
        // find the Product Details Record
        var pd_SearchResults = search.create({
            type: "customrecord_rics_product_details",
            filters: [
                ["isinactive","is","F"], "AND",
                ["custrecord_rics_pd_sku","is",_sku]
            ],
            columns: [
                search.createColumn({
                    name: "id",
                    sort: search.Sort.DESC,
                    label: "ID"
                }),
                search.createColumn({name: "internalid", label: "Internal ID"})
            ]
        }).run();
        var pd_SearchResultSet = pd_SearchResults.getRange({start: 0, end: 1});
        if (pd_SearchResultSet != null && pd_SearchResultSet.length > 0){
            result = pd_SearchResultSet[0].id;
        }
        log.audit({title: 'GF_MR_RICS_NonSellableBatch.js:find_pd_record', details: 'result: ' + result});
        return result;
    }

    /**
     * This function takes a set of values and runs a search in NetSuite to find the RICS Product Item custom record.
     * @param {*} _nsbd_sku - string - this should be the SKU of the item
     * @param {*} _nsbd_column - string - this should be the column value of the item
     * @param {*} _nsbd_row - string - this should be the row value of the item
     * @returns - integer - the internal id of the RICS Product Item custom record.
     */
    function find_pi_record(_nsbd_sku, _nsbd_column, _nsbd_row){
        log.audit({title: 'GF_MR_RICS_NonSellableBatch.js:find_pi_record', details: '_nsbd_sku: ' + _nsbd_sku + ', _nsbd_column: ' + _nsbd_column + ', _nsbd_row: ' + _nsbd_row});
        var result = 0;
        // find the Product Details Record
        var pi_SearchResults_filters = [
            ["custrecord_rics_pi_parent_pd.custrecord_rics_pd_sku","is",_nsbd_sku]
        ];
        if(_nsbd_column != 'NA'){ // column isn't empty
            pi_SearchResults_filters.push("AND");
            pi_SearchResults_filters.push(["custrecord_rics_pi_column","is",_nsbd_column]);
        }else{ // column is empty
            pi_SearchResults_filters.push("AND");
            pi_SearchResults_filters.push([["custrecord_rics_pi_column","isempty",null], "OR", ["custrecord_rics_pi_column","is","NA"]]);
        }
        if(_nsbd_row != 'NA'){ // row isn't empty
            pi_SearchResults_filters.push("AND");
            pi_SearchResults_filters.push(["custrecord_rics_pi_row","is",_nsbd_row]);
        }else{ // row is empty
            pi_SearchResults_filters.push("AND");
            pi_SearchResults_filters.push([["custrecord_rics_pi_row","isempty",null], "OR", ["custrecord_rics_pi_row","is","NA"]]);
        }
        var pi_SearchResults = search.create({
            type: "customrecord_rics_product_item",
            filters: pi_SearchResults_filters,
            columns: [
               "id",
               "custrecord_rics_pi_parent_pd",
               search.createColumn({
                  name: "custrecord_rics_pi_upcs",
                  sort: search.Sort.ASC,
                  label: "UPCs"
               })
            ]
         }).run();
        var pi_SearchResultSet = pi_SearchResults.getRange({start: 0, end: 1});
        if (pi_SearchResultSet != null && pi_SearchResultSet.length > 0){
            result = pi_SearchResultSet[0].id;
        }
        log.audit({title: 'GF_MR_RICS_NonSellableBatch.js:find_pd_record', details: 'result: ' + result});
        return result;
    }

});