/**
 *@NApiVersion 2.1
 *@NScriptType MapReduceScript
 *
 Script Name:   GF_MR_RICS_PO_Receipt.js
 Author:        Mark Robinson
 */

 define(['N/log', 'N/search', 'N/record', '/SuiteScripts/GF_LIB/gf_nsrics'], function(log, search, record, gf_nsrics) {

    /**
     * In this get input function we're making API calls to RICS to get all the purchase order data for Purchase Orders that have a related RICS Purchase Order custom record,
     * where the NetSuite transaction is not using the Rejected, Fully Billed, or Closed status. All of the response data is added to an array and passed o the map function.
     * @param {*} context - this variable is not utilized in this function.
     * @returns - an array containing the data returned from the API calls.
     */
    function rics_getInputData(context){
        try {
            var arr_PurchaseOrders = [];
            var customrecord_rics_po_detailsSearchObj = search.create({
                type: "customrecord_rics_po_details",
                filters: [
                    ["custrecord_rics_pod_ns_po.mainline","is","F"], "AND",
                    ["formulanumeric: CASE WHEN {custrecord_rics_pod_receivedquantity} > {custrecord_rics_pod_ns_po.quantityshiprecv} THEN 1 ELSE 0 END","greaterthan","0"], "AND",
                    ["formulanumeric: CASE WHEN {custrecord_rics_pod_line_unique_id} = {custrecord_rics_pod_ns_po.lineuniquekey} THEN 1 ELSE 0 END","greaterthan","0"], "AND",
                    ["custrecord_rics_pod_ns_po","noneof","@NONE@"], "AND",
                    ["custrecord_rics_pod_ns_po.status","noneof","PurchOrd:H","PurchOrd:G","PurchOrd:C"] // is none of Purchase Order:Closed, Purchase Order:Fully Billed, Purchase Order:Rejected by Supervisor
                ],
                columns: [
                    search.createColumn({
                        name: "lastmodified",
                        sort: search.Sort.DESC
                    }),
                    search.createColumn({
                        name: "name",
                        sort: search.Sort.ASC
                    }),
                    search.createColumn({
                        name: "custrecord_rics_po_purchaseordernumber",
                        join: "CUSTRECORD_RICS_POD_PARENT_PO"
                    }),
                    search.createColumn({
                        name: "custrecord_rics_pod_ns_po",
                        sort: search.Sort.ASC
                    }),
                    search.createColumn({
                        name: "custrecord_rics_pod_line_unique_id",
                        sort: search.Sort.ASC
                    }),
                    "custrecord_rics_pod_receivedquantity",
                    "custrecord_rics_pod_orderquantity",
                    search.createColumn({
                        name: "quantityshiprecv",
                        join: "CUSTRECORD_RICS_POD_NS_PO"
                    }),
                    search.createColumn({
                        name: "item",
                        join: "CUSTRECORD_RICS_POD_NS_PO"
                    }),
                    "custrecord_rics_pod_cost"
                ]
            });
		    var result_count = 0;
            customrecord_rics_po_detailsSearchObj.run().each(function(result){
				result_count += 1;
				if(result_count > 3998){ return false; }
                // .run().each has a limit of 4,000 results
                var obj_data = {};
                obj_data.po_num = result.getValue({ name: "custrecord_rics_po_purchaseordernumber", join: "CUSTRECORD_RICS_POD_PARENT_PO" });
                obj_data.po_id = result.getValue({ name: "custrecord_rics_pod_ns_po" });
                obj_data.po_line_id = result.getValue({ name: "custrecord_rics_pod_line_unique_id" });
                var num_previously_received_qty = result.getValue({ name: "quantityshiprecv", join: "CUSTRECORD_RICS_POD_NS_PO" });
                obj_data.received_qty = (num_previously_received_qty != '' && parseInt(num_previously_received_qty) > 0) ? parseInt(result.getValue({ name: "custrecord_rics_pod_receivedquantity" })) - parseInt(num_previously_received_qty) : result.getValue({ name: "custrecord_rics_pod_receivedquantity" });
                // obj_data.received_qty = result.getValue({ name: "custrecord_rics_pod_receivedquantity" });
                if(obj_data.received_qty < 1){ return true; }
                obj_data.item_id = result.getValue({ name: "item", join: "CUSTRECORD_RICS_POD_NS_PO" });
                obj_data.cost = result.getValue({ name: "custrecord_rics_pod_cost" });
                // log.debug({title: 'GF_MR_RICS_PO_Receipt.js:rics_getInputData', details: 'obj_data: ' + JSON.stringify(obj_data)});
                if(arr_PurchaseOrders[obj_data.po_id] == undefined){
                    arr_PurchaseOrders[obj_data.po_id] = {"line_data":[obj_data]};
                }else{
                    (arr_PurchaseOrders[obj_data.po_id].line_data).push(obj_data);
                }
               return true;
            });
            var arr_PurchaseOrders = arr_PurchaseOrders.filter(function(el){ return el != null; });
            log.debug({title: 'GF_MR_RICS_PO_Receipt.js:rics_getInputData', details: 'arr_PurchaseOrders: ' + JSON.stringify(arr_PurchaseOrders)});
            return arr_PurchaseOrders;
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_PO_Receipt.js:rics_getInputData', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this mapping function we take the data that was passed in from the get input function and map it out as a set of NetSuite writable objects.
     * Some validation is also performed that will help to prevent the creation (or re-creation) of receipts that have been previously billed in full.
     * @param {*} context - this should contain the data from the API calls in the get input function.
     * @returns void (note: the const context variable is written to and used in the reduce function)
     */
     function rics_map(context){
        try{
            var data = JSON.parse(context.value);
            if(data == null){ return true; }
            log.debug({title: 'GF_MR_RICS_PO_Receipt.js:rics_map', details: 'data: ' + JSON.stringify(data)});
            var po_num = data.line_data[0].po_num;
            var po_id = data.line_data[0].po_id;
            var ns_record_data = {};
            // find item receipt
            var item_receipt_id = find_item_receipt(po_num, po_id);
            // load po record
            var po_record = record.load({
                type: 'purchaseorder',
                id: parseInt(po_id),
                isDynamic: true
            });
            log.debug({title: 'GF_MR_RICS_PO_Receipt.js:rics_map', details: 'item_receipt_id: ' + item_receipt_id});
            if(parseInt(item_receipt_id) > 0){
                // need to validate the items for changes
                var po_fields = get_data_from_po(po_id);
                log.debug({title: 'GF_MR_RICS_PO_Receipt.js:rics_map', details: 'po_fields: ' + JSON.stringify(po_fields)});
                // more items were added to the PO and we need to rebuild the Item Receipt - OR Create a new Item Receipt if the current one has already been billed
                // find bill record (if bill exists we need to create a new item receipt for ONLY the new items if no bill exists we can rebuild the current item receipt)
                var bill_id = find_bill(po_num, po_id);
                if(parseInt(bill_id) > 0){
                    // bill exists we need to create a new item receipt for ONLY the new items
                    // get all upcs from the bill
                    var arr_bill_items = get_bill_items(bill_id); // ------------------------------------------------------- may have to change this function to use the item id instead of the upc for the array key
                    // for every matching line item - remove the matching object from the data.Details array
                    for(var item in data.line_data){
                        var item_data = data.line_data[item];
                        if(arr_bill_items[item_data.item_id] != undefined){
                            log.debug({title: 'GF_MR_RICS_PO_Receipt.js:rics_map', details: 'billed item id: ' + item_data.item_id + ', new ReceivedQuantity: ' + item_data.received_qty + ', billed qty: ' + arr_bill_items[item_data.item_id]});
                            // this item was billed check for partial or full
                            if(item_data.received_qty == arr_bill_items[item_data.item_id]){
                                // remove the item from processing if it is fully billed
                                delete data.line_data[item];
                            }else{
                                // reduce received quantity by the quantity already billed
                                data.line_data[item].received_qty = data.line_data[item].received_qty - arr_bill_items[item_data.item_id];
                            }
                        }
                    }
                }

                ns_record_data.recordtype = 'itemreceipt';
                ns_record_data.customform = 130;
                var vendor_id = po_fields.entity[0].value;
                ns_record_data.entity = vendor_id;
                var subsidiary_id = po_fields.subsidiary[0].value;
                ns_record_data.subsidiary = subsidiary_id;
                var location_id = po_fields.location[0].value;
                ns_record_data.location = location_id;
                var rics_po_str = po_fields.custbody_gf_internal_po;
                ns_record_data.custbody_gf_internal_po = rics_po_str;
                ns_record_data.createdfrom = po_id; // data.nspoid;

                // map line items
                var str_most_recent_update = po_fields.trandate; // the item dates should be more recent but this is our default value because this is a required field
                var arr_items = [];
                for(var item in data.line_data){
                    var item_data = data.line_data[item];
                    if(!item_data.hasOwnProperty('received_qty')){ continue; } // if we don't have received_qty then it was not received
                    log.debug({title: 'GF_MR_RICS_PO_Receipt.js:rics_map', details: 'map find_items - item_data: ' + JSON.stringify(item_data)});
                    if(item_data.item_id > 0){
                        arr_items.push(item_data);
                    }
                }
                ns_record_data.items = arr_items;
                ns_record_data.trandate = str_most_recent_update;
            }else{
                // need to build a new item receipt
                ns_record_data.recordtype = 'itemreceipt';
                ns_record_data.customform = 130;
                var po_fields = get_data_from_po(po_id);
                log.debug({title: 'GF_MR_RICS_PO_Receipt.js:rics_map', details: 'po_fields: ' + JSON.stringify(po_fields)});
                var vendor_id = po_fields.entity[0].value;
                ns_record_data.entity = vendor_id;
                var subsidiary_id = po_fields.subsidiary[0].value;
                ns_record_data.subsidiary = subsidiary_id;
                var location_id = po_fields.location[0].value;
                ns_record_data.location = location_id;
                var rics_po_str = po_fields.custbody_gf_internal_po;
                ns_record_data.custbody_gf_internal_po = rics_po_str;
                ns_record_data.createdfrom = po_id;

                // map line items
                var str_most_recent_update = po_fields.trandate; // the item dates should be more recent but this is our default value because this is a required field
                var arr_items = [];
                for(var item in data.line_data){
                    var item_data = data.line_data[item];
                    if(!item_data.hasOwnProperty('received_qty')){ continue; } // if we don't have received_qty then it was not received
                    log.debug({title: 'GF_MR_RICS_PO_Receipt.js:rics_map', details: 'map find_items - item_data: ' + JSON.stringify(item_data)});
                    if(item_data.item_id > 0){
                        arr_items.push(item_data);
                    }
                }
                ns_record_data.items = arr_items;
                ns_record_data.trandate = str_most_recent_update;
            }
            var nsrics = gf_nsrics.getIntegration();
            var open_period_range = nsrics.getOpenPeriodRange();
            if(new Date(ns_record_data.trandate) < new Date(open_period_range.earliest_open_date) || new Date(ns_record_data.trandate) > new Date(open_period_range.latest_open_date)){
                var dt = new Date();
                var str_dt = dt.getMonth()+1 + '-1-' + dt.getFullYear();
                ns_record_data.trandate = str_dt;
            }

            // log.debug({title: 'GF_MR_RICS_PO_Receipt.js:rics_map', details: 'ns_record_data: ' + JSON.stringify(ns_record_data)});
            if(ns_record_data.hasOwnProperty('items') && ns_record_data.items.length > 0){
                // pass data to reduce for creating item receipt
                context.write({ key: po_num, value: ns_record_data });
            }
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_PO_Receipt.js:rics_map', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this reduce function we take the mapped context objects and create or update all of the NetSuite Item Receipt transactions.
     * @param {*} context - this should be the mapped object containing all of the fields and data that need to be on the NetSuite Item Receipt record.
     * @returns void
     */
    function rics_reduce(context){
        try{
            var data = JSON.parse(context.values[0]);
            if(data == null){ return true; }
            log.debug({title: 'GF_MR_RICS_PO_Receipt.js:rics_reduce', details: 'data: ' + JSON.stringify(data)});
            var nsrics = gf_nsrics.getIntegration();
            var open_period_range = nsrics.getOpenPeriodRange(); // .earliest_open_date - .latest_open_date
            if(!data.hasOwnProperty('ir_id')){
                var arr_not_record_prop = [ // these values are for processing, and not to be directly written on the record
                    'recordtype',
                    'createdfrom'
                ];
                try{
                    var item_receipt_record = record.transform({
                        fromType : record.Type.PURCHASE_ORDER,
                        fromId : data.createdfrom,
                        toType : record.Type.ITEM_RECEIPT
                    });
                }catch(ex) {
                    log.audit({title: 'GF_MR_RICS_PO_Receipt.js:rics_reduce', details: 'Error - can\'t create IR Record: ' + ex.toString() + ' : ' + ex.stack + ', -- data: ' + JSON.stringify(data)});
                    return true;
                }
                if(new Date(data['trandate']) > new Date(open_period_range.earliest_open_date) && new Date(data['trandate']) < new Date(open_period_range.latest_open_date)){
                	item_receipt_record.setValue('trandate',new Date(data['trandate']));
                }else{
                    item_receipt_record.setValue('trandate',new Date());
                }
                for(var prop in data){
                    if(arr_not_record_prop.indexOf(prop) == -1 && item_receipt_record != undefined && prop != undefined && data[prop] != undefined){
                        if(prop == 'trandate'){
                            item_receipt_record.setValue(prop,new Date(data[prop]));
                        }else{
                            item_receipt_record.setValue(prop,data[prop]);
                        }
                    }
                }
            }else{
                // load the record in a normal way if it already exists
                if(new Date(data['trandate']) > new Date(open_period_range.earliest_open_date) && new Date(data['trandate']) < new Date(open_period_range.latest_open_date)){
                    var item_receipt_record = record.load({
                        type: record.Type.ITEM_RECEIPT,
                        id: data.ir_id
                    });
                    item_receipt_record.setValue('trandate',new Date(data['trandate']));
                }else{
                    var item_receipt_record = record.transform({
                        fromType : record.Type.PURCHASE_ORDER,
                        fromId : data.items[0].po_id,
                        toType : record.Type.ITEM_RECEIPT
                    });
                    item_receipt_record.setValue('trandate',new Date());
                }
            }

            var irLineCount = item_receipt_record.getLineCount({ sublistId : "item" });
            // log.audit({title: 'GF_MR_RICS_PO_Receipt.js:rics_reduce', details: 'irLineCount: ' + irLineCount + ', items.length: ' + (data.items).length });
            // First un-select all as we will manually perform the operation
            for (var i = 0; i < irLineCount; i++) {
                item_receipt_record.setSublistValue({sublistId: "item", fieldId: "itemreceive", value: false, line : i});
            }
            // Second select only matching by data object
            var count_lines_set = 0;
            for(var line in data.items){
                var line_data = data.items[line]; // from data object
                var lineNumber = item_receipt_record.findSublistLineWithValue({
                    sublistId: 'item',
                    fieldId: 'item',
                    value: line_data.item_id
                });
                // log.audit({title: 'GF_MR_RICS_PO_Receipt.js:rics_reduce', details: 'lineNumber: ' + lineNumber + ', line_data.item_id: ' +  line_data.item_id});
                if(lineNumber != -1 && line_data.received_qty > 0){
                    log.debug({title: 'GF_MR_RICS_PO_Receipt.js:rics_reduce', details: 'lineNumber: ' + lineNumber + ', line_data: ' + JSON.stringify(line_data)});
                    set_item_data_new_ir(lineNumber, item_receipt_record, line_data.item_id, data.location, line_data.received_qty, line_data.cost);
                    count_lines_set += 1;
                }
            }

            if(count_lines_set > 0){ // if we didn't set anything then there's nothing to receive
                try{
                    var saved_ir_id = item_receipt_record.save();
                }catch(ex){
                    log.error({title: 'GF_MR_RICS_PO_Receipt.js:rics_reduce-save failed', details: 'Error ' + ex.toString() + ' : ' + ex.stack + ', -- data: ' + JSON.stringify(data)});
                }
                log.debug({title: 'GF_MR_RICS_PO_Receipt.js:rics_reduce', details: 'saved_ir_id: ' + saved_ir_id});
            }else{
                // if no lines were set because this record had a previous IR that has not yet been billed - maybe it has been received but that receipt has not yet been billed
                if(data.hasOwnProperty('ir_id') && new Date(data['trandate']) > new Date(open_period_range.earliest_open_date) && new Date(data['trandate']) < new Date(open_period_range.latest_open_date)){
                    var item_receipt_record = record.transform({
                        fromType : record.Type.PURCHASE_ORDER,
                        fromId : data.items[0].po_id,
                        toType : record.Type.ITEM_RECEIPT
                    });
                    item_receipt_record.setValue('trandate',new Date(data['trandate']));

                    var irLineCount = item_receipt_record.getLineCount({ sublistId : "item" });
                    // log.audit({title: 'GF_MR_RICS_PO_Receipt.js:rics_reduce', details: 'irLineCount: ' + irLineCount + ', items.length: ' + (data.items).length });
                    // First un-select all as we will manually perform the operation
                    for (var i = 0; i < irLineCount; i++) {
                        item_receipt_record.setSublistValue({sublistId: "item", fieldId: "itemreceive", value: false, line : i});
                    }
                    // Second select only matching by data object
                    var count_lines_set = 0;
                    for(var line in data.items){
                        var line_data = data.items[line]; // from data object
                        var lineNumber = item_receipt_record.findSublistLineWithValue({
                            sublistId: 'item',
                            fieldId: 'item',
                            value: line_data.item_id
                        });
                        // log.audit({title: 'GF_MR_RICS_PO_Receipt.js:rics_reduce', details: 'lineNumber: ' + lineNumber + ', line_data.item_id: ' +  line_data.item_id});
                        if(lineNumber != -1 && line_data.received_qty > 0){
                            log.debug({title: 'GF_MR_RICS_PO_Receipt.js:rics_reduce', details: 'lineNumber: ' + lineNumber + ', line_data: ' + JSON.stringify(line_data)});
                            set_item_data_new_ir(lineNumber, item_receipt_record, line_data.item_id, data.location, line_data.received_qty, line_data.cost);
                            count_lines_set += 1;
                        }
                    }
                    if(count_lines_set > 0){ // if we didn't set anything then there's nothing to receive
                        try{
                            var saved_ir_id = item_receipt_record.save();
                        }catch(ex){
                            log.error({title: 'GF_MR_RICS_PO_Receipt.js:rics_reduce-save failed', details: 'Error ' + ex.toString() + ' : ' + ex.stack + ', -- data: ' + JSON.stringify(data)});
                        }
                        log.debug({title: 'GF_MR_RICS_PO_Receipt.js:rics_reduce', details: 'saved_ir_id: ' + saved_ir_id});
                    }
                }
            }
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_PO_Receipt.js:rics_reduce', details: 'Error ' + ex.toString() + ' : ' + ex.stack + ', -- data: ' + JSON.stringify(data)});
        }
    }

    /**
     * This summarize function is used to report important aspects about how the script performed
     * @param {*} summary - this object contains data regarding script performance.
     */
    function rics_summarize(summary) {
        try{
            log.audit('Usage Consumed', summary.usage);
            log.audit('Concurrency Number ', summary.concurrency);
            log.audit('Number of Yields', summary.yields);
            log.audit({title: 'Process Completed'});
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_PO_Receipt.js:rics_summarize', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    return {
        getInputData: rics_getInputData,
        map: rics_map,
        reduce: rics_reduce,
        summarize: rics_summarize
    };

    // =========================== extra functions
    /**
     * This function takes a string of the unique RICS PO number, and the ID of the NetSuite Purchase Order that created the Item Receipt,
     * and runs a search in NetSuite to find the NetSuite Item Receipt transaction record.
     * @param {*} _str_po_num - string - unique PO number from RICS
     * @param {*} _po_id - integer - the parent transaction record
     * @returns - integer - this is the internal id of the Item Receipt record.
     */
     function find_item_receipt(_str_po_num, _po_id){
        log.debug({title: 'GF_MR_RICS_PO_Receipt.js:find_item_receipt', details: '_str_po_num: ' + _str_po_num + ', _po_id: ' + _po_id});
        var result_id = 0;

        var itemreceiptSearchObj = search.create({
            type: "itemreceipt",
            filters: [
               ["type","anyof","ItemRcpt"], "AND",
               ["createdfrom","anyof",parseInt(_po_id)], "AND",
               ["custbody_gf_internal_po","is",_str_po_num], "AND",
               ["mainline","is","T"]
            ],
            columns: [
                search.createColumn({
                    name: "datecreated",
                    sort: search.Sort.DESC,
                    label: "Date Created"
                })
            ]
         });
         itemreceiptSearchObj.run().each(function(result){
            // .run().each has a limit of 4,000 results
            if(result_id == 0){
                result_id = result.id;
            }
            return true;
         });
         log.debug({title: 'GF_MR_RICS_PO_Receipt.js:find_item_receipt', details: 'result_id: ' + result_id});
        return result_id;
    }

    /**
     * This function uses a NetSuite search to try to find a vendor bill using the PO Number from RICS and the purchase order name of the NetSuite transaction.
     * @param {*} _str_po_num - string - the RICS PO Number
     * @param {*} _str_ns_po_num - string - the name of the related po that the bill was created from
     * @returns - integer - the internal id of the bill
     */
    function find_bill(_str_po_num, _str_ns_po_num){
        log.debug({title: 'GF_MR_RICS_PO_Receipt.js:find_bill', details: '_str_po_num: ' + _str_po_num + ', _str_ns_po_num: ' + _str_ns_po_num});
        var result_id = 0;

        var vendbillSearchObj = search.create({
            type: "vendorbill",
            filters: [
               ["type","anyof","VendBill"], "AND",
               ["custbody_nsts_vp_purchase_order","is",_str_ns_po_num], "AND",
               ["custbody_gf_internal_po","is",_str_po_num], "AND",
               ["mainline","is","T"]
            ],
            columns: [
                search.createColumn({
                    name: "datecreated",
                    sort: search.Sort.DESC,
                    label: "Date Created"
                })
            ]
         });
         vendbillSearchObj.run().each(function(result){
            // .run().each has a limit of 4,000 results
            if(result_id == 0){
                result_id = result.id;
            }
            return true;
         });
         log.debug({title: 'GF_MR_RICS_PO_Receipt.js:find_bill', details: 'result_id: ' + result_id});
        return result_id;
    }

    /**
     * This function will get all upc data related to a bill.
     * @param {*} _bill_id  - integer - this should be the internal id of the bill that we are going to return line item data from.
     * @returns - array - the array contains the quantity for each upc listed on the bill by key value pair
     */
    function get_bill_items(_bill_id){
        var arr_result = [];
        try{
            var bill_record = record.load({
                type: 'vendorbill',
                id: _bill_id,
                isDynamic: true
            });
            var currentLineCount = bill_record.getLineCount({ 'sublistId': 'item' });
            for(var i = 0; currentLineCount > 0 && i < currentLineCount; i++){
                bill_record.selectLine({ sublistId: 'item', line: i });
                var item_id = bill_record.getCurrentSublistValue({
                    sublistId: 'item',
                    fieldId: 'item'
                });
                var item_qty = bill_record.getCurrentSublistValue({
                    sublistId: 'item',
                    fieldId: 'quantity'
                });
                log.debug({title: 'GF_MR_RICS_PO_Receipt.js:get_bill_items', details: 'item_id: ' + item_id + ', item_qty: ' + item_qty});
                arr_result[item_id] = item_qty;
            }
            return arr_result;
        }catch(ex) {
            return arr_result;
        }
    }

    /**
     * This function is a simple lookupFields search that returns data for the specified NetSuite Purchase Order.
     * @param {*} _nspoid - the internal id of the NetSuite Purchase Order.
     * @returns - object - the data from the NetSuite Purchase Order record.
     */
    function get_data_from_po(_nspoid){
        var fieldLookUp = search.lookupFields({
            type: 'purchaseorder',
            id: _nspoid,
            columns: ['entity', 'subsidiary', 'location', 'trandate', 'custbody_gf_internal_po', 'tranid']
        });
        return fieldLookUp;
    }

    /**
     * This function uses the parameters to set the line item values on an Item Receipt
     * @param {*} _line_num - integer - this is the line number that will be set or updated
     * @param {*} _ir_rec - object - this is the item receipt record object that will be affected
     * @param {*} _item_id - integer - this is the internal id of the item for this line
     * @param {*} _location_id - integer - this is the internal id of the location for this line
     * @param {*} _item_qty - integer - this is the total count of items for this line
     * @param {*} _item_cost - decimal - this is the amount that each of these items costs
     */
    function set_item_data_new_ir(_line_num, _ir_rec, _item_id, _location_id, _item_qty, _item_cost){
        var flg_receive = (_item_qty > 0) ? true : false;
        // also add the line item
        _ir_rec.setSublistValue({
            sublistId: 'item',
            fieldId: 'item',
            line: _line_num,
            value: _item_id
        });
        _ir_rec.setSublistValue({
            sublistId: 'item',
            fieldId: 'itemreceive',
            line: _line_num,
            value: flg_receive
        });
        _ir_rec.setSublistValue({
            sublistId: 'item',
            fieldId: 'location',
            line: _line_num,
            value: _location_id
        });
        _ir_rec.setSublistValue({
            sublistId: 'item',
            fieldId: 'quantity',
            line: _line_num,
            value: _item_qty
        });
        _ir_rec.setSublistValue({
            sublistId: 'item',
            fieldId: 'rate',
            line: _line_num,
            value: _item_cost
        });
    }

});