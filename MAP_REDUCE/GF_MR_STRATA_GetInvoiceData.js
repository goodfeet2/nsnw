/**
 *@NApiVersion 2.1
 *@NScriptType MapReduceScript
 *
Script Name:   GF_MR_STRATA_GetInvoiceData.js
Author:        Mark Robinson
 */

define(['N/log', 'N/search', 'N/record', 'N/https', 'N/runtime'], function(log, search, record, https, runtime){

    /**
     * In this get input function we
     * @param {*} context  - this variable is not utilized in this function
     * @returns - an array containing
     */
    function strata_getInputData(context){
        try {
            var result_obj = {};
            var arr_invoices = [];
            var currentScript = runtime.getCurrentScript();
            // first step is to Authenticate with Strata
            var str_application_key = currentScript.getParameter({ name: 'custscript_strata_auth_appkey' }); // "iiUlK+qtf5xTjdj7FHvkwi2XhA9cHfl1d4m4i125epXUtOtgRLi2Hb2cY6qHNgaRxGq3pI+nKlvGNV5c7tuFA5kgBqJjli9IZuZRRQx5XIA5ymetfTsUYzoA7yy2BXVjowJIl1u5gb3hyc43agI1A6NceIBHtLha9BD6wgx7dbE="
            var str_ocp_apim_subscription_key = currentScript.getParameter({ name: 'custscript_strata_subkey' }); // '542f559c80b847eb967dca88b3c313eb'

            // send refresh token request
            result_obj.authtoken = getAuthToken(str_application_key, str_ocp_apim_subscription_key);

            if(typeof result_obj.authtoken == 'string'){
                // if successful - use token to send get invoices request
                let header = [];
                header['token'] = result_obj.authtoken;
                header['Content-Type'] = 'application/json';
                header['ocp-apim-subscription-key'] = str_ocp_apim_subscription_key;
                let response = https.get({
                    url: 'https://strata.azure-api.net/FinancialBridgeAP/api/V5/invoices/ids',
                    headers:header
                });
                let response_data = JSON.parse(response.body);
                log.debug({title: 'GF_MR_STRATA_GetInvoiceData.js:strata_getInputData', details: 'IDS response_data: ' + JSON.stringify(response_data)});
                arr_invoices = response_data;
            }
            result_obj.invoices = arr_invoices;

            return [ result_obj ]; // send the result data to the map function for additional calls to strata
        }catch(ex) {
            log.error({title: 'GF_MR_STRATA_GetInvoiceData.js:strata_getInputData', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this mapping function we
     * @param {*} context - Object - This should be the list of invoice ids that will allow us to get all of the strata invoice details
     * @returns void (note: the const context variable is impacted and used in the reduce function)
     */
    function strata_map(context){
        try{
            var currentScript = runtime.getCurrentScript();
            var str_ocp_apim_subscription_key = currentScript.getParameter({ name: 'custscript_strata_subkey' });
            var map_data = JSON.parse(context.value);
            log.debug({title: 'GF_MR_STRATA_GetInvoiceData.js:strata_map', details: 'map_data: ' + JSON.stringify(map_data)});
			if (map_data == null) {
				return true;
			}else{
                // use the strata invoice id and the auth token to call for the invoice details object
                try{
                    for(var inv_id in map_data.invoices){
                        // context.write({ key: strata_invoice_id?, value: invoice_detail_data? });
                        let strata_invoice_data = getStrataInvoiceDetail(map_data.authtoken, str_ocp_apim_subscription_key, map_data.invoices[inv_id]);
                        strata_invoice_data.authtoken = map_data.authtoken;
                        log.debug({title: 'GF_MR_STRATA_GetInvoiceData.js:strata_map-data', details: 'strata_invoice_data: ' + JSON.stringify(strata_invoice_data)});
                        context.write({ key: map_data.invoices[inv_id], value: strata_invoice_data });
                    }
                }catch(ex){
                    log.error({title: 'GF_MR_STRATA_GetInvoiceData.js:strata_map-loop', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
                }
            }

			// context.write({ key: context.key, value: map_data }); // pass through directly to reduce

			return true;
        }catch(ex) {
            log.error({title: 'GF_MR_STRATA_GetInvoiceData.js:strata_map', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this reduce function we write the data to a vendor bill record
     * @param {*} context - Object - This is the object that was built in the mapping function, it should conatin all the data we need for processing
     * @returns - void
     */
    function strata_reduce(context){
        var reduce_data = JSON.parse(context.values[0]);
        log.debug({title: 'GF_MR_STRATA_GetInvoiceData.js:strata_reduce', details: 'reduce_data: ' + JSON.stringify(reduce_data)});
        /*
        The data is currently aimed at creating BILL transatcion records
        Mapping according to Strata
        Source record field (REST API) -------------------------------------- Destination record field (NetSuite)
        invoiceDate --------------------------------------------------------- Date
        invoiceDueDate ------------------------------------------------------ Due Date
        currencyCode -------------------------------------------------------- Currency (InternalId) // should always be US Dollar
        LOOKUP -------------------------------------------------------------- Vendor (InternalId) // vendor.vendorName is used with the following regex {{regexReplace vendor.vendorName "-TV" "-S2"}} // all S2 stations should point to TV
        invoiceNumber ------------------------------------------------------- Reference No.
        invoiceNetCost ------------------------------------------------------ Amount (Field ID: usertotal)
        "true" -------------------------------------------------------------- Items : Replace All Lines // hardcoded
        id ------------------------------------------------------------------ Strata Invoice ID
        "true" -------------------------------------------------------------- Print Nlapi Debug Logs // hardcoded
        LOOKUP -------------------------------------------------------------- Items : Item (InternalId) // use vendor name to get item id vendor.vendorName is used with the following regex {{regexReplace vendor.vendorName "-TV" "-S2"}} // all S2 stations should point to TV
        "1" ----------------------------------------------------------------- Items : Quantity // hardcoded
        "317" --------------------------------------------------------------- Items : Location (InternalId) // hardcoded
        invoiceEstimates.0.invoiceDetails.0.invoiceOrderLines.0.marketName -- Items : Customer (InternalId) // LOOKUP SEE BELOW (should create a saved search to find this data)
        invoiceNetCost ------------------------------------------------------ Items : Rate
        _________________________________________________________________________________________________________________________

        Export field value ---------- Import field value
        Eugene-Springfield ---------- 1832
        Fargo-MOORHEAD -------------- 57461
        Portland, OR ---------------- 1831
        Columbia, SC ---------------- 3182162
        Charlotte-Gastonia-Rk Hill -- 1837
        Salt Lake City-Ogden -------- 1829
        Greenville-Spartanburg ------ 437027
        Spokane --------------------- 1834
        Atlanta --------------------- 265081
        Boise ----------------------- 1828
        Tri-Cities (Ric-Ken-Pas) ---- 1835
        Sarasota-Bradenton ---------- 2609749
        Florence, SC ---------------- ?
        */

        // search for the custom record by the id
        let strata_invoice_id = findStrataInvoiceId(reduce_data.id);
        if(strata_invoice_id == 0){
            // create the record
            let strata_invoice_record = record.create({
                type: 'customrecord_strata_invoice'
            });
            strata_invoice_record.setValue('custrecord_strata_invoice_json',JSON.stringify(reduce_data));
            let saved_inv_id = strata_invoice_record.save();
            log.debug({title: 'GF_MR_STRATA_GetInvoiceData.js:strata_reduce-create', details: 'saved_inv_id: ' + saved_inv_id});
        }else{
            // update the record
            let strata_invoice_record_update = record.submitFields({
                type: 'customrecord_strata_invoice',
                id: strata_invoice_id,
                values: { 'custrecord_strata_invoice_json': JSON.stringify(reduce_data) }
            });
            log.debug({title: 'GF_MR_STRATA_GetInvoiceData.js:strata_reduce-update', details: 'strata_invoice_record_update: ' + strata_invoice_record_update});
        }

        // Try to create the bill record _________________________________________________________________________________________________________________________
        try{
            let vendor_id = findVendorByName(reduce_data.vendor);
            if(vendor_id == 0){
                log.error({title: 'GF_MR_STRATA_GetInvoiceData.js:strata_reduce-vendor not found', details: 'CREATE VENDOR RECORD FOR Vendor: ' + JSON.stringify(reduce_data.vendor)});
                // I have the vendor data I can just build the vendor record and get the id (address may be required, use strata vendor endpoint) - enhancement
                return true;
            }else{
                let vendor_lookup = search.lookupFields({
                    type: 'vendor',
                    id: vendor_id,
                    columns: ['terms']
                });
                let vendor_terms = (vendor_lookup.terms && vendor_lookup.terms != '') ? vendor_lookup.terms : ''; // eventually we will use these to determine how the invoice due date should be set when it is blank
            }
            let customer_id = findCustomerByMarketName(reduce_data);
            if(customer_id == 0){
                // log.error({title: 'GF_MR_STRATA_GetInvoiceData.js:strata_reduce-customer not found', details: 'Customer: ' + JSON.stringify(reduce_data)});
                return true;
            }
            let item_id = findItemByName(reduce_data.vendor);
            if(item_id == 0){
                log.error({title: 'GF_MR_STRATA_GetInvoiceData.js:strata_reduce-item not found', details: 'CREATE ITEM RECORD FOR Item: ' + JSON.stringify(reduce_data.vendor)});
                return true;
            }
            let billfields = {};
            billfields.trandate = new Date((reduce_data.invoiceDate).replace('Z', ''));
            let date = new Date();
            let firstDayOfCurrentMonth = new Date(date.getFullYear(), date.getMonth(), 1);
            if((billfields.trandate).valueOf() < firstDayOfCurrentMonth.valueOf()){
                billfields.memo = `Original Invoice Date: ${billfields.trandate}`;
                billfields.trandate = firstDayOfCurrentMonth;
            }
            billfields.duedate = (reduce_data.invoiceDueDate != undefined && reduce_data.invoiceDueDate != null) ? new Date((reduce_data.invoiceDueDate).replace('Z', '')) : '';
            billfields.entity = vendor_id;
            billfields.tranid = reduce_data.invoiceNumber; // strata invoice number
            billfields.custbody2 = reduce_data.id; // strata invoice id
            billfields.usertotal = reduce_data.invoiceNetCost; // this will likely be over written by the line total calc
            log.debug({title: 'GF_MR_STRATA_GetInvoiceData.js:strata_reduce-billfields', details: 'billfields: ' + JSON.stringify(billfields)});
            var vendorbill_record = record.create({
                type: 'vendorbill'
            });
            for(var prop in billfields){
                vendorbill_record.setValue(prop,billfields[prop]);
            }
            // set line data
            vendorbill_record.setSublistValue({
                sublistId: 'item',
                fieldId: 'item',
                line: 0,
                value: item_id
            });
            vendorbill_record.setSublistValue({
                sublistId: 'item',
                fieldId: 'quantity',
                line: 0,
                value: 1
            });
            vendorbill_record.setSublistValue({
                sublistId: 'item',
                fieldId: 'rate',
                line: 0,
                value: reduce_data.invoiceNetCost
            });
            vendorbill_record.setSublistValue({
                sublistId: 'item',
                fieldId: 'location',
                line: 0,
                value: 317 // Summit Media LLC
            });
            vendorbill_record.setSublistValue({
                sublistId: 'item',
                fieldId: 'customer',
                line: 0,
                value: customer_id
            });

            var saved_bill_id = vendorbill_record.save();
            log.debug({title: 'GF_MR_STRATA_GetInvoiceData.js:strata_reduce-create bill', details: 'saved_bill_id: ' + saved_bill_id});
        }catch(e){
            log.error({title: 'GF_MR_STRATA_GetInvoiceData.js:strata_reduce-issue creating bill record', details: 'CONTACT NETSUITE DEVELOPER - Error: ' + ex.toString() + ' : ' + ex.stack});
        }
        // send response to strata
        var currentScript = runtime.getCurrentScript();
        var str_ocp_apim_subscription_key = currentScript.getParameter({ name: 'custscript_strata_subkey' });
        setInvoiceStatus(reduce_data.authtoken, str_ocp_apim_subscription_key, reduce_data.id);

        var strata_invoice_record_update = record.submitFields({
            type: 'customrecord_strata_invoice',
            id: strata_invoice_id,
            values: { 'custrecord_strata_vendbill': saved_bill_id }
        });
        log.debug({title: 'GF_MR_STRATA_GetInvoiceData.js:strata_reduce-update', details: 'strata_invoice_record_update: ' + strata_invoice_record_update});
        return true;
    }

    /**
     * This summarize function is used to report important aspects about how the script performed
     * @param {*} summary - this object contains data regarding script performance.
     */
    function strata_summarize(summary) {
        try{
            log.audit('Usage Consumed', summary.usage);
            log.audit('Concurrency Number ', summary.concurrency);
            log.audit('Number of Yields', summary.yields);
            log.audit({title: 'Process Completed'});
        }catch(ex) {
            log.error({title: 'GF_MR_STRATA_GetInvoiceData.js:strata_summarize', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    return {
        getInputData: strata_getInputData,
        map: strata_map,
        reduce: strata_reduce,
        summarize: strata_summarize
    };

    // =========================== extra functions
    /**
     * This function uses the application key and subscription key to return an authentication token
     * @param {string} _appkey
     * @param {string} _subkey
     * @returns {string,object}
     */
    function getAuthToken(_appkey, _subkey){
        // log.debug({title: 'GF_MR_STRATA_GetInvoiceData.js:getAuthToken', details: 'params - _appkey: ' + _appkey + ', _subkey: ' + _subkey});
        let header = [];
        header['Content-Type'] = 'application/json';
        header['ocp-apim-subscription-key'] = _subkey;
        let postData = { "applicationKey": _appkey };
        postData = JSON.stringify(postData);
        let response = https.post({
            url: 'https://strata.azure-api.net/FinancialBridgeAuth/api/V4/app/logon',
            headers:header,
            body: postData
        });
        let response_data = JSON.parse(response.body);
        return (response_data.token != undefined) ? response_data.token : response_data;
    }

    /**
     *
     * @param {*} _authtoken
     * @param {*} _subkey
     * @param {*} _inv_id
     * @returns 
     */
    function getStrataInvoiceDetail(_authtoken, _subkey, _inv_id){
        log.debug({title: 'GF_MR_STRATA_GetInvoiceData.js:getStrataInvoiceDetail', details: 'params - _authtoken: ' + _authtoken + ', _inv_id: ' + _inv_id});
        let header = [];
        header['token'] = _authtoken;
        header['Content-Type'] = 'application/json';
        header['ocp-apim-subscription-key'] = _subkey;
        let response = https.get({
            url: 'https://strata.azure-api.net/FinancialBridgeAP/api/V5/invoices/' + _inv_id,
            headers:header
        });
        let response_data = JSON.parse(response.body);
        log.debug({title: 'GF_MR_STRATA_GetInvoiceData.js:getStrataInvoiceDetail', details: 'Detail response_data: ' + JSON.stringify(response_data)});
        return response_data;
    }

    /**
     *
     * @param {*} _strata_id
     * @returns
     */
    function findStrataInvoiceId(_strata_id){
        let result_id = 0;
        let customrecord_strata_invoiceSearchObj = search.create({
            type: "customrecord_strata_invoice",
            filters: [
                ["custrecord_strata_invoice_json","contains","\"id\":"+_strata_id+","]
            ],
            columns: [
                "internalid"
            ]
        });
        customrecord_strata_invoiceSearchObj.run().each(function(result){
            // .run().each has a limit of 4,000 results
            if(result_id == 0){ result_id = result.id; }
            return true;
        });
        return result_id;
    }

    /**
     * This function uses the application key and subscription key to return an authentication token
     * @param {string} _appkey
     * @param {string} _subkey
     * @returns {string,object}
     */
    function setInvoiceStatus(_authtoken, _subkey, _inv_id){
        try{
            log.debug({title: 'GF_MR_STRATA_GetInvoiceData.js:setInvoiceStatus', details: 'params - _authtoken: ' + _authtoken + ', _subkey: ' + _subkey + ', _inv_id: ' + _inv_id});
            let header = [];
            header['token'] = _authtoken;
            header['Content-Type'] = 'application/json';
            header['ocp-apim-subscription-key'] = _subkey;
            let postData = { "id": _inv_id, "status": "OK" };
            postData = JSON.stringify(postData);
            let response = https.post({
                url: 'https://strata.azure-api.net/FinancialBridgeAP/api/V5/invoices/'+_inv_id+'/response',
                headers:header,
                body: postData
            });
            let response_data = JSON.parse(response.body);
            log.audit({title: 'GF_MR_STRATA_GetInvoiceData.js:setInvoiceStatus-response', details: 'Processed Invoice ID: ' + _inv_id + ', response_data: ' + JSON.stringify(response_data)});
            return response_data;
        }catch(ex) {
            log.error({title: 'GF_MR_STRATA_GetInvoiceData.js:setInvoiceStatus', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
            return '-';
        }
    }

    /**
     *
     * @param {*} _vendor_data
     * @returns
     */
    function findVendorByName(_vendor_data){
        log.debug({title: 'GF_MR_STRATA_GetInvoiceData.js:findVendorByName', details: 'params - _vendor_data: ' + JSON.stringify(_vendor_data)});
        let result_id = 0;
        let vendorSearchObj = search.create({
            type: "vendor",
            filters: [
                ["entityid","startswith",(_vendor_data.vendorName).substring(0,5)], "AND",
                ["entityid","doesnotcontain","S2"]
            ],
            columns:[]
        });
        vendorSearchObj.run().each(function(result){
            // .run().each has a limit of 4,000 results
            if(result_id == 0){ result_id = result.id; }
            return true;
        });
        log.debug({title: 'GF_MR_STRATA_GetInvoiceData.js:findVendorByName', details: 'result_id: ' + result_id});
        return result_id;
    }

    /**
     *
     * @param {*} _data
     * @returns
     */
    function findCustomerByMarketName(_data){
        try{
            log.debug({title: 'GF_MR_STRATA_GetInvoiceData.js:findCustomerByMarketName', details: 'params - _data: ' + JSON.stringify(_data)});
            let result_id = 0;
            let market_name = 'NO-ESTIMATES';
            if(_data.invoiceEstimates != undefined && _data.invoiceEstimates[0] != undefined && _data.invoiceEstimates[0] != null){
                let est = _data.invoiceEstimates[0];
                market_name = 'NO-INVOICE-DETAILS';
                if(est.invoiceDetails != undefined && est.invoiceDetails[0] != undefined && est.invoiceDetails[0] != null){
                    let det = est.invoiceDetails[0];
                    market_name = 'NO-INVOICE-ORDER-LINES';
                    if(det.invoiceOrderLines != undefined && det.invoiceOrderLines[0] != undefined && det.invoiceOrderLines[0] != null){
                        let line = det.invoiceOrderLines[0];
                        if(line.marketName != undefined && line.marketName != null && line.marketName != ''){
                            market_name = (line.marketName).substring(0,5);
                        }else{
                            market_name = 'NO-MARKET-NAME';
                        }
                    }
                }
            }
            if(market_name == 'Tri-C'){ result_id = 1835; } // special use cases -- ? Florence, SC
            if(market_name == 'Saras'){ result_id = 2609749; }
            if(result_id != 0){ return result_id; }
            if(market_name.length <= 5){
                let customerSearchObj = search.create({
                    type: "customer",
                    filters: [
                        ["entityid","contains","%"+market_name+"%"], "AND",
                        ["entityid","contains","G.F."], "AND",
                        ["isinactive","is","F"]
                    ],
                    columns: []
                });
                customerSearchObj.run().each(function(result){
                    // .run().each has a limit of 4,000 results
                    if(result_id == 0){ result_id = result.id; }
                    return true;
                });
            }else{
                log.error({title: 'GF_MR_STRATA_GetInvoiceData.js:findCustomerByMarketName- customer not found', details: 'CREATE CUSTOMER RECORD FOR market_name: ' + market_name});
            }
            log.debug({title: 'GF_MR_STRATA_GetInvoiceData.js:findCustomerByMarketName', details: 'result_id: ' + result_id});
            return result_id;
        }catch(ex) {
            log.error({title: 'GF_MR_STRATA_GetInvoiceData.js:findCustomerByMarketName', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
            return 0;
        }
    }

    /**
     *
     * @param {*} _vendor_data
     * @returns
     */
    function findItemByName(_vendor_data){
        log.debug({title: 'GF_MR_STRATA_GetInvoiceData.js:findItemByName', details: 'params - _vendor_data: ' + JSON.stringify(_vendor_data)});
        let result_id = 0;
        var noninventoryitemSearchObj = search.create({
            type: "noninventoryitem",
            filters: [
               ["type","anyof","NonInvtPart"], "AND",
               ["name","startswith",(_vendor_data.vendorName).substring(0,5)], "AND",
               ["name","doesnotcontain","S2"], "AND",
               ["isinactive","is","F"]
            ],
            columns: []
        });
        noninventoryitemSearchObj.run().each(function(result){
            // .run().each has a limit of 4,000 results
            if(result_id == 0){ result_id = result.id; }
            return true;
        });
        log.debug({title: 'GF_MR_STRATA_GetInvoiceData.js:findItemByName', details: 'result_id: ' + result_id});
        return result_id;
    }

});