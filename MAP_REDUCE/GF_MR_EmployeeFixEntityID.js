/**
 *@NApiVersion 2.1
 *@NScriptType MapReduceScript
 *
Script Name:   GF_MR_EmployeeFixEntityID.js
Author:        Mark Robinson
 */

 define(['N/log', 'N/search', 'N/record'], function(log, search, record) {

    /**
     * This get input function loads a search with all employee records where the entity id does not contain the first and last name
     * @param {*} context  - this variable is not utilized in this function
     * @returns - an array containing the full responses from the custom record search
     */
    function rics_getInputData(context){
        try {

            var employeeSearchObj = search.create({
                type: "employee",
                filters: [
                   ["formulanumeric: CASE WHEN INSTR({entityid}, {lastname}) = 0 AND INSTR({entityid}, {firstname}) = 0 THEN 1 ELSE 0 END","greaterthan","0"], "AND",
                   // ["isinactive","is","F"], "AND", // ["isinactive","any",""], "AND",
                   ["isinactive","any",""], "AND",
                   ["access","any",""]
                ],
                columns: [
                    search.createColumn({
                        name: "entityid",
                        sort: search.Sort.ASC
                    }),
                    "firstname",
                    "lastname",
                    "custentity_ntemployeeid",
                    "custentity2",
                    "giveaccess",
                    "datecreated",
                    "lastmodifieddate"
                ]
            });

            return employeeSearchObj;
        }catch(ex) {
            log.error({title: 'GF_MR_EmployeeFixEntityID.js:rics_getInputData', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this mapping function we set the custom form to use the Bamboo form which triggers the GF_CS_BAMBOO_EMP_Name script designed to force the name on the entity ID field
     * @param {*} context - Object - This should be the RICS NonSellable Batch custom record
     * @returns void (note: the const context variable is impacted and used in the reduce function)
     */
    function rics_map(context){
        try{
            // simple pass through
            var map_data = JSON.parse(context.value);
            log.audit({title: 'GF_MR_EmployeeFixEntityID.js:rics_map', details: 'map_data: ' + JSON.stringify(map_data)});
			if (map_data == null) {
				return true;
			}else{
                try{
                    var rec_id = map_data.id;
                    var rec = record.load({
                        type: record.Type.EMPLOYEE,
                        id: rec_id,
                        isDynamic: true
                    });
                    nameValidation(rec);
                    var commission_eligible = rec.getValue({fieldId: 'custentity1'});
                    if(commission_eligible == ''){ rec.setValue({fieldId: 'custentity1', value: 1}); } // this field is mandatory, if it's not set default to none
                    rec.save();
                }catch(ex){
                    log.error({title: 'GF_MR_EmployeeFixEntityID.js:rics_map-onSave', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
                }
            }

			return true;
        }catch(ex) {
            log.error({title: 'GF_MR_EmployeeFixEntityID.js:rics_map', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this reduce function we do nothing
     * @param {*} context - Object - This is the object that was built in the mapping function, it should conatin all the data we need for processing
     * @returns - void
     */
    function rics_reduce(context){
        return true;
    }

    /**
     * This summarize function is used to report important aspects about how the script performed
     * @param {*} summary - this object contains data regarding script performance.
     */
    function rics_summarize(summary) {
        try{
            log.audit('Usage Consumed', summary.usage);
            log.audit('Concurrency Number ', summary.concurrency);
            log.audit('Number of Yields', summary.yields);
            log.audit({title: 'Process Completed'});
        }catch(ex) {
            log.error({title: 'GF_MR_EmployeeFixEntityID.js:rics_summarize', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    return {
        getInputData: rics_getInputData,
        map: rics_map,
        reduce: rics_reduce,
        summarize: rics_summarize
    };

    // =========================== extra functions
    /**
     * This function evaluates replaces the entity id field with the last name and first name data on the record
     * @param {*} _record 
     * @returns 
     */
    function nameValidation(_record){
        // when number is found set entity id to name prior to save
        var str_firstname = _record.getValue({fieldId: 'firstname'});
        var str_lastname = _record.getValue({fieldId: 'lastname'});

        var str_new_entity = str_lastname + ', ' + str_firstname;
        // remove any numbers that have been placed in the first or last name fields
        str_new_entity = str_new_entity.replace(new RegExp(/\d/, "g"), "").trim();

        _record.setValue({fieldId: 'entityid', value: str_new_entity});
        log.audit({title: 'GF_MR_EmployeeFixEntityID.js:nameValidation-set to', details: 'str_new_entity: ' + str_new_entity});
        return true;
    }

});