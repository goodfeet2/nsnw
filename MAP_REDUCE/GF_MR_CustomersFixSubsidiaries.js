/**
 *@NApiVersion 2.1
 *@NScriptType MapReduceScript
 *
Script Name:   GF_MR_CustomersFixSubsidiaries.js
Author:        Mark Robinson
 */

 define(['N/log', 'N/search', 'N/record'], function(log, search, record) {

    /**
     * This get input function loads a search with all customer records that have not been modified today
     * @param {*} context  - this variable is not utilized in this function
     * @returns - an array containing the full responses from the custom record search
     */
    function rics_getInputData(context){
        try {

            var customerSearchObj = search.create({
                type: "customer",
                filters: [
                    ["isinactive","is","F"], "AND",
                    ["lastmodifieddate","before","today"]
                ],
                columns: [
                   search.createColumn({
                      name: "internalid",
                      summary: "GROUP"
                   }),
                   search.createColumn({
                      name: "altname",
                      summary: "GROUP",
                      sort: search.Sort.DESC
                   }),
                   search.createColumn({
                      name: "entityid",
                      summary: "GROUP"
                   }),
                   search.createColumn({
                      name: "formulatext",
                      summary: "MAX",
                      formula: "NS_CONCAT({msesubsidiary.internalid})"
                   })
                ]
             });

            return customerSearchObj;
        }catch(ex) {
            log.error({title: 'GF_MR_CustomersFixSubsidiaries.js:rics_getInputData', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this mapping function we pass through the data that will be  processed in the reduce phase
     * @param {*} context - Object - This should be the RICS NonSellable Batch custom record
     * @returns void (note: the const context variable is impacted and used in the reduce function)
     */
    function rics_map(context){
        try{
            // simple pass through
            var map_data = JSON.parse(context.value);
            log.audit({title: 'GF_MR_CustomersFixSubsidiaries.js:rics_map', details: 'map_data: ' + JSON.stringify(map_data)});
			if (map_data == null) {
				return true;
			}else{
                try{
                    // var rec_id = map_data.getValue({ name: "internalid", summary: "GROUP" });
                    var rec_id = map_data.values['GROUP(internalid)'].value;
                    var rec = record.load({
                        type: record.Type.CUSTOMER,
                        id: rec_id,
                        isDynamic: true
                    });
                    setSubsidiarySublist(rec);
                    rec.save();
                }catch(ex){
                    log.error({title: 'GF_MR_CustomersFixSubsidiaries.js:rics_map-onDelete', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
                }
            }

			// context.write({ key: context.key, value: map_data }); // pass through directly to reduce

			return true;
        }catch(ex) {
            log.error({title: 'GF_MR_CustomersFixSubsidiaries.js:rics_map', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this reduce function we do nothing
     * @param {*} context - Object - This is the object that was built in the mapping function, it should conatin all the data we need for processing
     * @returns - void
     */
    function rics_reduce(context){
        return true;
    }

    /**
     * This summarize function is used to report important aspects about how the script performed
     * @param {*} summary - this object contains data regarding script performance.
     */
    function rics_summarize(summary) {
        try{
            log.audit('Usage Consumed', summary.usage);
            log.audit('Concurrency Number ', summary.concurrency);
            log.audit('Number of Yields', summary.yields);
            log.audit({title: 'Process Completed'});
        }catch(ex) {
            log.error({title: 'GF_MR_CustomersFixSubsidiaries.js:rics_summarize', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    return {
        getInputData: rics_getInputData,
        map: rics_map,
        reduce: rics_reduce,
        summarize: rics_summarize
    };

    // =========================== extra functions
    function setSubsidiarySublist(_rec){
        try{
            var rec = _rec;
            var currentSubsidiaryCount = rec.getLineCount({ 'sublistId': 'submachine' });

            var arr_subs = ["7","3","4","2"];
            for(var i = 0; currentSubsidiaryCount > 0 && i < currentSubsidiaryCount; i++){
                // remove the existing subs from the array to write if the already exist in the customer record
                rec.selectLine({ sublistId: 'submachine', line: i });
                var sublistValue = rec.getCurrentSublistValue({
                    sublistId: 'submachine',
                    fieldId: 'subsidiary'
                });
                if(arr_subs.indexOf(sublistValue) > -1){
                    arr_subs.splice(arr_subs.indexOf(sublistValue), 1);
                }
            }

            // write all values remaining to the subsidiary sublist
            for(var i = 0; arr_subs.length > 0 && i < arr_subs.length; i++){
                rec.selectNewLine({ sublistId: 'submachine' });

                rec.setCurrentSublistValue({
                    sublistId: 'submachine',
                    fieldId: 'subsidiary',
                    value: arr_subs[i],
                    ignoreFieldChange: true
                });
                rec.commitLine({sublistId: 'submachine'});
            }

            return true;
        }catch(ex){
            log.error({title: 'Error: GF_MR_CustomersFixSubsidiaries.js:setSubsidiarySublist', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

 });