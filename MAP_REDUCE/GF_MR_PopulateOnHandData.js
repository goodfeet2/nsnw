/**
 *@NApiVersion 2.1
 *@NScriptType MapReduceScript
 *
Script Name:   GF_MR_PopulateOnHandData.js
Author:        Mark Robinson
 */

 define(['N/log', 'N/search', 'N/record'], function(log, search, record) {

    /**
     * This get input function loads a search with all RICS On Hand Custom records that have not yet been populated with data
     * @param {*} context  - this variable is not utilized in this function
     * @returns - an array containing the full responses from the custom record search
     */
    function gf_getInputData(context){
        try {
            var customrecord_rics_inventory_on_handSearchObj = search.create({
                type: "customrecord_rics_inventory_on_hand",
                filters: [
                    ["externalid","noneof","@NONE@"], "AND",
                    ["custrecord_rics_ioh_storecode","isempty",""]
                ], // must have External ID and the storecode is empty
                columns: [
                    "externalid"
                ]
            });

            return customrecord_rics_inventory_on_handSearchObj;
        }catch(ex) {
            log.error({title: 'GF_MR_PopulateOnHandData.js:rics_getInputData', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this mapping function we get the item and location data for the on hand record based on the external id
     * @param {*} context - Object - This should be the search results from the gf_getInputData function
     * @returns void (note: the const context variable is impacted and used in the reduce function)
     */
    function gf_map(context){
        try{
            // simple pass through
            var map_data = JSON.parse(context.value);
            // log.audit({title: 'GF_MR_PopulateOnHandData.js:rics_map', details: 'map_data: ' + JSON.stringify(map_data)});
			if (map_data == null) {
				return true;
			}else{
                try{
                    var data = {};
                    var str_extId = map_data.values.externalid.text;
                    // log.debug({title: 'GF_MR_PopulateOnHandData.js:rics_map-str_extId', details: 'str_extId: ' + str_extId});
                    var arr_extId = str_extId.split('_');
                    var str_storeCode = arr_extId[0];
                    var str_productId = arr_extId[1];
                    // get location data
                    var location_id = findLocation(str_storeCode);
                    var fieldLookUp = search.lookupFields({
                        type: 'location',
                        id: location_id,
                        columns: ['name']
                    });
                    var loc_name = (fieldLookUp != undefined && fieldLookUp.name != undefined) ? fieldLookUp.name : '';
                    var sub_id = findSubsidiary(location_id);
                    data.custrecord_rics_ioh_storename = loc_name;
                    data.custrecord_rics_ioh_storecode = str_storeCode;
                    data.custrecord_rics_ioh_ns_subsidiary = sub_id;
                    data.custrecord_rics_ioh_ns_location = location_id;

                    // get product data
                    var item_data = get_item_data(str_productId);
                    var pricing_obj = (item_data.price_json != '') ? JSON.parse(item_data.price_json) : {};
                    data.custrecord_rics_ioh_cost = ((pricing_obj.Cost != undefined && pricing_obj.Cost != '') ? pricing_obj.Cost : '');
                    data.custrecord_rics_ioh_retailprice = ((pricing_obj.RetailPrice != undefined && pricing_obj.RetailPrice != '') ? pricing_obj.RetailPrice : '');
                    data.custrecord_rics_ioh_activeprice = ((pricing_obj.ActivePrice != undefined && pricing_obj.ActivePrice != '') ? pricing_obj.ActivePrice : '');
                    data.custrecord_rics_ioh_parent_productdetail = item_data.pd_id;
                    data.custrecord_rics_ioh_parent_productitem = item_data.pi_id;

                    // update the rics on hand custom record
                    if(item_data.ns_subitem_id != 0){
                        data.custrecord_rics_ioh_ns_matrix_subitem = item_data.ns_subitem_id;
                        var rics_ioh_update_id = record.submitFields({
                            type: 'customrecord_rics_inventory_on_hand',
                            id: map_data.id,
                            values: data
                        });
                    }
                }catch(ex){
                    log.error({title: 'GF_MR_PopulateOnHandData.js:rics_map-onSave', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
                }
            }

			return true;
        }catch(ex) {
            log.error({title: 'GF_MR_PopulateOnHandData.js:rics_map', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this reduce function we do nothing
     * @param {*} context - Object - This is the object that was built in the mapping function, it should conatin all the data we need for processing
     * @returns - void
     */
    function gf_reduce(context){
        var customrecord_rics_inventory_on_handSearchObj = search.create({
            type: "customrecord_rics_inventory_on_hand",
            filters: [
               ["externalid","anyof","@NONE@"]
            ],
            columns: []
        });
        customrecord_rics_inventory_on_handSearchObj.run().each(function(result){
            // .run().each has a limit of 4,000 results
            record.delete("customrecord_rics_inventory_on_hand", result.id);
            return true;
        });
    }

    /**
     * This summarize function is used to report important aspects about how the script performed
     * @param {*} summary - this object contains data regarding script performance.
     */
    function gf_summarize(summary) {
        try{
            log.audit('Usage Consumed', summary.usage);
            log.audit('Concurrency Number ', summary.concurrency);
            log.audit('Number of Yields', summary.yields);
            log.audit({title: 'Process Completed'});
        }catch(ex) {
            log.error({title: 'GF_MR_PopulateOnHandData.js:rics_summarize', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    return {
        getInputData: gf_getInputData,
        map: gf_map,
        reduce: gf_reduce,
        summarize: gf_summarize
    };

    // =========================== extra functions
    /**
     * findLocation is used to return the location id based on the storecode indicated on the RICS Sales Details record
     * @param {*} _values
     */
    function findLocation(_storecode){
        var result_id = 0;

        if(_storecode != ''){
            var locationSearchObj = search.create({
                type: "location",
                filters: [
                ["externalid","anyof",_storecode], "AND",
                ["isinactive","is","F"]
                ],
                columns: [ ]
            });
            locationSearchObj.run().each(function(result){
                // .run().each has a limit of 4,000 results
                if(result_id == 0){
                    result_id = result.id;
                }
                return true;
            });
        }
        return result_id;
    }

    /**
     * This function takes the location id and gets the subsidiary internal id
     * @param {*} _location_id - integer - the NetSuite internal ID of the location record
     * @returns - integer - the NetSuite internal ID of the subsidiary for the location
     */
    function findSubsidiary(_location_id){
        var result = 0;
        if(_location_id != undefined && _location_id > 0){
            // load location record
            var location_record = record.load({
                type: 'location',
                id: _location_id,
                isDynamic: true
            });
            result = location_record.getValue('subsidiary');
        }

        return result;
    }


    function get_item_data(_str_productId){
        var result_obj = {
            "price_json": "",
            "pd_id": 0,
            "pi_id": 0,
            "ns_subitem_id": 0,
            "upc": ""
        };
        // custom record search
        var customrecord_rics_product_itemSearchObj = search.create({
            type: "customrecord_rics_product_item",
            filters: [
                ["isinactive","is","F"], "AND",
                ["custrecord_rics_pi_integration_id","is",_str_productId]
            ],
            columns: [
               search.createColumn({
                  name: "custrecord_rics_pd_pricing",
                  join: "CUSTRECORD_RICS_PI_PARENT_PD"
               }),
               "custrecord_rics_pi_parent_pd",
               "internalid",
               "custrecord_rics_pi_upc"
            ]
        });
        customrecord_rics_product_itemSearchObj.run().each(function(result){
            // .run().each has a limit of 4,000 results
            if(result_obj.pi_id == 0){
                result_obj.pi_id = result.id;
                result_obj.pd_id = result.getValue('custrecord_rics_pi_parent_pd');
                result_obj.price_json = result.getValue({ name: "custrecord_rics_pd_pricing", join: "CUSTRECORD_RICS_PI_PARENT_PD" });
                result_obj.upc = result.getValue('custrecord_rics_pi_upc');
            }
            return true;
        });
        if(result_obj.upc != ''){
            // ns matrix sub-item search
            var inventoryitemSearchObj = search.create({
                type: "inventoryitem",
                filters: [
                    ["type","anyof","InvtPart"], "AND",
                    ["isinactive","is","F"], "AND",
                    ["parent","noneof","@NONE@"], "AND",
                    ["upccode","is",result_obj.upc]
                ],
                columns: [
                    "internalid"
                ]
            });
            inventoryitemSearchObj.run().each(function(result){
                // .run().each has a limit of 4,000 results
                if(result_obj.ns_subitem_id == 0){
                    result_obj.ns_subitem_id = result.id;
                }
                return true;
            });
        }
        return result_obj;
    }

});