/**
 *@NApiVersion 2.1
 *@NScriptType MapReduceScript
 *
Script Name:   GF_MR_RICS_Inventory_OnHand.js
Author:        Mark Robinson
 */

define(['N/log', 'N/search', 'N/record', 'N/https', 'N/runtime', 'N/task', '/SuiteScripts/GF_LIB/gf_nsrics'], function(log, search, record, https, runtime, task, gf_nsrics) {

	/**
	 * This get input function uses a product item custom record search, and a location search to set up the API calls to RICS that will allow us to build / update the Inventory On Hand custom records.
	 * The product item custom record search ensures that we only search for items that have been built in NetSuite by the API, so we know the SKU and UPC should both exist in RICS.
	 * The location search acts as a filter for which items will be included in the on hand update. Only data related to locations that have been marked for updating will be added to the return array.
	 * This script is limited to exit the loop after it has added 1000 or more records to the return array. We don't stop at exactly 1000 because we don't want any partial updates. 
	 * @param {*} context - This variable is not utilized in this function.
	 * @returns - An array containing all of the objects that have been built from the API calls.
	 */
	function rics_getInputData(context) {
		try {
			var nsrics = gf_nsrics.getIntegration();
			// only execute the script when outside of core hours
			var dt_timestamp = new Date(); // this will be PST

			var allStores = [];
			var ricsInventory = [];
			var max_attempts = 3;
			var num_skip = 0;
			var current_attempt = 0;
			var current_store_id = 0;
			var current_store_name = '';
			var current_store_timezone = '';
			var current_store_open = 0;
			var current_store_close = 0;
			var current_store_skip = 0;

			// get list of all stores with external ids
			var locationSearchObj = search.create({
				type: "location",
				filters: [
					["isinactive", "is", "F"], "AND",
					["locationtype", "anyof", "1"], "AND",
					["custrecord_rics_upd_onhand_inventory", "is", "T"], "AND",
					["externalid", "noneof", "@NONE@"]
				], // type = store, externalid != blank
				columns: [
					search.createColumn({
						name: "externalid",
						label: "External ID"
					}),
					search.createColumn({
						name: "timezone",
						sort: search.Sort.ASC,
						label: "Time Zone"
					}),
					search.createColumn({
						name: "custrecord_gf_store_open",
						sort: search.Sort.ASC,
						label: "GF Store Open"
					}),
					search.createColumn({
						name: "namenohierarchy",
						sort: search.Sort.ASC,
						label: "Name"
					}),
					search.createColumn({
						name: "custrecord_gf_store_close",
						label: "GF Store Close"
					}),
					search.createColumn({
						name: "custrecord_gf_rics_onhand_skip",
						label: "GF On Hand Skip"
					}),
					search.createColumn({
						name: "custrecord_rics_upd_onhand_inventory",
						label: "RICS Update On-Hand Inventory"
					})
				]
			});
			locationSearchObj.run().each(function(result) {
				// .run().each has a limit of 4,000 results
				allStores[result.id] = result.getValue('externalid');
				if (result.getValue('custrecord_rics_upd_onhand_inventory') == true) {
					// set the current store NS data
					current_store_id = result.id;
					current_store_name = result.getValue('namenohierarchy');
					current_store_timezone = result.getValue('timezone');
					current_store_open = result.getValue('custrecord_gf_store_open');
					current_store_close = result.getValue('custrecord_gf_store_close');
					current_store_skip = result.getValue('custrecord_gf_rics_onhand_skip');
				}
				return true;
			});
			var arr_allStores = allStores.filter(function(el) {
				return el != null;
			});
			// log.debug({title: 'GF_MR_RICS_Inventory_OnHand.js:rics_getInputData', details: 'allStores.length: ' + allStores.length});
			// no stores - no process
			if (arr_allStores.length == 0) {
				return true;
			}
			var getInventoryOnHand = nsrics.baseurl + nsrics.getEndPoint('GetInventoryOnHand');
			var header = [];
			header['Content-Type'] = 'application/json';
			header['Token'] = nsrics.getActiveConnection();

			// log.debug({title: 'GF_MR_RICS_Inventory_OnHand.js:rics_getInputData', details: 'allStores: ' + JSON.stringify(allStores)});
			if (arr_allStores.length > 1) {
				var str_storecode = '493';
				var postData = {"SKU": "%","UPC": "%","StoreCode": str_storecode,"Skip": num_skip,"Take": 50};
			} else {
				var str_storecode = '';
				for (var s in arr_allStores) {
					if (str_storecode == '' && arr_allStores[s] != '') {
						str_storecode = parseInt(arr_allStores[s]);
					}
					dt_timestamp = adjustTimeZone(current_store_timezone, dt_timestamp);
					// check the operating hours of the store and only process during their operating hours - including a grace period prior to store open
					// if ((dt_timestamp.getHours()) > (current_store_open - 4) && dt_timestamp.getHours() < current_store_close) {
						// when the current local time is inside of the store's operational hours do not process
						// log.error({title: 'GF_MR_RICS_Inventory_OnHand.js:rics_getInputData', details: 'ts hours: ' + dt_timestamp.getHours() + ', current_store_open: ' + current_store_open + ', current_store_close: ' + current_store_close});
						// log.error({title: 'GF_MR_RICS_Inventory_OnHand.js:rics_getInputData', details: 'within operating hours'});
						// removeCompletedDeployments();
						// log.audit({title: 'GF_MR_RICS_Inventory_OnHand.js:rics_getInputData', details: '--STORE NOT CLOSED SET NEXT LOCATION--'});
						// setNextLocation(str_storecode);
						// return [];
					// }
					// check deployment id
					var scriptObj = runtime.getCurrentScript();
					if(scriptObj.deploymentId == 'customdeploy_mr_rics_inventory_onhand'){
						// when this is the regular on hand script, check for location specific scripts and stop processing this if those are running (otherwise we risk causing too many requests to the end point at one time)
						var scheduledscriptinstanceSearchObj = search.create({
							type: "scheduledscriptinstance",
							filters: [
								["status","noneof",["COMPLETE","CANCELED","FAILED"]], "AND",
								["script.internalid","anyof","1385"], "AND",
								["scriptdeployment.internalid","noneof","15161","15160"]
							],
							columns: [ ]
						});
						// count the results with this name - the only reason it should be anything other than zero is if the script has errored
						var searchResultCount = scheduledscriptinstanceSearchObj.runPaged().count;
						if(searchResultCount > 0){ return []; }
					}
				}
				num_skip = (current_store_skip > 0) ? current_store_skip : 0;
				var postData = {"SKU": "%","UPC": "%","StoreCode": str_storecode,"Skip": num_skip,"Take": 50};
			}
			// log.debug({title: 'GF_MR_RICS_Inventory_OnHand.js:rics_getInputData', details: 'get all values - postData: ' + JSON.stringify(postData)});

			// ===================
			// get all values
			do {
				try{
					var data = '';
					nsrics.sleep(3000);
					postData = {"SKU": "%","UPC": "%","StoreCode": str_storecode,"Skip": num_skip,"Take": 50};
					postData = JSON.stringify(postData);
					var response = https.post({
						url: getInventoryOnHand,
						headers: header,
						body: postData
					});
					if (response != undefined && response.hasOwnProperty('code') && response['code'] == 200) {
						current_attempt = 0;
						var data = JSON.parse(response.body);
						// log.debug({title: 'GF_MR_RICS_Inventory_OnHand.js:rics_getInputData', details: 'data.ResultStatistics: ' + JSON.stringify(data.ResultStatistics)});
						// var arr_stores = [];
						for (var key in data.Stores) {
							var str_store_code = (data.Stores[key].StoreCode).toString();
							if (allStores.indexOf(str_store_code) == -1) {
								continue;
							}
							var currentStoreData = data.Stores[key];
							if (currentStoreData.hasOwnProperty('Items')) {
								for (var item in currentStoreData.Items) {
									var obj = {};
									obj._sku = currentStoreData.Items[item].Sku;
									obj._col = (currentStoreData.Items[item].Column != undefined) ? currentStoreData.Items[item].Column : '';
									obj._row = (currentStoreData.Items[item].Row != undefined) ? currentStoreData.Items[item].Row : '';
									obj._upc = currentStoreData.Items[item].UPC;
									obj._summary = currentStoreData.Items[item].Summary;
									obj.custrecord_rics_ioh_storename = currentStoreData.StoreName;
									obj.custrecord_rics_ioh_storecode = currentStoreData.StoreCode;
									obj.custrecord_rics_ioh_ns_location = allStores.indexOf(str_store_code);
									obj.custrecord_rics_ioh_onhand = (currentStoreData.Items[item].OnHand != undefined) ? currentStoreData.Items[item].OnHand : 0;
									obj.custrecord_rics_ioh_cost = (currentStoreData.Items[item].Cost != undefined) ? currentStoreData.Items[item].Cost : 0;
									obj.custrecord_rics_ioh_retailprice = (currentStoreData.Items[item].RetailPrice != undefined) ? currentStoreData.Items[item].RetailPrice : 0;
									obj.custrecord_rics_ioh_activeprice = (currentStoreData.Items[item].ActivePrice != undefined) ? currentStoreData.Items[item].ActivePrice : 0;
                                    obj.custrecord_rics_ioh_json = JSON.stringify(currentStoreData.Items[item]);
									ricsInventory.push(obj);
								}
							}
						}
						num_skip = data.ResultStatistics.EndRecord;
					} else {
						current_attempt += 1;
					}
				}catch(ex){
					current_attempt += 1;
					log.error({title: 'Error: GF_MR_RICS_Inventory_OnHand.js:rics_getInputData-loop', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
					log.error({title: 'Error: GF_MR_RICS_Inventory_OnHand.js:rics_getInputData-loop', details: 'postData: ' + postData})
				}
				if(current_attempt == max_attempts && data.ResultStatistics.EndRecord < data.ResultStatistics.TotalRecords){
					throw 'GF_MAX_ATTEMPTS_REACHED';
				}
			} while (current_attempt < max_attempts && ((data.hasOwnProperty('ResultStatistics') && data.ResultStatistics.EndRecord < data.ResultStatistics.TotalRecords)));

			// log.audit({title: 'GF_MR_RICS_Inventory_OnHand.js:rics_getInputData', details: 'ricsInventory.length: ' + ricsInventory.length});
			// cycle the location to operate against next
			log.audit({title: 'GF_MR_RICS_Inventory_OnHand.js:rics_getInputData', details: '--COLLECTION COMPLETE FOR STORE # ' + str_storecode + ' SET NEXT LOCATION--'});
			setNextLocation();
			return ricsInventory;
		} catch (ex) {
			log.error({title: 'Error: GF_MR_RICS_Inventory_OnHand.js:rics_getInputData', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
			log.error({title: 'Error: GF_MR_RICS_Inventory_OnHand.js:rics_getInputData', details: 'postData: ' + postData})
			// log.error({title: 'Error: GF_MR_RICS_Inventory_OnHand.js:rics_getInputData', details: 'Error - num_skip:' + num_skip + ', data/response: ' + (data != null ? JSON.stringify(data) : JSON.stringify(response.body) ) });
			// if(arr_allStores.length == 1){ setNextLocation(); }
			try {
				if (arr_allStores.length == 1) {
					// if ((ex.toString()).indexOf('SSS_') > -1) {
						nsrics.sleep(30000); // wait for a 30 second interval
						updateLocationSkip(current_store_id, current_store_name, num_skip);
						log.audit({title: 'GF_MR_RICS_Inventory_OnHand.js:rics_getInputData', details: '--' + ex.name + ' FOR STORE # ' + str_storecode + ' SET NEXT LOCATION--'});
						setNextLocation();
					/* } /* else {
						setNextLocation();
					} */
				}
			} catch (err) {
				log.error({title: 'Error: GF_MR_RICS_Inventory_OnHand.js:rics_getInputData-error with location update', details: 'Error ' + err.toString() + ' : ' + err.stack});
				// if((err.toString()).indexOf('SSS_REQUEST_TIME_EXCEEDED') > -1){
					// run the deployment
					var call_onhand_js_od = task.create({
						taskType: task.TaskType.MAP_REDUCE,
						scriptId: 'customscript_mr_rics_inventory_onhand',
						deploymentId: 15160
					});
					call_onhand_js_od.submit();
				// }
			}
			return ricsInventory;
		}
	}

	/**
	 * This mapping function doesn't have the required governance to fully process our data set, so we're passing it through to the reduce function
	 * In this mapping function we take the data that was built in the get input function, and process it to create or update the custom RICS Inventory On Hand custom records.
	 * @param {*} context - This variable contains the data objects that were built in the get input funtion
	 * @returns void
	 */
	function rics_map(context) {
		try {
			// var scriptObj = runtime.getCurrentScript();
			// log.audit({title: 'GF_MR_RICS_Inventory_OnHand.js:rics_map', details: 'rics_map :: Remaining governance units: ' + scriptObj.getRemainingUsage()});
			var arr_data = JSON.parse(context.value);
			if (arr_data == null) {
				return true;
			}
			// log.audit({title: 'GF_MR_RICS_Inventory_OnHand.js:rics_map', details: 'data: ' + JSON.stringify(arr_data)});

			context.write({
				key: context.key,
				value: arr_data
			}); // pass through directly to reduce

			return true;
		} catch (ex) {
			log.error({title: 'Error: GF_MR_RICS_Inventory_OnHand.js:rics_map', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
		}
	}

	/**
	 * In this reduce function we take the data that was built in the get input function, and process it to create or update the custom RICS Inventory On Hand custom records.
	 * @param {*} context  - This variable contains the data objects that were built in the get input funtion
	 * @returns void
	 */
	function rics_reduce(context) {
		try {
			var data = JSON.parse(context.values[0]);
			// log.debug({title: 'GF_MR_RICS_Inventory_OnHand.js:rics_reduce', details: 'data: ' + JSON.stringify(data)});
			// find customrecord_rics_inventory_on_hand record
			var onhand_id = 0;

            // find inventory item record
			var rics_productitem_id = find_pi_record(data._upc);
			// look up upc on product item
			var pi_upc = getProductItemUpc(rics_productitem_id);
			if (pi_upc != data._upc) {
				data._upc = pi_upc;
			}
			var ns_item_id = find_item_record(data._sku, data._upc, data._summary);
            var parent_productitem = search.lookupFields({
				type: 'customrecord_rics_product_item',
				id: parseInt(rics_productitem_id),
				columns: ['custrecord_rics_pi_parent_pd']
			});
			var pd_id = parent_productitem.custrecord_rics_pi_parent_pd[0].value;
			var parent_productdetails = search.lookupFields({
				type: 'customrecord_rics_product_details',
				id: parseInt(pd_id),
				columns: ['custrecord_rics_pd_ns_item_id']
			});
			var ns_item_id_on_pd = (parent_productdetails.custrecord_rics_pd_ns_item_id[0] != undefined) ? parent_productdetails.custrecord_rics_pd_ns_item_id[0].value : 0;
			ns_item_id = (ns_item_id == 0 && ns_item_id_on_pd > 0) ? parseInt(ns_item_id_on_pd) : parseInt(ns_item_id);
			if (ns_item_id <= 0) { // -1 means the item exists but has been deactivated, 0 means the item was not found
				log.audit({title: 'GF_MR_RICS_Inventory_OnHand.js:rics_reduce', details: 'sku: ' + data._sku + ', upc: ' + data._upc + ', item: ' + (ns_item_id == 0 ? 'was not found' : 'exists but is inactive - id:' + ns_item_id)});
				return true;
			} else {
                log.audit({title: 'GF_MR_RICS_Inventory_OnHand.js:rics_reduce', details: 'sku: ' + data._sku + ', upc: ' + data._upc + ', item id:' + ns_item_id});
            }

			var arr_searchFilters = []; //custrecord_rics_ioh_ns_matrix_subitem - custrecord_rics_ioh_ns_location
            arr_searchFilters.push(["custrecord_rics_ioh_ns_matrix_subitem", "anyof", ns_item_id]);
			// arr_searchFilters.push(["custrecord_rics_ioh_parent_productdetail.custrecord_rics_pd_sku", "is", data._sku]);
			// arr_searchFilters.push("AND");
			// arr_searchFilters.push(["custrecord_rics_ioh_parent_productitem.custrecord_rics_pi_upcs", "contains", '"' + data._upc + '"']);
			arr_searchFilters.push("AND");
			arr_searchFilters.push(["custrecord_rics_ioh_ns_location", "anyof", data.custrecord_rics_ioh_ns_location]);
			var customrecord_rics_inventory_on_handSearchObj = search.create({
				type: "customrecord_rics_inventory_on_hand",
				filters: arr_searchFilters,
				columns: []
			}).run();
			var customrecord_rics_inventory_on_handSearchRes = customrecord_rics_inventory_on_handSearchObj.getRange({
				start: 0,
				end: 1
			});
			for (var searchResult in customrecord_rics_inventory_on_handSearchRes) {
				if (onhand_id == 0) {
					onhand_id = customrecord_rics_inventory_on_handSearchRes[searchResult].id;
					break;
				}
			}

			// if onhand_id is found then load, otherwise create a new On Hand record
			if (onhand_id > 0) {
				var rioh_update_id = record.submitFields({
					type: 'customrecord_rics_inventory_on_hand',
					id: onhand_id,
					values: {
						lastmodified: new Date()
					},
					options: {
						enableSourcing: false,
						ignoreMandatoryFields: true
					}
				});
				var rics_On_Hand = record.load({
					type: 'customrecord_rics_inventory_on_hand',
					id: onhand_id,
					isDynamic: true
				});
				// log.debug({title: 'GF_MR_RICS_Inventory_OnHand.js:rics_reduce', details: 'found id: ' + onhand_id + ', sku: ' + data._sku + ', upc: ' + data._upc + ', location id: ' + data.custrecord_rics_ioh_ns_location});
			} else {
				var rics_On_Hand = record.create({
					type: 'customrecord_rics_inventory_on_hand'
				});
				// log.debug({title: 'GF_MR_RICS_Inventory_OnHand.js:rics_reduce', details: 'CREATING NEW ON HAND FOR - sku: ' + data._sku + ', upc: ' + data._upc + ', location id: ' + data.custrecord_rics_ioh_ns_location});
			}

			rics_On_Hand.setValue('custrecord_rics_ioh_storename', data.custrecord_rics_ioh_storename);
			rics_On_Hand.setValue('custrecord_rics_ioh_storecode', data.custrecord_rics_ioh_storecode);
			rics_On_Hand.setValue('custrecord_rics_ioh_onhand', data.custrecord_rics_ioh_onhand);
			rics_On_Hand.setValue('custrecord_rics_ioh_cost', data.custrecord_rics_ioh_cost);
			rics_On_Hand.setValue('custrecord_rics_ioh_retailprice', data.custrecord_rics_ioh_retailprice);
			rics_On_Hand.setValue('custrecord_rics_ioh_activeprice', data.custrecord_rics_ioh_activeprice);
            rics_On_Hand.setValue('custrecord_rics_ioh_json', data.custrecord_rics_ioh_json);
			rics_On_Hand.setValue('custrecord_rics_ioh_ns_location', data.custrecord_rics_ioh_ns_location);
			rics_On_Hand.setValue('custrecord_rics_ioh_parent_productitem', rics_productitem_id);

			rics_On_Hand.setValue('custrecord_rics_ioh_ns_matrix_subitem', ns_item_id);

			log.audit({title: 'GF_MR_RICS_Inventory_OnHand.js:rics_reduce', details: 'pd_id: ' + pd_id + ', ns_item_id_on_pd: ' + ns_item_id_on_pd + ', ns_item_id: ' + ns_item_id + ', both > 0?: ' + ((ns_item_id_on_pd > 0 && ns_item_id > 0) ? 'TRUE' : 'FALSE')});
            if (ns_item_id > 0) {
				rics_On_Hand.setValue('custrecord_rics_ioh_ns_items', [parseInt(ns_item_id)]);
			}

			var saved_onhand_id = rics_On_Hand.save();
			// log.debug({title: 'GF_MR_RICS_Inventory_OnHand.js:rics_reduce', details: 'SAVED id: ' + saved_onhand_id + ', sku: ' + data._sku + ', upc: ' + data._upc + ', location id: ' + data.custrecord_rics_ioh_ns_location});

			var pi_update_id = record.submitFields({
				type: 'customrecord_rics_product_item',
				id: rics_productitem_id,
				values: {
					custrecord_rics_pi_ns_onhand_update: new Date()
				},
				options: {
					enableSourcing: false,
					ignoreMandatoryFields: true
				}
			});

			var scriptObj = runtime.getCurrentScript();
			// log.debug({title: 'GF_MR_RICS_Inventory_OnHand.js:rics_reduce', details: 'rics_reduce :: Remaining governance units: ' + scriptObj.getRemainingUsage()});
			return true;
		} catch (ex) {
			log.error({title: 'Error: GF_MR_RICS_Inventory_OnHand.js:rics_reduce', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
            // log.error({title: 'GF_MR_RICS_Inventory_OnHand.js:rics_reduce', details: 'pd_id: ' + pd_id + ', ns_item_id_on_pd: ' + ns_item_id_on_pd + ', ns_item_id: ' + ns_item_id + ', both > 0?: ' + ((ns_item_id_on_pd > 0 && ns_item_id > 0) ? 'TRUE' : 'FALSE')});
		}
	}

	/**
	 * This summarize function is used to report important aspects about how the script performed
	 * @param {*} summary - this object contains data regarding script performance.
	 */
	function rics_summarize(summary) {
		try {
            // find any script deployments that are finished running, and not one of the main two deployments and delete them
            removeCompletedDeployments();
            removeDuplicateOnHandRecords();
			log.audit({title: 'Process Completed', details: 'Usage Consumed: ' + summary.usage + ', Concurrency: ' + summary.concurrency + ', # of Yields: ' + summary.yields});
		} catch (ex) {
			log.error({title: 'Error: GF_MR_RICS_Inventory_OnHand.js:rics_summarize', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
		}
	}

	return {
		getInputData: rics_getInputData,
		map: rics_map,
		reduce: rics_reduce,
		summarize: rics_summarize
	};

	// =========================== extra functions
	/**
	 * This function takes strings of the sku and upc values and tries to find the related netsuite inventory item record
	 * @param {*} _sku - string - the sku of the item the function will try to find
	 * @param {*} _upc - string - the upc of the item the function will try to find
	 * @param {*} _summary - string - the summary of the item the function will try to find (when it has ** that indicates a service or non-inventory item)
	 * @returns integer - the internal id of the inventory item that was found
	 */
	function find_item_record(_sku, _upc, _summary) {
		var result_id = 0;
		var flg_inactive = false;
		// log.debug({title: 'GF_MR_RICS_Inventory_OnHand.js:find_item_record', details: '_sku: ' + _sku + ', _upc: ' + _upc});

        // check for **
        if(_summary != undefined && _summary.indexOf('** ') > -1){
            // search Non-inventory
            var inventoryitemSearchObj = search.create({
                type: "noninventoryitem",
                filters: [
                    ["type", "anyof", "NonInvtPart"], "AND",
                    ["isinactive", "any", ""], "AND",
                    ["vendorname", "is", _sku], "AND",
                    ["upccode", "is", _upc]
                ],
                columns: [
                    search.createColumn({
                        name: "isinactive",
                        label: "Inactive"
                    })
                ]
            });
        }else{
            var inventoryitemSearchObj = search.create({
                type: "inventoryitem",
                filters: [
                    ["type", "anyof", "InvtPart"], "AND",
                    ["isinactive", "any", ""], "AND",
                    ["vendorname", "is", _sku], "AND",
                    ["upccode", "is", _upc], "AND",
                    ["parent", "noneof", "@NONE@"]
                ],
                columns: [
                    search.createColumn({
                        name: "isinactive",
                        label: "Inactive"
                    })
                ]
            });
        }
		inventoryitemSearchObj.run().each(function(result) {
			// .run().each has a limit of 4,000 results
			// log.debug({title: 'GF_MR_RICS_Inventory_OnHand.js:updateOnHand', details: 'result.id: ' + result.id});
			if (result_id <= 0) { // using <= here prevents any deactivated items from being the only thing returned by this function
				flg_inactive = result.getValue('isinactive');
				if (flg_inactive == false) {
					result_id = result.id;
				} else {
					result_id = -1;
				}
			}
			// log.debug({title: 'GF_MR_RICS_Inventory_OnHand.js:updateOnHand', details: 'id: ' + result.id + ', inactive: ' + flg_inactive});
			return true;
		});

		return result_id;
	}

	/**
	 * This function takes a string of the upc value and tries to find the related RICS Product Item custom record
	 * @param {*} _upc - string - the upc of the item the function will try to find
	 * @returns integer - the internal id of the inventory item that was found
	 */
	function find_pi_record(_upc) {
		var result = 0;
		// find the Product Details Record
		var pi_SearchResults = search.create({
			type: "customrecord_rics_product_item",
			filters: [
				["custrecord_rics_pi_upcs", "contains", '"' + _upc + '"'], "AND",
				["isinactive","is","F"]
			],
			columns: [
				search.createColumn({
					name: "id",
					sort: search.Sort.ASC,
					label: "ID"
				}),
				search.createColumn({
					name: "internalid",
					label: "Internal ID"
				})
			]
		}).run();
		var pi_SearchResultSet = pi_SearchResults.getRange({start: 0, end: 1});
		if (pi_SearchResultSet != null && pi_SearchResultSet.length > 0) {
			result = pi_SearchResultSet[0].id;
		}

		return result;
	}

	/**
	 * This function takes an integer of the RICS Product Item internal id and returns the value of the UPC field on that record
	 * @param {*} _pi_id - integer - the internal id of the RICS Product Item
	 * @returns - string - the value of the UPC field on that record
	 */
	function getProductItemUpc(_pi_id) {
		var result = search.lookupFields({
			type: 'customrecord_rics_product_item',
			id: parseInt(_pi_id),
			columns: ['custrecord_rics_pi_upc']
		});
		return result.custrecord_rics_pi_upc;
	}

	/**
	 * This function gets all store locations and sorts them by timezone, then store hours, then name and based on the currently selected store
	 * that is processing RICS On Hand inventory, it will deselect that store and select the next one.
	 * @returns - void
	 */
	function setNextLocation() {
		try {
			// set list of locations - in order by timezone, then store hours, then name
			var current_loc_id = 0;
			var arr_loc_ids_in_order = []; // [314,328,329,351,322,340,319,304,347,348,352,349,350,353,354,307,11,9,12,10,14,15,6,4,5,3,7];
			var arr_loc_names_in_order = [];
			var arr_loc_id_of_completed_update = getListOfCompletedLocations();
			// get list of all stores with external ids
			var locationSearchObj = search.create({
				type: "location",
				filters: [
					["isinactive", "is", "F"], "AND",
					["locationtype", "anyof", "1"], "AND", // store
					["externalid", "noneof", "@NONE@"]
				], // type = store, externalid != blank
				columns: [
					search.createColumn({
						name: "externalid",
						label: "External ID"
					}),
					search.createColumn({
						name: "timezone",
						sort: search.Sort.ASC,
						label: "Time Zone"
					}),
					search.createColumn({
						name: "custrecord_gf_store_open",
						sort: search.Sort.ASC,
						label: "GF Store Open"
					}),
					search.createColumn({
						name: "namenohierarchy",
						sort: search.Sort.ASC,
						label: "Name"
					}),
					search.createColumn({
						name: "custrecord_gf_store_close",
						label: "GF Store Close"
					}),
					search.createColumn({
						name: "custrecord_rics_upd_onhand_inventory",
						label: "RICS Update On-Hand Inventory"
					})
				]
			});
			locationSearchObj.run().each(function(result) {
				// .run().each has a limit of 4,000 results
				arr_loc_ids_in_order.push(result.id);
				arr_loc_names_in_order.push(result.getValue('namenohierarchy'));
				if (result.getValue('custrecord_rics_upd_onhand_inventory') == true) {
					current_loc_id = (result.id).toString();
				}
				// set the current store NS id
				return true;
			});

			for(var i = 0; i < arr_loc_ids_in_order.length; i++){
				// set update for all locations to false
				if(arr_loc_ids_in_order[i] != undefined && parseInt(arr_loc_ids_in_order[i]) > 0){
					if(arr_loc_id_of_completed_update.indexOf(arr_loc_ids_in_order[i]) > -1){
						var vals = {
							name: arr_loc_names_in_order[i],
							custrecord_gf_rics_onhand_skip: 0,
							custrecord_rics_upd_onhand_inventory: false
						};
					}else{
						var vals = {
							name: arr_loc_names_in_order[i],
							custrecord_rics_upd_onhand_inventory: false
						};
					}
					var upd_current_id = record.submitFields({
						type: 'location',
						id: arr_loc_ids_in_order[i],
						values: vals,
						options: {
							enableSourcing: false,
							ignoreMandatoryFields: true
						}
					});
				}
			}
			// take completed locations out of the next location pool
			for(var i in arr_loc_id_of_completed_update){
				var id_to_remove = arr_loc_id_of_completed_update[i];
				var index_in_target = arr_loc_ids_in_order.indexOf(id_to_remove);
				if(current_loc_id == id_to_remove){ // if this is the current loc decrement do that when we increment later we will be incrementing from the correct position
					current_loc_id = (index_in_target != 0) ? arr_loc_ids_in_order[arr_loc_ids_in_order.indexOf(index_in_target - 1)] : arr_loc_ids_in_order[arr_loc_ids_in_order.length -1];
				}
				if(index_in_target > -1){
					arr_loc_ids_in_order.splice(index_in_target, 1);
					arr_loc_names_in_order.splice(index_in_target, 1);
				}
			}
			// set next location
			// if current loc id is in the remaining loc ids - increment from that and if that's the last value wrap to the beginning of the array
			var next_index = (arr_loc_ids_in_order.indexOf(current_loc_id) != -1 && (arr_loc_ids_in_order.indexOf(current_loc_id) + 1) < arr_loc_ids_in_order.length) ? (arr_loc_ids_in_order.indexOf(current_loc_id) + 1) : 0;
			var str_next_store_name = arr_loc_names_in_order[next_index];
			if(arr_loc_ids_in_order[next_index] != undefined){
				var upd_next_id = record.submitFields({
					type: 'location',
					id: arr_loc_ids_in_order[next_index],
					values: {
						name: str_next_store_name,
						custrecord_rics_upd_onhand_inventory: true
					},
					options: {
						enableSourcing: false,
						ignoreMandatoryFields: true
					}
				});
				// log.audit({title: 'GF_MR_RICS_Inventory_OnHand.js:setNextLocation', details: 'upd_next_id: ' + upd_next_id});
				buildNewScriptDeployment(upd_next_id);
			}else{
				var upd_next_id = record.submitFields({
					type: 'location',
					id: '314',
					values: {
						name: 'Magnolia Park',
						custrecord_rics_upd_onhand_inventory: true
					},
					options: {
						enableSourcing: false,
						ignoreMandatoryFields: true
					}
				});
				// log.audit({title: 'GF_MR_RICS_Inventory_OnHand.js:setNextLocation-all-updated', details: 'upd_next_id: ' + upd_next_id});
			}
		} catch (ex) {
			log.error({title: 'Error: GF_MR_RICS_Inventory_OnHand.js:setNextLocation', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
		}
	}

	/**
	 * This function takes the required data to update a location record with a new value in the custrecord_gf_rics_onhand_skip field
	 * @param {*} _store_id - string - this should be the internal id of the location that is getting the update
	 * @param {*} _store_name - string - this should be the name without the hierarchy of the store that is being updated
	 * @param {*} _skip - integer - this should be the skip value that needs to be set for the store so that the next run will pick up where the last one left off
	 */
	function updateLocationSkip(_store_id, _store_name, _skip) {
		var upd_skip_id = record.submitFields({
			type: 'location',
			id: ''+_store_id+'',
			values: {
				name: _store_name,
				custrecord_gf_rics_onhand_skip: _skip
			},
			options: {
				enableSourcing: false,
				ignoreMandatoryFields: true
			}
		});
		buildNewScriptDeployment(upd_skip_id);
		var scriptObj = runtime.getCurrentScript();
		// log.audit({title: 'GF_MR_RICS_Inventory_OnHand.js:updateLocationSkip', details: '_store_id: ' + _store_id + ', _store_name: ' + _store_name + ', _skip: ' + _skip + ', upd_skip_id: ' + upd_skip_id + ', deploymentId: ' + scriptObj.deploymentId});
	}

	/**
	 * This function takes a timezone string and a date object and converts the timezone of the date object using the timezone string
	 * @param {*} _str_tz - string - assumed to be a timezone string i.e. 'America/Denver'
	 * @param {*} _dt - date object - assumed to be the current server datetime needs to be adjusted to the target timezone
	 * @returns - date object - adjusted to the timezone that was passed in
	 */
	function adjustTimeZone(_str_tz, _dt) {
		var addHrs = 0;
		switch (_str_tz) {
			case 'America/New_York':
				addHrs = 3;
				break;
			case 'America/Chicago':
				addHrs = 2;
				break;
			case 'America/Denver':
				addHrs = 1;
				break;
		}
		var return_dt = new Date();
		if (addHrs > 0) {
			return_dt.setHours(return_dt.getHours() + addHrs);
		}
		// log.debug({title: 'GF_MR_RICS_Inventory_OnHand.js:adjustTimeZone', details: '_str_tz: ' + _str_tz + ', _dt-hours: ' + _dt.getHours() + ', addHrs: ' + addHrs});
		return return_dt;
		// if(_str_tz == undefined || _str_tz == ''){ return new Date() }
		// return new Date(_dt.toLocaleString("en-US", {timeZone: _str_tz}));
	}

	/**
	 * This function takes in the id of the location that a deployment would need to be built for and then creates and calls that deployment
	 * @param {*} _loc_id - integer/string - this should be the internal id of the location record that is being processed
	 * @returns - void
	 */
	function buildNewScriptDeployment(_loc_id){
		try{
			// find running script instances
			var scheduledscriptinstanceSearchObj = search.create({
				type: "scheduledscriptinstance",
				filters: [
					["script.internalid","anyof","1385"], "AND",
					["mapreducestage","anyof","GET_INPUT"], "AND",
					["status","anyof","PROCESSING"]
				],
				columns: [ ]
			});
			var scriptinstanceCount = scheduledscriptinstanceSearchObj.runPaged().count;
			// log.audit({title: 'GF_MR_RICS_Inventory_OnHand.js:buildNewScriptDeployment', details: 'scriptinstanceCount: ' + scriptinstanceCount});
			if(scriptinstanceCount != null && scriptinstanceCount > 1){ return true; }

			var location_fields = search.lookupFields({
				type: 'location',
				id: parseInt(_loc_id),
				columns: ['namenohierarchy']
			});
			var strLocName = location_fields.namenohierarchy;
			if(strLocName == undefined || strLocName == ''){ return true; }
			// find any existing deployments made from this location
			var scriptdeploymentSearchObj = search.create({
				type: "scriptdeployment",
				filters: [
				["script","anyof","1385"], "AND", // GF_MR_RICS_Inventory_OnHand
				["title","contains",strLocName]
				],
				columns: [ ]
			});
			// count the results with this name - the only reason it should be anything other than zero is if the script has errored
			var searchResultCount = scriptdeploymentSearchObj.runPaged().count;
			// set a unique deployment name allow for multiples
			var str_deployName = (searchResultCount < 1) ? 'GF_MR_RICS_Inventory_OnHand ' + strLocName : 'GF_MR_RICS_Inventory_OnHand ' + strLocName + ' ' + searchResultCount;
			// create a new on demand script deployment for this location and run the deployment
			var deployment_rec = record.copy ({
				type: record.Type.SCRIPT_DEPLOYMENT,
				id: 15160
			});
			deployment_rec.setValue ({fieldId: 'isdeployed', value: true});
			deployment_rec.setValue ({fieldId: 'title', value: str_deployName});
			deployment_rec.setValue ({fieldId: 'startdate', value: new Date()});
			var deploy_id = deployment_rec.save();

			if(deploy_id > 0){
				// get the script id of the saved deployment
				var script_deployment_fields = search.lookupFields({
					type: 'scriptdeployment',
					id: parseInt(deploy_id),
					columns: ['scriptid']
				});
				var deploy_script_id = script_deployment_fields.scriptid;
				// run the deployment
				var call_onhand_js = task.create({
					taskType: task.TaskType.MAP_REDUCE,
					scriptId: 'customscript_mr_rics_inventory_onhand',
					deploymentId: deploy_script_id
				});
				call_onhand_js.submit();
				// log.audit({title: 'GF_MR_RICS_Inventory_OnHand.js:buildNewScriptDeployment', details: 'created and called deploy_script_id: ' + deploy_script_id});
			}
		}catch(ex){
			log.error({title: 'Error: GF_MR_RICS_Inventory_OnHand.js:buildNewScriptDeployment', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
		}
	}

	/**
	 * This function will find any script deployments that are finished running, and not one of the main two deployments and delete them
	 * @returns - void
	 */
	function removeCompletedDeployments(){
		// main two deployments: customdeploy_mr_rics_inventory_onhand, customdeploy_mr_rics_inventory_onhand_od
		var arr_deployment_ids = [];
		var scheduledscriptinstanceSearch = search.create({
			type: "scheduledscriptinstance",
			filters: [
				["status","anyof","COMPLETE","CANCELED"], "AND",
				["script.internalid","anyof","1385"], "AND",
				["scriptdeployment.internalid","noneof","15161","15160"], "AND",
				["enddate","onorbefore","hoursago24"]
			],
			columns: [
				search.createColumn({
					name: "name",
					join: "script"
				}),
				search.createColumn({
					name: "scriptid",
					join: "scriptDeployment"
				}),
				search.createColumn({
					name: "internalid",
					join: "scriptDeployment"
				}),
				search.createColumn({
					name: "timestampcreated",
					sort: search.Sort.DESC
				}),
				"mapreducestage",
				"status",
				"startdate",
				"enddate"
			]
		}).run();
		for (var k = 0; k < 2; k++) { // max pages 2, up to 2,000 records total
			var results = scheduledscriptinstanceSearch.getRange({start: k * 1000, end: (k * 1000) + 1000});
			for (var i = 0; i < results.length; i++) {
				var this_deployment_id = results[i].getValue({name: 'internalid', join: 'scriptDeployment'});
				if(arr_deployment_ids.indexOf(this_deployment_id) == -1){
					arr_deployment_ids.push(this_deployment_id);
				}
			}
		}

		// remove the deployments
		for(var dep_id in arr_deployment_ids){
			var deployment_id = arr_deployment_ids[dep_id];
			try{
				record.delete({type: 'scriptdeployment', id: deployment_id});
			}catch(ex){
				log.error({title: 'Error: GF_MR_RICS_Inventory_OnHand.js:removeCompletedDeployments', details: 'deployment_id: ' + deployment_id + ', Error ' + ex.toString() + ' : ' + ex.stack});
			}
		}
	}

	/**
	 * This function will find all location ids where the on-hand custom records have been fully updated and do not need any additional updates for at least 6 hours
	 * @returns - array - This array is a list of location ids where all of the on-hand records have meen modifified recently and do not need further updates
	*/
	function getListOfCompletedLocations(){
		var arr_completed_loc_ids = [];
		try{
			var customrecord_rics_inventory_on_handSearchObj = search.create({
				type: "customrecord_rics_inventory_on_hand",
				filters: [
				   ["min(lastmodified)","onorafter","hoursago6"],"AND",
                   ["custrecord_rics_ioh_parent_productitem.isinactive","is","F"],"AND",
                   ["custrecord_rics_ioh_ns_matrix_subitem.isinactive","is","F"]
				//    , "AND", ["max(lastmodified)","onorafter","hoursago24"]
				],
				columns: [
				   search.createColumn({
					  name: "internalid",
					  summary: "COUNT"
				   }),
				   search.createColumn({
					  name: "internalid",
					  join: "CUSTRECORD_RICS_IOH_NS_LOCATION",
					  summary: "GROUP"
				   }),
				   search.createColumn({
					  name: "custrecord_rics_ioh_ns_location",
					  summary: "GROUP"
				   }),
				   search.createColumn({
					  name: "lastmodified",
					  summary: "MIN",
					  sort: search.Sort.ASC
				   }),
				   search.createColumn({
					  name: "lastmodified",
					  summary: "MAX",
					  sort: search.Sort.ASC
				   })
				]
			});
			customrecord_rics_inventory_on_handSearchObj.run().each(function(result){
				// .run().each has a limit of 4,000 results
				arr_completed_loc_ids.push(result.getValue({
					name: "internalid",
					join: "CUSTRECORD_RICS_IOH_NS_LOCATION",
					summary: "GROUP"
				}));
				return true;
			});
			return arr_completed_loc_ids;
		}catch(ex){
			log.error({title: 'Error: GF_MR_RICS_Inventory_OnHand.js:buildNewScriptDeployment', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
		}
	}

    function removeDuplicateOnHandRecords(){
        var customrecord_rics_inventory_on_handSearchObj = search.create({
        type: "customrecord_rics_inventory_on_hand",
        filters: [
          ["count(internalid)","greaterthan","1"]
        ],
        columns: [
          search.createColumn({
            name: "internalid",
            summary: "COUNT"
          }),
          search.createColumn({
            name: "custrecord_rics_ioh_ns_matrix_subitem",
            summary: "GROUP"
          }),
          search.createColumn({
            name: "custrecord_rics_ioh_ns_location",
            summary: "GROUP"
          }),
          search.createColumn({
            name: "formulatext",
            summary: "MIN",
            formula: "NS_CONCAT({internalid})"
          })
        ]
      });
      customrecord_rics_inventory_on_handSearchObj.run().each(function(result){
        // .run().each has a limit of 4,000 results
        var str_ids = result.getValue({ name: "formulatext", summary: "MIN", formula: "NS_CONCAT({internalid})" });
        var arr = str_ids.split(',');
        try{
            for(var a in arr){
                if(a == (arr.length - 1)){ break; }
                record.delete({type: 'customrecord_rics_inventory_on_hand', id: arr[0]});
            }
        }catch(ex){
            log.error({title: 'Error: GF_MR_RICS_Inventory_OnHand.js:removeDuplicateOnHandRecords', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
        return true;
      });

    }

});