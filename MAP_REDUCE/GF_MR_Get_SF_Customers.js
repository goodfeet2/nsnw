/**
 *@NApiVersion 2.1
 *@NScriptType MapReduceScript
 *
Script Name:   GF_MR_Get_SF_Customers.js
Author:        Mark Robinson
 */

define(['N/log', 'N/search', 'N/record', 'N/https', 'N/runtime'], function(log, search, record, https, runtime){

    /**
     * In this get input function we get all of the RICS Sales Ticket custom records where the customer information was not found in NetSuite
     * @param {*} context  - this variable is not utilized in this function
     * @returns - an array containing the full responses from the custom record search
     */
    function gf_getInputData(context){
        try {
            var currentScript = runtime.getCurrentScript();
            var ticket_id = currentScript.getParameter({ name: 'custscript_getcustomer_sales_ticket_id' });
            var arr_filters = [
                // CUST269538 Walk-In Customer (this is the default customer used when the RICS customer information is not found)
                ["custbody_gf_rics_sales_ticket_id.name","anyof","4716780"],  "AND",
                ["formulanumeric: CASE WHEN {custrecord_rics_sd_parent.custrecord_rics_sd_ticketlinenumber} = 1 THEN 1 ELSE 0 END","greaterthan","0"], "AND",
                ["custbody_gf_rics_sales_ticket_id.mainline","is","T"], "AND",
                [
                    ["usernotes.note","isnot","CUSTOMER RECORD NOT FOUND"],"AND",
                    ["usernotes.note","isnot","NEW CUSTOMER RECORD CREATED"]
                ]
            ];
            if(ticket_id != null){
                arr_filters.push("AND");
                arr_filters.push(["internalid","anyof",ticket_id]);
            }
            var customrecord_rics_sales_ticketSearchObj = search.create({
                type: "customrecord_rics_sales_ticket",
                filters: arr_filters,
                columns: [
                    search.createColumn({
                        name: "formulanumeric",
                        summary: "MIN",
                        formula: "NS_CONCAT({internalid})"
                    }),
                    search.createColumn({
                        name: "custrecord_rics_st_customeraccountnumber",
                        summary: "GROUP"
                    }),
                    search.createColumn({
                        name: "custrecord_rics_st_customerid",
                        summary: "GROUP"
                    })
                ]
            });

            return customrecord_rics_sales_ticketSearchObj;
        }catch(ex) {
            log.error({title: 'GF_MR_Get_SF_Customers.js:gf_getInputData', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this mapping function we we send a request out to AWS to get the SF data from the DB that the webhook connects to
     * @param {*} context - Object - This should be the RICS NonSellable Batch custom record
     * @returns void (note: the const context variable is impacted and used in the reduce function)
     */
    function gf_map(context){
        try{
            var map_data = JSON.parse(context.value);
            // log.audit({title: 'GF_MR_Get_SF_Customers.js:gf_map', details: 'map_data: ' + JSON.stringify(map_data)});
			if (map_data == null) {
				return true;
			}else{
                var arr_netsuite_internal_ids = (map_data.values['MIN(formulanumeric)']).split(',');
                var account_number = map_data.values['GROUP(custrecord_rics_st_customeraccountnumber)'];
                var rics_identifier = map_data.values['GROUP(custrecord_rics_st_customerid)'];
                // log.audit({title: 'GF_MR_Get_SF_Customers.js:gf_map', details: 'account_number: ' + account_number + ', rics_identifier: ' + rics_identifier});
                // search the AWS DB for this customer
                var arr_result_data = [];
                var header = [];
                header['Content-Type'] = 'application/json';
                header['x-api-key'] = 'A6xeIKveD1nBRIuKQ75g7IjxLsEFh4t73Dszaa5b';
                // remove SF from the front of the acct number (if it exists)
                if(account_number.indexOf('SF') == 0){ account_number = account_number.substring(2); }
                var postData = { "acct_num": account_number, "rics_id":rics_identifier };
                // log.debug({title: 'GF_MR_Get_SF_Customers.js:gf_map', details: 'postData: ' + JSON.stringify(postData)});
                postData = JSON.stringify(postData);
                var endpoint = 'https://hg06i49148.execute-api.us-west-2.amazonaws.com/test/getAccount'
                https.request.promise({
                    method: https.Method.POST,
                    url: endpoint,
                    body: postData,
                    headers: header
                }).then(function(response){
                        // log.debug({ title: 'Response', details: response });
                        var res_body = JSON.parse(response.body);
                        for(var rows in res_body){
                            // if we got data back pass data to reduce to either
                            // log.audit({title: 'GF_MR_Get_SF_Customers.js:gf_map', details: 'res_body: ' + JSON.stringify(res_body[rows])});
                            var reduce_data = {
                                "ns_ids": arr_netsuite_internal_ids
                            }
                            if(res_body[rows].sf_id != undefined && res_body[rows].sf_id != ''){
                                // - grab customer data
                                var res_data = res_body[rows];
                                for(var prop in res_data){
                                    reduce_data[prop] = res_data[prop];
                                }
                            }
                            reduce_data.customer_rec_id = findCustomer(reduce_data);
                            arr_result_data.push(reduce_data);
                        }
                        // log.audit({title: 'GF_MR_Get_SF_Customers.js:gf_map', details: 'context.key: ' + context.key + ', arr_result_data: ' + JSON.stringify(arr_result_data)});
                        if(arr_result_data.length > 0){
                            context.write({ key: context.key, value: arr_result_data }); // pass through directly to reduce
                        }else{
                            var reduce_data = {
                                "ns_ids": arr_netsuite_internal_ids,
                                "error": 'CUSTOMER RECORD NOT FOUND'
                            }
                            arr_result_data.push(reduce_data);
                            context.write({ key: context.key, value: arr_result_data });
                        }
                    }).catch(function onRejected(reason) {
                        log.debug({ title: 'Invalid Request: ', details: reason });
                    })
            }
			return true;
        }catch(ex) {
            log.error({title: 'GF_MR_Get_SF_Customers.js:gf_map', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this reduce function we use the response from the web server to either build a customer record or add a user note to the customer record
     * @param {*} context - Object - This is the object that was built in the mapping function, it should conatin all the data we need for processing
     * @returns - void
     */
    function gf_reduce(context){
        try{
            // log.audit({title: 'GF_MR_Get_SF_Customers.js:gf_reduce', details: 'context.value: ' + JSON.stringify(context)});
            for(var data in context.values){
                var reduce_data = JSON.parse(context.values[data]);
                log.audit({title: 'GF_MR_Get_SF_Customers.js:gf_reduce', details: 'reduce_data: ' + JSON.stringify(reduce_data)});
                for(var data in reduce_data){
                    var data_obj = reduce_data[data];
                    var sales_ticket_id = data_obj.ns_ids[st_rec];
                    // when error is not set
                    if(data_obj.error == undefined){
                        // check if customer was found
                        if(data_obj.customer_rec_id != 0){
                            var customerRecordId = data_obj.customer_rec_id;
                        }else{
                            var sales_ticket_id = data_obj.ns_ids[0];
                            // create the customer record
                            var sales_ticket_record = record.load({
                                type: 'customrecord_rics_sales_ticket',
                                id: sales_ticket_id,
                                isDynamic: true
                            });
                            var cust_specific_data = {
                                "firstname": (sales_ticket_record.getValue('custrecord_rics_st_customerfirstname')).toUpperCase(),
                                "lastname": (sales_ticket_record.getValue('custrecord_rics_st_customerlastname')).toUpperCase(),
                                "phone": ((data_obj.phone && data_obj.phone != '') ? data_obj.phone : sales_ticket_record.getValue('custrecord_rics_st_customerphonenumber')),
                                "email": ((data_obj.email && data_obj.email != '') ? (data_obj.email).toLowerCase() : (sales_ticket_record.getValue('custrecord_rics_st_customeremail')).toLowerCase()),
                                "custentity_gf_rics_customer_id": ((data_obj.rics_id && data_obj.rics_id != '') ? data_obj.rics_id : sales_ticket_record.getValue('custrecord_rics_st_customerid')),
                                "custentity_gf_rics_cust_acct_number": ((data_obj.sf_id && data_obj.sf_id != '') ? data_obj.sf_id : sales_ticket_record.getValue('custrecord_rics_st_customeremail'))
                            };
                            var rec = record.create({
                                type: record.Type.CUSTOMER
                            });
                            // set customer record fields
                            for(var cust_data in cust_specific_data){
                                if(cust_data == 'custentity_gf_rics_cust_acct_number' && cust_specific_data[cust_data].indexOf('SF') == -1){
                                    rec.setValue(cust_data, 'SF'+cust_specific_data[cust_data]);
                                }else{
                                    rec.setValue(cust_data, cust_specific_data[cust_data]);
                                }
                            }
                            rec.setValue('giveaccess', false);
                            rec.setValue('subsidiary', '1'); // primary Subsidiary == MRQD Inc.
                            // save record first - then set address and subsidiaries
                            var customerRecordId = rec.save({ ignoreMandatoryFields: true });
                            if(customerRecordId && customerRecordId > 0){
                                var saved_rec = record.load({
                                    type: record.Type.CUSTOMER,
                                    id: customerRecordId,
                                    isDynamic: true
                                });
                                // set customer subsidiaries
                                setSubsidiarySublist(saved_rec); // add all other subsidiaries
                                saved_rec.save({ ignoreMandatoryFields: true });
                            }
                            if(saved_rec && saved_rec > 0){
                                saveUserNote(sales_ticket_id, 1237, 'NEW CUSTOMER RECORD CREATED', 'Ticket Internal ID: ' + sales_ticket_id + ' - ' + 'Cash Sale Deposited - Customer Record ID: ' + customerRecordId);
                            }
                        }
                    }else{
                        // add user note to the custom record
                        for(var st_rec in data_obj.ns_ids){
                            var sales_ticket_id = data_obj.ns_ids[st_rec];
                            saveUserNote(sales_ticket_id, 1237, data_obj.error, 'Ticket Internal ID: ' + sales_ticket_id + ' - ' + data_obj.error);
                        }
                    }
                }
            }
            return true;
        }catch(ex) {
            log.error({title: 'GF_MR_Get_SF_Customers.js:gf_reduce', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * This summarize function is used to report important aspects about how the script performed
     * @param {*} summary - this object contains data regarding script performance.
     */
    function gf_summarize(summary) {
        try{
            log.audit('Usage Consumed', summary.usage);
            log.audit('Concurrency Number ', summary.concurrency);
            log.audit('Number of Yields', summary.yields);
            log.audit({title: 'Process Completed'});
        }catch(ex) {
            log.error({title: 'GF_MR_Get_SF_Customers.js:rics_summarize', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    return {
        getInputData: gf_getInputData,
        map: gf_map,
        reduce: gf_reduce,
        summarize: gf_summarize
    };

    // =========================== extra functions
    /**
     * findCustomer is used to return the most closely matched customer record id based on the values from the RICS Sales Ticket record related through the RICS Sales Details record
     * @param {*} _values
     */
     function findCustomer(_values){
        /*
        {
            "sf_id":"0013l00002Rc0P8AAJ",
            "firstname":null,
            "lastname":null,
            "phone":"(801) 698-3956",
            "email":"vkpoll1@gmail.com",
            "rics_id":"9c8cb7e8-e1a3-4595-8493-ad7501436504",
            "rics_acct":null,
            "street":null,
            "city":"CLEARFIELD",
            "state":"UT",
            "zip":"84015"
        }
        */
        var result_id = 0;
        // set customer values
        var phoneNum = (_values['phone'] != undefined) ? _values['phone'] : '';
        var email = (_values['email'] != undefined) ? _values['email'] : '';
        var rics_acct = (_values['rics_acct'] != undefined) ? _values['rics_acct'] : '';
        var rics_cust_id = (_values['rics_id'] != undefined) ? _values['rics_id'] : '';

        var arr_customer_matches = [];
        var arr_filters = [];
        arr_filters.push(["isinactive","is","F"]);
        arr_filters.push("AND");
        if(rics_acct != '' && rics_cust_id != ''){
            arr_filters.push([
                ["custentity_gf_rics_cust_acct_number","contains",rics_acct],"OR",
                ["custentity_gf_rics_customer_id","is",rics_cust_id]
            ]);
        }else if(rics_acct != '' || rics_cust_id != ''){
            if(rics_acct != ''){
                arr_filters.push(["custentity_gf_rics_cust_acct_number","contains",rics_acct]);
            }else{
                arr_filters.push(["custentity_gf_rics_customer_id","is",rics_cust_id]);
            }
        }
        // find the Customer Record
        var customer_Search = search.create({
            type: "customer",
            filters: arr_filters,
            columns: [
                'phone',
                'altphone',
                'mobilephone',
                'email',
                'altemail',
                'custentity_gf_rics_customer_id',
                'custentity_gf_rics_cust_acct_number'
            ]
        });
        customer_Search.run().each(function(result){
            // .run().each has a limit of 4,000 results
            var customer_match = {};
            customer_match.match_val = 0;
            customer_match.id = result.id;
            var result_phone = result.getValue('phone');
            if(result_phone == phoneNum){ customer_match.match_val += 1; };
            var result_altphone = result.getValue('altphone');
            if(result_altphone == phoneNum){ customer_match.match_val += 1; };
            var result_mobilephone = result.getValue('mobilephone');
            if(result_mobilephone == phoneNum){ customer_match.match_val += 1; };
            var result_email = result.getValue('email');
            if(result_email == email){ customer_match.match_val += 1; };
            var result_altemail = result.getValue('altemail');
            if(result_altemail == email){ customer_match.match_val += 1; };
            var result_custentity_gf_rics_customer_id = result.getValue('custentity_gf_rics_customer_id');
            if(result_custentity_gf_rics_customer_id == rics_cust_id){ customer_match.match_val += 5; };
            var result_custentity_gf_rics_cust_acct_number = result.getValue('custentity_gf_rics_cust_acct_number');
            if(result_custentity_gf_rics_cust_acct_number == rics_acct){ customer_match.match_val += 5; };
            arr_customer_matches.push(customer_match);
            return true;
        });
        if(arr_customer_matches[0] != undefined){
            arr_customer_matches.sort(function(a, b) {
                return b.match_val - a.match_val; // descending sort with biggest value on top
            });
            // when the top result has any match weight at all it is assumed to be the best match
            // result_id = (arr_customer_matches[0] > 0) ? Object.keys(arr_customer_matches)[0] : 0;
            result_id = arr_customer_matches[0].id;
            log.debug({title: 'GF_MR_Get_SF_Customers.js:findCustomer-arr_customer_matches', details: 'arr_customer_matches: ' + JSON.stringify(arr_customer_matches)});
        }
        log.debug({title: 'GF_MR_Get_SF_Customers.js:findCustomer', details: 'result_id: ' + result_id});
        return result_id;
    }

    /**
     * This function will set the subsidiary sublist on a customer record.
     * @param {*} _rec 
     * @returns true
     */
    function setSubsidiarySublist(_rec){
        try{
            var rec = _rec;
            var currentSubsidiaryCount = rec.getLineCount({ 'sublistId': 'submachine' });

            var arr_subs = ["7","3","4","2"];
            for(var i = 0; currentSubsidiaryCount > 0 && i < currentSubsidiaryCount; i++){
                // remove the existing subs from the array to write if the already exist in the customer record
                rec.selectLine({ sublistId: 'submachine', line: i });
                var sublistValue = rec.getCurrentSublistValue({
                    sublistId: 'submachine',
                    fieldId: 'subsidiary'
                });
                if(arr_subs.indexOf(sublistValue) > -1){
                    arr_subs.splice(arr_subs.indexOf(sublistValue), 1);
                }
            }

            // write all values remaining to the subsidiary sublist
            for(var i = 0; arr_subs.length > 0 && i < arr_subs.length; i++){
                rec.selectNewLine({ sublistId: 'submachine' });

                rec.setCurrentSublistValue({
                    sublistId: 'submachine',
                    fieldId: 'subsidiary',
                    value: arr_subs[i],
                    ignoreFieldChange: true
                });
                rec.commitLine({sublistId: 'submachine'});
            }

            return true;
        }catch(ex){
            log.error({title: 'Error: GF_MR_Get_SF_Customers.js:setSubsidiarySublist', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
            var obj_return = {};
            obj_return.error = ex;
            obj_return.error_text = ex.toString() + ' : ' + ex.stack;
            return obj_return;
        }
    }

    /**
     * This function takes the params required to write a user note on a custom record and returns void
     * @param {number} _rec_id - internal id of the custom record that the note will be attached to
     * @param {number} _rec_type - internal id of the custom record RECORD TYPE that the note will be attached to
     * @param {string} _str_note - the actual note text
     * @param {string} _str_title - the note title
     */
	function saveUserNote(_rec_id, _rec_type, _str_note, _str_title){
		var noteRec = record.create({ type: record.Type.NOTE });
		noteRec.setValue({ fieldId : 'title', value : _str_title });
		noteRec.setValue({ fieldId : 'notetype', value : 9 }); // Integration
		noteRec.setValue({ fieldId : 'author', value : 2226 }); // Application Program Integration
		noteRec.setValue({ fieldId : 'record', value : _rec_id });
		noteRec.setValue({ fieldId : 'recordtype', value : _rec_type });
		noteRec.setValue({ fieldId : 'note', value : _str_note.substring(0, 4000) });
		try{
			var note_id = noteRec.save();
			log.audit({title: 'GF_MR_Get_SF_Customers.js:saveUserNote', details: 'note_id: ' + note_id});
		}catch(ex){
			log.error({title: 'GF_MR_Get_SF_Customers.js:saveUserNote', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
		}
     }

});