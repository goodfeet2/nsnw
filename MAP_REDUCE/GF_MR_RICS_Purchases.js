/**
 *@NApiVersion 2.1
 *@NScriptType MapReduceScript
 *
 Script Name:   GF_MR_RICS_Purchases.js
 Author:        Mark Robinson
 */

 define(['N/log', 'N/search', 'N/format', 'N/record', 'N/runtime', '/SuiteScripts/GF_LIB/gf_nsrics'], function(log, search, format, record, runtime, gf_nsrics) {

    /**
     * In this get input function we're getting all of the RICS Purchase Order custom records that need to have their data pushed to a new
     * NetSuite Purchase Order or Update an existing one.
     * @param {*} context - this variable is not utilized in this function
     * @returns - an array containing the full resultset array from the search
     */
    function rics_getInputData(context){
        var currentScript = runtime.getCurrentScript();
        var ns_rics_po_id = currentScript.getParameter({ name: 'custscript_ns_rics_purchase_order_id' });
        var nsrics = gf_nsrics.getIntegration();
        var open_period_range = nsrics.getOpenPeriodRange(); // .earliest_open_date - .latest_open_date // moved to verification in reduce phase for new POs only
        // Get all customrecord_rics_purchase_order Data data from RICS with a search
        // Map phase will create a NetSuite writable object
        // Reduce phase will save the NetSuite writable object
        try {
            var arr_Transactions = [];
            if(ns_rics_po_id == null){
                var customrecord_rics_purchase_orderSearchObj = search.create({
                    type: "customrecord_rics_purchase_order",
                    filters: [
                        ["custrecord_rics_po_purchaseorder","isempty",""]
                    ],
                    columns: [ ]
                });

                return customrecord_rics_purchase_orderSearchObj;
            }else{
                log.audit({title: 'GF_MR_RICS_Purchases.js:rics_getInputData', details: 'ns_rics_po_id: ' + JSON.stringify(ns_rics_po_id)});
                var trans_data = {id: ns_rics_po_id};
                arr_Transactions = [trans_data];
                log.debug({title: 'GF_MR_RICS_Purchases.js:rics_getInputData', details: 'arr_Transactions: ' + JSON.stringify(arr_Transactions)});

                return arr_Transactions;
            }
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_Purchases.js:rics_getInputData', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * In this mapping function we build a NetSuite writable object based on the data that was gathered in the Get Input function
     * @param {*} context - this is expected to be the search results from the Get Input function
     * @returns - void (note: the const context variable is impacted and used in the reduce function)
     */
    function rics_map(context){
        try{
            var currentScript = runtime.getCurrentScript();
            var data = JSON.parse(context.value);
            if(data == null){
                log.error({title: 'GF_MR_RICS_Purchases.js:rics_map-null data', details: 'context.value: ' + JSON.stringify(context.value)});
                return true;
            }
            // build a NetSuite writable object
            var ns_record_data = {};
            if(data.po_record == undefined){
                // get record and children
                var po_data = {};
                var arr_pods = [];
                var purchase_order_record = record.load({
                    type: 'customrecord_rics_purchase_order',
                    id: data.id,
                    isDynamic: true
                });
                po_data.po_record = purchase_order_record;
                var customrecord_rics_po_detailsSearchObj = search.create({
                    type: "customrecord_rics_po_details",
                    filters: [
                        ["custrecord_rics_pod_parent_po","anyof",data.id], "AND",
                        ["custrecord_rics_pod_product_details","noneof","@NONE@"], "AND",
                        ["custrecord_rics_pod_product_details.custrecord_rics_pd_avail_pos_on","onorbefore","yearsfromnow1"], "AND",
                        ["custrecord_rics_pod_product_item","noneof","@NONE@"]
                    ],
                    columns: [
                        search.createColumn({
                            name: "formulanumeric",
                            formula: "NVL2({custrecord_rics_pod_line_unique_id}, 1, 0)",
                            sort: search.Sort.ASC
                        })
                    ]
                });
                customrecord_rics_po_detailsSearchObj.run().each(function(result_pod){
                    arr_pods.push(result_pod.id);
                    return true;
                });
                po_data.pod_records = arr_pods;
                // process po_data ===================================================================================================
                ns_record_data.recordtype = 'purchaseorder';
                ns_record_data.source_id = data.id;
                ns_record_data.custbody_gf_rics_purchase_order_id = data.id;
                ns_record_data.customform = 120; // GF - Primary Purchase Order Form
                var loc_id = find_location_by_ext_id(purchase_order_record.getValue('custrecord_rics_po_shiptostorecode'));
                // lookup subsidiary
                var sub_id = find_subsidiary(loc_id);
                // lookup vendor
                var vendor_id = find_vendor(purchase_order_record.getValue('custrecord_rics_po_suppliercode'));
                if(vendor_id != 0){
                    ns_record_data.subsidiary = sub_id;
                    ns_record_data.location = loc_id;
                    ns_record_data.trandate = purchase_order_record.getValue('custrecord_rics_po_shipon'); //purchase_order_record.getValue('custrecord_rics_po_orderedon');
                    ns_record_data.shipdate = purchase_order_record.getValue('custrecord_rics_po_shipon');

                    var ns_po_id = purchase_order_record.getValue('custrecord_rics_po_purchaseorder');
                    var nsrics = gf_nsrics.getIntegration();
                    var str_trandt = format.format({ value : ns_record_data.trandate, type : format.Type.DATE});
                    var open_period_range = nsrics.getOpenPeriodRange(); // .earliest_open_date - .latest_open_date
                    // when the transaction date is not within the open period, do not allow the record to be generated
					if((ns_po_id == null || ns_po_id == '' || ns_po_id == 0) && (new Date(str_trandt) < new Date(open_period_range.earliest_open_date) || new Date(str_trandt) > new Date(open_period_range.latest_open_date))){
                        log.debug({title: 'GF_MR_RICS_Purchases.js:rics_map', details: 'closed period check - current open date range: ' + open_period_range.earliest_open_date + ' to ' + open_period_range.latest_open_date + ', str_trandt: ' + str_trandt});
                        return true;
                    }

                    // add vendor
                    ns_record_data.entity = vendor_id;
                    ns_record_data.currency = "1";
                    ns_record_data.custbody_gf_internal_po = purchase_order_record.getValue('custrecord_rics_po_purchaseordernumber');

                    var arr_pod_recs = [];
                    log.debug({title: 'GF_MR_RICS_Purchases.js:rics_map', details: 'map arr_pods: ' + JSON.stringify(arr_pods)});
                    for(var pod in arr_pods){
                        log.audit({title: 'GF_MR_RICS_Purchases.js:rics_map-pod='+(parseInt(pod)+1)+'/'+arr_pods.length, details: 'remaining usage: ' + currentScript.getRemainingUsage()});
                        if(currentScript.getRemainingUsage() < 200){ break; }
                        var po_details_id = arr_pods[pod];
                        var po_details_record = record.load({
                            type: 'customrecord_rics_po_details',
                            id: po_details_id,
                            isDynamic: true
                        });
                        var item_data = po_details_record.toJSON();
                        log.debug({title: 'GF_MR_RICS_Purchases.js:rics_map', details: 'map find_items - po_details_record: ' + JSON.stringify(po_details_record)});
                        item_data.item_id = find_item(item_data.fields.custrecord_rics_pod_json, item_data.fields.custrecord_rics_pod_product_item);
                        if(item_data.item_id > 0){
                            arr_pod_recs.push(item_data);
                        }
                    }
                    log.debug({title: 'GF_MR_RICS_Purchases.js:rics_map', details: 'arr_pod_recs: ' + JSON.stringify(arr_pod_recs)});
                    ns_record_data.pod_records = arr_pod_recs;

                    ns_record_data.status = (purchase_order_record.getValue('custrecord_rics_po_purchaseordertype')).toLowerCase();

                    // pass data to reduce for setting on hand
                    context.write({ key: purchase_order_record.getValue('custrecord_rics_po_purchaseordernumber'), value: ns_record_data });
                }
            }else{
                log.debug({title: 'GF_MR_RICS_Purchases.js:rics_map', details: 'data: ' + JSON.stringify(data)});
                // process data ===================================================================================================
                ns_record_data.recordtype = 'purchaseorder';
                ns_record_data.source_id = data.po_record.id;
                ns_record_data.custbody_gf_rics_purchase_order_id = data.po_record.id;
                var loc_id = find_location_by_ext_id(data.po_record.fields.custrecord_rics_po_shiptostorecode);
                // lookup subsidiary
                var sub_id = find_subsidiary(loc_id);
                // lookup vendor
                var vendor_id = find_vendor(data.po_record.fields.custrecord_rics_po_suppliercode);
                if(vendor_id != 0){
                    ns_record_data.subsidiary = sub_id;
                    ns_record_data.location = loc_id;
                    ns_record_data.trandate = data.po_record.fields.custrecord_rics_po_shipon; // data.po_record.fields.custrecord_rics_po_orderedon;
                    ns_record_data.shipdate = data.po_record.fields.custrecord_rics_po_shipon;
                    // add vendor
                    ns_record_data.entity = vendor_id;
                    ns_record_data.currency = "1";
                    ns_record_data.custbody_gf_internal_po = data.po_record.fields.custrecord_rics_po_purchaseordernumber;

                    var arr_pod_recs = [];
                    log.debug({title: 'GF_MR_RICS_Purchases.js:rics_map', details: 'map data.pod_records: ' + JSON.stringify(data.pod_records)});
                    for(var pod in data.pod_records){
                        var item_data = data.pod_records[pod];
                        log.debug({title: 'GF_MR_RICS_Purchases.js:rics_map', details: 'map find_items - item_data: ' + JSON.stringify(item_data)});
                        item_data.item_id = find_item(item_data.fields.custrecord_rics_pod_json, item_data.fields.custrecord_rics_pod_product_item);
                        if(item_data.item_id > 0){
                            arr_pod_recs.push(item_data);
                        }
                    }
                    log.debug({title: 'GF_MR_RICS_Purchases.js:rics_map', details: 'arr_pod_recs: ' + JSON.stringify(arr_pod_recs)});
                    ns_record_data.pod_records = arr_pod_recs;

                    ns_record_data.status = (data.po_record.fields.custrecord_rics_po_purchaseordertype).toLowerCase();

                    // pass data to reduce for setting on hand
                    context.write({ key: data.po_record.fields.custrecord_rics_po_purchaseordernumber, value: ns_record_data });
                }
            }
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_Purchases.js:rics_map', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * This reduce function takes the object created in the mapping function and either updates an existing PO or creates a new one as needed
     * @param {*} context - this should be a javascript object with properties that match up with the field names on the NetSuite record
     * @returns void
     */
    function rics_reduce(context){
        try{
            var nsrics = gf_nsrics.getIntegration();
            var open_period_range = nsrics.getOpenPeriodRange(); // .earliest_open_date - .latest_open_date
            // update pricing and on hand quantity on all of the items by UPC and storecode
            var data = JSON.parse(context.values[0]);
            if(data == null){ return true; }
            log.audit({title: 'GF_MR_RICS_Purchases.js:rics_reduce', details: 'data: ' + JSON.stringify(data)});
            if(data.hasOwnProperty('custbody_gf_internal_po') && data.hasOwnProperty('pod_records') && (data.pod_records).length > 0){
                var arr_not_record_prop = [ // the values are for processing, and not to be directly written on the record
                    'recordtype',
                    'status',
                    'source_id',
                    'pod_records'
                ];
                var saved_po_id = 0;
                var ns_po_id = find_po_record(data.custbody_gf_internal_po);
                // LOCATION CHANGE UPDATE CHECK =================================================
                if(ns_po_id > 0){
                    var temp_purchase_order_record = record.load({
                        type: data.recordtype,
                        id: ns_po_id,
                        isDynamic: true
                    });
                    // detect change in location
                    var current_loc = temp_purchase_order_record.getValue('location');
                    log.debug({title: 'GF_MR_RICS_Purchases.js:rics_reduce- LOCATION UPDATE', details: 'old loc id: ' + JSON.stringify(current_loc) + ', new loc id: ' + data.location + ', matching: ' + (current_loc != data.location ? 'NO':'YES')});
                    if(current_loc != data.location){
                        try{
                            // Close/VOID the NS PO
                            temp_purchase_order_record.setValue({ fieldId:'memo', value: 'VOID' });
                            var lineCount = temp_purchase_order_record.getLineCount({ 'sublistId': 'item' });
                            for(var i = 0; i < lineCount; i++){
                                temp_purchase_order_record.selectLine({ sublistId: 'item', line: i });
                                temp_purchase_order_record.setCurrentSublistValue({ sublistId: 'item', fieldId: 'isclosed', value: true });
                                temp_purchase_order_record.commitLine({sublistId: 'item'});
                            }
                            // Blank the link on the RICS PO Custom Record
                            var rics_po_Id = record.submitFields({
                                type: 'customrecord_rics_purchase_order',
                                id: data.source_id,
                                values: {
                                    'custrecord_rics_po_purchaseorder': ''
                                }
                            });
                            // Blank the PO Line Unique ID, and NetSuite Purchase Order Record fields on the POD records
                            reset_pod_connection_fields(data.source_id);
                            // Force crating a new PO
                            ns_po_id = 0;
                            temp_purchase_order_record.save();
                        }catch(exception){
                            log.error({title: 'GF_MR_RICS_Purchases.js:rics_reduce - detect change in location section', details: 'Error: ' + exception.toString() + ' : ' + exception.stack});
                        }
                    }
                }
                // LOCATION CHANGE UPDATE CHECK =================================================
                if(ns_po_id == 0){
                    // NEW PURCHASE ORDER =================================================
                    // no tranfer order record was found we need to create one
                    var purchase_order_record = record.create({
                        type: data.recordtype
                    });
                    for(var prop in data){
                        if(arr_not_record_prop.indexOf(prop) == -1){
                            if(prop == 'trandate' || prop == 'shipdate'){
                                purchase_order_record.setValue(prop,new Date(data[prop]));
                                if(prop == 'shipdate'){ var dt_ship_date = new Date(data[prop]); }
                            }else{
                                purchase_order_record.setValue(prop,data[prop]);
                            }
                        }
                    }
                    var line_num = 0;
                    for(var line in data.pod_records){
                        var item_data = data.pod_records[line];
                        log.debug({title: 'GF_MR_RICS_Purchases.js:rics_reduce', details: 'set_item_data_new_po - item_data: ' + JSON.stringify(item_data)});
                        var order_qty = (item_data.fields.custrecord_rics_pod_receivedquantity > item_data.fields.custrecord_rics_pod_orderquantity) ? item_data.fields.custrecord_rics_pod_receivedquantity : item_data.fields.custrecord_rics_pod_orderquantity;
                        set_item_data_new_po(line_num, purchase_order_record, item_data.item_id, order_qty, item_data.fields.custrecord_rics_pod_cost, dt_ship_date);
                        line_num += 1;
                    }
                    // save the record
                    saved_po_id = purchase_order_record.save();
                    // NEW PURCHASE ORDER =================================================
                }else{
                    // EXISTING PURCHASE ORDER =================================================
                    log.debug({title: 'GF_MR_RICS_Purchases.js:rics_reduce', details: 'ns_po_id: ' + ns_po_id});
                    // a purchase order record was found we should update it
                    var purchase_order_record = record.load({
                        type: data.recordtype,
                        id: ns_po_id,
                        isDynamic: true
                    });
                    for(var prop in data){
                        if(arr_not_record_prop.indexOf(prop) == -1){
                            if(prop == 'trandate' || prop == 'shipdate'){
                                purchase_order_record.setValue(prop,new Date(data[prop]));
                                if(prop == 'shipdate'){ var dt_ship_date = new Date(data[prop]); }
                            }else{
                                purchase_order_record.setValue(prop,data[prop]);
                            }
                        }
                    }
                    // also update the line items
                    // - first update existing line items
                    var arr_updated_lines = [];
                    var lineCount = purchase_order_record.getLineCount({ 'sublistId': 'item' });
                    for(var i = 0; i < lineCount; i++){
                        purchase_order_record.selectLine({ sublistId: 'item', line: i });
                        var item_id = purchase_order_record.getCurrentSublistValue({
                            sublistId: 'item',
                            fieldId: 'item'
                        });
                        var lineuniquekey_id = purchase_order_record.getCurrentSublistValue({
                    		sublistId: 'item',
                      		fieldId: 'lineuniquekey'
                		});
                        for(var line in data.pod_records){
                            var item_data = data.pod_records[line];
                            if(item_id == item_data.item_id){
                                try{
                                    // log.audit({title: 'GF_MR_RICS_Purchases.js:rics_reduce-pod update attempt', details: 'item_data.fields.id: ' + item_data.fields.id + ', lineuniquekey_id: ' + lineuniquekey_id});
                              		var pod_update_id = record.submitFields({
                    					type: 'customrecord_rics_po_details',
                    					id: item_data.fields.id,
                    					values: { custrecord_rics_pod_line_unique_id: lineuniquekey_id, custrecord_rics_pod_ns_po: ns_po_id }
                					});
                                }catch(e){
                                  	log.error({title: 'GF_MR_RICS_Purchases.js:rics_reduce-pod update failure', details: 'item_data.fields.id: ' + item_data.fields.id + ', lineuniquekey_id: ' + lineuniquekey_id});
                                }
                            }
                            if(item_id == item_data.item_id && item_data.fields.custrecord_rics_pod_orderquantity != undefined && item_data.fields.custrecord_rics_pod_cost != undefined){
                                log.debug({title: 'GF_MR_RICS_Purchases.js:rics_reduce', details: 'update_item_data_existing_po - item_data: ' + JSON.stringify(item_data)});
                                var order_qty = (item_data.fields.custrecord_rics_pod_receivedquantity > item_data.fields.custrecord_rics_pod_orderquantity) ? item_data.fields.custrecord_rics_pod_receivedquantity : item_data.fields.custrecord_rics_pod_orderquantity;
                                update_item_data_existing_po(purchase_order_record, item_data.item_id, order_qty, item_data.fields.custrecord_rics_pod_cost, dt_ship_date);
                                purchase_order_record.commitLine({sublistId: 'item'});
                                arr_updated_lines.push(item_data.id);
                                break;
                            }
                        }
                    }
                    // then add any line items that were not updated
                    for(var line in data.pod_records){
                        var item_data = data.pod_records[line];
                        if(arr_updated_lines.indexOf(item_data.id) == -1 && item_data.fields.custrecord_rics_pod_orderquantity != undefined && item_data.fields.custrecord_rics_pod_cost != undefined){
                            log.debug({title: 'GF_MR_RICS_Purchases.js:rics_reduce', details: 'additional line items - item_data: ' + JSON.stringify(item_data)});
                            var order_qty = (item_data.fields.custrecord_rics_pod_receivedquantity > item_data.fields.custrecord_rics_pod_orderquantity) ? item_data.fields.custrecord_rics_pod_receivedquantity : item_data.fields.custrecord_rics_pod_orderquantity;
                            purchase_order_record.selectNewLine({ sublistId: 'item' });
                            update_item_data_existing_po(purchase_order_record, item_data.item_id, order_qty, item_data.fields.custrecord_rics_pod_cost);
                            purchase_order_record.commitLine({sublistId: 'item'});
                            arr_updated_lines.push(item_data.id);
                        }
                    }

                    // update_status(purchase_order_record, data.status);

                    saved_po_id = purchase_order_record.save();
                    // EXISTING PURCHASE ORDER =================================================
                }
                if(saved_po_id > 0){
                    var rics_po_Id = record.submitFields({
                        type: 'customrecord_rics_purchase_order',
                        id: data.source_id,
                        values: {
                            'custrecord_rics_po_purchaseorder': saved_po_id,
                            'custrecord_rics_po_ns_purch_order_update': new Date()
                        }
                    });
                }
                log.audit({title: 'GF_MR_RICS_Purchases.js:rics_reduce', details: 'saved_po_id: ' + saved_po_id + ', data: ' + JSON.stringify(data)});
            }
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_Purchases.js:rics_reduce', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    /**
     * This summarize function is used to report important aspects about how the script performed
     * @param {*} summary - this object contains data regarding script performance.
     */
    function rics_summarize(summary) {
        try{
            log.audit('Usage Consumed', summary.usage);
            log.audit('Concurrency Number ', summary.concurrency);
            log.audit('Number of Yields', summary.yields);
            log.audit({title: 'Process Completed'});
        }catch(ex) {
            log.error({title: 'GF_MR_RICS_Purchases.js:rics_summarize', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    return {
        getInputData: rics_getInputData,
        map: rics_map,
        reduce: rics_reduce,
        summarize: rics_summarize
    };

    // =========================== extra functions
    /**
     * This function take the RICS PO Number and finds the NetSuite Purchase Order
     * @param {*} _po_number - string - this is the RICS Purchase Order number
     * @returns - integer - this is the NetSuite internal id
     */
    function find_po_record(_po_number){
        var result_id = 0;

        var purchaseorderSearchObj = search.create({
            type: "purchaseorder",
            filters: [
               ["type","anyof","PurchOrd"], "AND",
               ["custbody_gf_internal_po","is",_po_number], "AND",
               ["status","noneof","PurchOrd:H"], "AND",
               ["mainline","is","T"]
            ],
            columns: [ ]
        });
        purchaseorderSearchObj.run().each(function(result){
            // .run().each has a limit of 4,000 results
            if(result_id == 0){
                result_id = result.id;
            }
            return true;
        });

        return result_id;
    }

    /**
     * This function takes the RICS location id (which is mapped in NetSuite as the External ID) and finds the NetSuite ID for that location
     * @param {*} _ext_id - integer/string - RICS Store Code
     * @returns - integer - the NetSuite location internal id
     */
    function find_location_by_ext_id(_ext_id){
        var result_id = 0;

        var locationSearchObj = search.create({
            type: "location",
            filters: [
               ["externalid","anyof",_ext_id], "AND",
               ["isinactive","is","F"]
            ],
            columns: [ ]
        });
        locationSearchObj.run().each(function(result){
            // .run().each has a limit of 4,000 results
            if(result_id == 0){
                result_id = result.id;
            }
            return true;
        });

        return result_id;
    }

    /**
     * This function takes the location id and gets the subsidiary internal id
     * @param {*} _po_location - integer - the NetSuite internal ID of the location record
     * @returns - integer - the NetSuite internal ID of the subsidiary for the location
     */
    function find_subsidiary(_po_location){
        var result = 0;

        // load location record
        var location_record = record.load({
            type: 'location',
            id: _po_location,
            isDynamic: true
        });
        result = location_record.getValue('subsidiary');

        return result;
    }

    /**
     * This function takes the supplier code (the 4 character abbreviation) and tries to find the NetSuite Vendor record.
     * @param {*} _str_suppliercode - string - the 4 character abbreviation used to identify the supplier
     * @returns - integer - the NetSuite internal ID of the Vendor record
     */
    function find_vendor(_str_suppliercode){
        var result_id = 0;

        var vendorSearchObj = search.create({
            type: "vendor",
            filters: [ 
                ["formulanumeric: LENGTH({entityid})","equalto","4"], "AND",
                ["entityid","haskeywords",_str_suppliercode]
            ],
            columns: [
                search.createColumn({
                    name: "entityid",
                    sort: search.Sort.ASC,
                    label: "Name"
                })
            ]
        });
        vendorSearchObj.run().each(function(result){
            // .run().each has a limit of 4,000 results
            if(result_id == 0){
                result_id = result.id;
            }
            return true;
        });

        return result_id;
    }

    /**
     * This function takes the json from the product item and the product item ID and tries to find the NetSuite Item record
     * @param {*} _json - string - this is a JSON parse-able string
     * @param {*} _rics_product_item_id - integer - this is the custom record internal ID for the product item
     * @returns - integer - the internal ID of the NetSuite Inventory Item record
     */
    function find_item(_json, _rics_product_item_id){
        var result_id = 0;

        var data = JSON.parse(_json);
        var str_upc = data.ProductItem.UPC;
        var str_sku = data.ProductItem.Sku;
        var pi_fields = search.lookupFields({
            type: 'customrecord_rics_product_item',
            id: _rics_product_item_id,
            columns: ['custrecord_rics_pi_upc']
        });
        var str_upc = (pi_fields.hasOwnProperty('custrecord_rics_pi_upc')) ? pi_fields.custrecord_rics_pi_upc : '';
        var str_upc = (str_upc != data.ProductItem.UPC) ?  str_upc : data.ProductItem.UPC;

        if(str_upc !== null && str_upc != '' && str_sku !== null && str_sku != ''){
            var itemSearchObj = search.create({
                type: "item",
                filters: [
                    ["upccode","is",str_upc], "AND",
                    ["vendorname","is",str_sku], "AND",
                    ["isinactive","is","F"]
                ],
                columns: [ ]
            });
            itemSearchObj.run().each(function(result){
                // .run().each has a limit of 4,000 results
                if(result_id == 0){
                    var vals = {};
                    if(data.hasOwnProperty('Cost')){ vals.cost = data.Cost; }
                    if(data.hasOwnProperty('RetailPrice')){ vals.custitem_gf_msrp = data.RetailPrice; }
                    vals.itemoptions = ['CUSTCOL1', 'CUSTCOL2'];
                    result_id = result.id;
                    try{
                    	var itemId = record.submitFields({
                        	type: result.recordType,
                        	id: result_id,
                        	values: vals
                    	});
                        return false;
                    }catch(ex){
                        log.error({title: 'GF_MR_RICS_Purchases.js:find_item-submitFields', details: 'vals: ' + JSON.stringify(vals) + ', Error: ' + ex.toString() + ' : ' + ex.stack});
                    }
                }
                return true;
            });
        }else{
            log.debug({title: 'GF_MR_RICS_Purchases.js:find_item', details: 'insufficient data - str_upc: ' + str_upc + ', str_sku:' + str_sku});
        }
        if(result_id == 0){
            log.debug({title: 'GF_MR_RICS_Purchases.js:find_item', details: 'insufficient data - _rics_product_item_id: ' + _rics_product_item_id + ', _json:' + JSON.stringify(_json)});
            log.debug({title: 'GF_MR_RICS_Purchases.js:find_item', details: 'insufficient data - str_upc: ' + str_upc + ', str_sku:' + str_sku});
        }

        return result_id;
    }

    /**
     * This function takes the line number, and other required line data required to set the values of a line on the PO record
     * @param {*} _line_num - integer - this is the number of the line on the PO
     * @param {*} _po_rec - integer - this is the NetSuite internal ID of the PO record
     * @param {*} _item_id - integer - this is the NetSuite internal ID of the Item record
     * @param {*} _item_qty - integer - this is how many of the item are on order
     * @param {*} _item_cost - currency - this is the cost of each item
     * @param {*} _ship_dt - date - this is the expected receipt date of the item
     */
    function set_item_data_new_po(_line_num, _po_rec, _item_id, _item_qty, _item_cost, _ship_dt){
        // also add the line item
        _po_rec.setSublistValue({
            sublistId: 'item',
            fieldId: 'item',
            line: _line_num,
            value: _item_id
        });
        if(_item_qty == 0){
            _po_rec.setSublistValue({
                sublistId: 'item',
                fieldId: 'isclosed',
                line: _line_num,
                value: true
            });
        }
        _po_rec.setSublistValue({
            sublistId: 'item',
            fieldId: 'quantity',
            line: _line_num,
            value: _item_qty
        });
        _po_rec.setSublistValue({
            sublistId: 'item',
            fieldId: 'rate',
            line: _line_num,
            value: _item_cost
        });
        _po_rec.setSublistValue({
            sublistId: 'item',
            fieldId: 'expectedreceiptdate',
            line: _line_num,
            value: _ship_dt
        });
    }

    /**
     * This function takes the line number, and other required line data required to update the values of a line on the PO record
     * @param {*} _po_rec - integer - this is the NetSuite internal ID of the PO record
     * @param {*} _item_id - integer - this is the NetSuite internal ID of the Item record
     * @param {*} _item_qty - integer - this is how many of the item are on order
     * @param {*} _item_cost - currency - this is the cost of each item
     */
    function update_item_data_existing_po(_po_rec, _item_id, _item_qty, _item_cost, _ship_dt){
        log.debug({title: 'GF_MR_RICS_Purchases.js:update_item_data_existing_po', details: '_po_rec id: ' + _po_rec.id + ', _item_id:' + _item_id + ', _item_qty:' + _item_qty + ', _item_cost:' + _item_cost});
        // also add the line item
        _po_rec.setCurrentSublistValue({
            sublistId: 'item',
            fieldId: 'item',
            value: _item_id
        });
        _po_rec.setCurrentSublistValue({
            sublistId: 'item',
            fieldId: 'rate',
            value: _item_cost
        });
        if(_item_qty == 0){
            _po_rec.setCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'isclosed',
                value: true
            });
            _po_rec.setCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'quantity',
                value: _item_qty
            });
        }else{
            _po_rec.setCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'isclosed',
                value: false
            });
            _po_rec.setCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'quantity',
                value: _item_qty
            });
        }
        _po_rec.setCurrentSublistValue({
            sublistId: 'item',
            fieldId: 'expectedreceiptdate',
            value: _ship_dt
        });
    }

    /**
     * This function takes a RICS Purchase Order custom record id, finds all RICS Purchase Order Detail custom records generated from the PO and blanks out their PO Line Unique ID and NetSuite Purchase Order fields
     * @param {*} _source_id - integer - this is the internal ID of the RICS Purchase Order custom record
     */
    function reset_pod_connection_fields(_source_id){
        var customrecord_rics_po_detailsSearchObj = search.create({
            type: "customrecord_rics_po_details",
            filters: [
                ["custrecord_rics_pod_parent_po","anyof",_source_id]
            ],
            columns: []
        });
        customrecord_rics_po_detailsSearchObj.run().each(function(result){
            // .run().each has a limit of 4,000 results
            var rics_pod_Id = record.submitFields({
                type: 'customrecord_rics_po_details',
                id: result.id,
                values: {
                    'custrecord_rics_pod_line_unique_id': '',
                    'custrecord_rics_pod_ns_po': ''
                }
            });
            return true;
        });
    }

});