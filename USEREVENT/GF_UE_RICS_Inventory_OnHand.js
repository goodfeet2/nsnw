/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 *
Script Name:   GF_UE_RICS_Inventory_OnHand.js
Author:        Mark Robinson
 */

 define(['N/record', 'N/search', 'N/format'], function (record, search, format) {

    /**
     * 
     * @param {*} scriptContext 
     */
    function afterSubmit(scriptContext){
        if(scriptContext.type != 'delete') {
            // get the values from the record to run the on hand comparison search
            var rioh_rec = scriptContext.newRecord; // rioh_rec.id for record internal id
            var item_id = rioh_rec.getValue({fieldId: 'custrecord_rics_ioh_ns_matrix_subitem'});
            if(item_id == null || item_id == ''){ return true; } // can't process if no item connection exists
            var loc_id = rioh_rec.getValue({fieldId: 'custrecord_rics_ioh_ns_location'});
            var curent_diff_qty = rioh_rec.getValue({fieldId: 'custrecord_rics_ioh_diff_qty'});

            // run the On Hand Comparison Search
            var inventoryitemSearchObj = search.create({
                type: "item",
                filters: [
                   ["inventorylocation","anyof",loc_id], "AND",
                   ["custrecord_rics_ioh_ns_matrix_subitem.custrecord_rics_ioh_ns_location","anyof",loc_id], "AND",
                   ["internalid","anyof",item_id]
                ],
                columns: [
                   search.createColumn({
                      name: "formulanumeric",
                      formula: "( NVL({custrecord_rics_ioh_ns_matrix_subitem.custrecord_rics_ioh_onhand}, 0) - NVL({locationquantityonhand}, 0) )"
                   }),
                   search.createColumn({
                      name: "formulacurrency",
                      formula: "{custrecord_rics_ioh_ns_matrix_subitem.custrecord_rics_ioh_cost} * ( ( NVL({custrecord_rics_ioh_ns_matrix_subitem.custrecord_rics_ioh_onhand}, 0) - NVL({locationquantityonhand}, 0) ) )"
                   })
                ]
            });
            var data = {};
            data.diff_flg = false;
            data.diff_qty = 0;
            data.diff_amt = 0.00;
            inventoryitemSearchObj.run().each(function(result){
                // .run().each has a limit of 4,000 results
                var num_diff = result.getValue({name: 'formulanumeric'});
                num_diff = (num_diff != '') ? parseInt(num_diff) : 0;
                var cur_diff = result.getValue({name: 'formulacurrency'});
                cur_diff = (cur_diff != '') ? cur_diff : 0.00;
                data.diff_qty = num_diff;
                data.diff_amt = cur_diff;
                data.diff_flg = true;
                return true;
            });

            // if the difference quantity is different than the current difference quantity
            if(data.diff_qty != curent_diff_qty){
                // - update the difference fields for this On Hand Inventory Record
                if(data.diff_qty == 0){
                    // -- when the difference quantity is zero - reset all difference fields to blank clearing the difference
                    var rioh_update_id = record.submitFields({
                        type: 'customrecord_rics_inventory_on_hand',
                        id: rioh_rec.id,
                        values: {
                            custrecord_rics_ioh_diff_detected: false,
                            custrecord_rics_ioh_dt_diff_detected: '',
                            custrecord_rics_ioh_diff_qty: 0,
                            custrecord_rics_ioh_diff_cost: 0.00
                        },
                        options: {
                            enableSourcing: false,
                            ignoreMandatoryFields : true
                        }
                    });
                    log.audit({title: 'GF_UE_RICS_Inventory_OnHand.js:afterSubmit', details: 'Diff Resolved RIOH ID:' + rioh_rec.id});
                }else{
                    // -- when the difference quantity is NOT zero
                    // --- mark the difference detected checkbox
                    // --- set the date difference detected to the current date and time
                    // --- set the difference quantity and cost values based on the search
                    var rioh_update_id = record.submitFields({
                        type: 'customrecord_rics_inventory_on_hand',
                        id: rioh_rec.id,
                        values: {
                            custrecord_rics_ioh_diff_detected: true,
                            custrecord_rics_ioh_dt_diff_detected: new Date(),
                            custrecord_rics_ioh_diff_qty: data.diff_qty,
                            custrecord_rics_ioh_diff_cost: data.diff_amt
                        },
                        options: {
                            enableSourcing: false,
                            ignoreMandatoryFields : true
                        }
                    });
                    log.audit({title: 'GF_UE_RICS_Inventory_OnHand.js:afterSubmit', details: 'Diff Detected RIOH ID:' + rioh_rec.id + ', diff data: ' + JSON.stringify(data)});
                }
            }
        }
    }

    return {
        afterSubmit: afterSubmit
    };

 });