/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 *
Script Name:   GF_UE_RICS_SalesTicket.js
Author:        Mark Robinson
 */

 define(['N/record'], function (record) {

    /**
     * This function runs after the ticket record has been submitted
     * It will use the receipt text set on the record to determine what the ticket total is
     * @param {*} scriptContext - object - this object contains context specific to the record that called the script as well as the script itself
     */
    function afterSubmit(scriptContext){
        // load the record to a variable
        var ticket_rec = scriptContext.newRecord;
        // get the receipt text
        var str_receipttext = ticket_rec.getValue({fieldId: 'custrecord_rics_st_receipttext'});

        if(str_receipttext != null && str_receipttext != ''){
            var total_index = str_receipttext.indexOf('Total: ');
            var str_total = str_receipttext.substring(total_index + 8, total_index + 18);
            str_total = str_total.trim(); // clear off leading and trailing values
            str_total = str_total.replace(/,/g, '');; // remove any commas in the string
            var total = parseFloat(str_total);
            if(total != null && total != '' && typeof total == 'number'){
                // use record.load to make edits
                var ticket_id = ticket_rec.id;
                var ticket_record = record.load({
                    type: 'customrecord_rics_sales_ticket',
                    id: ticket_id
                });
                ticket_record.setValue('custrecord_rics_st_receipt_total', total);
                ticket_record.save();
            }
        }
    }

    return {
        afterSubmit: afterSubmit
    };

});