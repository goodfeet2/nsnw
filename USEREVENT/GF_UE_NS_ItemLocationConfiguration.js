/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 *
Script Name:   GF_UE_NS_ItemLocationConfiguration.js
Author:        Mark Robinson
 */

 define(['N/record', 'N/search'], function (record, search) {

    /**
     * This function runs only after the item record has been submitted during the creation event
     * It will use the subsidiaries set on the record to determine which locations need a config record
     * @param {*} scriptContext - object - this object contains context specific to the record that called the script as well as the script itself
     */
    function afterSubmit(scriptContext){
        // if(scriptContext.type == 'create') {
            // load the record to a variable
            var item_rec = scriptContext.newRecord;
            // get the list of subsidiaries
            var arr_rec_subs = item_rec.getValue({fieldId: 'subsidiary'});
            var addSubs = false;
            if(arr_rec_subs.indexOf('2') == -1){ arr_rec_subs.push('2'); addSubs = true; }
            if(arr_rec_subs.indexOf('3') == -1){ arr_rec_subs.push('3'); addSubs = true; }
            if(arr_rec_subs.indexOf('4') == -1){ arr_rec_subs.push('4'); addSubs = true; }
            if(arr_rec_subs.indexOf('7') == -1){ arr_rec_subs.push('7'); addSubs = true; }
            if(addSubs){ item_rec.setValue({fieldId: 'subsidiary', value: arr_rec_subs }); }
            var arr_all_subs = getListOfSubs();

            // get all locations for those subs
            var arr_location_ids = [];
            var locationSearchObj = search.create({
                type: "location",
                filters: [
                    ["subsidiary","anyof",arr_rec_subs], "AND",
                    ["isinactive","is","F"], "AND",
                    ["locationtype","anyof","1"]
                ],
                columns: [
                    'name',
                    'subsidiary'
                ]
            });
            locationSearchObj.run().each(function(result){
                // .run().each has a limit of 4,000 results
                var str_name = result.getValue('name');
                var sub_id = arr_all_subs[result.getValue('subsidiary')];
                arr_location_ids.push({id: result.id, name: str_name, sub: sub_id});
                return true;
            });

            createItemLocations(item_rec.id, arr_location_ids);
        // }
    }

    return {
        afterSubmit: afterSubmit
    };

    // =========================== extra functions
    /**
     * This function returns an array of all NetSuite subsidiaries where the name is the key and the internal id is the value
     * @returns - array - all subsidiary ids with the names as the key
     */
    function getListOfSubs(){
        var arr_result = [];
        var subsidiarySearchObj = search.create({
            type: "subsidiary",
            filters: [ ],
            columns: [
                search.createColumn({
                    name: "namenohierarchy",
                    sort: search.Sort.ASC
                })
            ]
        });
        subsidiarySearchObj.run().each(function(result){
            // .run().each has a limit of 4,000 results
            var str_sub_name = result.getValue('namenohierarchy');
            arr_result[str_sub_name] = result.id;
            return true;
        });
        return arr_result;
    }

    /**
     * This function uses the passed in data to generate itemlocationconfiguration records for a NetSuite Inventory Item
     * @param {*} _item_id - integer - the internal id of the item the locations will be added to
     * @param {*} _arr_location_ids - array - an array of objects generated based on the data in the subsidiary field of the NetSuite Inventory Item
     */
    function createItemLocations(_item_id, _arr_location_ids){
        for(var loc in _arr_location_ids){
            var loc_data = _arr_location_ids[loc];
          	if(loc_data.id == 315 || loc_data.id == 316){ continue; }
            var loc_config = record.create({
                type: 'itemlocationconfiguration'
            });
            loc_config.setValue('name', _item_id + ' - ' +loc_data.sub + ' - ' +loc_data.id);
            loc_config.setValue('item', _item_id);
            loc_config.setValue('subsidiary', loc_data.sub);
            loc_config.setValue('location', loc_data.id);
            try{
                var lc_id = loc_config.save();
            }catch(e){
                // could not create the location
                log.error({title: 'GF_UE_NS_ItemLocationConfiguration.js:createItemLocations', details: 'Failed to create location - _item_id:' + _item_id + ', loc_data.sub: ' + loc_data.sub + ', loc_data.id: ' + loc_data.id});
            }
        }
    }

 });