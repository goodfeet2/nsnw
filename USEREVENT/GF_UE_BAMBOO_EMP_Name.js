/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 *
Script Name:   GF_UE_BAMBOO_EMP_Name.js
Author:        Mark Robinson
 */

define(['N/record', 'N/search', 'N/runtime'], function (record, search, runtime) {

    /**
     * This function will check the current user prior to loading the record and set the entity ID if the user is the bamboo integration
     * @param {*} scriptContext - object - this object contains context specific to the record that called the script as well as the script itself
     */
  	function beforeLoad_FieldsDisable(scriptContext){
        var userObj = runtime.getCurrentUser();
        if(userObj.role == 1086){ // BambooHR Custom OneWorld Web Services
            var emp_rec = scriptContext.newRecord;
            var str_novatime = emp_rec.getValue({fieldId: 'custentity_ntemployeeid'});
            emp_rec.setValue({fieldId: 'entityid', value: str_novatime});
        }
    }

    /**
     * This function will adjust the entity ID if the record has been altered in such a way that the entity ID field does not contain the employee's name
     * @param {*} scriptContext - object - this object contains context specific to the record that called the script as well as the script itself
     */
    function beforeSubmit_NameValidation(scriptContext){
        // load the record to a variable
        var emp_rec = scriptContext.newRecord;
        // grab and test the entityid - bamboo has been setting these to the emp number and it should be the name
        var str_entity = emp_rec.getValue({fieldId: 'entityid'});
        if(hasNumber(str_entity)){
            // when number is found set entity id to name prior to save
            log.audit({title: 'GF_UE_BAMBOO_EMP_Name.js:saveRecord_NameValidation-found', details: 'found number in entityid: ' + str_entity});
            var str_firstname = emp_rec.getValue({fieldId: 'firstname'});
            var str_lastname = emp_rec.getValue({fieldId: 'lastname'});

            var str_new_entity = str_lastname + ', ' + str_firstname;
            // remove any numbers that have been placed in the first or last name fields
            str_new_entity = str_new_entity.replace(new RegExp(/\d/, "g"), "").trim();

            emp_rec.setValue({fieldId: 'entityid', value: str_new_entity});
            log.audit({title: 'GF_UE_BAMBOO_EMP_Name.js:saveRecord_NameValidation-set to', details: 'str_new_entity: ' + str_new_entity});
        }
        return true;
    }

    return {
        beforeLoad: beforeLoad_FieldsDisable,
        beforeSubmit: beforeSubmit_NameValidation
    };

    // =========================== extra functions
    /**
     * This function is used to determine if a string contains a number
     * @param {*} _str - string - any string value.
     * @returns - bool - true if a number was found, false otherwise
     */
    function hasNumber(_str) {
        return /\d/.test(_str);
    }

});