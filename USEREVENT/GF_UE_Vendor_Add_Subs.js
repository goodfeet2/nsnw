/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 *
Script Name:   GF_UE_Vendor_Add_Subs.js
Author:        Mark Robinson
 */

define(['N/record', 'N/search'], function (record, search) {

    /**
     * This function runs just before the VendorBill record is saved
     * It will check the quantity of all of the lines and set the GF Quantity Total field
     * @param {*} scriptContext - object - this object contains context specific to the record that called the script as well as the script itself
     */
    function afterSubmit(scriptContext){
        // load the record to a variable
        var vendor_rec = scriptContext.newRecord;
        var currentLineCount = vendor_rec.getLineCount({ 'sublistId': '' });



        var currentLineCount = vendor_bill_rec.getLineCount({ 'sublistId': 'item' });
        var quantity_count = 0;
        for(var line_num = 0; line_num < currentLineCount; line_num++){
            var line_qty = vendor_bill_rec.getSublistValue({ sublistId: 'item', fieldId: 'quantity', line: line_num });
            quantity_count += parseInt(line_qty);
        }
        vendor_bill_rec.setValue({fieldId: 'custbody_gf_quantity_total', value: quantity_count});
        return true;
    }

    return {
        afterSubmit: afterSubmit
    };

});