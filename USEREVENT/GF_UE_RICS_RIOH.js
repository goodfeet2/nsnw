/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 *
Script Name:   GF_UE_RICS_RIOH.js
Author:        Mark Robinson
 */

define(['N/record', 'N/search'], function (record, search) {

    /**
     * This function runs after the ticket record has been submitted
     * It will use the receipt text set on the record to determine what the ticket total is
     * @param {*} scriptContext - object - this object contains context specific to the record that called the script as well as the script itself
     */
    function afterSubmit(scriptContext){
        if(scriptContext.type != 'delete') {
            // load the record to a variable
            var rioh_rec = scriptContext.newRecord;
            // get the connected item
            var item_id = rioh_rec.getValue({fieldId: 'custrecord_rics_ioh_ns_matrix_subitem'});
            var pd_id = rioh_rec.getValue({fieldId: 'custrecord_rics_ioh_parent_productdetail'});
            if(pd_id && pd_id != ''){
                // get the costing method of the currently connected item
                var lookup_item_fields = search.lookupFields({
                    type: 'customrecord_rics_product_details',
                    id: 84501,
                    columns: ['custrecord_rics_pd_ns_item_id']
                });
                // console.log(lookup_item_fields.custrecord_rics_pd_ns_item_id[0].value);
                var pd_item_id = (lookup_item_fields && lookup_item_fields.custrecord_rics_pd_ns_item_id != undefined && lookup_item_fields.custrecord_rics_pd_ns_item_id[0].value != undefined) ? lookup_item_fields.custrecord_rics_pd_ns_item_id[0].value : '';
                if(pd_item_id != '' && pd_item_id != item_id){
                    // change connected item
                    var rioh_record = record.load({
                        type: 'customrecord_rics_inventory_on_hand',
                        id: rioh_rec.id
                    });
                    rioh_record.setValue('custrecord_rics_ioh_ns_matrix_subitem', pd_item_id);
                    rioh_record.save();
                }
            }
        }
    }

    return {
        afterSubmit: afterSubmit
    };

});