/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 *
Script Name:   GF_UE_GF_LocationExtIdUpdate.js
Author:        Mark Robinson
 */

 define(['N/record'], function (record) {

    /**
     * This function runs after the location record has been submitted
     * It will use the novatime location id to set the external id of the location record
     * @param {*} scriptContext - object - this object contains context specific to the record that called the script as well as the script itself
     */
    function afterSubmit(scriptContext){
        // load the record to a variable
        var location_rec = scriptContext.newRecord;
        // get the receipt text
        var str_externalId = location_rec.getValue({fieldId: 'externalid'});
        var str_ntlocationid = location_rec.getValue({fieldId: 'custrecord_ntlocationid'});

        if(str_ntlocationid != null && str_ntlocationid != '' && str_ntlocationid != str_externalId){
            var location_record = record.load({
                type: 'location',
                id: location_rec.id
            });
            location_record.setValue('externalid', str_ntlocationid);
            location_record.save();
        }
    }

    return {
        afterSubmit: afterSubmit
    };

});