/**
 * @NApiVersion 2.1
 * @NScriptType UserEventScript
 *
Script Name:   GF_UE_RICS_PO_Details.js
Author:        Mark Robinson
 */

define(['N/record', 'N/search'], function (record, search) {

    /**
     * This function runs after the PO Details record has been submitted
     * It will use the parent record to determine if a NS PO native record has been created - if it has this script will update the PO Details record with the PO and the unique line id
     * @param {*} scriptContext - object - this object contains context specific to the record that called the script as well as the script itself
     */
    function afterSubmit(scriptContext){
      if(scriptContext.type != 'delete') {
        // load the record to a variable
        var pod_rec = scriptContext.newRecord;
        // get the parent internal id
        var parent_rec_id = pod_rec.getValue({fieldId: 'custrecord_rics_pod_parent_po'});
        var ns_po_id = 0;
        if(parent_rec_id != undefined && parent_rec_id > 0){
            log.debug({title: 'GF_UE_RICS_PO_Details.js:afterSubmit-parent ' + pod_rec.id, details: 'parent_rec_id: ' + parent_rec_id + ', type: ' + scriptContext.type});

            var fieldLookUp = search.lookupFields({
                type: 'customrecord_rics_purchase_order',
                id: parent_rec_id,
                columns: ['custrecord_rics_po_purchaseorder']
            });
            log.debug({title: 'GF_UE_RICS_PO_Details.js:afterSubmit-lookup ' + pod_rec.id, details: 'fieldLookUp: ' + JSON.stringify(fieldLookUp)});
            if(fieldLookUp != undefined && fieldLookUp.custrecord_rics_po_purchaseorder != undefined && fieldLookUp.custrecord_rics_po_purchaseorder[0] != undefined ){
                // set the po id
                ns_po_id = fieldLookUp.custrecord_rics_po_purchaseorder[0].value;
            }

            if(ns_po_id > 0){
                var pod_record = record.load({
                    type: 'customrecord_rics_po_details',
                    id: pod_rec.id
                });
                // get the pod json field
                var str_json = pod_rec.getValue({fieldId: 'custrecord_rics_pod_json'});
                var json = JSON.parse(str_json);
                // get the item name
                var str_item_name = json.ProductItem.Sku;
                str_item_name += (json.ProductItem.Row != undefined) ? '-' + json.ProductItem.Row : '';
                str_item_name += (json.ProductItem.Column != undefined) ? '-' + json.ProductItem.Column : '';
                if(json.ProductItem.UPC != undefined){
	                var str_item_upc = json.ProductItem.UPC;
                	// match by item name
                	var purchaseorderSearchObj = search.create({
                	    type: "purchaseorder",
                	    filters: [
                	        ["type","anyof","PurchOrd"], "AND",
                	        ["internalid","anyof",ns_po_id], "AND",
                	        ["mainline","is","F"], "AND",
                	        ["item.name","is",str_item_name], "AND",
                	        ["item.upccode","is",str_item_upc]
                	    ],
                	    columns: [
                	        "lineuniquekey",
                	        "item",
                	        search.createColumn({
                	            name: "displayname",
                	            join: "item"
                	        }),
                	        search.createColumn({
                	            name: "vendorname",
                	            join: "item"
                	        }),
                	        search.createColumn({
                	            name: "itemid",
                	            join: "item"
                	        })
                	    ]
                	});
                	purchaseorderSearchObj.run().each(function(result){
                	    // .run().each has a limit of 4,000 results
                	    var line_unique_key = result.getValue("lineuniquekey");
                	    log.debug({title: 'GF_UE_RICS_PO_Details.js:afterSubmit-posearch ' + pod_rec.id, details: 'line_unique_key: ' + line_unique_key});
                    	pod_record.setValue('custrecord_rics_pod_line_unique_id', line_unique_key);
                    	return true;
                	});
                	log.debug({title: 'GF_UE_RICS_PO_Details.js:afterSubmit-nspo ' + pod_rec.id, details: 'ns_po_id: ' + ns_po_id});
                	pod_record.setValue('custrecord_rics_pod_ns_po', ns_po_id);
                	pod_record.save();
                }
            }
        }
      }
    }

    return {
        afterSubmit: afterSubmit
    };

});