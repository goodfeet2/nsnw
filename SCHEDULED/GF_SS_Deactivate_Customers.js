/**
*@NApiVersion 2.0
*@NScriptType ScheduledScript
*
Script Name:   GF_SS_Deactivate_Customers.js
Author:        Mark Robinson
*/
 define(['N/runtime', 'N/record', 'N/search', 'N/task'], function (runtime, record, search, task) {
    
    function deactivateCustomers(){

    	try{
            log.debug({title: 'GF_SS_Deactivate_Customers.js:deactivateCustomers', details: 'start'} );
            
            //run search
            var runsearch_customer = search.load({
                type: 'customer',
                id: 'customsearch1160' // MARK Customer with NO Transactions
            }).run();
            var arr_allResults = [];
            for (var k = 0; k < 50; k++) { // max pages 50, up to 50,000 records total
                var results = runsearch_customer.getRange({start: k * 1000, end: (k * 1000) + 1000});
                if (results.length === 0){ break; } // if the results of the page are 0 then exit
                for (var i = 0; i < results.length; i++) {
                    arr_allResults.push(results[i].id);
                }
            }
            log.debug({title: 'DEBUG: GF_SS_Deactivate_Customers.js:deactivateCustomers', details: 'customsearch1160 Total: ' + (results != null ? arr_allResults.length : '0')} );
            for (var res = 0; arr_allResults.length > 0 && res < arr_allResults.length; res++) {
                try{
                    var id = record.submitFields({
                        type: 'customer',
                        id: arr_allResults[res],
                        values: {
                            isinactive: true
                        },
                        options: {
                            enableSourcing: false,
                            ignoreMandatoryFields : true
                        }
                    });
                    requeueScript();
                }catch(ex){
                    log.error({title: 'ERROR: GF_SS_Deactivate_Customers.js:deactivateCustomers-loop', details: 'ID: ' + arr_allResults[res] + ', Error: ' + ex.toString() + ' : ' + ex.stack} );
                }
            }

            log.debug({title: 'GF_SS_Deactivate_Customers.js:deactivateCustomers', details: 'complete'} );

 	    } catch (ex) {
            log.error({title: 'ERROR: GF_SS_Deactivate_Customers.js:deactivateCustomers-main', details: 'Error: ' + ex.toString() + ' : ' + ex.stack} );
        }
	}

    return {
        execute: function (context){
            deactivateCustomers()
        }
    }

    // =========================== extra functions
    function errText(_e){
        // usage : nlapiLogExecution('ERROR', 'function', 'Error: ' + ex.toString() + ' : ' + ex.stack);
        var txt = '';
        if(_e instanceof nlobjError){
             //this is netsuite specific error
             txt = 'NLAPI ' + _e.getCode() + ' :: ' + _e.getDetails() + ' :: ' + _e.getStackTrace().join(', ');
        }else{
             //this is generic javascript error
             txt = 'JavaScript/Other ' + _e.toString();
             txt += (typeof _e.stack != 'undefined') ? ' : ' + _e.stack : '';
        }
        return txt;
    }

    function requeueScript(){
        var scriptObj = runtime.getCurrentScript();
        if(scriptObj.getRemainingUsage() < 100){
            var ssTask = task.create({
                taskType: task.TaskType.SCHEDULED_SCRIPT,
                scriptId: 1127,
                deploymentId: 'customdeploy_ss_deactivate_customers_od'
            });
            ssTask.submit();
        }
    }

});
