/**
*@NApiVersion 2.1
*@NScriptType ScheduledScript
*
Script Name:   GF_SS_Get_RICS_Purchase_Orders.js
Author:        Mark Robinson
*/
define(['N/runtime', 'N/search', 'N/record', 'N/task'], function (runtime, search, record, task) {

	/**
	 * This function uses a search to load the RICS Purchase Order custom records that have not been cancelled or closed and it passes them through the
	 * purchase_orders and purchases scripts to update the custom record, and update the NetSuite Transaction record
	 */
	function get_purchase_orders() {

		try {
			log.audit({ title: 'GF_SS_Get_RICS_Purchase_Orders.js:get_purchase_orders', details: 'start' });

			var po_count = 0;
			removeCompletedDeployments();
			var scriptObj = runtime.getCurrentScript();
			if (scriptObj.getRemainingUsage() < 100) { return true; }
			//run search
			let customrecord_rics_po_detailsSearchObj = search.create({
				type: "customrecord_rics_po_details",
				filters: [
					["custrecord_rics_pod_json", "doesnotcontain", "DeletedOn"], "AND",
					["formulanumeric: CASE WHEN NVL({custrecord_rics_pod_receivedquantity}, 0) < NVL({custrecord_rics_pod_orderquantity}, 0) THEN 1 ELSE 0 END", "greaterthan", "0"]
				],
				columns: [
					search.createColumn({
						name: "formulanumeric",
						summary: "GROUP",
						formula: "NVL2({custrecord_rics_pod_latest_update}, 1, 0)",
						sort: search.Sort.ASC
					}),
					search.createColumn({
						name: "custrecord_rics_pod_latest_update",
						summary: "MIN",
						sort: search.Sort.ASC
					}),
					search.createColumn({
						name: "custrecord_rics_po_purchaseordernumber",
						join: "CUSTRECORD_RICS_POD_PARENT_PO",
						summary: "GROUP"
					}),
					search.createColumn({
						name: "formulatext",
						summary: "MIN",
						formula: "NS_CONCAT({internalid})"
					})
				]
			});
			customrecord_rics_po_detailsSearchObj.run().each(function (result) {
				// .run().each has a limit of 4,000 results
				if (po_count > 100 || scriptObj.getRemainingUsage() < 100) { return false; }
				let rics_po_num = result.getValue({ name: "custrecord_rics_po_purchaseordernumber", join: "CUSTRECORD_RICS_POD_PARENT_PO", summary: "GROUP" });
				buildNewScriptDeploymentPO(rics_po_num);
				try {
					let arr_ids = (result.getValue({ name: "formulatext", summary: "MIN", formula: "NS_CONCAT({internalid})" })).split(',');
					for (let i in arr_ids) {
						let pod_id = arr_ids[i];
						let rics_po_details_id = record.submitFields({
							type: 'customrecord_rics_po_details',
							id: pod_id,
							values: { 'custrecord_rics_pod_latest_update': new Date() }
						});
					}
				} catch (ex) {
					log.error({ title: 'ERROR: GF_SS_Get_RICS_Purchase_Orders.js:get_purchase_orders-updatePOD', details: 'Error: ' + ex.toString() + ' : ' + ex.stack });
					return false;
				}
				po_count += 1;
				log.debug({ title: 'GF_SS_Get_RICS_Purchase_Orders.js:get_purchase_orders', details: `In Search 1 - RemainingUsage: ${scriptObj.getRemainingUsage()}, po_count: ${po_count}` });
				return true;
			});
			log.audit({ title: 'GF_SS_Get_RICS_Purchase_Orders.js:get_purchase_orders', details: `After Search 1 - RemainingUsage: ${scriptObj.getRemainingUsage()}` });

			let purchaseorderSearchObj = search.create({
				type: "purchaseorder",
				filters: [
					["type", "anyof", "PurchOrd"], "AND",
					["mainline", "is", "T"], "AND",
					["formulanumeric: CASE WHEN TO_CHAR({custrecord_rics_po_purchaseorder.custrecord_rics_po_shiptostorecode}) = {location.externalid} THEN 0 ELSE 1 END", "greaterthan", "0"], "AND",
					["custbody_gf_rics_purchase_order_id", "noneof", "@NONE@"], "AND",
					["status", "noneof", "PurchOrd:H", "PurchOrd:G"], "AND",
                    ["trandate", "onorbefore", "daysfromnow60"]
				],
				columns: [
					search.createColumn({
						name: "trandate",
						sort: search.Sort.ASC
					}),
					"tranid",
					"custbody_gf_internal_po",
					search.createColumn({
						name: "internalid",
						join: "location"
					}),
					"locationnohierarchy",
					"custbody_gf_rics_purchase_order_id",
					search.createColumn({
						name: "custrecord_rics_po_shiptostorecode",
						join: "CUSTRECORD_RICS_PO_PURCHASEORDER"
					}),
					search.createColumn({
						name: "custrecord_rics_po_shiptostorename",
						join: "CUSTRECORD_RICS_PO_PURCHASEORDER"
					}),
					search.createColumn({
						name: "lastmodified",
						join: "CUSTRECORD_RICS_PO_PURCHASEORDER"
					}),
					search.createColumn({
						name: "custrecord_rics_po_ns_purch_order_update",
						join: "CUSTRECORD_RICS_PO_PURCHASEORDER"
					}),
					"statusref"
				]
			});
			purchaseorderSearchObj.run().each(function (result) {
				// .run().each has a limit of 4,000 results
				if (po_count > 200 || scriptObj.getRemainingUsage() < 100) { return false; }
				let rics_po_num = result.getValue('custbody_gf_rics_purchase_order_id');
				buildNewScriptDeploymentPurchases(rics_po_num);
				po_count += 1;
				return true;
			});
			log.audit({ title: 'GF_SS_Get_RICS_Purchase_Orders.js:get_purchase_orders', details: `After Search 2 - RemainingUsage: ${scriptObj.getRemainingUsage()}` });

			log.audit({ title: 'GF_SS_Get_RICS_Purchase_Orders.js:get_purchase_orders', details: 'complete' });
			return true;

		} catch (ex) {
			log.error({ title: 'ERROR: GF_SS_Get_RICS_Purchase_Orders.js:get_purchase_orders', details: 'Error: ' + ex.toString() + ' : ' + ex.stack });
		}
	}

	return {
		execute: function (context) {
			get_purchase_orders()
		}
	}


	function removeCompletedDeployments() {
		// main two deployments: customdeploy_mr_rics_purchases, customdeploy_mr_rics_purchases_od
		let arr_deployment_ids = [];
		let scheduledscriptinstanceSearch = search.create({
			type: "scheduledscriptinstance",
			filters: [
				["status", "anyof", "COMPLETE", "CANCELED"], "AND",
				["script.internalid", "anyof", "1598", "1599"], "AND",
				["scriptdeployment.internalid", "noneof", "61894", "61895", "61896", "61897", "1510176", "1509483", "1509483", "733225", "477903"] // , "AND",
				// ["enddate","onorbefore","hoursago1"]
			],
			columns: [
				search.createColumn({
					name: "name",
					join: "script"
				}),
				search.createColumn({
					name: "scriptid",
					join: "scriptDeployment"
				}),
				search.createColumn({
					name: "internalid",
					join: "scriptDeployment"
				}),
				search.createColumn({
					name: "timestampcreated",
					sort: search.Sort.DESC
				}),
				"mapreducestage",
				"status",
				"startdate",
				"enddate"
			]
		}).run();
		for (let k = 0; k < 2; k++) { // max pages 2, up to 2,000 records total
			let results = scheduledscriptinstanceSearch.getRange({ start: k * 1000, end: (k * 1000) + 1000 });
			for (let i = 0; i < results.length; i++) {
				let this_deployment_id = results[i].getValue({ name: 'internalid', join: 'scriptDeployment' });
				if (arr_deployment_ids.indexOf(this_deployment_id) == -1) {
					arr_deployment_ids.push(this_deployment_id);
				}
			}
		}

		// remove the deployments
		for (let dep_id in arr_deployment_ids) {
			let deployment_id = arr_deployment_ids[dep_id];
			try {
				record.delete({ type: 'scriptdeployment', id: deployment_id });
			} catch (ex) {
				log.audit({ title: 'Error: GF_SS_Get_RICS_Purchase_Orders.js:removeCompletedDeployments', details: 'deployment_id: ' + deployment_id + ', Error ' + ex.toString() + ' : ' + ex.stack });
			}
		}
	}

	function buildNewScriptDeploymentPO(_param) {
		try {
			// find any existing deployments
			let scriptdeploymentSearchObj = search.create({
				type: "scriptdeployment",
				filters: [
					["script", "anyof", "1598"]
				],
				columns: []
			});
			// count the results with this name - the only reason it should be anything other than zero is if the script has errored
			let searchResultCount = scriptdeploymentSearchObj.runPaged().count;
			// set a unique deployment name allow for multiples
			let str_random = (Math.floor(Math.random() * 1000000000)).toString();
			let str_deployName = (searchResultCount < 1) ? 'GF_MR_RICS_Purchase_Orders_0' : 'GF_MR_RICS_Purchase_Orders_' + str_random;
			// create a new on demand script deployment for this location and run the deployment
			let deployment_rec = record.copy({
				type: record.Type.SCRIPT_DEPLOYMENT,
				id: 61894
			});
			deployment_rec.setValue({ fieldId: 'isdeployed', value: true });
			deployment_rec.setValue({ fieldId: 'title', value: str_deployName });
			deployment_rec.setValue({ fieldId: 'scriptid', value: '_mr_rics_po_od_' + str_random });
			deployment_rec.setValue({ fieldId: 'startdate', value: new Date() });
			let deploy_id = deployment_rec.save();

			if (deploy_id > 0) {
				// get the script id of the saved deployment
				let script_deployment_fields = search.lookupFields({
					type: 'scriptdeployment',
					id: parseInt(deploy_id),
					columns: ['scriptid']
				});
				let deploy_script_id = script_deployment_fields.scriptid;
				log.audit({ title: 'GF_SS_Get_RICS_Purchase_Orders.js:buildNewScriptDeploymentPO', details: 'deploy_script_id: ' + deploy_script_id + ', _param: ' + _param });
				// run the deployment
				let call_po_on_demand_js = task.create({
					taskType: task.TaskType.MAP_REDUCE,
					scriptId: 'customscript_mr_rics_purchase_orders',
					deploymentId: deploy_script_id,
					params: { custscript_rics_po_number: _param }
				});
				call_po_on_demand_js.submit();
			}
		} catch (ex) {
			log.error({ title: 'Error: GF_SS_Get_RICS_Purchase_Orders.js:buildNewScriptDeploymentPO', details: 'Error ' + ex.toString() + ' : ' + ex.stack });
		}
	}

	function buildNewScriptDeploymentPurchases(_param) {
		try {
			// find any existing deployments
			let scriptdeploymentSearchObj = search.create({
				type: "scriptdeployment",
				filters: [
					["script", "anyof", "1599"]
				],
				columns: []
			});
			// count the results with this name - the only reason it should be anything other than zero is if the script has errored
			let searchResultCount = scriptdeploymentSearchObj.runPaged().count;
			// set a unique deployment name allow for multiples
			let str_random = (Math.floor(Math.random() * 1000000)).toString();
			let str_deployName = (searchResultCount < 1) ? 'GF_MR_RICS_Purchases_0' : 'GF_MR_RICS_Purchases_' + str_random;
			// create a new on demand script deployment for this location and run the deployment
			let deployment_rec = record.copy({
				type: record.Type.SCRIPT_DEPLOYMENT,
				id: 61897
			});
			deployment_rec.setValue({ fieldId: 'isdeployed', value: true });
			deployment_rec.setValue({ fieldId: 'title', value: str_deployName });
			deployment_rec.setValue({ fieldId: 'scriptid', value: '_mr_rics_purch_od_' + str_random });
			deployment_rec.setValue({ fieldId: 'startdate', value: new Date() });
			let deploy_id = deployment_rec.save();

			if (deploy_id > 0) {
				// get the script id of the saved deployment
				let script_deployment_fields = search.lookupFields({
					type: 'scriptdeployment',
					id: parseInt(deploy_id),
					columns: ['scriptid']
				});
				let deploy_script_id = script_deployment_fields.scriptid;
				log.audit({ title: 'GF_SS_Get_RICS_Purchase_Orders.js:buildNewScriptDeploymentPurchases', details: 'deploy_script_id: ' + deploy_script_id + ', _param: ' + _param });
				// run the deployment
				let call_po_on_demand_js = task.create({
					taskType: task.TaskType.MAP_REDUCE,
					scriptId: 'customscript_mr_rics_purchases',
					deploymentId: deploy_script_id,
					params: { custscript_rics_po_number: _param }
				});
				call_po_on_demand_js.submit();
			}
		} catch (ex) {
			log.error({ title: 'Error: GF_SS_Get_RICS_Purchase_Orders.js:buildNewScriptDeploymentPurchases', details: 'Error ' + ex.toString() + ' : ' + ex.stack });
		}
	}

});