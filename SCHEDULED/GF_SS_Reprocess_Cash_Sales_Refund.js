/**
*@NApiVersion 2.1
*@NScriptType ScheduledScript
*
Script Name:   GF_SS_Reprocess_Cash_Sales_Refund.js
Author:        Mark Robinson
*/
define(['N/runtime', 'N/search', 'N/record', 'N/task'], function (runtime, search, record, task) {

    /**
     * This function uses a search to load the RICS Purchase Order custom records that have not been cancelled or closed and it passes them through the
     * purchase_orders and purchases scripts to update the custom record, and update the NetSuite Transaction record
     */
    function reprocess_cash_sales_refund(){

    	try{
            log.audit({title: 'GF_SS_Reprocess_Cash_Sales_Refund.js:reprocess_cash_sales_refund', details: 'start'} );

			var ticket_count = 0;
            removeCompletedDeployments();
			var transactionSearchObj = search.create({
				type: "transaction",
				filters: [
					["lastmodifieddate","onorbefore","minutesago5"], "AND",
					["custbody_gf_rics_sales_ticket_id","noneof","@NONE@"], "AND",
					["custbody_gf_rics_sales_ticket_id.custrecord_rics_st_saledatetime","onorafter","startofthismonth"], "AND",
					["mainline","is","T"], "AND",
					["custbody_gf_rics_sales_ticket_id.custrecord_rics_st_receipt_total","isnotempty",""], "AND",
					["type","anyof","CashSale","CashRfnd"], "AND",
					[
						["custbody_gf_rics_exchange_record_id.mainline","is","T"],"OR",
						["custbody_gf_rics_exchange_record_id","anyof","@NONE@"]
					], "AND",
					["formulanumeric: CASE WHEN ((NVL2({custbody_gf_rics_exchange_record_id}, ({amount} + {custbody_gf_rics_exchange_record_id.amount}), {amount}) - {custbody_gf_rics_sales_ticket_id.custrecord_rics_st_receipt_total})) > 0 THEN 1 ELSE 0 END","greaterthan","0"], "AND",
					["formulanumeric: CASE WHEN ({type} = 'Cash Sale' AND {status} = 'Not Deposited') OR ({type} != 'Cash Sale' AND {custbody_gf_rics_exchange_record_id.status} = 'Not Deposited') THEN 1 ELSE 0 END","greaterthan","0"], "AND",
					["trandate","onorafter","startofthismonth"]
				],
				columns: [
					search.createColumn({ name: "custbody_gf_rics_sales_ticket_id" })
				]
			});
			transactionSearchObj.run().each(function(result){
				// .run().each has a limit of 4,000 results
				if(ticket_count > 100){ return false; }
				// var rics_ticket_id = result.getValue({ name: "internalid" });
                var rics_ticket_id = result.getValue({name: "custbody_gf_rics_sales_ticket_id" });
                buildNewScriptDeployment(rics_ticket_id);
				ticket_count += 1;
				return true;
			});

			if(ticket_count < 100){
				//run search
				var customrecord_rics_sales_ticketSearchObj = search.create({
					type: "customrecord_rics_sales_ticket",
					filters: [
					["custrecord_rics_st_saledatetime","onorafter","startofthismonth"], "AND",
					["custrecord_rics_st_saledatetime","onorbefore","daysago2"], "AND",
					["custrecord_rics_st_ticketvoided","is","F"], "AND",
					["custbody_gf_rics_sales_ticket_id.internalid","anyof","@NONE@"]
					],
					columns: [
					"internalid"
					]
				});
				customrecord_rics_sales_ticketSearchObj.run().each(function(result){
					// .run().each has a limit of 4,000 results
					if(ticket_count > 100){ return false; }
					// var rics_ticket_id = result.getValue({ name: "internalid" });
					var rics_ticket_id = result.getValue({name: "custbody_gf_rics_sales_ticket_id" });
					buildNewScriptDeployment(rics_ticket_id);
					ticket_count += 1;
					return true;
				});
			}

            log.audit({title: 'GF_SS_Reprocess_Cash_Sales_Refund.js:reprocess_cash_sales_refund', details: 'complete'});

            return true;

 	    } catch (ex) {
            log.error({title: 'ERROR: GF_SS_Reprocess_Cash_Sales_Refund.js:reprocess_cash_sales_refund', details: 'Error: ' + ex.toString() + ' : ' + ex.stack} );
        }
	}

    return {
        execute: function (context){
            reprocess_cash_sales_refund()
        }
    }

	function removeCompletedDeployments(){
		// main deployments: customdeploy_gf_mr_rics_cash_sale_refund, customdeploy_mr_rics_cash_sale_refund_od, customdeploy_mr_rics_cash_sale_refundod2, customdeploy_mr_rics_cash_sale_refundod3
		var arr_deployment_ids = [];
		var scheduledscriptinstanceSearch = search.create({
			type: "scheduledscriptinstance",
			filters: [
				["status","anyof","COMPLETE","CANCELED"], "AND",
				["script.internalid","anyof","1604"], "AND",
				["scriptdeployment.internalid","noneof","61907","61908","61909","61910"]
			],
			columns: [
				search.createColumn({
					name: "name",
					join: "script"
				}),
				search.createColumn({
					name: "scriptid",
					join: "scriptDeployment"
				}),
				search.createColumn({
					name: "internalid",
					join: "scriptDeployment"
				}),
				search.createColumn({
					name: "timestampcreated",
					sort: search.Sort.DESC
				}),
				"mapreducestage",
				"status",
				"startdate",
				"enddate"
			]
		}).run();
		for (var k = 0; k < 2; k++) { // max pages 2, up to 2,000 records total
			var results = scheduledscriptinstanceSearch.getRange({start: k * 1000, end: (k * 1000) + 1000});
			for (var i = 0; i < results.length; i++) {
				var this_deployment_id = results[i].getValue({name: 'internalid', join: 'scriptDeployment'});
				if(arr_deployment_ids.indexOf(this_deployment_id) == -1){
					arr_deployment_ids.push(this_deployment_id);
				}
			}
		}

		// remove the deployments
		for(var dep_id in arr_deployment_ids){
			var deployment_id = arr_deployment_ids[dep_id];
			try{
				record.delete({type: 'scriptdeployment', id: deployment_id});
			}catch(ex){
				log.audit({title: 'Error: GF_SS_Reprocess_Cash_Sales_Refund.js:removeCompletedDeployments', details: 'deployment_id: ' + deployment_id + ', Error ' + ex.toString() + ' : ' + ex.stack});
			}
		}
	}

	function buildNewScriptDeployment(_param){
		try{
			// find any existing deployments
			var scriptdeploymentSearchObj = search.create({
				type: "scriptdeployment",
				filters: [
					["script","anyof","1604"]
				],
				columns: [ ]
			});
			// count the results with this name - the only reason it should be anything other than zero is if the script has errored
			var searchResultCount = scriptdeploymentSearchObj.runPaged().count;
			// set a unique deployment name allow for multiples
          	var str_random = (Math.floor(Math.random()*1000000000)).toString();
			var str_deployName = (searchResultCount < 1) ? 'GF_MR_RICS_Cash_Sale_Refund_0' : 'GF_MR_RICS_Cash_Sale_Refund_' + str_random;
			// create a new on demand script deployment for this location and run the deployment
			var deployment_rec = record.copy ({
				type: record.Type.SCRIPT_DEPLOYMENT,
				id: 61910
			});
			deployment_rec.setValue ({fieldId: 'isdeployed', value: true});
			deployment_rec.setValue ({fieldId: 'title', value: str_deployName});
            deployment_rec.setValue ({fieldId: 'scriptid', value: '_mr_rics_cscr_repr_'+str_random});
			deployment_rec.setValue ({fieldId: 'startdate', value: new Date()});
			var deploy_id = deployment_rec.save();

			if(deploy_id > 0){
				// get the script id of the saved deployment
				var script_deployment_fields = search.lookupFields({
					type: 'scriptdeployment',
					id: parseInt(deploy_id),
					columns: ['scriptid']
				});
				var deploy_script_id = script_deployment_fields.scriptid;
                log.audit({title: 'GF_SS_Reprocess_Cash_Sales_Refund.js:buildNewScriptDeployment', details: 'deploy_script_id: ' + deploy_script_id + ', _param: ' + _param});
				// run the deployment
				var call_cscr_on_demand_js = task.create({
					taskType: task.TaskType.MAP_REDUCE,
					scriptId: 'customscript_mr_rics_cash_sale_refund',
					deploymentId: deploy_script_id,
                    params: { custscript_sales_ticket_id: _param }
				});
				call_cscr_on_demand_js.submit();
			}
		}catch(ex){
			log.error({title: 'Error: GF_SS_Reprocess_Cash_Sales_Refund.js:buildNewScriptDeployment', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
		}
	}

});