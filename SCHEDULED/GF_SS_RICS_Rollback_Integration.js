/**
*@NApiVersion 2.0
*@NScriptType ScheduledScript
*
Script Name:   GF_SS_RICS_Rollback_Integration.js
Author:        Mark Robinson
*/
 define(['N/runtime', 'N/record', 'N/search', 'N/task'], function (runtime, record, search, task) {

    function runRollback(){

    	try{
            log.debug({title: 'GF_SS_RICS_Rollback_Integration.js:runRollback', details: 'start'} );

            // undeploy all script deployments (except those tied to GF_MR_RICS_Rollback_Integration)
            var arr_script_deployments = [
                'GF_MR_RICS_Product_Details',
                'GF_MR_RICS_Cash_Sale_Refund',
                'GF_MR_RICS_VRMA',
                'GF_MR_RICS_GetPOSTransaction',
                'GF_MR_RICS_Transfers',
                'GF_MR_RICS_NonSellableBatch',
                'GF_MR_RICS_Purchase_Orders',
                'GF_MR_RICS_Purchases',
                'GF_MR_RICS_PO_Receipt',
                'GF_MR_RICS_Transfer_Orders',
                'GF_MR_RICS_Daily_Deposit',
                'GF_MR_CustomersFixSubsidiaries',
                'GF_UE_RICS_SalesTicket',
                'GF_SS_Get_RICS_Purchase_Orders',
                'GF_SS_GetPOSTransactionJSON'
            ];
            for(var script in arr_script_deployments){
                undeploy_scripts(arr_script_deployments[script]);
            }
            // run the GF_MR_RICS_Rollback_Integration map reduce deployments
            // GF_MR_RICS_Rollback_Integration Product Item
            var call_rollback_js = task.create({
                taskType: task.TaskType.MAP_REDUCE,
                scriptId: 'customscript_mr_rics_rollbackintegration',
                deploymentId: 'customdeploy_mr_rics_rollback_proitem'
            });
            call_rollback_js.submit();
            // GF_MR_RICS_Rollback_Integration Product Details
            var call_rollback_js = task.create({
                taskType: task.TaskType.MAP_REDUCE,
                scriptId: 'customscript_mr_rics_rollbackintegration',
                deploymentId: 'customdeploy_mr_rics_rollback_prodetails'
            });
            call_rollback_js.submit();
            // GF_MR_RICS_Rollback_Integration On Hand
            var call_rollback_js = task.create({
                taskType: task.TaskType.MAP_REDUCE,
                scriptId: 'customscript_mr_rics_rollbackintegration',
                deploymentId: 'customdeploy_mr_rics_rollback_onhand'
            });
            call_rollback_js.submit();
            // GF_MR_RICS_Rollback_Integration Tender Type Mapping
            var call_rollback_js = task.create({
                taskType: task.TaskType.MAP_REDUCE,
                scriptId: 'customscript_mr_rics_rollbackintegration',
                deploymentId: 'customdeploy_mr_rics_rollback_ttmapping'
            });
            call_rollback_js.submit();
            // GF_MR_RICS_Rollback_Integration Transfer Order
            var call_rollback_js = task.create({
                taskType: task.TaskType.MAP_REDUCE,
                scriptId: 'customscript_mr_rics_rollbackintegration',
                deploymentId: 'customdeploy_mr_rics_rollback_trnfrord'
            });
            call_rollback_js.submit();
            // GF_MR_RICS_Rollback_Integration Sales Details
            var call_rollback_js = task.create({
                taskType: task.TaskType.MAP_REDUCE,
                scriptId: 'customscript_mr_rics_rollbackintegration',
                deploymentId: 'customdeploy_mr_rics_rollback_salesdet'
            });
            call_rollback_js.submit();
            // GF_MR_RICS_Rollback_Integration Sales Tenders
            var call_rollback_js = task.create({
                taskType: task.TaskType.MAP_REDUCE,
                scriptId: 'customscript_mr_rics_rollbackintegration',
                deploymentId: 'customdeploy_mr_rics_rollback_salestend'
            });
            call_rollback_js.submit();
            // GF_MR_RICS_Rollback_Integration Sales Ticket
            var call_rollback_js = task.create({
                taskType: task.TaskType.MAP_REDUCE,
                scriptId: 'customscript_mr_rics_rollbackintegration',
                deploymentId: 'customdeploy_mr_rics_rollback_salestick'
            });
            call_rollback_js.submit();
            // GF_MR_RICS_Rollback_Integration PO Details
            var call_rollback_js = task.create({
                taskType: task.TaskType.MAP_REDUCE,
                scriptId: 'customscript_mr_rics_rollbackintegration',
                deploymentId: 'customdeploy_mr_rics_rollback_podetails'
            });
            call_rollback_js.submit();
            // GF_MR_RICS_Rollback_Integration Purchase Order
            var call_rollback_js = task.create({
                taskType: task.TaskType.MAP_REDUCE,
                scriptId: 'customscript_mr_rics_rollbackintegration',
                deploymentId: 'customdeploy_mr_rics_rollback_purchord'
            });
            call_rollback_js.submit();
            // GF_MR_RICS_Rollback_Integration NonSellable Batch Details
            var call_rollback_js = task.create({
                taskType: task.TaskType.MAP_REDUCE,
                scriptId: 'customscript_mr_rics_rollbackintegration',
                deploymentId: 'customdeploy_mr_rics_rollback_nsbd'
            });
            call_rollback_js.submit();
            // GF_MR_RICS_Rollback_Integration NonSellable Batch
            var call_rollback_js = task.create({
                taskType: task.TaskType.MAP_REDUCE,
                scriptId: 'customscript_mr_rics_rollbackintegration',
                deploymentId: 'customdeploy_mr_rics_rollback_nsb'
            });
            call_rollback_js.submit();

            log.debug({title: 'GF_SS_RICS_Rollback_Integration.js:runRollback', details: 'complete'} );

 	    } catch (ex) {
            log.error({title: 'ERROR: GF_SS_RICS_Rollback_Integration.js:runRollback-main', details: 'Error: ' + ex.toString() + ' : ' + ex.stack} );
        }
	}

    return {
        execute: function (context){
            runRollback()
        }
    }

    // =========================== extra functions
    function errText(_e){
        // usage : nlapiLogExecution('ERROR', 'function', 'Error: ' + ex.toString() + ' : ' + ex.stack);
        var txt = '';
        if(_e instanceof nlobjError){
             //this is netsuite specific error
             txt = 'NLAPI ' + _e.getCode() + ' :: ' + _e.getDetails() + ' :: ' + _e.getStackTrace().join(', ');
        }else{
             //this is generic javascript error
             txt = 'JavaScript/Other ' + _e.toString();
             txt += (typeof _e.stack != 'undefined') ? ' : ' + _e.stack : '';
        }
        return txt;
    }

    function undeploy_scripts(_str_script_name){
        if(_str_script_name == null){ return false; } // MUST HAVE SCRIPT NAME TO WORK
        // undeploy scripts
        var scriptdeploymentSearchObj = search.create({
            type: "scriptdeployment",
            filters: [
                ["script.name","startswith",_str_script_name], "AND",
                ["isdeployed","is","T"]
            ],
            columns: [ ]
        });
        scriptdeploymentSearchObj.run().each(function(result){
            // .run().each has a limit of 4,000 results - hopefully there's not more than 4k deployments
            log.debug({title: 'GF_SS_RICS_Rollback_Integration.js:undeploy_scripts', details: 'result.id: ' + result.id} );
            // var deployment_id = record.submitFields({
            //     type: 'scriptdeployment',
            //     id: result.id,
            //     values: { 'isdeployed': 'F' }
            // });
            return true;
        });
    }

});