/**
*@NApiVersion 2.1
*@NScriptType ScheduledScript
*
Script Name:   GF_SS_GetPOSTransactionJSON.js
Author:        Mark Robinson
*/
define(['N/log', 'N/search', 'N/https', 'N/format', 'N/runtime', 'N/file', 'N/record', '/SuiteScripts/GF_LIB/gf_nsrics'], function(log, search, https, format, runtime, file, record, gf_nsrics) {

    /**
     * This function calls the RICS API endpoint that returns the POS Transaction data, and it saves the responses to a raw JSON file in the file cabinet.
     */
    function execute(context) {
        try {
            var currentScript = runtime.getCurrentScript();
            var dt_current_time = new Date();
          	if (dt_current_time.getHours() < 5) {
            	return true;
	        } // never let this process start before 5 am
    	    removeOldJsonFiles();
            var nsrics = gf_nsrics.getIntegration();
            var max_attempts = 3;
            var getPOSTransaction = nsrics.baseurl + nsrics.getEndPoint('GetPOSTransaction');
            var header = [];
            header['Content-Type'] = 'application/json';
            header['Token'] = nsrics.getActiveConnection();
            var num_skip = 0;
            var current_attempt = 0;
            var dt_today = new Date();
            var dt_yesterday = new Date();
            dt_yesterday.setDate(dt_today.getDate() - 1);
            var str_today = format.format({
                value: dt_today,
                type: format.Type.DATE
            });
            var str_yesterday = format.format({
                value: dt_yesterday,
                type: format.Type.DATE
            });
            var dt_twomonthsago = new Date();
            dt_twomonthsago.setDate(dt_twomonthsago.getDate() - 62);
            var str_twomonthsago = format.format({
                value: dt_twomonthsago,
                type: format.Type.DATE
            });
            var str_batchStart = str_twomonthsago + ' 00:00:00 AM';
            if ((dt_today.getHours() - 1) < 12) {
                var str_ticketStart = str_today + ' ' + String(dt_today.getHours() - 1).padStart(2, '0') + ':00:00 AM';
            } else { // noon or later shift to PM
                var str_ticketStart = str_today + ' ' + String((dt_today.getHours() > 13 ? dt_today.getHours() - 13 : 12)).padStart(2, '0') + ':00:00 PM';
            }
            var str_startDate = str_yesterday + ' 00:00:00 AM';
            var str_endDate = '1/1/9999 11:59:59 PM'; // RICS recommends this in order to capture transactions even if the store forgets to close the batch
            var previous_total_records_count = -1;
            var files_saved = 0;
            if(currentScript.deploymentId == 'customdeploy_ss_getpostransactionjson_od'){ dt_current_time.setHours(23); }

            if (dt_current_time.getHours() < 7) {
                // unclosed batches =======================================================================
            	var arr_unclosed_batch_ticket_numbers = getUnclosedBatchNumbers();
                for (var i in arr_unclosed_batch_ticket_numbers) {
                    nsrics.sleep(3000);
                    var arr_batch_data = arr_unclosed_batch_ticket_numbers[i].split('|');
                    var str_ticket_num = arr_batch_data[0];
                    var str_store_num = arr_batch_data[1];
                    var str_ns_custom_rec_id = arr_batch_data[2];
                    num_skip = 0;
                    current_attempt = 0;
                    do {
                        // get unclosed batch tickets
                        var postData = {
                            "BatchStartDate": str_batchStart,
                            "BatchEndDate": str_endDate,
                            "TicketNumber": str_ticket_num,
                            "StoreCode": str_store_num,
                            "Skip": num_skip
                        };
                        log.audit({
                            title: 'GF_SS_GetPOSTransactionJSON.js:execute',
                            details: 'postData: ' + JSON.stringify(postData)
                        });
                        try {
                            postData = JSON.stringify(postData);
                            var response = https.post({
                                url: getPOSTransaction,
                                headers: header,
                                body: postData
                            });
                            if (response != undefined && response['code'] != 200) {
                                current_attempt += 1;
                            } else {
                                current_attempt = 0;
                                var data = JSON.parse(response.body);

                                try {
                                    // when total records haven't changed there is no update to get
                                    log.audit({
                                        title: 'GF_SS_GetPOSTransactionJSON.js:execute-RESPONSE',
                                        details: 'Data: ' + JSON.stringify(data)
                                    });
                                    var rics_batch_end_date = (data.Sales != undefined && (data.Sales).length > 0 && data.Sales[0].BatchEndDate != undefined) ? data.Sales[0].BatchEndDate : '';
                                    log.audit({
                                        title: 'GF_SS_GetPOSTransactionJSON.js:execute-UPDATE-BATCH_END_DATE',
                                        details: 'rics_batch_end_date: ' + rics_batch_end_date + ', str_ns_custom_rec_id: ' + str_ns_custom_rec_id
                                    });
                                    if(rics_batch_end_date != ''){
                                      	var rics_sales_ticket_update_id = record.submitFields({
                                            type: 'customrecord_rics_sales_ticket',
                                            id: parseInt(str_ns_custom_rec_id),
                                            values: {
                                                'custrecord_rics_st_batchenddate': new Date(rics_batch_end_date)
                                            }
                                        });
                                    }
                                } catch (ex) {
                                    log.error({
                                        title: 'GF_SS_GetPOSTransactionJSON.js:execute-UPDATE-BATCH_END_DATE',
                                        details: 'str_ticket_num: ' + str_ticket_num + ', str_store_num: ' + str_store_num + ', ns custom record id: ' + str_ns_custom_rec_id + ', rics_batch_end_date: ' + rics_batch_end_date
                                    });
                                    log.error({
                                        title: 'GF_SS_GetPOSTransactionJSON.js:execute-UPDATE-BATCH_END_DATE',
                                        details: 'Error ' + ex.toString() + ' : ' + ex.stack
                                    });
                                }

                                num_skip = data.ResultStatistics.EndRecord;
                            }
                        } catch (err) {
                            current_attempt += 1;
                        }
                    } while (current_attempt < max_attempts && ((response != undefined && response['code'] != 200) || (data != undefined && data.hasOwnProperty('ResultStatistics') && data.ResultStatistics.EndRecord < data.ResultStatistics.TotalRecords)));
                }
                // unclosed batches =======================================================================
            } else {
                // daily processing =======================================================================
                do {
                    // first process only
                    if (num_skip == 0) {
                        previous_total_records_count = getPreviousTotalRecordsCount(num_skip, str_startDate, currentScript);
                    }
                    nsrics.sleep(3000); // three second wait
                    if (dt_current_time.getHours() <= 9) {
                        // get everything from the previous day until now
                        var postData = {
                            "BatchStartDate": str_batchStart,
                            "BatchEndDate": str_endDate,
                            "ModifiedOnStart": str_startDate,
                            "ModifiedOnEnd": str_endDate,
                            "Skip": num_skip
                        }; // drop the specific ticket number
                    } else if (dt_current_time.getHours() == 23) {
                        // get data from DataWarehouse
                        process_missing_data(header, str_batchStart, str_endDate, getPOSTransaction, nsrics, currentScript);
                        return true;
                    } else {
                        // get everything from today
                        str_startDate = str_today + ' 00:00:00 AM';
                        var postData = {
                            "BatchStartDate": str_batchStart,
                            "BatchEndDate": str_endDate,
                            "ModifiedOnStart": str_startDate,
                            "ModifiedOnEnd": str_endDate,
                            "Skip": num_skip
                        };
                    }
                    // } else {
                    //     // use hourly data
                    //     var postData = {
                    //         "BatchStartDate": str_batchStart,
                    //         "BatchEndDate": str_endDate,
                    //         "ModifiedOnStart": str_startDate,
                    //         "ModifiedOnEnd": str_endDate,
                    //         "TicketDateStart": str_ticketStart,
                    //         "TicketDateEnd": str_endDate,
                    //         "Skip": num_skip
                    //     }; // drop the specific ticket number
                    // }
                    log.audit({
                        title: 'GF_SS_GetPOSTransactionJSON.js:execute',
                        details: 'postData: ' + JSON.stringify(postData)
                    });
                    try {
                        postData = JSON.stringify(postData);
                        var response = https.post({
                            url: getPOSTransaction,
                            headers: header,
                            body: postData
                        });
                        if (response != undefined && response['code'] != 200) {
                            current_attempt += 1;
                        } else {
                            current_attempt = 0;
                            var data = JSON.parse(response.body);

                            try {
                                // when total records haven't changed there is no update to get
                                log.audit({
                                    title: 'GF_SS_GetPOSTransactionJSON.js:execute',
                                    details: 'previous_total_records_count: ' + previous_total_records_count + ', data.ResultStatistics.TotalRecords: ' + data.ResultStatistics.TotalRecords
                                });
                                if (previous_total_records_count > -1 && previous_total_records_count == data.ResultStatistics.TotalRecords) {
                                    // return false;
                                    log.error({
                                        title: 'GF_SS_GetPOSTransactionJSON.js:execute-exit',
                                        details: 'previous_total_records_count: ' + previous_total_records_count + ', data.ResultStatistics.TotalRecords: ' + data.ResultStatistics.TotalRecords
                                    });
                                    max_attempts = 5;
                                } else {
                                    // set filename
                                    var file_name = num_skip.toString() + '__' + str_startDate.replace(/\//g, '_');
                                    file_name = file_name.substring(0, file_name.indexOf(' ')) + '__' + currentScript.deploymentId;
                                    // set leading zeros for sorting purposes
                                    if(file_name.indexOf('_') < 5){
                                        var num_of_zeros_to_add = 5 - file_name.indexOf('_');
                                        for(var addmorezeros = 0; addmorezeros < num_of_zeros_to_add; addmorezeros++){
                                            file_name = '0' + file_name;
                                        }
                                    }
                                    var file_id = getExistingFileId(file_name); // if this file name already exists - then replace the file
                                    if (file_id > 0) {
                                        file.delete({
                                            id: file_id
                                        });
                                    }
                                    // create file
                                    var fileObj = file.create({
                                        name: file_name + '.json',
                                        fileType: file.Type.JSON,
                                        contents: response.body
                                    });
                                    fileObj.folder = 3550;
                                    fileObj.isOnline = true;
                                    // save file
                                    var fileId = fileObj.save();
                                    log.audit({
                                        title: 'GF_SS_GetPOSTransactionJSON.js:execute-JSON-FILE',
                                        details: 'SAVED file_name: ' + file_name + ', fileId: ' + fileId
                                    });
                                    files_saved += 1;
                                }
                            } catch (ex) {
                                log.error({
                                    title: 'GF_SS_GetPOSTransactionJSON.js:execute-JSON-FILE',
                                    details: 'file_name: ' + file_name
                                });
                                log.error({
                                    title: 'GF_SS_GetPOSTransactionJSON.js:execute-JSON-FILE',
                                    details: 'Error ' + ex.toString() + ' : ' + ex.stack
                                });
                            }

                            num_skip = data.ResultStatistics.EndRecord;
                        }
                    } catch (err) {
                        current_attempt += 1;
                    }
                } while (current_attempt < max_attempts && ((response != undefined && response['code'] != 200) || (data != undefined && data.hasOwnProperty('ResultStatistics') && data.ResultStatistics.EndRecord < data.ResultStatistics.TotalRecords)));
                // daily processing =======================================================================
            }
            log.audit({ title: 'GF_SS_GetPOSTransactionJSON.js:execute', details: 'complete - files_saved: ' + files_saved });
        } catch (ex) {
            log.error({ title: 'ERROR: GF_SS_GetPOSTransactionJSON.js:execute', details: 'Error: ' + ex.toString() + ' : ' + ex.stack });
        }
    }

    return {
        execute: execute
    }

    // =========================== extra functions
    /**
     * This function returns the total records count of the most recent file run
     * @param {*} _num_skip - integer - the leading number on the file
     * @returns - integer - total records from the last run
     */
    function getPreviousTotalRecordsCount(_num_skip, _str_startDate, _currentScript) {
        var ptrc = 0; // previous_total_records_count
        var file_name = _num_skip.toString() + '__' + _str_startDate.replace(/\//g, '_');
        file_name = file_name.substring(0, file_name.indexOf(' ')) + '__' + _currentScript.deploymentId + '.json';
        var file_id = getExistingFileId(file_name);
        if (file_id > 0) {
            var existingFileObj = file.load({
                id: file_id
            });
            if (existingFileObj.size < 10485760) { // specific NS limitation - if the file is bigger than this we have to read it differently - but they shouldn't really ever be bigger than this
                var str_json = existingFileObj.getContents();
                var jsonObj = JSON.parse(str_json);
                // log.audit({title: 'GF_SS_GetPOSTransactionJSON.js:getPreviousTotalRecordsCount', details: 'existingFileObj: ' + JSON.stringify(jsonObj)});
                // log.audit({title: 'GF_SS_GetPOSTransactionJSON.js:getPreviousTotalRecordsCount', details: 'existingFileObj.ResultStatistics.TotalRecords: ' + JSON.stringify(jsonObj.ResultStatistics.TotalRecords)});
                ptrc = jsonObj.ResultStatistics.TotalRecords;
            }
        }
        return ptrc;
    }

    /**
     * 
     * @param {*} _file_name - string - the name of the file that will be used to locate the specific json file
     * @returns 
     */
    function getExistingFileId(_file_name) {
        var result_id = 0;

        var fileSearchObj = search.create({
            type: "file",
            filters: [
                ["folder", "anyof", "3550"], "AND",
                ["name", "startswith", "0__"], "AND",
                ["name", "contains", _file_name]
            ],
            columns: [
                search.createColumn({
                    name: "name",
                    sort: search.Sort.ASC,
                    label: "Name"
                }),
                search.createColumn({
                    name: "folder",
                    label: "Folder"
                }),
                search.createColumn({
                    name: "documentsize",
                    label: "Size (KB)"
                }),
                search.createColumn({
                    name: "url",
                    label: "URL"
                }),
                search.createColumn({
                    name: "created",
                    label: "Date Created"
                }),
                search.createColumn({
                    name: "modified",
                    label: "Last Modified"
                }),
                search.createColumn({
                    name: "filetype",
                    label: "Type"
                })
            ]
        });
        var searchResultCount = fileSearchObj.runPaged().count;
        log.audit({
            title: 'GF_SS_GetPOSTransactionJSON.js:getExistingFileId',
            details: 'fileSearchObj result count: ' + searchResultCount
        });
        fileSearchObj.run().each(function(result) {
            // .run().each has a limit of 4,000 results
            result_id = result.id;
            return true;
        });

        return result_id;
    }

    /**
     * This function removes files that were create on or before yesterday from the GetPOSTransaction_Response folder
     */
    function removeOldJsonFiles() {
        var fileSearchObj = search.create({
            type: "file",
            filters: [
                ["folder", "anyof", "3550"], "AND",
                // ["created", "onorbefore", "yesterday"]
                ["created", "onorbefore", "hoursago6"]
            ],
            columns: []
        });
        fileSearchObj.run().each(function(result) {
            // .run().each has a limit of 4,000 results
            if (result.id > 0) {
                file.delete({
                    id: result.id
                });
            }
            return true;
        });
    }

    function getUnclosedBatchNumbers() {
        var arr_return = [];
        var customrecord_rics_sales_ticketSearchObj = search.create({
            type: "customrecord_rics_sales_ticket",
            filters: [
                ["custrecord_rics_st_batchenddate", "isempty", ""], "AND",
                ["custrecord_rics_st_saledatetime", "within", "daysago60", "daysago1"]
            ],
            columns: [
                "custrecord_rics_st_storecode",
                "custrecord_rics_st_storename",
                "id",
                "custrecord_rics_st_ticketnumber",
                search.createColumn({
                    name: "custrecord_rics_st_saledatetime",
                    sort: search.Sort.DESC
                }),
                search.createColumn({
                    name: "custrecord_rics_st_terminaldescription",
                    sort: search.Sort.ASC
                }),
                "custrecord_rics_st_customerfirstname",
                "custrecord_rics_st_customerlastname",
                search.createColumn({
                    name: "custrecord_rics_st_batchstartdate",
                    sort: search.Sort.ASC
                }),
                "custrecord_rics_st_batchenddate",
                "internalid",
                search.createColumn({
                    name: "custrecord_rics_st_batchstartdate",
                    sort: search.Sort.ASC
                }),
                "custrecord_rics_st_receipt_total"
            ]
        });
        customrecord_rics_sales_ticketSearchObj.run().each(function(result) {
            // .run().each has a limit of 4,000 results
            arr_return.push(result.getValue('custrecord_rics_st_ticketnumber') + '|' + result.getValue('custrecord_rics_st_storecode') + '|' + result.getValue('internalid'));
            return true;
        });

        return arr_return;
    }


    function process_missing_data(_header, _str_batchStart, _str_endDate, _getPOSTransaction, _nsrics, _currentScript){
      	// call to AWS to get list of missing data
        var aws_header = [];
        aws_header['Content-Type'] = 'application/json';
        aws_header['x-api-key'] = 'A6xeIKveD1nBRIuKQ75g7IjxLsEFh4t73Dszaa5b';
        var aws_postData = { "acct_num": "", "query": "1" };
        // log.debug({title: 'GF_MR_Get_SF_Customers.js:gf_map', details: 'aws_postData: ' + JSON.stringify(aws_postData)});
        aws_postData = JSON.stringify(aws_postData);
        var endpoint = 'https://hg06i49148.execute-api.us-west-2.amazonaws.com/test/getAccount';
        
        var aws_response = https.post({
            url: endpoint,
            body: aws_postData,
            headers: aws_header
        });
        if (aws_response != undefined && aws_response['code'] == 200) {
            // log.debug({ title: 'Response', details: aws_response });
            var num_skip = 0;
            var current_attempt = 0;
            var max_attempts = 3;
            var res_body = JSON.parse(aws_response.body);
            for(var rows in res_body){
                _nsrics.sleep(2000);
                var str_ticket_num = res_body[rows].rTicketNumber;
                var str_store_num = res_body[rows].rStoreCode;
                var files_saved = 0;
                num_skip = 0;
                current_attempt = 0;
                do {
                    // get unclosed batch tickets
                    var postData = {
                        "BatchStartDate": _str_batchStart,
                        "BatchEndDate": _str_endDate,
                        "TicketNumber": str_ticket_num,
                        "StoreCode": str_store_num,
                        "Skip": num_skip
                    };
                    log.audit({ title: 'GF_SS_GetPOSTransactionJSON.js:execute', details: 'postData: ' + JSON.stringify(postData) });
                    try {
                        postData = JSON.stringify(postData);
                        var rics_response = https.post({
                            url: _getPOSTransaction,
                            headers: _header,
                            body: postData
                        });
                        if (rics_response != undefined && rics_response['code'] != 200) {
                            current_attempt += 1;
                        } else {
                            current_attempt = 0;
                            var data = JSON.parse(rics_response.body);
                            try {
                                // when total records haven't changed there is no update to get
                                log.audit({ title: 'GF_SS_GetPOSTransactionJSON.js:process_missing_data-RESPONSE', details: 'Data: ' + JSON.stringify(data) });
                                // set filename
                                var file_name = num_skip.toString() + '__missing_data_' + str_store_num + '_' + str_ticket_num + ' ';
                                file_name = file_name.substring(0, file_name.indexOf(' ')) + '__' + _currentScript.deploymentId;
                                // set leading zeros for sorting purposes
                                if(file_name.indexOf('_') < 5){
                                    var num_of_zeros_to_add = 5 - file_name.indexOf('_');
                                    for(var addmorezeros = 0; addmorezeros < num_of_zeros_to_add; addmorezeros++){
                                        file_name = '0' + file_name;
                                    }
                                }
                                var file_id = getExistingFileId(file_name); // if this file name already exists - then replace the file
                                if (file_id > 0) { file.delete({ id: file_id }); }
                                // create file
                                var fileObj = file.create({
                                    name: file_name + '.json',
                                    fileType: file.Type.JSON,
                                    contents: rics_response.body
                                });
                                fileObj.folder = 3550;
                                fileObj.isOnline = true;
                                // save file
                                var fileId = fileObj.save();
                                log.audit({ title: 'GF_SS_GetPOSTransactionJSON.js:execute-JSON-FILE', details: 'SAVED file_name: ' + file_name + ', fileId: ' + fileId });
                                files_saved += 1;
                            } catch (ex) {
                                log.error({ title: 'GF_SS_GetPOSTransactionJSON.js:process_missing_data', details: 'Error ' + ex.toString() + ' : ' + ex.stack });
                            }
                            num_skip = data.ResultStatistics.EndRecord;
                        }
                    } catch (err) {
                        current_attempt += 1;
                    }
                } while (current_attempt < max_attempts && ((rics_response != undefined && rics_response['code'] != 200) || (data != undefined && data.hasOwnProperty('ResultStatistics') && data.ResultStatistics.EndRecord < data.ResultStatistics.TotalRecords)));
            }
        }
        return true;
    }

});