/**
*@NApiVersion 2.1
*@NScriptType ScheduledScript
*
Script Name:   GF_SS_Reprocess_Purchases.js
Author:        Mark Robinson
*/
define(['N/runtime', 'N/search', 'N/record', 'N/task'], function (runtime, search, record, task) {

    /**
     * This function uses a search to load the RICS Purchase Order custom records that have not been cancelled or closed and it passes them through the
     * purchase_orders and purchases scripts to update the custom record, and update the NetSuite Transaction record
     */
    function reprocess_purchases(){

    	try{
            log.audit({title: 'GF_SS_Reprocess_Purchases.js:reprocess_purchases', details: 'start'} );
			var scriptObj = runtime.getCurrentScript();
			var po_count = 0;
            removeCompletedDeployments();
            //run search
            var customrecord_rics_po_detailsSearchObj = search.create({
				type: "customrecord_rics_po_details",
				/* filters: [
					[
						["custrecord_rics_pod_ns_po","anyof","@NONE@"] // no po
					], "OR",
					[
						["custrecord_rics_pod_line_unique_id","isempty",""], "AND", // has po but this line was not added
						["custrecord_rics_pod_ns_po.mainline","is","T"]
					], "OR",
					[
						["formulanumeric: CASE WHEN {custrecord_rics_pod_line_unique_id} = {custrecord_rics_pod_ns_po.lineuniquekey} AND {custrecord_rics_pod_orderquantity} <> {custrecord_rics_pod_ns_po.quantity} THEN 1 ELSE 0 END","greaterthan","0"], "AND",
						["custrecord_rics_pod_ns_po.mainline","is","F"] // has line but order qty is different
					]
				], */
				filters: [
                  [
                    ["custrecord_rics_pod_ns_po","anyof","@NONE@"]
                  ], "OR",
                  [
                    ["custrecord_rics_pod_line_unique_id","isempty",""],"AND",
                    ["custrecord_rics_pod_ns_po.mainline","is","T"]
                  ], "OR",
                  [
                    ["formulanumeric: CASE WHEN {custrecord_rics_pod_line_unique_id} = {custrecord_rics_pod_ns_po.lineuniquekey} AND ( ( {custrecord_rics_pod_receivedquantity} = 0 AND {custrecord_rics_pod_orderquantity} != {custrecord_rics_pod_ns_po.quantity} ) OR ({custrecord_rics_pod_receivedquantity} > {custrecord_rics_pod_orderquantity} AND ( {custrecord_rics_pod_receivedquantity} != {custrecord_rics_pod_ns_po.quantity} AND {custrecord_rics_pod_receivedquantity} != {custrecord_rics_pod_ns_po.quantityshiprecv} ) ) ) THEN 1 ELSE 0 END","greaterthan","0"],"AND",
                    ["custrecord_rics_pod_ns_po.mainline","is","F"],"AND",
                    ["custrecord_rics_pod_ns_po.closed","is","F"]
                  ]
                ],
				columns:
				[
				   search.createColumn({
					  name: "formulanumeric",
					  summary: "GROUP",
					  formula: "NVL2({custrecord_rics_pod_latest_conversion}, 1, 0)",
					  sort: search.Sort.ASC
				   }),
				   search.createColumn({
					  name: "custrecord_rics_pod_parent_po",
					  summary: "GROUP"
				   }),
				   search.createColumn({
					  name: "custrecord_rics_pod_latest_update",
					  summary: "MIN",
					  sort: search.Sort.ASC
				   }),
				   search.createColumn({
					 name: "formulatext",
					 summary: "MIN",
					 formula: "NS_CONCAT({internalid})"
				   })
				]
			 });
            customrecord_rics_po_detailsSearchObj.run().each(function(result){
                // .run().each has a limit of 4,000 results
				if(po_count > 100){ return false; }
                var rics_po_id = result.getValue({ name: "custrecord_rics_pod_parent_po", summary: "GROUP" });
                if(scriptObj.getRemainingUsage() <= 20){ return false; }
                buildNewScriptDeployment(rics_po_id);
                try{
                    var arr_ids = (result.getValue({ name: "formulatext", summary: "MIN", formula: "NS_CONCAT({internalid})" })).split(',');
                    if(scriptObj.getRemainingUsage() > (2 * arr_ids.length)){
                        for(var i in arr_ids){
                            var pod_id = arr_ids[i];
                            var rics_po_details_id = record.submitFields({
                                type: 'customrecord_rics_po_details',
                                id: pod_id,
                                values: { 'custrecord_rics_pod_latest_conversion': new Date() }
                            });
                        }
                    }
                }catch(ex){
                  	log.error({title: 'ERROR: GF_SS_Reprocess_Purchases.js:reprocess_purchases-updatePOD', details: 'Error: ' + ex.toString() + ' : ' + ex.stack} );
                    return false;
                }
				po_count += 1;
                return true;
            });

            log.audit({title: 'GF_SS_Reprocess_Purchases.js:reprocess_purchases', details: 'complete'});
            return true;

 	    } catch (ex) {
            log.error({title: 'ERROR: GF_SS_Reprocess_Purchases.js:reprocess_purchases', details: 'Error: ' + ex.toString() + ' : ' + ex.stack} );
        }
	}

    return {
        execute: function (context){
            reprocess_purchases()
        }
    }

	function removeCompletedDeployments(){
		// main two deployments: customdeploy_mr_rics_purchases, customdeploy_mr_rics_purchases_od
		var arr_deployment_ids = [];
		var scheduledscriptinstanceSearch = search.create({
			type: "scheduledscriptinstance",
			filters: [
				["status","anyof","COMPLETE","CANCELED"], "AND",
				["script.internalid","anyof","1599"], "AND",
				["scriptdeployment.internalid","noneof","61897","61896", "1510176"] // , "AND",
				// ["enddate","onorbefore","hoursago1"]
			],
			columns: [
				search.createColumn({
					name: "name",
					join: "script"
				}),
				search.createColumn({
					name: "scriptid",
					join: "scriptDeployment"
				}),
				search.createColumn({
					name: "internalid",
					join: "scriptDeployment"
				}),
				search.createColumn({
					name: "timestampcreated",
					sort: search.Sort.DESC
				}),
				"mapreducestage",
				"status",
				"startdate",
				"enddate"
			]
		}).run();
		for (var k = 0; k < 2; k++) { // max pages 2, up to 2,000 records total
			var results = scheduledscriptinstanceSearch.getRange({start: k * 1000, end: (k * 1000) + 1000});
			for (var i = 0; i < results.length; i++) {
				var this_deployment_id = results[i].getValue({name: 'internalid', join: 'scriptDeployment'});
				if(arr_deployment_ids.indexOf(this_deployment_id) == -1){
					arr_deployment_ids.push(this_deployment_id);
				}
			}
		}

		// remove the deployments
		for(var dep_id in arr_deployment_ids){
			var deployment_id = arr_deployment_ids[dep_id];
			try{
				record.delete({type: 'scriptdeployment', id: deployment_id});
			}catch(ex){
				log.audit({title: 'Error: GF_SS_Reprocess_Purchases.js:removeCompletedDeployments', details: 'deployment_id: ' + deployment_id + ', Error ' + ex.toString() + ' : ' + ex.stack});
			}
		}
	}

	function buildNewScriptDeployment(_param){
		try{
			// find any existing deployments
			var scriptdeploymentSearchObj = search.create({
				type: "scriptdeployment",
				filters: [
					["script","anyof","1599"]
				],
				columns: [ ]
			});
			// count the results with this name - the only reason it should be anything other than zero is if the script has errored
			var searchResultCount = scriptdeploymentSearchObj.runPaged().count;
			// set a unique deployment name allow for multiples
            var str_random = (Math.floor(Math.random()*1000000)).toString();
			var str_deployName = (searchResultCount < 1) ? 'GF_MR_RICS_Purchases_0' : 'GF_MR_RICS_Purchases_' + str_random;
			// create a new on demand script deployment for this location and run the deployment
			var deployment_rec = record.copy ({
				type: record.Type.SCRIPT_DEPLOYMENT,
				id: 61897
			});
			deployment_rec.setValue ({fieldId: 'isdeployed', value: true});
			deployment_rec.setValue ({fieldId: 'title', value: str_deployName});
            deployment_rec.setValue ({fieldId: 'scriptid', value: '_mr_rics_purch_od_'+str_random});
			deployment_rec.setValue ({fieldId: 'startdate', value: new Date()});
			var deploy_id = deployment_rec.save();

			if(deploy_id > 0){
				// get the script id of the saved deployment
				var script_deployment_fields = search.lookupFields({
					type: 'scriptdeployment',
					id: parseInt(deploy_id),
					columns: ['scriptid']
				});
				var deploy_script_id = script_deployment_fields.scriptid;
                log.audit({title: 'GF_SS_Reprocess_Purchases.js:buildNewScriptDeployment', details: 'deploy_script_id: ' + deploy_script_id + ', _param: ' + _param});
				// run the deployment
				var call_po_on_demand_js = task.create({
					taskType: task.TaskType.MAP_REDUCE,
					scriptId: 'customscript_mr_rics_purchases',
					deploymentId: deploy_script_id,
                    params: { custscript_ns_rics_purchase_order_id: _param }
				});
				call_po_on_demand_js.submit();
			}
		}catch(ex){
			log.error({title: 'Error: GF_SS_Reprocess_Purchases.js:buildNewScriptDeployment', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
		}
	}

});