/**
*@NApiVersion 2.1
*@NScriptType ScheduledScript
*
Script Name:   GF_SS_Upload_OnHand_DW.js
Author:        Mark Robinson
*/
define(['N/crypto', 'N/runtime', 'N/file', 'N/search', 'N/https', '/SuiteScripts/GF_LIB/crypto'], function(crypto, runtime, file, search, https, CryptoJS) {

    /**
     * This function uses a search to load the RICS Purchase Order custom records that have not been cancelled or closed and it passes them through the
     * purchase_orders and purchases scripts to update the custom record, and update the NetSuite Transaction record
     */
    function main_process(){

    	try{
            log.audit({title: 'GF_SS_Upload_OnHand_DW.js:main_process', details: 'start'} );

			// ************* TASK 0: GET S3 BUCKET DETAILS *************
			let currentScript = runtime.getCurrentScript();
			let awsData = new Object();
			awsData.s3bucketName = currentScript.getParameter({ name: 'custscript_aws_bucket_name' });
			awsData.s3bucketRegion = currentScript.getParameter({ name: 'custscript_aws_bucket_region' });
			awsData.s3bucketFolder = currentScript.getParameter({ name: 'custscript_aws_bucket_folder' });
			awsData.s3bucket_URI = awsData.s3bucketName + '.s3.' + awsData.s3bucketRegion + '.amazonaws.com' + awsData.s3bucketFolder;
			awsData.awsAccessKeyId = currentScript.getParameter({ name: 'custscript_aws_access_key' });
			awsData.awsSecretKey = currentScript.getParameter({ name: 'custscript_aws_secret_key' });
			awsData.awsRegion = awsData.s3bucketRegion;
			awsData.host = awsData.s3bucketName + '.s3.'+ awsData.awsRegion + '.amazonaws.com';
			awsData.NetSuiteFolderID = 3204; // AWS-Upload

			// if any of these details are not set exit the script
			let flg_property_undefined = false;
			for(let prop in awsData){
				if(awsData[prop] == undefined){
					flg_property_undefined = true;
					break;
				}
			}
			if(flg_property_undefined){
				log.error({title: 'GF_SS_Upload_OnHand_DW.js:main_process', details: `awsData: ${JSON.stringify(awsData)}`  } );
				return false;
			}
			let f_id = save_search_results_to_csv();
			let file_to_upload = file.load({ id:f_id });
			let file_contents = file_to_upload.getContents();

			let amzDate = getAmzDate();
			let datestamp = amzDate.split("T")[0];
			let signingKey = getSignatureKey(awsData.awsSecretKey, datestamp, awsData.awsRegion, 's3');
			let payloadHash = crypto.createHash({ algorithm: crypto.HashAlg.SHA256 });
			payloadHash.update({ input: file_contents });
			let amzContentSha256 = payloadHash.digest().toLowerCase();

			// ************* TASK 1: CREATE A CANONICAL REQUEST *************
                /** (AWS example)
                    PUT
                    /test%24file.text

                    date:Fri, 24 May 2013 00:00:00 GMT
                    host:examplebucket.s3.amazonaws.com
                    x-amz-content-sha256:44ce7dd67c959e0d3524ffac1771dfbba87d2b6b4b4e99e42034a8b803f8b072
                    x-amz-date:20130524T000000Z

                    date;host;x-amz-content-sha256;x-amz-date;x-amz-storage-class
                    44ce7dd67c959e0d3524ffac1771dfbba87d2b6b4b4e99e42034a8b803f8b072
                */
			let str_filename = 'ns_ioh.csv';
			let str_encoded_filename = rfc3986EncodeURIComponent(str_filename);
			let canonicalUri = 'https://' + awsData.s3bucket_URI + str_encoded_filename;
			let str_date = '';
			// let canonicalHeaders = 'content-type:application/json\n';
			let canonicalHeaders = 'date:' + str_date + '\n';
			canonicalHeaders += 'host:' + awsData.host + '\n';
			canonicalHeaders += 'x-amz-content-sha256:' + amzContentSha256 + '\n';
			canonicalHeaders += 'x-amz-date:' + amzDate + '\n';
			let signedHeaders = 'date;host;x-amz-content-sha256;x-amz-date';
			let canonicalRequest = 'PUT\n';
			canonicalRequest += awsData.s3bucketFolder + str_encoded_filename + '\n\n';
			canonicalRequest += canonicalHeaders + '\n';
			canonicalRequest += signedHeaders + '\n';
			canonicalRequest += amzContentSha256;

			log.debug({title: 'GF_SS_Upload_OnHand_DW.js: TASK 1', details: 'canonical request: ' + JSON.stringify(canonicalRequest)});

			// ************* TASK 2: CREATE THE STRING TO SIGN*************
			/** (AWS example)
				AWS4-HMAC-SHA256
				20130524T000000Z
				20130524/us-east-1/s3/aws4_request
				9e0e90d9c76de8fa5b200d8c849cd5b8dc7a3be3951ddb7f6a76b4158342019d
			*/
			let algorithm = 'AWS4-HMAC-SHA256';
			let credentialScope = datestamp + '/' + awsData.s3bucketRegion + '/s3/aws4_request';
			let canonicalHash = crypto.createHash({ algorithm: crypto.HashAlg.SHA256 });
			canonicalHash.update({ input: canonicalRequest });
			let stringToSign = algorithm + '\n';
			stringToSign += amzDate + '\n';
			stringToSign += credentialScope + '\n';
			stringToSign += canonicalHash.digest().toLowerCase();

			log.debug({title: 'GF_SS_Upload_OnHand_DW.js: TASK 2', details: 'string to sign: ' + stringToSign});

			// ************* TASK 3: CALCULATE THE SIGNATURE *************
			let signature = CryptoJS.HmacSHA256(stringToSign, signingKey);
			let authorizationHeader = algorithm + ' ';
			authorizationHeader += 'Credential=' + awsData.awsAccessKeyId + '/' + credentialScope + ', ';
			authorizationHeader += 'SignedHeaders=' + signedHeaders + ', ';
			authorizationHeader +=  'Signature=' + signature.toString();
			log.debug({title: 'GF_SS_Upload_OnHand_DW.js: TASK 3', details: 'authorizationHeader: ' + JSON.stringify(authorizationHeader)});

			let s3_headers = {
				'x-amz-content-sha256': amzContentSha256,
				'x-amz-date': amzDate,
				'Authorization': authorizationHeader
			};
			log.debug({title: 'GF_SS_Upload_OnHand_DW.js: TASK 3', details: 's3_headers: ' + JSON.stringify(s3_headers)});

			let response = https.put({
				url: canonicalUri,
				body: file_contents,
				headers: s3_headers
			});

			log.debug({title: 'GF_SS_Upload_OnHand_DW.js: TASK 3', details: 'response: ' + JSON.stringify(response)});

			let str_success = 'Failed';
			if (response.code == 200) {
				// log successful message
				str_success = 'Successful';
				log.audit({title: `GF_SS_Upload_OnHand_DW.js:Upload ${str_success}`, details: `sent file name: ${str_filename}`});
			}else{
				// show failure message - try again
				log.audit({title: `GF_SS_Upload_OnHand_DW.js:Upload ${str_success}`, details: `file name: ${str_filename}`});
			}
			log.audit({title: 'GF_SS_Upload_OnHand_DW.js:Upload '+str_success+'-code', details: 'response.code: ' + response.code});
			log.audit({title: 'GF_SS_Upload_OnHand_DW.js:Upload '+str_success+'-body', details: 'response.body: ' + JSON.stringify(response.body)});
			log.audit({title: 'GF_SS_Upload_OnHand_DW.js:Upload '+str_success+'-sign', details: 'stringToSign: ' + stringToSign});
			log.audit({title: 'GF_SS_Upload_OnHand_DW.js:Upload '+str_success+'-sent', details: 'canonicalRequest: ' + canonicalRequest});
			// context.response.write(str_html_page);
			file.delete({ id: f_id });

            log.audit({title: 'GF_SS_Upload_OnHand_DW.js:main_process', details: 'complete'});
            return true;

 	    } catch (ex) {
            log.error({title: 'ERROR: GF_SS_Upload_OnHand_DW.js:main_process', details: 'Error: ' + ex.toString() + ' : ' + ex.stack} );
        }
	}

    return {
        execute: function (context){
            main_process()
        }
    }

	// =========================== extra functions
	/** 
	 * This function returns the id of the file that was created in the file cabinet
	 * @returns integer
	 */
	function save_search_results_to_csv(){
		try{
			let itemSearchObj = search.create({
				type: "item",
				filters: [
					["isinactive","is","F"], "AND",
					["inventorylocation.custrecord_gf_location_type","anyof","1"], "AND",
					["parent","noneof","@NONE@"], "AND",
					[
						["locationquantityonhand","isnotempty",""],"OR",
						["locationquantitybackordered","isnotempty",""],"OR",
						["locationquantityintransit","isnotempty",""],"OR",
						["locationquantityonorder","isnotempty",""]
					]
				],
				columns: [
					search.createColumn({name: "internalid"}),
					search.createColumn({
						name: "namenohierarchy",
						join: "inventoryLocation",
						sort: search.Sort.ASC,
						label: "LOCATION"
					}),
					search.createColumn({
						name: "entityid",
						join: "vendor",
						sort: search.Sort.ASC,
						label: "Vendor Code"
					}),
					search.createColumn({
						name: "itemid",
						sort: search.Sort.ASC,
						label: "Name"
					}),
					search.createColumn({name: "vendorname", label: "SKU"}),
					search.createColumn({name: "custitem_psgss_product_color", label: "ROW"}),
					search.createColumn({name: "custitem_psgss_product_size", label: "COL"}),
					search.createColumn({name: "upccode", label: "UPC Code"}),
					search.createColumn({
						name: "formulanumeric",
						formula: "NVL({locationquantityonhand}, 0)",
						label: "On Hand"
					}),
					search.createColumn({
						name: "formulanumeric",
						formula: "NVL({locationquantitybackordered}, 0)",
						label: "Back Ordered"
					}),
					search.createColumn({
						name: "formulanumeric",
						formula: "NVL({locationquantityintransit}, 0)",
						label: "In Transit"
					}),
					search.createColumn({
						name: "formulanumeric",
						formula: "NVL({locationquantityonorder}, 0)",
						label: "On Order"
					})
				]
			});
			let searchResultCount = itemSearchObj.runPaged().count;
			log.debug({title: 'GF_SS_Upload_OnHand_DW.js:save_search_results_to_csv', details: `itemSearchObj result count: ${searchResultCount}` });
			let myPagedData = itemSearchObj.runPaged({pageSize: 1000});
			let resultSet = new Array();
			resultSet.push('INTERNAL ID, LOCATION, VENDOR CODE, NAME, SKU, ROW, COL, UPC CODE, ON HAND, BACK ORDERED, IN TRANSIT, ON ORDER');
			myPagedData.pageRanges.forEach(function(pageRange){
				let myPage = myPagedData.fetch({
					index: pageRange.index
				});

				myPage.data.forEach(function(result){
					try{
						let resultArr = new Array();

						resultArr.push('"' + result.getValue(itemSearchObj.columns[0]) + '"'); // INTERNAL ID
						resultArr.push('"' + result.getValue(itemSearchObj.columns[1]) + '"'); // LOCATION
						resultArr.push('"' + result.getValue(itemSearchObj.columns[2]) + '"'); // VENDOR CODE
						resultArr.push('"' + result.getValue(itemSearchObj.columns[3]) + '"'); // NAME
						resultArr.push('"' + result.getValue(itemSearchObj.columns[4]) + '"'); // SKU
						resultArr.push('"' + result.getValue(itemSearchObj.columns[5]) + '"'); // ROW
						resultArr.push('"' + result.getValue(itemSearchObj.columns[6]) + '"'); // COL
						resultArr.push('"' + result.getValue(itemSearchObj.columns[7]) + '"'); // UPC CODE
						resultArr.push('"' + result.getValue(itemSearchObj.columns[8]) + '"'); // ON HAND
						resultArr.push('"' + result.getValue(itemSearchObj.columns[9]) + '"'); // BACK ORDERED
						resultArr.push('"' + result.getValue(itemSearchObj.columns[10]) + '"'); // IN TRANSIT
						resultArr.push('"' + result.getValue(itemSearchObj.columns[11]) + '"'); // ON ORDER

						resultSet.push(resultArr);
					} catch (ex) {
						log.error({title: 'ERROR: GF_SS_Upload_OnHand_DW.js:save_search_results_to_csv-in page loop-resultArr', details: 'Error: ' + JSON.stringify(resultArr)} );
                        log.error({title: 'ERROR: GF_SS_Upload_OnHand_DW.js:save_search_results_to_csv-in page loop-error', details: 'Error: ' + ex.toString() + ' : ' + ex.stack} );
					}
				}); // PAGE LOOP

			}); // SEARCH LOOP

			// Creating a file name
			let fname = 'ns_ioh';

			// Write to a CSV file
			let fileObj = file.create({
				name: fname,
				fileType: file.Type.CSV,
				contents: resultSet.join('n')
			});
			fileObj.folder = 3204;
			let file_id = fileObj.save();

			return file_id;

		} catch (ex) {
            log.error({title: 'ERROR: GF_SS_Upload_OnHand_DW.js:save_search_results_to_csv', details: 'Error: ' + ex.toString() + ' : ' + ex.stack} );
        }
	}

	/**
     * This function returns an AMZ acceptable formatted date as a string
     * @returns string
     */
	 function getAmzDate() {
        let amzDate = new Date().toISOString().split('.')[0];
        amzDate = amzDate.replace(/-/g, '').replace(/:/g, '');
        amzDate += 'Z';
        return amzDate;
    }

    /**
     * This function will change the html special characters of a string to meet the rfc3986 standard
     * @param {*} str
     * @returns
     */
    function rfc3986EncodeURIComponent(str) {
        return encodeURIComponent(str).replace(/[!'()*]/g, function(c){'%'+c.charCodeAt(0).toString(16)});
    }

	/**
     * This function uses the parameters to create a valid AWS signature
     * @param {*} key
     * @param {*} dateStamp
     * @param {*} regionName
     * @param {*} serviceName
     * @returns string
     */
	 function getSignatureKey(key, dateStamp, regionName, serviceName) {
        log.audit({title: 'GF_SS_Upload_OnHand_DW.js: getSignatureKey', details: 'params - key: ' + key + ', dateStamp: ' + dateStamp + ', regionName: ' + regionName + ', serviceName: ' + serviceName});
        let kDate = CryptoJS.HmacSHA256(dateStamp, 'AWS4'+key);
        let kRegion = CryptoJS.HmacSHA256(regionName, kDate);
        let kService = CryptoJS.HmacSHA256(serviceName, kRegion);
        log.audit({title: 'GF_SS_Upload_OnHand_DW.js: getSignatureKey', details: 'kService: ' + kService});
        return CryptoJS.HmacSHA256('aws4_request', kService);
    }

});