/**
 *@NApiVersion 2.1
 *@NScriptType Portlet
 *
Script Name:   GF_PL_STRATA_Invoices.js
Author:        Mark Robinson
 */

// This sample creates a portlet that includes a simple form with a text field and a submit button
define(['N/task', 'N/ui/serverWidget'], function(task, serverWidget) {
    function render(params) {
        var portlet = params.portlet;
        portlet.title = 'Process Strata Invoices';
        portlet.setSubmitButton({
            url: '/app/site/hosting/scriptlet.nl?script=1739&deploy=1',
            label: 'Run',
            target: '_self'
        });
        var log_link = portlet.addField({
            id: 'custpage_loglink',
            type: serverWidget.FieldType.INLINEHTML,
            label: ' '
        }).defaultValue = '<a href="https://4942365.app.netsuite.com/app/common/search/searchresults.nl?searchid=1966&saverun=T&whence=" target="_blank">VIEW LOGS</a>';
    }

    return {
        render: render
    };
});