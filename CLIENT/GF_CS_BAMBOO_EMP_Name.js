/**
 * @NApiVersion 2.1
 * @NScriptType ClientScript
 *
Script Name:   GF_CS_BAMBOO_EMP_Name.js
Author:        Mark Robinson
 */

define(['N/record', 'N/search'], function (record, search) {

    /**
     * This function runs after the Employee record has been submitted
     * It will review the entity id and detect if the field contains and digits, when this happens the digits are replaced with the LastName, FirstName of the employee
     * @param {*} scriptContext - object - this object contains context specific to the record that called the script as well as the script itself
     */
    function saveRecord_NameValidation(scriptContext){
        // load the record to a letiable
        let emp_rec = scriptContext.currentRecord;
        // grab and test the entityid - bamboo has been setting these to the emp number and it should be the name
        let str_entity = emp_rec.getValue({fieldId: 'entityid'});
        if(hasNumber(str_entity)){
            // when number is found set entity id to name prior to save
            log.audit({title: 'GF_CS_BAMBOO_EMP_Name.js:saveRecord_NameValidation-found', details: 'found number in entityid: ' + str_entity});
            let str_firstname = emp_rec.getValue({fieldId: 'firstname'});
            let str_lastname = emp_rec.getValue({fieldId: 'lastname'});

            let str_new_entity = str_lastname + ', ' + str_firstname;
            // remove any numbers that have been placed in the first or last name fields
            str_new_entity = str_new_entity.replace(new RegExp(/\d/, "g"), "").trim();

            emp_rec.setValue({fieldId: 'entityid', value: str_new_entity});
            log.audit({title: 'GF_CS_BAMBOO_EMP_Name.js:saveRecord_NameValidation-set to', details: 'str_new_entity: ' + str_new_entity});
        }
        return true;
    }

    return {
        saveRecord: saveRecord_NameValidation
    };

    // =========================== extra functions
    /**
     * This function is used to determine if a string contains a number
     * @param {string} _str - string - any string value.
     * @returns - bool - true if a number was found, false otherwise
     */
    function hasNumber(_str) {
        return /\d/.test(_str);
    }

});