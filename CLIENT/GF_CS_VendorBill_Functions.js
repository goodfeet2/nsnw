/**
 * @NApiVersion 2.1
 * @NScriptType ClientScript
 *
Script Name:   GF_CS_VendorBill_Functions.js
Author:        Mark Robinson
 */

define(['N/record', 'N/search'], function (record, search) {

    /**
     * This function runs when the check record is loaded
     * It will check the quantity of all of the lines and set the GF Quantity Total field
     * @param {*} scriptContext - object - this object contains context specific to the record that called the script as well as the script itself
     */
    function pageInit(scriptContext){
        // load the record to a letiable
        let vendor_bill_rec = scriptContext.currentRecord;
        let currentLineCount = vendor_bill_rec.getLineCount({ 'sublistId': 'item' });
        let quantity_count = 0;
        for(let line = 0; line < currentLineCount; line++){
            vendor_bill_rec.selectLine({ sublistId: 'item', line: line });
            let line_qty = vendor_bill_rec.getCurrentSublistValue({ sublistId: 'item', fieldId: 'quantity' });
            quantity_count += parseInt(line_qty);
        }
        vendor_bill_rec.setValue({fieldId: 'custbody_gf_quantity_total', value: quantity_count});
        return true;
    }

    return {
        pageInit: pageInit
    };

});