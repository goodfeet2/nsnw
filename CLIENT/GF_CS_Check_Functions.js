/**
 * @NApiVersion 2.1
 * @NScriptType ClientScript
 *
Script Name:   GF_CS_Check_Functions.js
Author:        Mark Robinson
 */

define(['N/record', 'N/search'], function (record, search) {

    /**
     * This function runs when the check record is loaded
     * It will validate the tranid (Check#) field and the transactionnumber (Transaction Number) field, when the transaction number field is 'To Be Generated'
     * and the Check# field has numbers in it, the check is assumed to be new and the numbers are removed.
     * @param {*} scriptContext - object - this object contains context specific to the record that called the script as well as the script itself
     */
    function pageInit(scriptContext){
        // load the record to a letiable
        let check_rec = scriptContext.currentRecord;
        // grab and test the tranid and transactionnumber - clear the tranid when the check is new
        let str_tranid = check_rec.getValue({fieldId: 'tranid'});
        let str_transactionnumber = check_rec.getValue({fieldId: 'transactionnumber'});
        if(hasNumber(str_tranid) && str_transactionnumber == 'To Be Generated'){
            // when number is found set tranid to blank
            log.audit({title: 'GF_CS_Check_Functions.js:pageInit-found', details: 'found number in Check#: ' + str_tranid});
            // set value to blank
            check_rec.setValue({fieldId: 'tranid', value: ''});
        }
        return true;
    }

    return {
        pageInit: pageInit
    };

    // =========================== extra functions
    /**
     * This function is used to determine if a string contains a number
     * @param {string} _str - string - any string value.
     * @returns - bool - true if a number was found, false otherwise
     */
    function hasNumber(_str) {
        return /\d/.test(_str);
    }

});