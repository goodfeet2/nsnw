/**
 * @NApiVersion 2.1
 * @NScriptType ClientScript
 *
Script Name:   GF_CS_Variance.js
Author:        Mark Robinson
 */

define(['N/currentRecord', 'N/ui/dialog', 'N/record', 'N/format', 'N/url', 'N/search', 'N/ui/message', 'N/runtime'], function(currentRecord, dialog, record, format, url, search, message, runtime) {

    /**
     * This function runs when 
     * @param {*} scriptContext - object - this object contains context specific to the record that called the script as well as the script itself
     */
    function pageInit(scriptContext){
        // let suitelt_rec = scriptContext.currentRecord; // use suitelt_rec.getValue({fieldId: 'field'}); to get values
        document.getElementById("custpage_items_sublist_variance_listmarkall").onclick = function(){markall();};
        document.getElementById("custpage_items_sublist_variance_listunmarkall").onclick = function(){
            let current_rec = scriptContext.currentRecord;
            current_rec.setValue({ fieldId	: 'custpage_num_items_selected', value	: 0 });
            current_rec.setValue({ fieldId	: 'custpage_qty_items_selected', value	: 0 });
            current_rec.setValue({ fieldId	: 'custpage_qty_variance_selected', value	: 0 });
            current_rec.setValue({ fieldId	: 'custpage_cost_variance_selected', value	: 0.00 });
            let lines = current_rec.getLineCount({sublistId: 'custpage_items_sublist_variance_list'});
            for(let line = 0; line < lines; line++){
                let chkbx = document.getElementById('custpage_items_sublist_flgprocess'+(parseInt(line)+1));
                chkbx.value = 'F';
                chkbx.checked = false;
            }
            return true;
        };
        build_process_element(); // for display when processing
    }

    function fieldChanged(scriptContext){
        // add field validation for Vendor Code (4 characters, convert to upper, no spaces)
        let fieldId	= scriptContext.fieldId;
        if(fieldId == 'custpage_items_sublist_flgprocess'){
            // load the page
            let current_rec = scriptContext.currentRecord;
            let num_custpage_selected_count = current_rec.getValue({fieldId: 'custpage_num_items_selected'});
            let num_custpage_selected_qty = current_rec.getValue({fieldId: 'custpage_qty_items_selected'});
            let num_custpage_selected_variance = current_rec.getValue({fieldId: 'custpage_qty_variance_selected'});
            let num_custpage_cost_variance_selected = current_rec.getValue({fieldId: 'custpage_cost_variance_selected'});
            // get line data
            let flgprocess = current_rec.getSublistValue({
                sublistId	: 'custpage_items_sublist_variance_list',
                fieldId		: 'custpage_items_sublist_flgprocess',
                line		: scriptContext.line
            });
            let num_ns_qty = current_rec.getSublistValue({
                sublistId	: 'custpage_items_sublist_variance_list',
                fieldId		: 'custpage_items_sublist_ns_item_qty',
                line		: scriptContext.line
            });
            let str_rics_qty = current_rec.getSublistValue({
                sublistId	: 'custpage_items_sublist_variance_list',
                fieldId		: 'custpage_items_sublist_rics_item_qty',
                line		: scriptContext.line
            });
            let num_variance_cost = current_rec.getSublistValue({
                sublistId	: 'custpage_items_sublist_variance_list',
                fieldId		: 'custpage_items_sublist_variance_cost',
                line		: scriptContext.line
            });
            if(flgprocess){
                current_rec.setValue({ fieldId	: 'custpage_num_items_selected', value	: (num_custpage_selected_count + 1) });
                current_rec.setValue({ fieldId	: 'custpage_qty_items_selected', value	: (num_custpage_selected_qty + num_ns_qty) });
                current_rec.setValue({ fieldId	: 'custpage_qty_variance_selected', value	: (num_custpage_selected_variance + (str_rics_qty - num_ns_qty)) });
                current_rec.setValue({ fieldId	: 'custpage_cost_variance_selected', value	: (num_custpage_cost_variance_selected + num_variance_cost).toFixed(2) });
            }else{
                current_rec.setValue({ fieldId	: 'custpage_num_items_selected', value	: (num_custpage_selected_count - 1) });
                current_rec.setValue({ fieldId	: 'custpage_qty_items_selected', value	: (num_custpage_selected_qty - num_ns_qty) });
                current_rec.setValue({ fieldId	: 'custpage_qty_variance_selected', value	: (num_custpage_selected_variance - (str_rics_qty - num_ns_qty)) });
                current_rec.setValue({ fieldId	: 'custpage_cost_variance_selected', value	: (num_custpage_cost_variance_selected - num_variance_cost).toFixed(2) });
            }
        }
        if(fieldId == 'custpage_flg_process_adjustments'){
            let current_rec = scriptContext.currentRecord;
            let flg_process_adjustments = current_rec.getValue({fieldId: 'custpage_flg_process_adjustments'});
            if(flg_process_adjustments){
                let formdata = getFormData();
                buildVarianceInventoryAdjustment(formdata);
                window.onbeforeunload = function(){};
                window.location.href = 'https://4942365-sb2.app.netsuite.com/app/common/search/searchresults.nl?searchid=1881';
                // proc_box.style.display = 'none';
            }
        }
    }

    return {
        pageInit: pageInit,
        fieldChanged: fieldChanged,
        createInventoryAdjustments: createInventoryAdjustments,
        getItems: getItems
    };

    // =========================== extra functions
    /**
     *
     */
    //  function getVariance(){
    function createInventoryAdjustments(){
        try{
            // get the form data
            let formdata = getFormData();
            let current_rec = currentRecord.get();
            current_rec.setValue({ fieldId : 'custpage_json', value : (JSON.stringify(formdata)) });

            if((formdata.lines_to_process).length != 0){
                let options = {};
                if((formdata.lines_to_process).length != formdata.total_lines){
                    options = {
                        title: "Get Variance - Lines To Process",
                        message: "You have selected " + (formdata.lines_to_process).length + " items, click OK if you'd like to process them."
                    };
                }else{
                    options = {
                        title: "Get Variance - Lines To Process",
                        message: "You have selected ALL " + (formdata.lines_to_process).length + " items. Selecting ALL items may require additional processing time, click OK if you'd like to process them."
                    };
                }
                dialog.confirm(options).then(getVarianceDialogSuccessOne).catch(getVarianceDialogFailure);
                
            }else{
                // no items selected
                let options = {
                    title 	: "Get Variance - Lines To Process",
	                message	: "Please select at least one item to process."
                };
                dialog.alert(options).then(getSelectLines);
            }
        } catch (ex) {
            log.error({title: 'GF_SL_Variance.js:getVariance', details: 'Error: ' +  ex.toString() + ' : ' + ex.stack});
            let error_message = message.create({
                title   : 'Error',
                message : 'An ERROR has occurred:\n' + ex.toString() + ' : ' + ex.stack,
                type    : message.Type.WARNING
            });
            error_message.show();
        }
    }

    /**
     * 
     * @param {*} result 
     */
    function getVarianceDialogSuccessOne(result){ 
        log.debug({title: 'GF_SL_Variance.js:getVariance-OK', details: "Success with value " + result});
        if(result){
            // ok
            let options = {
                title: "GL IMPACT WARNING",
                message: "BY CLICKING 'OK' YOU ACKNOWLEDGE THAT PROCESSING THESE ITEMS WILL HAVE A DIRECT IMPACT ON THE GENERAL LEDGER"
            };
            return dialog.confirm(options).then(getVarianceDialogSuccessTwo).catch(getVarianceDialogFailure);
        }
    }

    /**
     * 
     * @param {*} result 
     */
    function getVarianceDialogSuccessTwo(result){
        log.debug({title: 'GF_SL_Variance.js:getVariance-OK', details: "Success with value " + result});
        if(result){
            // ok
            // create VIA (Variance Inventory Adjustment) Record(s)
            let proc_box = document.getElementById("gf_process_bar");
            proc_box.style.display = 'block';
            let current_rec = currentRecord.get();
            current_rec.setValue('custpage_flg_process_adjustments', true);
        }
    }

    /**
     * Failed to select lines
     * @param {*} result
     */
    function getSelectLines(result){
        log.debug({title: 'GF_SL_Variance.js:getVariance-getSelectLines', details: "Failure: " + result});
    }

    /**
     * Failed to execute
     * @param {*} reason 
     */
    function getVarianceDialogFailure(reason){
        log.debug({title: 'GF_SL_Variance.js:getVariance-Cancel', details: "Failure: " + reason});
    }

    /**
     * 
     * @returns {object} current_formdata
     */
    function getFormData(){
        try{
            let current_rec = currentRecord.get();
            let lines = current_rec.getLineCount({sublistId: 'custpage_items_sublist_variance_list'});
            let current_formdata = {};
            current_formdata.total_lines = lines;
            let lines_to_process = [];
            for(let line = 0; line < lines; line++){
                let chkbx = document.getElementById('custpage_items_sublist_flgprocess'+(parseInt(line)+1));
                let process = chkbx.checked;
                if(process == true){
                    let line_data = {};
                    line_data.ns_internal_id = current_rec.getSublistValue({
                        sublistId	: 'custpage_items_sublist_variance_list',
                        fieldId		: 'custpage_items_sublist_internal_item_id',
                        line		: line
                    });
                    line_data.ns_subsidiary_id = current_rec.getSublistValue({
                        sublistId	: 'custpage_items_sublist_variance_list',
                        fieldId		: 'custpage_items_sublist_subsidiary',
                        line		: line
                    });
                    line_data.location_id = current_rec.getSublistValue({
                        sublistId	: 'custpage_items_sublist_variance_list',
                        fieldId		: 'custpage_items_sublist_item_loc',
                        line		: line
                    });
                    line_data.str_vendor_code = current_rec.getSublistValue({
                        sublistId	: 'custpage_items_sublist_variance_list',
                        fieldId		: 'custpage_items_sublist_vendor_code',
                        line		: line
                    });
                    line_data.class_id = current_rec.getSublistValue({
                        sublistId	: 'custpage_items_sublist_variance_list',
                        fieldId		: 'custpage_items_sublist_item_class',
                        line		: line
                    });
                    line_data.str_sku = current_rec.getSublistValue({
                        sublistId	: 'custpage_items_sublist_variance_list',
                        fieldId		: 'custpage_items_sublist_item_sku',
                        line		: line
                    });
                    line_data.str_row = current_rec.getSublistValue({
                        sublistId	: 'custpage_items_sublist_variance_list',
                        fieldId		: 'custpage_items_sublist_item_row',
                        line		: line
                    });
                    line_data.str_col = current_rec.getSublistValue({
                        sublistId	: 'custpage_items_sublist_variance_list',
                        fieldId		: 'custpage_items_sublist_item_col',
                        line		: line
                    });
                    line_data.str_upc = current_rec.getSublistValue({
                        sublistId	: 'custpage_items_sublist_variance_list',
                        fieldId		: 'custpage_items_sublist_item_upc',
                        line		: line
                    });
                    line_data.location_qty = current_rec.getSublistValue({
                        sublistId	: 'custpage_items_sublist_variance_list',
                        fieldId		: 'custpage_items_sublist_ns_item_qty',
                        line		: line
                    });
                    line_data.rics_item_qty = current_rec.getSublistValue({
                        sublistId	: 'custpage_items_sublist_variance_list',
                        fieldId		: 'custpage_items_sublist_rics_item_qty',
                        line		: line
                    });
                    line_data.ns_item_cost = current_rec.getSublistValue({
                        sublistId	: 'custpage_items_sublist_variance_list',
                        fieldId		: 'custpage_items_sublist_ns_item_cost',
                        line		: line
                    });
                    line_data.variance_cost = current_rec.getSublistValue({
                        sublistId	: 'custpage_items_sublist_variance_list',
                        fieldId		: 'custpage_items_sublist_variance_cost',
                        line		: line
                    });
                    lines_to_process.push(line_data);
                }
            }
            current_formdata.lines_to_process = lines_to_process;
            return current_formdata;
        }catch(ex){
            log.error({title: 'GF_SL_Variance.js:getVariance', details: 'Error: ' +  ex.toString() + ' : ' + ex.stack});
            let error_message = message.create({
                title   : 'Error',
                message : 'An ERROR has occurred:\n' + ex.toString() + ' : ' + ex.stack,
                type    : message.Type.WARNING
            });
            error_message.show();
        }
    }


    function getItems(){
        let current_rec = currentRecord.get();

        let str_baseurl = window.location.search; // example ?script=1728&deploy=1
        let str_custpage_vend = current_rec.getValue({fieldId: 'custpage_vend'});
        let str_custpage_sku = current_rec.getValue({fieldId: 'custpage_sku'});
        let str_custpage_row = current_rec.getValue({fieldId: 'custpage_row'});
        let str_custpage_col = current_rec.getValue({fieldId: 'custpage_col'});
        let str_custpage_location = current_rec.getValue({fieldId: 'custpage_location'});

        str_baseurl = adjustUrlParams(str_baseurl, str_custpage_vend, 'custscript_param_vend');
        str_baseurl = adjustUrlParams(str_baseurl, str_custpage_sku, 'custscript_param_sku');
        str_baseurl = adjustUrlParams(str_baseurl, str_custpage_row, 'custscript_param_row');
        str_baseurl = adjustUrlParams(str_baseurl, str_custpage_col, 'custscript_param_col');
        str_baseurl = adjustUrlParams(str_baseurl, str_custpage_location, 'custscript_param_loc');

        window.onbeforeunload = function(){}; // prevents "Leave Site" popup
        window.location.search = str_baseurl;
    }


    function adjustUrlParams(_str_baseurl, _param_field_value, _str_param_name){
        if(_param_field_value != ''){
            if(_str_baseurl.indexOf(_str_param_name) == -1){
                _str_baseurl = _str_baseurl + '&' + _str_param_name + '=' + _param_field_value;
            }else{
                let arr_url = _str_baseurl.split('&');
                for(let param in arr_url){
                    let parameter = arr_url[param];
                    let arr_param = parameter.split('=');
                    if(arr_param[0].indexOf(_str_param_name) != -1 && arr_param[1] !== _param_field_value){
                        _str_baseurl = _str_baseurl.replace('&' + arr_param[0] + '=' + arr_param[1], '&' + _str_param_name + '=' + _param_field_value);
                    }
                }
            }
        }else if(_param_field_value == '' && _str_baseurl.indexOf(_str_param_name) != -1){
            // paramater has been removed
            let arr_url = _str_baseurl.split('&');
                for(let param in arr_url){
                    let parameter = arr_url[param];
                    let arr_param = parameter.split('=');
                    if(arr_param[0].indexOf(_str_param_name) != -1){
                        _str_baseurl = _str_baseurl.replace('&' + arr_param[0] + '=' + arr_param[1], '');
                    }
                }
        }
        return _str_baseurl;
    }


    function markall(){
        let current_rec = currentRecord.get();
        let num_custpage_selected_qty = 0;
        let num_custpage_selected_variance = 0;
        let num_custpage_cost_variance_selected = 0;
        let lines = current_rec.getLineCount({sublistId: 'custpage_items_sublist_variance_list'});
        for(let line = 0; line < lines; line++){
            let chkbx = document.getElementById('custpage_items_sublist_flgprocess'+(parseInt(line)+1));
            chkbx.value = 'T';
            chkbx.checked = true;
            let str_ns_qty = current_rec.getSublistValue({
                sublistId: 'custpage_items_sublist_variance_list',
                fieldId: 'custpage_items_sublist_ns_item_qty',
                line: line
            });
            let str_rics_qty = current_rec.getSublistValue({
                sublistId: 'custpage_items_sublist_variance_list',
                fieldId: 'custpage_items_sublist_rics_item_qty',
                line: line
            });
            let num_variance_cost = current_rec.getSublistValue({
                sublistId	: 'custpage_items_sublist_variance_list',
                fieldId		: 'custpage_items_sublist_variance_cost',
                line		: line
            });
            num_custpage_selected_qty += parseInt(str_ns_qty);
            num_custpage_selected_variance += parseInt(str_rics_qty) - parseInt(str_ns_qty);
            num_custpage_cost_variance_selected += parseFloat(num_variance_cost);
        }

        current_rec.setValue({ fieldId	: 'custpage_num_items_selected', value	: lines });
        current_rec.setValue({ fieldId	: 'custpage_qty_items_selected', value	: num_custpage_selected_qty });
        current_rec.setValue({ fieldId	: 'custpage_qty_variance_selected', value	: num_custpage_selected_variance });
        current_rec.setValue({ fieldId	: 'custpage_cost_variance_selected', value	: num_custpage_cost_variance_selected.toFixed(2) });
        return true;
    }


    function buildVarianceInventoryAdjustment(_formdata){
        let scriptObj = runtime.getCurrentScript();
        console.log('Starting Usage:' + scriptObj.getRemainingUsage());

        let sorted_data = _formdata.lines_to_process.sort(function(a,b){ return a.location_id - b.location_id });
        // split by subsidiary and location combos
        let process_total = 0;
        let process_count = 0;
        let arr_grouped_data = [];
        for(let line in sorted_data){
            let line_data = sorted_data[line];
            let key = line_data.ns_subsidiary_id + '_' + line_data.location_id + '_' + line_data.class_id;
            if(arr_grouped_data[key] == undefined){
                arr_grouped_data[key] = [line_data];
            }else{
                arr_grouped_data[key].push(line_data);
            }
            process_total += 1;
        }
        // build one adjustment record per group
        for(let grp in arr_grouped_data){
            let arr_data = arr_grouped_data[grp];

            // search via for the combo
            let customrecord_gf_variance_inventory_adjSearchObj = search.create({
                type: "customrecord_gf_variance_inventory_adj",
                filters: [
                    ["custrecord_gf_via_class","anyof",arr_data[0].class_id], "AND",
                    ["custrecord_gf_via_location","anyof",arr_data[0].location_id], "AND",
                    ["custrecord_gf_via_subsidiary","anyof",arr_data[0].ns_subsidiary_id], "AND",
                    ["isinactive","is","F"]
                ],
                columns: [
                    search.createColumn({
                        name: "internalid",
                        join: "CUSTRECORD_GF_VII_VIA_ID"
                     }),
                     search.createColumn({
                        name: "custrecord_gf_vii_item",
                        join: "CUSTRECORD_GF_VII_VIA_ID"
                    }),
                    search.createColumn({
                        name: "custrecord_gf_vii_qty_adjustment",
                        join: "CUSTRECORD_GF_VII_VIA_ID"
                    })
                ]
            });
            let searchResultCount = customrecord_gf_variance_inventory_adjSearchObj.runPaged().count;
            let saved_via_id = 0;
            if(searchResultCount == 0){
                // new data
                // create parent record
                let via_record = record.create({ type: 'customrecord_gf_variance_inventory_adj' }); // 2 units
                via_record.setValue('custrecord_gf_via_subsidiary',arr_data[0].ns_subsidiary_id);
                via_record.setValue('custrecord_gf_via_location',arr_data[0].location_id);
                via_record.setValue('custrecord_gf_via_class',arr_data[0].class_id);
                try{
                    saved_via_id = via_record.save();
                }catch(e){
                    // nothing
                    console.log(e);
                }
            }else{
                customrecord_gf_variance_inventory_adjSearchObj.run().each(function(result){
                    // .run().each has a limit of 4,000 results
                    if(saved_via_id == 0){ saved_via_id = result.id; }
                    return true;
                });
            }

            // load JS array with vii ids
            let arr_vii_rec_ids = [];
            customrecord_gf_variance_inventory_adjSearchObj.run().each(function(result){ // 10 units
                // .run().each has a limit of 4,000 results
                vii_rec_id = result.getValue({ name: "internalid", join: "CUSTRECORD_GF_VII_VIA_ID" });
                arr_vii_rec_ids.push(vii_rec_id);
                return true;
            });

            if(saved_via_id > 0){
                for(line in arr_data){
                    let vii_rec_id = -1;
                    if(arr_vii_rec_ids.indexOf(arr_data[line].ns_internal_id) > -1){
                        vii_rec_id = arr_vii_rec_ids[ arr_data[line].ns_internal_id ];
                    }
                    if(vii_rec_id == -1){
                        // create child records
                        let vii_record = record.create({ type: 'customrecord_gf_variance_inventory_item' }); // 2 units
                        vii_record.setValue('custrecord_gf_vii_item',arr_data[line].ns_internal_id);
                        vii_record.setValue('custrecord_gf_vii_qty_adjustment',(arr_data[line].rics_item_qty - arr_data[line].location_qty));
                        vii_record.setValue('custrecord_gf_vii_vendorcode',arr_data[line].str_vendor_code);
                        vii_record.setValue('custrecord_gf_vii_via_id',saved_via_id);
                        try{
                            vii_record.save();
                            process_count += 1;
                        }catch(e){
                            // nothing
                            console.log(e);
                        }
                    }else{
                        // same item id in the same sub_loc_class group - just update the qty
                        let vii_update_id = record.submitFields({
                            type: 'customrecord_gf_variance_inventory_item',
                            id: vii_rec_id,
                            values: { 'custrecord_gf_vii_qty_adjustment': (arr_data[line].rics_item_qty - arr_data[line].location_qty) }
                        });
                        process_count += 1;
                    }
                    console.log(process_count + ' / ' + process_total + ' --- Remaining Usage:' + scriptObj.getRemainingUsage());
                    if(scriptObj.getRemainingUsage() < 15){
                        // refresh items list - might want to just exit the loop
                        getItems();
                    }else{
                        // update inner HTML
                        let proc_text = document.getElementById("gf_process_bar_text");
                        proc_text.innerHTML = update_process_text(proc_text.innerHTML);
                    }
                }
            }
            // if any message should be added to show via progress do it here
            // console.log();
        }
    }

    /**
     * 
     */
    function build_process_element(){
        let progress_bar = document.createElement("div");
        progress_bar.setAttribute("id", "gf_process_bar");
        progress_bar.setAttribute("style", "display: none; position: fixed; padding: .3em; text-align: center; color: rgba(255,255,255,100); background: #333333; border-radius: 4px 4px 4px 4px; font-size: xx-large;  opacity:.85; width: 60%; top: 40%; left: 20%;");
        let progress_bar_text = document.createElement("p");
        progress_bar_text.setAttribute("id", "gf_process_bar_text");
        let node = document.createTextNode("Processing ...");
        let body_element = document.getElementById("div__body");
        body_element.appendChild(progress_bar);
        progress_bar.appendChild(progress_bar_text);
        progress_bar_text.appendChild(node);
    }

    /**
     *
     * @param {*} _text
     * @returns
     */
    function update_process_text(_text){
        let str_update = '';
        switch(_text){
            case 'Processing ...':
                str_update = 'Processing .';
                break;
            case 'Processing .':
                str_update = 'Processing ..';
                break;
            case 'Processing ..':
                str_update = 'Processing ...';
                break;
        }
        return str_update;
    }

});