/**
 *@NApiVersion 2.1
 *@NScriptType Restlet

Script Name:    GF_RL_RICS_AWS.js
Author:         Mark Robinson
Version:        1.0.0
*/
define(['N/record', 'N/log', 'N/search', 'N/task', 'N/format'],
function(record, log, search, task, format) {

    // Get a standard NetSuite record
    function rics_get(context) {
        try{
            log.audit({title: '!!! Request: GF_RL_RICS_AWS.js:rics_get', details: context});
            if(isEmpty(context)){
                var str_usage = 'The GET method is not configured for this endpoint.';
                var obj_return = {};
                obj_return.message = str_usage;
                log.audit({title: 'Audit Response: GF_RL_RICS_AWS.js:rics_get', details: obj_return});
                return obj_return;
            }else{
                return {};
            }
        }catch(ex){
            log.error({title: 'Error: GF_RL_RICS_AWS.js:rics_get', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
            var obj_return = {};
            obj_return.error = ex;
            obj_return.error_text = ex.toString() + ' : ' + ex.stack;
            return obj_return;
        }
    }


    // Create a NetSuite record from request params
    function rics_post(context) {
        // sleep(20000); // 20 second delay - for testing latency on remotes side
        try{
            log.audit({title: '!!! Request: GF_RL_RICS_AWS.js:rics_post', details: context});
            var obj_return = {};
            if(context.hasOwnProperty('endpoint')){
                // log.audit({title: 'GF_RL_RICS_AWS.js:rics_post', details: 'typeof endpoint: ' + (typeof context.endpoint) + ', endpoint: ' + context.endpoint});
                // detect endpoint
                switch(context.endpoint){
                    case 'pos_rics_inventory_prod':
                        obj_return = process_on_hand(context);
                        break;
                    default:
                        obj_return = {'ERROR': 'FUNCTION NOT FOUND FOR THIS ENDPOINT', 'DATA': context};
                        break;
                }
            }
            log.audit({title: 'GF_RL_RICS_AWS.js:rics_post', details: 'obj_return: ' + JSON.stringify(obj_return)});
            return obj_return;
        }catch(ex){
            log.error({title: 'Error: GF_RL_RICS_AWS.js:rics_post', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
            var obj_return = {};
            obj_return.error = ex;
            obj_return.error_text = ex.toString() + ' : ' + ex.stack;
            return obj_return;
        }
    }

    // Upsert a NetSuite record from request param
    function rics_put(context) {
        try{
            log.audit({title: '!!! Request: GF_RL_RICS_AWS.js:rics_put', details: context});
            var str_usage = 'The PUT method is not configured for this endpoint.';
            var obj_return = {};
            obj_return.message = str_usage;
            log.audit({title: 'Audit Response: GF_RL_RICS_AWS.js:rics_put', details: obj_return});
            return obj_return;
        }catch(ex){
            log.error({title: 'Error: GF_RL_RICS_AWS.js:rics_put', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
            var obj_return = {};
            obj_return.error = ex;
            obj_return.error_text = ex.toString() + ' : ' + ex.stack;
            return obj_return;
        }
    }
    return {
        get: rics_get,
        post: rics_post,
        put: rics_put
    };

    // =========================== processing functions
    // ====================================================== onhand
    function process_on_hand(_context){
        // set return object
        var tmp_return = {};
        tmp_return.Success = false;
        if(_context.StoreCode == undefined || _context.ID == undefined  || _context.OnHand == undefined){
            var str_error = 'The following required data was not provided for this endpoint: ';
            var arr_err = [];
            if(_context.StoreCode == undefined){ arr_err.push('StoreCode'); }
            if(_context.ID == undefined){ arr_err.push('ID'); }
            if(_context.OnHand == undefined){ arr_err.push('OnHand'); }
            str_error += arr_err.join(', ');
            tmp_return.message = str_error;
            return tmp_return;
        }

        /* EXAMPLE ON HAND DATA
        {
            "endpoint":"pos_rics_inventory_prod",
            "StoreCode":"2388",
            "SKU":"WMORBK2",
            "GridRow":"B",
            "GridCol":"7",
            "OnHand":"0",
            "CurrentOnOrder":"0",
            "FutureOnOrder":"0",
            "Price":"150.0",
            "SupplierCode":"NEWB",
            "ClassName":"Women's Running",
            "SupplierSku":"WMORBK2",
            "Summary":"Women's Running",
            "SKUDescription":"Women's Running",
            "Color":"Black/White /Gray",
            "UPC":"194182014495",
            "ModelQuantity":"0",
            "MaxModel":"0",
            "ReorderQuantity":"0d",
            "ID":"017013b2-f71b-496c-9f7a-ab600181a880",
            "LastProcessedDateTime":"2022-08-08 16:30:37",
            "guid":"2388_017013b2-f71b-496c-9f7a-ab600181a880"
        }
        */
        // Find On Hand Record by ProductItem.IntegrationId and StoreCode and OnHand does not match the context OnHand
        var customrecord_rics_inventory_on_handSearchObj = search.create({
            type: "customrecord_rics_inventory_on_hand",
            filters: [
                ["custrecord_rics_ioh_storecode","is",_context.StoreCode], "AND",
                ["custrecord_rics_ioh_parent_productitem.custrecord_rics_pi_integration_id","is",_context.ID], "AND",
                ["custrecord_rics_ioh_onhand","notequalto",_context.OnHand]
            ],
            columns: []
         });
         var flg_onhand_found = false;
         customrecord_rics_inventory_on_handSearchObj.run().each(function(result){
            // .run().each has a limit of 4,000 results
            if(!flg_onhand_found){ // when the record is found set the on hand field only
                flg_onhand_found = true;
                record.submitFields({
                    type: 'customrecord_rics_inventory_on_hand',
                    id: result.id,
                    values: {
                        custrecord_rics_ioh_onhand: _context.OnHand
                    },
                    options: {
                        enableSourcing: false,
                        ignoreMandatoryFields : true
                    }
                });
                tmp_return.Success = true;
                tmp_return.id = result.id;
            }
            return true;
        });
        if(!flg_onhand_found){
            // create a new on hand record for this object
            var onhand_record = record.create({
                type: 'customrecord_rics_inventory_on_hand'
            });
            onhand_record.setValue('custrecord_rics_ioh_integration_record', '1');
            onhand_record.setValue('custrecord_rics_ioh_storecode', _context.StoreCode);
            // get location id and store name record from store code
            var loc_data = findLocation(_context.StoreCode);
            if(loc_data.id != 0){
                onhand_record.setValue('custrecord_rics_ioh_ns_location', loc_data.id);
                onhand_record.setValue('custrecord_rics_ioh_storename', _context.StoreCode + 'Good Feet ' + loc_data.name);
                onhand_record.setValue('custrecord_rics_ioh_json', JSON.stringify(_context));
                // get product item id
                var pi_record_id = find_product_item(_context.UPC, _context.SKU);
                onhand_record.setValue('custrecord_rics_ioh_parent_productitem', pi_record_id);
                // get product details id
                var pd_record_id = find_product_details(_context.SKU);
                onhand_record.setValue('custrecord_rics_ioh_parent_productdetail', pd_record_id);
                // get ns native item id
                var nsitem_record_id = find_item_record(_context.SKU, _context.UPC, _context.Summary);
                if(nsitem_record_id > 0){
                    onhand_record.setValue('custrecord_rics_ioh_ns_items', nsitem_record_id);
                    onhand_record.setValue('custrecord_rics_ioh_ns_matrix_subitem', nsitem_record_id);
                    onhand_record.setValue('custrecord_rics_ioh_ns_subsidiary', loc_data.sub_id);
                    onhand_record.setValue('custrecord_rics_ioh_onhand', _context.OnHand);
                    try{
                        var saved_onhand_id = onhand_record.save();
                        tmp_return.Success = true;
                        tmp_return.id = saved_onhand_id;
                    }catch(ex){
                        log.error({title: 'Error: GF_RL_RICS_AWS.js:process_on_hand-save onhand_record', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
                    }
                }
            }
        }

        return tmp_return;
    }
    // ====================================================== onhand
    
    // ======================================================================================================================================= extra functions
    function isEmpty(_map) {
        for(var key in _map) {
            if (_map.hasOwnProperty(key)) {
                return false;
            }
        }
        return true;
    }

    function sleep(_ms){
        var start = new Date().getTime();
        var end = new Date().getTime() + _ms;
        do{
            start = new Date().getTime();
        }while((end - start) >= 0);
        return true;
    }

    function find_product_item(_upc, _sku){
        var result = 0;
        // log.debug({title: 'GF_RL_RICS_AWS.js:find_product_item', details: '_upc: ' + _upc + ', _sku: ' + _sku});
        // find the Product Details Record
        var pd_SearchFilters = [
            ["isinactive","is","F"], "AND",
            ["custrecord_rics_pi_parent_pd.custrecord_rics_pd_sku","is",_sku]
        ];
        if(_upc && _upc != ''){
            pd_SearchFilters.push("AND");
            pd_SearchFilters.push(["custrecord_rics_pi_upcs","contains",'"'+_upc+'"']);
        }
        var pd_SearchResults = search.create({
            type: "customrecord_rics_product_item",
            filters: pd_SearchFilters,
            columns: [
                search.createColumn({
                    name: "id",
                    sort: search.Sort.ASC,
                    label: "ID"
                }),
                search.createColumn({name: "internalid"}),
                search.createColumn({name: "custrecord_rics_pi_upc"})
            ]
        }).run();
        var pd_SearchResultSet = pd_SearchResults.getRange({start: 0, end: 1});
        if (pd_SearchResultSet != null && pd_SearchResultSet.length > 0){
            result = pd_SearchResultSet[0].id;
            var result_upc = pd_SearchResultSet[0].getValue("custrecord_rics_pi_upc");
        }
        // log.debug({title: 'GF_RL_RICS_AWS.js:find_product_item', details: 'result: ' + result});
        if(_upc === null){ result = result_upc; } // if upc was blank then return only the upc value
        return result;
    }

    function find_product_details(_Sku){
        var result = 0;
        // log.debug({title: 'GF_RL_RICS_AWS.js:find_product_details', details: '_Sku: ' + _Sku});
        // find the Product Details Record
        var pd_SearchResults = search.create({
             type: "customrecord_rics_product_details",
             filters: [
                 ["isinactive","is","F"], "AND",
                 ["custrecord_rics_pd_sku","is",_Sku]
             ],
             columns: [
                 search.createColumn({
                     name: "id",
                     sort: search.Sort.ASC,
                     label: "ID"
                 }),
                 search.createColumn({name: "internalid", label: "Internal ID"})
             ]
         }).run();
         var pd_SearchResultSet = pd_SearchResults.getRange({start: 0, end: 1});
         if (pd_SearchResultSet != null && pd_SearchResultSet.length > 0){
            result = pd_SearchResultSet[0].id;
         }
        //  log.debug({title: 'GF_RL_RICS_AWS.js:find_product_details', details: 'result: ' + result});
         return result;
    }

    /**
     * findLocation is used to return the location id based on the storecode indicated on the RICS Sales Details record
     * @param {*} _values
     */
     function findLocation(_storeCode){
        var result_obj = {id: 0, name: '', sub_id: 0};

        if(_storeCode != ''){
            var locationSearchObj = search.create({
                type: "location",
                filters: [
                    ["externalid","anyof",_storeCode], "AND",
                    ["isinactive","is","F"]
                ],
                columns: [
                    search.createColumn("namenohierarchy"),
                    search.createColumn("subsidiary"),
                ]
            });
            locationSearchObj.run().each(function(result){
                // .run().each has a limit of 4,000 results
                if(result_obj.id == 0){
					var location_record = record.load({
						type: 'location',
						id: result.id,
						isDynamic: true
		            });
                    result_obj.id = result.id;
                    result_obj.name = result.getValue('namenohierarchy');
					result_obj.sub_id = location_record.getValue('subsidiary');
                }
                return true;
            });

			if(result_obj.sub_name != ''){
				
			}
        }
        return result_obj;
    }

    /**
	 * This function takes strings of the sku and upc values and tries to find the related netsuite inventory item record
	 * @param {*} _sku - string - the sku of the item the function will try to find
	 * @param {*} _upc - string - the upc of the item the function will try to find
	 * @param {*} _summary - string - the summary of the item the function will try to find (when it has ** that indicates a service or non-inventory item)
	 * @returns integer - the internal id of the inventory item that was found
	 */
	function find_item_record(_sku, _upc, _summary) {
		var result_id = 0;
		var flg_inactive = false;
		// log.debug({title: 'GF_RL_RICS_AWS.js:find_item_record', details: '_sku: ' + _sku + ', _upc: ' + _upc});

        // check for **
        if(_summary != undefined && _summary.indexOf('** ') > -1){
            // search Non-inventory
            var inventoryitemSearchObj = search.create({
                type: "noninventoryitem",
                filters: [
                    ["type", "anyof", "NonInvtPart"], "AND",
                    ["isinactive", "any", ""], "AND",
                    ["vendorname", "is", _sku], "AND",
                    ["upccode", "is", _upc]
                ],
                columns: [
                    search.createColumn({
                        name: "isinactive",
                        label: "Inactive"
                    })
                ]
            });
        }else{
            var inventoryitemSearchObj = search.create({
                type: "inventoryitem",
                filters: [
                    ["type", "anyof", "InvtPart"], "AND",
                    ["isinactive", "any", ""], "AND",
                    ["vendorname", "is", _sku], "AND",
                    ["upccode", "is", _upc], "AND",
                    ["parent", "noneof", "@NONE@"]
                ],
                columns: [
                    search.createColumn({
                        name: "isinactive",
                        label: "Inactive"
                    })
                ]
            });
        }
		inventoryitemSearchObj.run().each(function(result) {
			// .run().each has a limit of 4,000 results
			// log.debug({title: 'GF_RL_RICS_AWS.js:find_item_record', details: 'result.id: ' + result.id});
			if (result_id <= 0) { // using <= here prevents any deactivated items from being the only thing returned by this function
				flg_inactive = result.getValue('isinactive');
				if (flg_inactive == false) {
					result_id = result.id;
				} else {
					result_id = -1;
				}
			}
			// log.debug({title: 'GF_RL_RICS_AWS.js:find_item_record', details: 'id: ' + result.id + ', inactive: ' + flg_inactive});
			return true;
		});

		return result_id;
	}

});