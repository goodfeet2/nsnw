/**
 *@NApiVersion 2.1
 *@NScriptType Restlet

Script Name:    GF_RL_RICS.js
Author:         Mark Robinson
Version:        1.0.0
*/
define(['N/record', 'N/error', 'N/log', 'N/search', 'N/task', 'N/format'],
function(record, error, log, search, task, format) {

    // Get a standard NetSuite record
    function rics_get(context) {
        try{
            log.audit({title: '!!! Request: GF_RL_RICS.js:rics_get', details: context});
            if(isEmpty(context)){
                var str_usage = 'The GET method is not configured for this endpoint.';
                var obj_return = {};
                obj_return.message = str_usage;
                log.audit({title: 'Audit Response: GF_RL_RICS.js:rics_get', details: obj_return});
                return obj_return;
            }else{
                return {};
            }
        }catch(ex){
            log.error({title: 'Error: GF_RL_RICS.js:rics_get', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
            var obj_return = {};
            obj_return.error = ex;
            obj_return.error_text = ex.toString() + ' : ' + ex.stack;
            return obj_return;
        }
    }


    // Create a NetSuite record from request params
    function rics_post(context) {
        // sleep(20000); // 20 second delay - for testing latency on remotes side
        try{
            log.audit({title: '!!! Request: GF_RL_RICS.js:rics_post', details: context});
            var obj_return = {};
            if(context.hasOwnProperty('endpoint')){
                // log.audit({title: 'GF_RL_RICS.js:rics_post', details: 'typeof endpoint: ' + (typeof context.endpoint) + ', endpoint: ' + context.endpoint});
                // detect endpoint
                switch(context.endpoint){
                    case 'product':
                        obj_return = process_product_data(context);
                        break;
                    case 'pricing':
                        obj_return = process_pricing_data(context);
                        break;
                    case 'inventory':
                        obj_return = process_inventory_data(context);
                        break;
                    case 'getsales':
                        obj_return = process_getsales_data(context);
                        break;
                    case 'confirmsales':
                        obj_return = process_confirmsales_data(context);
                        break;
                }
            }
            log.audit({title: 'GF_RL_RICS.js:rics_post', details: 'obj_return: ' + JSON.stringify(obj_return)});
            return obj_return;
        }catch(ex){
            log.error({title: 'Error: GF_RL_RICS.js:rics_post', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
            var obj_return = {};
            obj_return.error = ex;
            obj_return.error_text = ex.toString() + ' : ' + ex.stack;
            return obj_return;
        }
    }

    // Upsert a NetSuite record from request param
    function rics_put(context) {
        try{
            log.audit({title: '!!! Request: GF_RL_RICS.js:rics_put', details: context});
            var obj_return = {};
            return obj_return;
        }catch(ex){
            log.error({title: 'Error: GF_RL_RICS.js:rics_put', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
            var obj_return = {};
            obj_return.error = ex;
            obj_return.error_text = ex.toString() + ' : ' + ex.stack;
            return obj_return;
        }
    }
    return {
        get: rics_get,
        post: rics_post,
        put: rics_put
    };

    // =========================== processing functions
    // ====================================================== product
    function process_product_data(_context){
        // set return object
        var tmp_return = {};
        tmp_return.Success = false;

        if(_context.hasOwnProperty('Product') && _context.Product.Action == 'delete'){ // if a product is deleted in RICS we should deactivate it in NetSuite
            try{
                var inventoryitemSearchObj = search.create({
                    type: "inventoryitem",
                    filters: [
                        ["type","anyof","InvtPart"], "AND", 
                        ["vendorname","is",_context.Product.SKU], "AND", 
                        ["isinactive","is","F"]
                    ],
                    columns: [ ]
                });
                inventoryitemSearchObj.run().each(function(result){
                    // .run().each has a limit of 4,000 results
                    // deactivate the inventory item
                    record.submitFields({
                        type: 'inventoryitem',
                        id: result.id,
                        values: { isinactive: true },
                        options: {
                            enableSourcing: false,
                            ignoreMandatoryFields : true
                        }
                    });
                    log.debug({title: 'GF_RL_RICS.js:process_product_data', details: 'deactivated item - item_id: ' + result.id});
                    return true;
                });
                
                // delete any existing on hand records for this item
                var customrecord_rics_inventory_on_handSearchObj = search.create({
                    type: "customrecord_rics_inventory_on_hand",
                    filters: [
                        ["custrecord_rics_ioh_parent_productdetail.custrecord_rics_pd_sku","is",_context.Product.SKU]
                    ],
                    columns: [ ]
                });
                customrecord_rics_inventory_on_handSearchObj.run().each(function(result){
                    // .run().each has a limit of 4,000 results
                    try{
                        record.delete({ type: 'customrecord_rics_inventory_on_hand', id: result.id });
                    }catch(ex){
                        log.error({title: 'Error: GF_RL_RICS.js:process_product_data-delete on hand', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
                    }
                    return true;
                });
                // deactivate the PD and PI as well
                var pd_id = find_product_details(_context.Product.SKU);
                var customrecord_rics_product_itemSearchObj = search.create({
                    type: "customrecord_rics_product_item",
                    filters: [
                        ["custrecord_rics_pi_parent_pd","anyof",pd_id]
                    ],
                    columns: [ ]
                });
                customrecord_rics_product_itemSearchObj.run().each(function(result){
                    // .run().each has a limit of 4,000 results
                    record.submitFields({
                        type: 'customrecord_rics_product_item',
                        id: result.id,
                        values: { isinactive: true },
                        options: {
                            enableSourcing: false,
                            ignoreMandatoryFields : true
                        }
                    }); 
                    return true;
                });
                record.submitFields({
                    type: 'customrecord_rics_product_details',
                    id: pd_id,
                    values: { isinactive: true },
                    options: {
                        enableSourcing: false,
                        ignoreMandatoryFields : true
                    }
                });
                // return data
                tmp_return.LiveProducts = [_context.Product.IntegrationId];
                delete tmp_return.Success;
            }catch(ex){
                // not an item that needs to be deleted
            }
        }else{
            var arr_integration_ids = [];
            for(var prod in _context.Products){
                var product = _context.Products[prod];
                if(arr_integration_ids.indexOf(product.IntegrationId) == -1){
                    arr_integration_ids.push(product.IntegrationId);
                }
            }
            var num_product_count = arr_integration_ids.length;
            // process context data
            // determine product type (Non-Inventory Service (i.e. Main Plan / Bag Fee), Normal Invenotry has no row or column data, Matrix inventory has row and column data)
            /*
            {
                "endpoint":"product",
                "Products":[{
                    "Action":"update",
                    "AllUPCs":["012110"], // unless the UPC hasn't been set
                    "Brand":"Drs OWN",
                    "Category":"Arch Supports||Exerciser",
                    "Color":"GREY",
                    "DisplayOrder":11,
                    "GridColumn":"11 / 367",
                    "GridRow":"W",
                    "IntegrationId":"fd58b500-ef84-4da5-acc4-f79d80c2f97c",
                    "ListPrice":"0",
                    "LongDescription":"ALZNNER",
                    "Price":"429",
                    "ProductId":"6a32c047-98ef-4944-9163-74e7a8b56d99",
                    "RetailPrice":"429",
                    "SKU":"ALZNNER",
                    "ShortDescription":"ALZNNER",
                    "SkuStyle":"",
                    "SupplierSKU":"ALZNNER",
                    "UPC":"012110" // VERY rare that this property is set but it IS possible
                }]
            }
            */
            var arr_Sku_Created_PD = []; // add to this array when a new PD record is created and check it to prevent duplicates
            var arr_Sku_Updated_PD = [];
            for(var prod in _context.Products){
                var product = _context.Products[prod];

                // -- find existing product
                var str_upc = '';
                if(product.hasOwnProperty('AllUPCs')){
                    for(var i in product.AllUPCs){
                        if(str_upc == ''){
                            str_upc = product.AllUPCs[i];
                            break;
                        }
                    }
                }
                if(str_upc != ''){
                    var pi_id = find_product_item(str_upc, product.SKU);
                    if(pi_id > 0){
                        var pi_data = search.lookupFields({
                            type: 'customrecord_rics_product_item',
                            id: parseInt(pi_id),
                            columns: ['custrecord_rics_pi_parent_pd']
                        });
                        var pd_id = pi_data.custrecord_rics_pi_parent_pd[0].value;
                    }
                }else{
                    var pi_id = 0;
                    var pd_id = 0;
                }
                // log.debug({title: 'GF_RL_RICS.js:process_product_data', details: 'pi_id: ' + pi_id + ', pd_id: ' + pd_id});
                // log.debug({title: 'GF_RL_RICS.js:process_product_data', details: 'pi_data: ' + JSON.stringify(pi_data)});
                if(pi_id > 0 && pd_id > 0){
                    // -- if found - update
                    var pd_id = pi_data.custrecord_rics_pi_parent_pd[0].value;
                    // log.debug({title: 'GF_RL_RICS.js:process_product_data', details: 'pd_id: ' + pd_id});
                    // --- if the object doesn't contain IntegrationId it has been deleted from RICS - so we need to deactivate it in NetSuite
                    var pd_val_obj = {};
                    pd_val_obj.custrecord_rics_pd_ns_item_update = '';
                    pd_val_obj.custrecord_rics_pd_suppliername = product.Brand;
                    // pd_val_obj.custrecord_rics_pd_pricing = JSON.stringify({"Cost":0,"RetailPrice":product.RetailPrice,"ActivePrice":product.Price}); // ListPrice
                    pd_val_obj.custrecord_rics_pd_description = product.LongDescription;
                    pd_val_obj.custrecord_rics_pd_summary = product.ShortDescription;
                    pd_val_obj.custrecord_rics_pd_suppliersku = product.SupplierSKU;
                    pd_val_obj.custrecord_rics_pd_product_id = product.ProductId;
                    var pd_upd_id = record.submitFields({
                        type: 'customrecord_rics_product_details',
                        id: pd_id,
                        values: pd_val_obj
                    });
                    // log.debug({title: 'GF_RL_RICS.js:process_product_data', details: 'updated pd_id: ' + pd_id + ', result pd_upd_id: ' + pd_upd_id});
                    var pi_val_obj = {};
                    pi_val_obj.custrecord_rics_pi_column = product.GridColumn;
                    pi_val_obj.custrecord_rics_pi_row = product.GridRow;
                    pi_val_obj.custrecord_rics_pi_integration_id = product.IntegrationId;
                    var pi_upd_id = record.submitFields({
                        type: 'customrecord_rics_product_item',
                        id: pi_id,
                        values: pi_val_obj
                    });
                    // log.debug({title: 'GF_RL_RICS.js:process_product_data', details: 'updated pi_id: ' + pi_id + ', result pi_upd_id: ' + pi_upd_id});
                    // log.debug({title: 'GF_RL_RICS.js:process_product_data', details: 'pd_upd_id: ' + pd_upd_id + ', pi_upd_id: ' + pi_upd_id + ', product.IntegrationId: ' + product.IntegrationId});
                    if(parseInt(pd_upd_id) > 0 && parseInt(pi_upd_id) > 0){
                        arr_Sku_Updated_PD.push(product.IntegrationId);
                    }
                }else{
                    // -- if the item is not found - create the pd and pi records as needed
                    // try to find the details record by the SKU only
                    var pd_id = find_product_details(product.SKU);
                    // log.debug({title: 'GF_RL_RICS.js:process_product_data', details: 'product.SKU: ' + product.SKU + ', arr_Sku_Created_PD: ' + arr_Sku_Created_PD});
                    if(product.hasOwnProperty('IntegrationId') && arr_Sku_Created_PD.indexOf(product.IntegrationId) == -1){
                        if(pd_id > 0){ // we haven't created the PD from the Sku it existed before this run but doesn't have the upc from this product item
                            // add pi to pd
                            var productItem_Record = create_productItem(product, pd_id, str_upc);
                            var create_child_item = record.create({
                                type: 'customrecord_rics_product_item'
                            });
                            for(var prop in productItem_Record){
                                create_child_item.setValue(prop,productItem_Record[prop]);
                            }
                            if(str_upc != ''){
                                create_child_item.save();
                                arr_Sku_Created_PD.push(product.IntegrationId);
                            }
                        }else{
                            // create pd and add pi
                            var productDetails_Record = create_productDetails(_context);
                            var create_parent_item = record.create({
                                type: 'customrecord_rics_product_details'
                            });
                            for(var prop in productDetails_Record){
                                create_parent_item.setValue(prop,productDetails_Record[prop]);
                            }
                            parent_NS_ID = create_parent_item.save();
                            arr_Sku_Created_PD.push(product.IntegrationId);
                            if(str_upc != ''){
                                var productItem_Record = create_productItem(product, parent_NS_ID, str_upc);
                                var create_child_item = record.create({
                                    type: 'customrecord_rics_product_item'
                                });
                                for(var prop in productItem_Record){
                                    create_child_item.setValue(prop,productItem_Record[prop]);
                                }
                                create_child_item.save();
                                arr_Sku_Created_PD.push(product.IntegrationId);
                            }
                        }
                    }else{
                        if(pd_id > 0){
                            // we HAVE created the Sku in this run and we only need to add the product items
                            var productItem_Record = create_productItem(product, pd_id, str_upc);
                            var create_child_item = record.create({
                                type: 'customrecord_rics_product_item'
                            });
                            for(var prop in productItem_Record){
                                create_child_item.setValue(prop,productItem_Record[prop]);
                            }
                            create_child_item.save();
                            arr_Sku_Created_PD.push(product.IntegrationId);
                        }
                    }
                }
            }
            // log.debug({title: 'GF_RL_RICS.js:process_product_data', details: 'arr_Sku_Created_PD.length: ' + arr_Sku_Created_PD.length + ', arr_Sku_Updated_PD.length: ' + arr_Sku_Updated_PD.length});
            if(arr_Sku_Created_PD.length > 0 || arr_Sku_Updated_PD.length > 0){
                var arr_LiveProducts = [];
                for(var pd in arr_Sku_Created_PD){
                    if(arr_LiveProducts.indexOf(arr_Sku_Created_PD[pd]) == -1){
                        arr_LiveProducts.push(arr_Sku_Created_PD[pd]);
                    }
                }
                for(var pd in arr_Sku_Updated_PD){
                    if(arr_LiveProducts.indexOf(arr_Sku_Updated_PD[pd]) == -1){
                        arr_LiveProducts.push(arr_Sku_Updated_PD[pd]);
                    }
                }
                log.debug({title: 'GF_RL_RICS.js:process_product_data', details: 'arr_LiveProducts: ' + arr_LiveProducts});
                log.debug({title: 'GF_RL_RICS.js:process_product_data', details: 'num_product_count: ' + num_product_count + ', arr_LiveProducts.length: ' + arr_LiveProducts.length}); 
                // if(arr_LiveProducts.length >= num_product_count){ // only return the success when we have updated all of the products, otherwise make RICS send it again
                    tmp_return.LiveProducts = arr_LiveProducts;
                    delete tmp_return.Success;
                // }
                if(pd_id > 0){
                    var call_product_js = task.create({
                        taskType: task.TaskType.MAP_REDUCE,
                        scriptId: 'customscript_mr_rics_product',
                        deploymentId: 'customdeploy_mr_rics_product_od',
                        params: { custscript_product_details_id: parseInt(pd_id) }
                    });
                    call_product_js.submit();
                }
            }
        }
        return tmp_return;
    }
    // ====================================================== product
    // ====================================================== pricing
    function process_pricing_data(_context){
        // the pricing end point will be disabled until further notice
        var tmp_return = {};
        tmp_return.Success = false;

        // /*
        // "PriceItems":[
        //     {
        //         "Brand":"New Balance Athletics",
        //         "GridColumn":"7.5",
        //         "GridRow":"2A",
        //         "IntegrationId":"1a72b0dd-a1f2-4377-9126-02452f6e38d6",
        //         "Prices":[
        //             {
        //                 "Amount":125,
        //                 "PriceType":3,
        //                 "StartDate":"/Date(1588809600000+0000)/"
        //             },
        //             {
        //                 "Amount":139.99,
        //                 "PriceType":1,
        //                 "StartDate":"/Date(1560470400000+0000)/"
        //             }
        //         ],
        //         "SKU":"WW928BK3",
        //         "SupplierSKU":"WW928BK3",
        //         "UPC":"191264370708"
        //     },
        //     {
        //         "Brand":"New Balance Athletics",
        //         "GridColumn":"7.5",
        //         "GridRow":"B",
        //         "IntegrationId":"d554c243-cbd3-4e8f-b767-04f750b2bbe1",
        //         "Prices":[
        //             {
        //                 "Amount":139.99,
        //                 "PriceType":1, // Retail Price
        //                 "StartDate":"/Date(1560470400000+0000)/"
        //             },
        //             {
        //                 "Amount":125,
        //                 "PriceType":3, // Active Price
        //                 "StartDate":"/Date(1588809600000+0000)/"
        //             }
        //         ],
        //         "SKU":"WW928BK3",
        //         "SupplierSKU":"WW928BK3",
        //         "UPC":"191264370449"
        //     }
        // ]
        // */

        // // process context data
        // // this endpoint ONLY updates Sales Price on existing items
        // if(_context.hasOwnProperty('PriceItems')){
        //     // count number of skus for successful update
        //     var arr_count_skus = [];
        //     for(var price_item in _context.PriceItems){
        //         var pricing_data = _context.PriceItems[price_item];
        //         var str_sku = (pricing_data.hasOwnProperty('SKU')) ? pricing_data.SKU : '';
        //         if(arr_count_skus.indexOf(str_sku) == -1){
        //             arr_count_skus.push(str_sku);
        //         }
        //     }
        //     var updated_skus = [];
        //     for(var price_item in _context.PriceItems){
        //         var pricing_data = _context.PriceItems[price_item];
        //         var str_sku = (pricing_data.hasOwnProperty('SKU')) ? pricing_data.SKU : '';
        //         var pd_id = find_product_details(str_sku);
        //         if(pd_id > 0 && updated_skus.indexOf(str_sku) == -1){ // all pricing on the SKU should match
        //             var pd_pricing_data = search.lookupFields({
        //                 type: 'customrecord_rics_product_details',
        //                 id: pd_id,
        //                 columns: ['custrecord_rics_pd_pricing', 'custrecord_rics_pd_ns_pricing_update']
        //             });
                    
        //             if(pd_pricing_data.custrecord_rics_pd_pricing != undefined && pd_pricing_data.custrecord_rics_pd_pricing != ''){
        //                 // pricing data exists
        //                 var json_pricing_data = JSON.parse(pd_pricing_data.custrecord_rics_pd_pricing);
        //             }else{
        //                 // build pricing data
        //                 var json_pricing_data = new Object();
        //                 json_pricing_data.Cost = 0;
        //                 json_pricing_data.RetailPrice = 0;
        //                 json_pricing_data.ActivePrice = 0;
        //             }
        //             for(var p in pricing_data.Prices){
        //                 var rics_price = pricing_data.Prices[p];
        //                 if(rics_price.PriceType == 1){
        //                     // set Retail Price
        //                     json_pricing_data.RetailPrice = rics_price.Amount;
        //                 }
        //                 if(rics_price.PriceType == 3){
        //                     // set Active Price
        //                     json_pricing_data.ActivePrice = rics_price.Amount;
        //                 }
        //             }
        //             if((json_pricing_data.ActivePrice == 0 || json_pricing_data.ActivePrice == '') && json_pricing_data.RetailPrice != 0){
        //                 json_pricing_data.ActivePrice = json_pricing_data.RetailPrice;
        //             }
        //             var id = record.submitFields({
        //                 type: 'customrecord_rics_product_details',
        //                 id: pd_id,
        //                 values: {
        //                     custrecord_rics_pd_pricing: JSON.stringify(json_pricing_data),
        //                     custrecord_rics_pd_ns_pricing_update: new Date(),
        //                     custrecord_rics_pd_ns_item_update: ''
        //                 },
        //                 options: {
        //                     enableSourcing: false,
        //                     ignoreMandatoryFields : true
        //                 }
        //             });
        //             updated_skus.push(str_sku);
        //             try{
        //                 var call_pricing_js = task.create({
        //                     taskType: task.TaskType.MAP_REDUCE,
        //                     scriptId: 'customscript_mr_rics_product',
        //                     deploymentId: 'customdeploy_mr_rics_product_od',
        //                     params: { custscript_product_details_id: parseInt(pd_id) }
        //                 });
        //                 call_pricing_js.submit();
        //             }catch(ex){
        //                 // if the script is already running this trigger will fail
        //                 // most pricing updates also come through on the Product endpoint
        //             }
        //         }else{
        //             if(updated_skus.indexOf(str_sku) > -1){
        //                 log.audit({title: 'GF_RL_RICS.js:process_pricing_data', details: 'pd_id already processed - sku: ' + str_sku + ', pd_id: ' + pd_id});
        //             }else{
        //                 log.audit({title: 'GF_RL_RICS.js:process_pricing_data', details: 'pd_id not found - sku: ' + str_sku});
        //             }
        //         }
        //     }
        //     log.debug({title: 'GF_RL_RICS.js:process_pricing_data', details: 'updated_skus.length: ' + updated_skus.length + ', arr_count_skus.length: ' + arr_count_skus.length});
        //     if(updated_skus.length >= arr_count_skus.length){
        //         tmp_return.Success = true;
        //     }
        // }

        return tmp_return;
    }
    // ====================================================== pricing
    // ====================================================== inventory
    function process_inventory_data(_context){
        var tmp_return = {};
        // process context data
        tmp_return.Success = false;
        /*
        "InventoryItems": [{
            "Brand": "string",
            "CustomData": [{
                "Key": "string",
                "Value": "string"
            ]},
            "GridColumn": "string",
            "GridRow": "string",
            "IntegrationId": "string",
            "QOH": integer, (optional - not included if value is zero)
            "SKU": "string",
            "StoreItems": [{
                "OnHand": integer, (optional - not included if value is zero)
                "StoreCode": integer
            ]},
            "SupplierSKU": "string",
            "UPC": "string"
        }]
        */
        // find RICS Product Item by sku and upc
        // update RICS Product Details IntegrationId
        // update RICS Product Item QOH field
        // get Inventory Item from RICS Product Details
        // loop through all of the locations - any location not found in the StoreItems array should be set to zero
        // create an Inventory Adjustment Worksheet record for the item
        return tmp_return;
    }
    // ====================================================== inventory
    // ====================================================== getsales
    function process_getsales_data(_context){
        var tmp_return = {};
        // process context data
        tmp_return.Success = false;
        return tmp_return;
    }
    // ====================================================== getsales
    // ====================================================== confirmsales
    function process_confirmsales_data(_context){
        var tmp_return = {};
        // process context data
        tmp_return.Success = false;
        return tmp_return;
    }
    // ====================================================== confirmsales
    // ======================================================================================================================================= extra functions
    function isEmpty(_map) {
        for(var key in _map) {
            if (_map.hasOwnProperty(key)) {
                return false;
            }
        }
        return true;
    }

    function sleep(_ms){
        var start = new Date().getTime();
        var end = new Date().getTime() + _ms;
        do{
            start = new Date().getTime();
        }while((end - start) >= 0);
        return true;
    }

    function find_product_item(_upc, _sku){
        var result = 0;
        // log.debug({title: 'GF_RL_RICS.js:find_product_item', details: '_upc: ' + _upc + ', _sku: ' + _sku});
        // find the Product Details Record
        var pd_SearchFilters = [
            ["isinactive","is","F"], "AND",
            ["custrecord_rics_pi_parent_pd.custrecord_rics_pd_sku","is",_sku]
        ];
        if(_upc && _upc != ''){
            pd_SearchFilters.push("AND");
            pd_SearchFilters.push(["custrecord_rics_pi_upcs","contains",'"'+_upc+'"']);
        }
        var pd_SearchResults = search.create({
            type: "customrecord_rics_product_item",
            filters: pd_SearchFilters,
            columns: [
                search.createColumn({
                    name: "id",
                    sort: search.Sort.ASC,
                    label: "ID"
                }),
                search.createColumn({name: "internalid"}),
                search.createColumn({name: "custrecord_rics_pi_upc"})
            ]
        }).run();
        var pd_SearchResultSet = pd_SearchResults.getRange({start: 0, end: 1});
        if (pd_SearchResultSet != null && pd_SearchResultSet.length > 0){
            result = pd_SearchResultSet[0].id;
            var result_upc = pd_SearchResultSet[0].getValue("custrecord_rics_pi_upc");
        }
        // log.debug({title: 'GF_RL_RICS.js:find_product_item', details: 'result: ' + result});
        if(_upc === null){ result = result_upc; } // if upc was blank then return only the upc value
        return result;
    }

    function find_product_details(_Sku){
        var result = 0;
        // log.debug({title: 'GF_RL_RICS.js:find_product_details', details: '_Sku: ' + _Sku});
        // find the Product Details Record
        var pd_SearchResults = search.create({
             type: "customrecord_rics_product_details",
             filters: [
                 ["isinactive","is","F"], "AND",
                 ["custrecord_rics_pd_sku","is",_Sku]
             ],
             columns: [
                 search.createColumn({
                     name: "id",
                     sort: search.Sort.ASC,
                     label: "ID"
                 }),
                 search.createColumn({name: "internalid", label: "Internal ID"})
             ]
         }).run();
         var pd_SearchResultSet = pd_SearchResults.getRange({start: 0, end: 1});
         if (pd_SearchResultSet != null && pd_SearchResultSet.length > 0){
            result = pd_SearchResultSet[0].id;
         }
        //  log.debug({title: 'GF_RL_RICS.js:find_product_details', details: 'result: ' + result});
         return result;
    }

    function find_vendor_name(_Brand){
        var result = '';
        // log.debug({title: 'GF_RL_RICS.js:find_vendor', details: '_Brand: ' + _Brand});
        // find the Vendor Record
        var vendorSearchObj = search.create({
            type: "vendor",
            filters: [
               ["entityid","contains",_Brand], "AND", 
               ["formulanumeric: LENGTH({entityid})","lessthanorequalto","7"]
            ],
            columns: [
               search.createColumn({
                  name: "entityid",
                  sort: search.Sort.ASC
               })
            ]
        }).run();
         var vendor_SearchResultSet = vendorSearchObj.getRange({start: 0, end: 1});
         if (vendor_SearchResultSet != null && vendor_SearchResultSet.length > 0){
            result = vendor_SearchResultSet[0].getValue('entityid');
         }
         // log.debug({title: 'GF_RL_RICS.js:find_vendor', details: 'result: ' + result});
         return result;
    }

    function deactivate_by_parent_id(_parent_item_id){
        // get any sub-matrix items
        var runsearch_inventoryItem_records = search.create({
            type: "inventoryitem",
            filters: [
                ["type","anyof","InvtPart"], "AND", 
                ["isinactive","is","F"], "AND", 
                ["parent","anyof",_parent_item_id]
            ],
            columns: [
                search.createColumn({name: "upccode"})
            ]
        });
        // deactivate all sub-matrix items first
        runsearch_inventoryItem_records.run().each(function(result){
            var id = record.submitFields({
                type: 'inventoryitem',
                id: result.id,
                values: { isinactive: true },
                options: {
                    enableSourcing: false,
                    ignoreMandatoryFields : true
                }
            });
            return true; // this is a required line for the anonymous function
        });
        // deactivate the item record
        var id = record.submitFields({
            type: 'inventoryitem',
            id: _parent_item_id,
            values: { isinactive: true },
            options: {
                enableSourcing: false,
                ignoreMandatoryFields : true
            }
        });
    }

    function create_productDetails(_data){
        // log.debug({title: 'GF_RL_RICS.js:create_productDetails', details: '_data: ' + JSON.stringify(_data)});
        var result = new Object();
        result.custrecord_rics_pd_sku = (_data.Products[0].SKU != null) ? _data.Products[0].SKU : '';
        result.custrecord_rics_pd_suppliersku = (_data.Products[0].SupplierSKU != null) ? _data.Products[0].SupplierSKU : '';
        result.custrecord_rics_pd_style = (_data.Products[0].SkuStyle != null) ? _data.Products[0].SkuStyle : '';
        result.custrecord_rics_pd_summary = (_data.Products[0].ShortDescription != null) ? _data.Products[0].ShortDescription : '';
        result.custrecord_rics_pd_description = (_data.Products[0].LongDescription != null) ? _data.Products[0].LongDescription : '';
        result.custrecord_rics_pd_suppliername = (_data.Products[0].Brand != null) ? _data.Products[0].Brand : '';
        result.custrecord_rics_pd_suppliercode = (_data.Products[0].Brand != null) ? find_vendor_name(_data.Products[0].Brand) : '';
        result.custrecord_rics_pd_avail_pos_on = format.parse({ value : new Date(), type : format.Type.DATE});
        result.custrecord_rics_pd_colors = (_data.Products[0].GridRow != null) ? '[{"TagTree":"' + _data.Products[0].GridRow + '"}]' : '';
        if(_data.Products[0].hasOwnProperty('Category') && _data.Products[0].Category != ''){
            var str_class = _data.Products[0].Category;
            var strip_str = str_class.replace(/([||])+/gm, '|');
            var arr_tags = strip_str.split('|');
            var str_result = '';
            for(var str = (arr_tags.length-1); str != -1; str--){
                str_result += '{"TagTree":"'+arr_tags[str]+'"}';
                arr_tags[str-1] = arr_tags[str-1]+'|'+arr_tags[str];
            }
            var add_commas = str_result.replace(/(}{)+/gm, '},{');
            str_result = '['+add_commas+']';
            result.custrecord_rics_pd_classes = str_result;
        }
        // result.custrecord_rics_pd_pricing = JSON.stringify({"Cost":0,"RetailPrice":_data.Products[0].RetailPrice,"ActivePrice":_data.Products[0].Price}); // ?? ListPrice
        var json = new Object();
        json.Sku = result.custrecord_rics_pd_sku;
        json.Style = result.custrecord_rics_pd_style;
        json.Summary = result.custrecord_rics_pd_summary;
        json.Description = result.custrecord_rics_pd_description;
        json.Alert = '';
        json.SupplierSku = result.custrecord_rics_pd_suppliersku;
        json.AvailableToPOSOn = '';
        json.CreatedOn = '';
        json.SupplierCode = result.custrecord_rics_pd_suppliercode;
        json.SupplierName = result.custrecord_rics_pd_suppliername;
        json.LabelType = '';
        json.ColumnName = '';
        json.RowName = '';
        var arr_All_Products = [];
        for(var prod in _data.Products){
            var prod_data = _data.Products[prod];
            var prodObj = new Object();
            prodObj.Column = prod_data.GridColumn;
            prodObj.ColumnOrdinal = '';
            prodObj.Row = prod_data.GridRow;
            prodObj.RowOrdinal = '';
            prodObj.UPC = (prod_data.hasOwnProperty('AllUPCs')) ? prod_data.AllUPCs[0] : '';
            prodObj.UPCs = prod_data.AllUPCs;
            arr_All_Products.push(prodObj);
        }
        json.ProductItems = arr_All_Products;
        json.Classes = result.custrecord_rics_pd_classes;
        result.custrecord_rics_pd_json = JSON.stringify(json);
        result.custrecord_rics_pd_product_id = _data.Products[0].ProductId;
        if((result.custrecord_rics_pd_json).length > 4000){
            result.custrecord_rics_pd_json = (result.custrecord_rics_pd_json).substring(0, 3950);
        }
        // log.debug({title: 'GF_RL_RICS.js:create_productDetails', details: 'result: ' + JSON.stringify(result)});

        return result;
    }

    function create_productItem(_data, _parentId, _upc){
        var result = new Object();
        result.custrecord_rics_pi_parent_pd = (_parentId != null) ? _parentId : '';
        result.custrecord_rics_pi_column = (_data.GridColumn != null) ? _data.GridColumn : '';
        result.custrecord_rics_pi_row = (_data.GridRow != null) ? _data.GridRow : '';
        result.custrecord_rics_pi_upc = (_upc != null) ? _upc : '';
        // check to make sure the upcs list contains the upc
        if(_data.AllUPCs != undefined && (JSON.stringify(_data.AllUPCs)).indexOf('"'+_upc+'"') == -1){
            var arr_upcs = _data.AllUPCs;
            arr_upcs.push(_upc);
            result.custrecord_rics_pi_upcs = JSON.stringify(arr_upcs);
        }else{
            result.custrecord_rics_pi_upcs = (_data.hasOwnProperty('AllUPCs') && _data.AllUPCs != null) ? JSON.stringify(_data.AllUPCs) : '';
        }
        result.custrecord_rics_pi_integration_id = (_data.IntegrationId != null) ? _data.IntegrationId : '';
        var json = new Object();
        json.Column = result.custrecord_rics_pi_column;
        json.ColumnOrdinal = '';
        json.Row = result.custrecord_rics_pi_row;
        json.RowOrdinal = '';
        json.UPC = result.custrecord_rics_pi_upc;
        json.UPCs = _data.AllUPCs;
        result.custrecord_rics_pi_json = JSON.stringify(json);
        if((result.custrecord_rics_pi_json).length > 4000){
            result.custrecord_rics_pi_json = (result.custrecord_rics_pi_json).substring(0, 3950);
        }
        // log.debug({title: 'GF_RL_RICS.js:create_productItem', details: 'result: ' + JSON.stringify(result)});

        return result;
    }

});