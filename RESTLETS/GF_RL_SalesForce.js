/**
 *@NApiVersion 2.1
 *@NScriptType Restlet

Script Name:    GF_RL_SalesForce.js
Author:         Mark Robinson
Version:        1.1.2
Most Recent:    Added functionality to the POST method that will re-activate a Customer when they are inactive to avoid a possible duplication issue
*/
define(['N/record', 'N/log', 'N/search'],
function(record, log, search) {

    var arr_allowed_overwrite = new Array();
    arr_allowed_overwrite.push('firstname');
    arr_allowed_overwrite.push('lastname');
    arr_allowed_overwrite.push('email');
    arr_allowed_overwrite.push('phone');
    arr_allowed_overwrite.push('custentity_celigo_sfnc_salesforce_id');
    arr_allowed_overwrite.push('custentity_gf_rics_customer_id');
    arr_allowed_overwrite.push('custentity_gf_rics_cust_acct_number');
    arr_allowed_overwrite.push('address');
    arr_allowed_overwrite.push('salesrep');

    // Get a standard NetSuite record
    /**
     * This function is designed to accept json data, and return a json object that contains an id or a useful message about what went wrong
     * @param {*} context
     * @returns json
     */
    function sf_get(context) {
        try{
            log.audit({title: '!!! Request: GF_RL_SalesForce.js:sf_get', details: context});
            if(isEmpty(context)){
                var str_usage = 'USAGE - The following properties are expected: ';
                str_usage += 'rics_id (required) {string} the id from the RICS system that will identify the customer, ';
                str_usage += 'phone (required) {string} the phone number from the RICS system that will identify the customer, ';
                str_usage += 'i.e. { "rics_id": "111a1aa1-2b22-3cc3-d44d-e5555555e55e", "phone": "888-999-1111" }, ';
                str_usage += '-- RETURN: when everything works correctly the NetSuite record id will be returned in JSON fromat.';
                var obj_return = {};
                obj_return.message = str_usage;
                log.audit({title: 'Audit Response: GF_RL_SalesForce.js:sf_get', details: obj_return});
                return obj_return;
            }else{
                var obj_return = {};
                if(context.phone == null){
                    obj_return.message = 'The customer phone number is required';
                    var returnRec = obj_return;
                }
                var customerSearch = search.create({
                    type: 'customer',
                    filters: [
                        ['custentity_gf_rics_customer_id', 'is', context.rics_id], 'and',
                        ['phone', 'is', context.phone], 'and',
                        ['isinactive', 'is', 'F']
                    ],
                    columns: [
                        'firstname',
                        'lastname',
                        'phone',
                        'custentity_gf_rics_customer_id',
                        'custentity_gf_rics_cust_acct_number'
                    ]
                }).run();
              var customerSearchResults = customerSearch.getRange(0, 1000);
              if(customerSearchResults.length > 0){
                    obj_return.id = customerSearchResults[0].id;
                	var returnRec = obj_return;
                }else{
                    obj_return.message = 'No active customer with this RICS ID ( ' + context.rics_id + ' ) and this phone ( ' + context.phone + ' ) has been found.';
                    var returnRec = obj_return;
                }
                log.audit({title: 'Audit Response: GF_RL_SalesForce.js:sf_get', details: (returnRec.hasOwnProperty('id') ? {"id" : returnRec.id} : returnRec)});
                return (returnRec.hasOwnProperty('id') ? {"id" : returnRec.id} : returnRec);
            }
        }catch(ex){
            log.error({title: 'Error: GF_RL_SalesForce.js:sf_get', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
            var obj_return = {};
            obj_return.error = ex;
            obj_return.error_text = ex.toString() + ' : ' + ex.stack;
            return obj_return;
        }
    }


    // Create a NetSuite record from request params
    /**
     * This function is designed to accept json data, and use that data to create a new NetSuite customer record.
     * If the customer already exists the function pass the data to the sf_put function for an update.
     * If the post object is empty the function will return a message about how to use this endpoint.
     * The function returns the id of the newly created customer record.
     * @param {*} context
     * @returns json
     */
    function sf_post(context) {
        // sleep(20000); // 20 second delay - for testing latency on SF side
        if(!isEmpty(context) && context.hasOwnProperty('id') && context.id != null){
            return sf_put(context);
        }
        try{
            log.audit({title: '!!! Request: GF_RL_SalesForce.js:sf_post', details: context});
            if((context).hasOwnProperty('custentity_gf_rics_customer_id')){
                var cust_search_filters = [];
              	cust_search_filters.push(['custentity_gf_rics_customer_id', 'is', context.custentity_gf_rics_customer_id]);
              	if(context.phone != undefined && context.phone != null && context.firstname != undefined && context.firstname != null && context.lastname != undefined && context.lastname != null){
                   	var arr_phone_and_name = [
                      ['phone', 'is', context.phone], 'and',
                      ['firstname', 'is', context.firstname], 'and',
                      ['lastname', 'is', context.lastname]
                    ];
                  	cust_search_filters.push('or');
                  	cust_search_filters.push(arr_phone_and_name);
                }
                var customerSearch = search.create({
                    type: 'customer',
                    filters: cust_search_filters,
                    columns: [
                        'firstname',
                        'lastname',
                        'phone',
                        'custentity_gf_rics_customer_id',
                        'custentity_gf_rics_cust_acct_number',
                        'isinactive'
                    ]
                }).run();
                var customerSearchResults = customerSearch.getRange(0, 1);
                log.audit({title: 'Audit Response: GF_RL_SalesForce.js:ID-Found', details: 'ID: '+ JSON.stringify(customerSearchResults)});
                if(customerSearchResults.length > 0){
                    var flg_cust_inactive = customerSearchResults[0].getValue('isinactive');
                    if(flg_cust_inactive == 'T'){
                        // - re-activate
                        var cust_id = record.submitFields({
                            type: 'customer',
                            id: customerSearchResults[0].id,
                            values: { 'isinactive': false }
                        });
                        log.audit({title: 'Request: GF_RL_SalesForce.js:sf_post', details: 'reactivated found inactive cust id: ' + customerSearchResults[0].id});
                    }
                    context.id = customerSearchResults[0].id;
                    return sf_put(context);
                }
            }
            if(!isEmpty(context)){
                var rec = record.create({
                    type: record.Type.CUSTOMER
                });
                for(var k in context){
                    if(k != 'address'){ // the address is set after the record is created and saved
                        if(typeof context[k] === 'string' && k != 'custentity_gf_rics_customer_id'){
                            // adjust string casing for everything except the rics id
                            if(k != 'email'){
                                if(k == 'phone'){
                                    var editedVal = context[k];
                                    editedVal = '('+editedVal.substr(0,3)+') '+editedVal.substr(3,3)+'-'+editedVal.substr(6,4);
                                }else{
                                    var editedVal = context[k].toUpperCase();
                                }
                            }else{
                                var editedVal = context[k].toLowerCase();
                            }
                            rec.setValue(k, editedVal);
                        }else{
                            rec.setValue(k, context[k]);
                        }
                    }else{
                        var addrObj = context[k];
                    }
                }
                rec.setValue('giveaccess', false);
                rec.setValue('subsidiary', '1'); // primary Subsidiary == MRQD Inc.

                var recordId = rec.save({ ignoreMandatoryFields: true });
                if(recordId && recordId > 0){
                    var rec = record.load({
                        type: record.Type.CUSTOMER,
                        id: recordId,
                        isDynamic: true
                    });
                    if(addrObj != null){
                        setAddressSubRecord(rec, addrObj);
                    }
                    setSubsidiarySublist(rec); // add all other subsidiaries
                    rec.save({ ignoreMandatoryFields: true });
                }

                log.audit({title: 'Audit Response: GF_RL_SalesForce.js:sf_post', details: {"id" : recordId}});
                return {"id" : recordId};
            }else{
                var obj_return = {};
                var str_usage = 'USAGE - The following properties are expected, but generally most of the fields that show up as part of the record object when using the GET method can be set as a property: ';
                    str_usage += 'firstname (required) {string} the customers first name, ';
                    str_usage += 'lastname (required) {string} the customers last name, ';
                    str_usage += 'email (optional) {string} the customers email, ';
                    str_usage += 'phone (optional) {string} the customers phone, ';
                    str_usage += 'custentity_gf_rics_customer_id (optional) {string} the customers RICS ID from the RICS system, ';
                    str_usage += 'custentity_gf_rics_cust_acct_number (optional) {string} the customers RICS account number from the RICS system, ';
                    str_usage += 'address (optional) {object} the customers address object, ';
                    str_usage += 'address.country (optional) {string} the customers two character country code i.e. US, ';
                    str_usage += 'address.addr1 (optional) {string} the customers street address, ';
                    str_usage += 'address.addr2 (optional) {string} the customers suite or apt. number if applicable, ';
                    str_usage += 'address.city (optional) {string} the customers city, ';
                    str_usage += 'address.state (optional) {string} the customers state, ';
                    str_usage += 'address.zip (optional) {string} the customers zip, ';
                    str_usage += '-- RETURN: when everything works correctly the NetSuite ID {integer} will be returned. ';
                    obj_return.message = str_usage;
                obj_return.message = str_usage;
                log.audit({title: 'Audit Response: GF_RL_SalesForce.js:sf_post', details: obj_return});
                return obj_return;
            }
        }catch(ex){
            log.error({title: 'Error: GF_RL_SalesForce.js:sf_post', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
            var obj_return = {};
            obj_return.error = ex;
            obj_return.error_text = ex.toString() + ' : ' + ex.stack;
            // return obj_return;
            return true;
        }
    }


    // Upsert a NetSuite record from request param
    /**
     * This function is designed to accept json data, and use that data to update an existing NetSuite customer record.
     * If the put object is empty the function will return a message about how to use this endpoint.
     * The function returns the id of the recently updated customer record.
     * @param {*} context
     * @returns
     */
    function sf_put(context) {
        try{
            log.audit({title: '!!! Request: GF_RL_SalesForce.js:sf_put', details: context});
            if(!isEmpty(context) && context.hasOwnProperty('id')){
                var rec = record.load({
                    type: record.Type.CUSTOMER,
                    id: context.id,
                    isDynamic: true
                });
                for(var k in context){
                    if(arr_allowed_overwrite.indexOf(k) == -1){ continue; } // control specific field overwrite
                    if(k != 'address'){
                        if(typeof context[k] === 'string' && k != 'custentity_gf_rics_customer_id'){
                            if(k != 'email'){
                                if(k == 'phone'){
                                    var editedVal = context[k];
                                    editedVal = '('+editedVal.substr(0,3)+') '+editedVal.substr(3,3)+'-'+editedVal.substr(6,4);
                                }else{
                                    var editedVal = context[k].toUpperCase();
                                }
                            }else{
                                var editedVal = context[k].toLowerCase();
                            }
                            rec.setValue(k, editedVal);
                        }else{
                            rec.setValue(k, context[k]);
                        }
                    }
                }
                if(context.hasOwnProperty('address')){
                    setAddressSubRecord(rec, context.address);
                }
                setSubsidiarySublist(rec);

                rec.save();
                log.audit({title: 'Audit Response: GF_RL_SalesForce.js:sf_put', details: {"id": rec.id}});
                return {"id": rec.id};
            }else{
                var obj_return = {};
                if(!context.hasOwnProperty('id')){
                    obj_return.message = 'ERROR: The id parameter is missing. This parameter is required and should contain the integer of an active NetSuite Customer record internalid.';
                }
                if(isEmpty(context)){
                    var str_usage = 'USAGE - The following properties are expected, but generally most of the fields that show up as part of the record object when using the GET method can be set as a property: ';
                    str_usage += 'id - (required) {integer} This parameter is required and should contain the integer of an active NetSuite Customer record internalid, ';
                    str_usage += 'email (optional) {string} the customers email, ';
                    str_usage += 'phone (optional) {string} the customers phone, ';
                    str_usage += 'custentity_gf_rics_customer_id (optional) {string} the customers RICS ID from the RICS system, ';
                    str_usage += 'custentity_gf_rics_cust_acct_number (optional) {string} the customers RICS account number from the RICS system, ';
                    str_usage += 'address (optional) {object} the customers address object, ';
                    str_usage += 'address.country (optional) {string} the customers two character country code i.e. US, ';
                    str_usage += 'address.addr1 (optional) {string} the customers street address, ';
                    str_usage += 'address.addr2 (optional) {string} the customers suite or apt. number if applicable, ';
                    str_usage += 'address.city (optional) {string} the customers city, ';
                    str_usage += 'address.state (optional) {string} the customers state, ';
                    str_usage += 'address.zip (optional) {string} the customers zip, ';
                    str_usage += '-- RETURN: when everything works correctly the NetSuite ID {integer} will be returned. ';
                    obj_return.message = str_usage;
                }
                log.audit({title: 'Audit Response: GF_RL_SalesForce.js:sf_put', details: obj_return});
                return obj_return;
            }

        }catch(ex){
            log.error({title: 'Error: GF_RL_SalesForce.js:sf_put', details: `Error ${ex.toString()} : ${ex.stack} - context: ${JSON.stringify(context)}`});
            var obj_return = {};
            obj_return.error = ex;
            obj_return.error_text = ex.toString() + ' : ' + ex.stack;
            // return obj_return;
            return {"id": context.id};;
        }
    }
    return {
        get: sf_get,
        post: sf_post,
        put: sf_put
    };

    // =========================== extra functions
    /**
     * This function is designed to determine if the value passed in contains any data
     * @param {*} _map
     * @returns boolean
     */
    function isEmpty(_map) {
        for(var key in _map) {
            if (_map.hasOwnProperty(key)) {
                return false;
            }
        }
        return true;
    }

    /**
     * This function will set or update the Address subrecord on a customer record
     * @param {*} _rec
     * @param {*} _addrObj
     * @returns true
     */
    function setAddressSubRecord(_rec, _addrObj){
        try{
            var rec = _rec;
            var currentAddressCount = rec.getLineCount({ 'sublistId': 'addressbook' });
            if (currentAddressCount === 0){
                rec.selectNewLine({
                    sublistId: 'addressbook'
                });
            }else{
                rec.selectLine({
                    sublistId: 'addressbook',
                    line: 0
                });
            }

            rec.setCurrentSublistValue({
                sublistId: 'addressbook',
                fieldId: 'label',
                value: (_addrObj.addr1).toUpperCase()
            });

            var addrSubrecord = rec.getCurrentSublistSubrecord({
                sublistId: 'addressbook',
                fieldId: 'addressbookaddress'
            });

            var upperCountry = (typeof _addrObj.country != 'undefined' && _addrObj.country != null) ? (_addrObj.country).toUpperCase() : '';
            var upperAddr1 = (typeof _addrObj.addr1 != 'undefined' && _addrObj.addr1 != null) ? (_addrObj.addr1).toUpperCase() : '';
            var upperCity = (typeof _addrObj.city != 'undefined' && _addrObj.city != null) ? (_addrObj.city).toUpperCase() : '';
            var upperState = (typeof _addrObj.state != 'undefined' && _addrObj.state != null) ? (_addrObj.state).toUpperCase() : '';

            addrSubrecord.setValue({fieldId: 'country', value: upperCountry });
            addrSubrecord.setValue({fieldId: 'addr1', value: upperAddr1 });
            if(_addrObj.hasOwnProperty('addr2')){
                var upperAddr2 = (typeof _addrObj.addr2 != 'undefined' && _addrObj.addr2 != null) ? (_addrObj.addr2).toUpperCase() : '';
                addrSubrecord.setValue({fieldId: 'addr2', value: upperAddr2 });
            }
            addrSubrecord.setValue({fieldId: 'city', value: upperCity });
            addrSubrecord.setValue({fieldId: 'state', value: upperState });
            addrSubrecord.setValue({fieldId: 'zip', value: _addrObj.zip });
            addrSubrecord.setValue({fieldId: 'isnewline', value: true });
            addrSubrecord.setValue({fieldId: 'defaultbilling', value: true });
            addrSubrecord.setValue({fieldId: 'defaultshipping', value: true });
            addrSubrecord.setValue({fieldId: 'addressee', value: rec.getValue({ fieldId: 'firstname' }) + ' ' + rec.getValue({ fieldId: 'lastname' })});

            rec.commitLine({sublistId: 'addressbook'});

            return true;
        }catch(ex){
            log.error({title: 'Error: GF_RL_SalesForce.js:setAddressSubRecord', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
            var obj_return = {};
            obj_return.error = ex;
            obj_return.error_text = ex.toString() + ' : ' + ex.stack;
            return obj_return;
        }
    }

    /**
     * This function will set the subsidiary sublist on a customer record.
     * @param {*} _rec
     * @returns true
     */
    function setSubsidiarySublist(_rec){
        try{
            var rec = _rec;
            var currentSubsidiaryCount = rec.getLineCount({ 'sublistId': 'submachine' });

            var arr_subs = ["7","3","4","2"];
            for(var i = 0; currentSubsidiaryCount > 0 && i < currentSubsidiaryCount; i++){
                // remove the existing subs from the array to write if the already exist in the customer record
                rec.selectLine({ sublistId: 'submachine', line: i });
                var sublistValue = rec.getCurrentSublistValue({
                    sublistId: 'submachine',
                    fieldId: 'subsidiary'
                });
                if(arr_subs.indexOf(sublistValue) > -1){
                    arr_subs.splice(arr_subs.indexOf(sublistValue), 1);
                }
            }

            // write all values remaining to the subsidiary sublist
            for(var i = 0; arr_subs.length > 0 && i < arr_subs.length; i++){
                rec.selectNewLine({ sublistId: 'submachine' });

                rec.setCurrentSublistValue({
                    sublistId: 'submachine',
                    fieldId: 'subsidiary',
                    value: arr_subs[i],
                    ignoreFieldChange: true
                });
                rec.commitLine({sublistId: 'submachine'});
            }

            return true;
        }catch(ex){
            log.error({title: 'Error: GF_RL_SalesForce.js:setSubsidiarySublist', details: 'Error ' + ex.toString() + ' : ' + ex.stack});
            var obj_return = {};
            obj_return.error = ex;
            obj_return.error_text = ex.toString() + ' : ' + ex.stack;
            return obj_return;
        }
    }

    /**
     * This function is designed to create a code based delay for the number of milliseconds entered
     * @param {*} _ms 
     * @returns true
     */
    function sleep(_ms){
        var start = new Date().getTime();
        var end = new Date().getTime() + _ms;
        do{
            start = new Date().getTime();
        }while((end - start) >= 0);
        return true;
    }

});