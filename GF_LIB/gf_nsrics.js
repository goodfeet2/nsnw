/**
 *@NApiVersion 2.1
 *
 Script Name:   GF_NSRICS.js
 Author:        Mark Robinson
 */

define(['N/record', 'N/search', 'N/format'], function (record, search, format) {

    function integration(){
        var integration_obj = new Object;

        var nsrics = record.load({
            type: 'customrecord_rics_integration',
            id: 1
        });
        var str_endpoints = nsrics.getValue('custrecord_rics_integration_apiendpoints');
        var arr_endpoints = str_endpoints.split('|');
        var str_connections = nsrics.getValue('custrecord_rics_integration_connections');
        var arr_connections = str_connections.split('|');
        var active_connection_name = nsrics.getValue('custrecord_rics_integration_active_conn');

        integration_obj.baseurl = nsrics.getValue('custrecord_rics_integration_baseurl');
        integration_obj.arr_endpoints = arr_endpoints;
        integration_obj.arr_connections = arr_connections;
        integration_obj.active_connection_name = active_connection_name;

        // get endpoint as specified
        integration_obj.getEndPoint = function (_ep) {
            var ep_string = '';
            for(var i in arr_endpoints){
                var endpoint = JSON.parse(arr_endpoints[i]);
                var str_eps = endpoint.endpoints;
                var arr_eps = str_eps.split('_');
                for(var ep_name in arr_eps){
                    if(arr_eps[ep_name] === _ep){
                        ep_string = endpoint.parent + '/' + _ep;
                        break;
                    }
                }
            }
            return ep_string;
        };

        // get connection : has override - pass in named connection to use else use active connection from record
        integration_obj.getActiveConnection = function (_conn) {
            if(_conn == undefined){ _conn = active_connection_name; }
            for(var i in arr_connections){
                var conn = JSON.parse(arr_connections[i]);
                if(conn.name === _conn){ return conn.token; }
            }
        };

        // wait until the allotted amount of time has elapsed
        integration_obj.sleep = function(_ms){
            var start = new Date().getTime();
            var end = new Date().getTime() + _ms;
            do{
                start = new Date().getTime();
            }while((end - start) >= 0);
            return true;
        }

        // reformat a RICS date string - example parameter: "2021-09-16T20:39:05.115855" - example return: "09/16/2021 8:39 pm"
        integration_obj.str_reformat_date = function(_str){
            var str_result = '';
            if(_str != undefined && _str != ''){
                var str_month = _str.substring(5,7);
                var str_day = _str.substring(8,10);
                var str_year = _str.substring(0,4);
                var str_hour = _str.substring(11,13);
                var str_ampm = (parseInt(str_hour) > 11) ? ' pm' : ' am';
                str_hour = (parseInt(str_hour) > 12) ? parseInt(str_hour) - 12 : parseInt(str_hour);
                var str_minute = _str.substring(14,16);
                str_result = str_month + '/' + str_day + '/' + str_year + ' ' + str_hour + ':' + str_minute + str_ampm;
            }

            return str_result;
        }

        integration_obj.getOpenPeriodRange = function(){
            var result_obj = {};

            var accountingperiodSearchObj = search.create({
                type: "accountingperiod",
                filters: [
                    ["aplocked","is","F"], "AND",
                    ["arlocked","is","F"], "AND",
                    ["closed","is","F"], "AND",
                    ["isquarter","is","F"], "AND",
                    ["isyear","is","F"], "AND",
                    ["isadjust","is","F"]
                ],
                columns: [
                    "internalid",
                    "periodname",
                    search.createColumn({
                        name: "startdate",
                        sort: search.Sort.ASC
                    }),
                    "enddate"
                ]
            });
            var dt_StartDate = new Date('1-1-9999'); // future
            var dt_EndDate = new Date('1-1-1980'); // past
            accountingperiodSearchObj.run().each(function(result){
                // .run().each has a limit of 4,000 results
                var acct_per_data = result.toJSON();
                try{
                    var dt_acct_per_start = new Date(acct_per_data.values.startdate);
                    var dt_acct_per_end = new Date(acct_per_data.values.enddate);
                    if(dt_acct_per_start < dt_StartDate){ dt_StartDate = new Date(dt_acct_per_start); }
                    if(dt_acct_per_end > dt_EndDate){ dt_EndDate = new Date(dt_acct_per_end); }
                }catch(ex){
                    log.debug({title: 'GF_MR_RICS_VRMA.js:getOpenPeriodRange', details: 'result: ' + JSON.stringify(result.toJSON())});
                }
                return true;
            });

            result_obj.earliest_open_date = format.format({ value : dt_StartDate, type : format.Type.DATE});
            result_obj.latest_open_date = format.format({ value : dt_EndDate, type : format.Type.DATE});

            return result_obj;
        }

        return integration_obj;
    }

    return {
        getIntegration: integration
    };
});