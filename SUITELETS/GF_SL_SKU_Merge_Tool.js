/**
 *@NApiVersion 2.1
 *@NScriptType Suitelet

Script Name:    GF_SL_SKU_Merge_Tool.js
Author:         Mark Robinson
 */
define(['N/log', 'N/runtime', 'N/ui/serverWidget'], function(log, runtime, serverWidget) {

    /**
     * This function serves the SKU Merge Tool form that will allow the system to find all existing refrences to a SKU that is being merged to another SKU, once found
     * the function then deactivates the old RICS Product Details and Product Item records
     * @param {*} context - During the GET method context is only tested once and used to serve the form,
     *                      during the POST method it will contain a file that we will upload to the predefined S3 bucket
     * @returns void
     */
    function onRequest(context) {
        if (context.request.method === 'GET') {
            // ============================================== PRESENT FORM
            var form = serverWidget.createForm({
                title: 'SKU Merge Tool'
            });

            // var field = form.addField({
            //     id: 'custpage_file',
            //     type: 'file',
            //     label: 'Document'
            // });

            form.addSubmitButton({
                label: 'Merge'
            });

            context.response.writePage(form);

        }else{
            // ============================================== PROCESS DATA
            try{
                // logContextFileData(_context);
                var isProduction = (runtime.envType == 'PRODUCTION') ? true : false;
                var awsData = new Object();
                awsData.s3bucketName = (isProduction) ? 'blix-clients' : 'zf-testdrv'; // in production this will be 'blix-clients'
                awsData.s3bucketRegion = (isProduction) ? 'ap-southeast-2' : 'us-west-2'; // in production this will be 'ap-southeast-2'
                awsData.s3bucketFolder = (isProduction) ? '/production/import-api/Goodfeet/Transactions/' : '/uploads/'; // in production this will be '/production/import-api/Goodfeet/Transactions/' -- recommended file naming scheme follows this format transactions_2018-09-06.csv
                awsData.s3bucket_URI = awsData.s3bucketName + '.s3.' + awsData.s3bucketRegion + '.amazonaws.com' + awsData.s3bucketFolder;
                awsData.awsAccessKeyId = (isProduction) ? 'AKIAXANQSIDCHZAJFZMM' : 'AKIAUELA5XKMUSF6Y75E'; // replace
                awsData.awsSecretKey = (isProduction) ? '5HCQuQ8TFDn+voobyIIzU48wi6fnYfdhtKtpmCEm' : 'GjCDPplvfvkx4dIZx7Y/9Cjyz9l7dHS7/rIwowvs'; // replace
                awsData.awsRegion = awsData.s3bucketRegion;
                awsData.host = awsData.s3bucketName + '.s3.'+ awsData.awsRegion + '.amazonaws.com';
                awsData.NetSuiteFolderID = (isProduction) ? 3204 : 3354; // in production this will be set to the ID of the AWS-Upload folder : this folder is used during file upload errors as a means of understanding what happened with the upload
                
                var flg_FileIsBig = false;
                if(context.request.files.custpage_file != undefined){
                    if(context.request.files.custpage_file.size >= 10485760){ // file should be smaller than 10 MB -- next understand how to send larger files
                        flg_FileIsBig = true;
                        if(context.request.files.custpage_file.size >= 2147483648){ // file should be smaller than 2 GB otherwise display error and stop processing
                            var str_html_page = '<!DOCTYPE html><html><head><title>Upload Successful</title></head>';
                            str_html_page += '<body><div style="margin:auto;width:90%;padding-top:2vh;"><h3 onclick="window.history.go(-1); return false;" style="Tahoma,Geneva,sans-serif; font-size: 30px; font-weight:bold; color:#995c74;">Upload Failed</h3>';
                            str_html_page += '<p>File size exceeds 2 GB limit</p><p><a href="#" onclick="window.history.go(-1); return false;">BACK</a></p></div></body>';
                            str_html_page += '</html>';
                            log.error({title: 'GF_SL_File_Upload.js:Upload Failed', details: 'File Size (bytes): ' + context.request.files.custpage_file.size});
                            context.response.write(str_html_page);
                            return false;
                        }
                    }
                }else{
                    // no file loaded
                    var str_html_page = '<!DOCTYPE html><html><head><title>Upload Successful</title></head>';
                    str_html_page += '<body><div style="margin:auto;width:90%;padding-top:2vh;"><h3 onclick="window.history.go(-1); return false;" style="Tahoma,Geneva,sans-serif; font-size: 30px; font-weight:bold; color:#995c74;">Upload Failed</h3>';
                    str_html_page += '<p>No File was selected</p><p><a href="#" onclick="window.history.go(-1); return false;">BACK</a></p></div></body>';
                    str_html_page += '</html>';
                    log.error({title: 'GF_SL_File_Upload.js:Upload Failed', details: 'No File was selected'});
                    context.response.write(str_html_page);
                    return false;
                }

                var str_filename = context.request.files.custpage_file.name;
                var str_encoded_filename = rfc3986EncodeURIComponent(str_filename);
                log.debug({title: 'GF_SL_File_Upload.js: send POST', details: 'str_filename: ' + str_filename});

                // upload to the netsuite file cabinet
                var fileObj = context.request.files.custpage_file;
                fileObj.folder = awsData.NetSuiteFolderID;
                var f_id = fileObj.save();
                var file_to_upload = file.load({ id:f_id });
                if(!flg_FileIsBig){
                    var file_contents = file_to_upload.getContents();
                    // log.audit({title: 'GF_SL_File_Upload.js: READ FILE', details: 'file_contents: ' + JSON.stringify(file_contents)});
                }else{
                    try{
                    var file_reader = file_to_upload.getReader();
                    var file_contents = '';
                    do{
                        var next100Characters = file_reader.readChars(100);
                        file_contents += next100Characters;
                    }while(next100Characters != null);
                    // log.audit({title: 'GF_SL_File_Upload.js: READ FILE', details: 'file_reader: ' + JSON.stringify(file_contents)});
                    }catch(ex){
                        // failed to load the file contents
                        var str_html_page = '<!DOCTYPE html><html><head><title>Upload Successful</title></head>';
                        str_html_page += '<body><div style="margin:auto;width:90%;padding-top:2vh;"><h3 onclick="window.history.go(-1); return false;" style="Tahoma,Geneva,sans-serif; font-size: 30px; font-weight:bold; color:#995c74;">Upload Failed</h3>';
                        str_html_page += '<p>Failed to load the file contents. Please try again.</p><p><a href="#" onclick="window.history.go(-1); return false;">BACK</a></p></div></body>';
                        str_html_page += '</html>';
                        log.error({title: 'GF_SL_File_Upload.js:Upload Failed', details: 'File Size (bytes): ' + context.request.files.custpage_file.size});
                        context.response.write(str_html_page);
                        return false;
                    }
                }
                
                // upload to S3 directly
                var amzDate = getAmzDate();
                var datestamp = amzDate.split("T")[0];
                var signingKey = getSignatureKey(awsData.awsSecretKey, datestamp, awsData.awsRegion, 's3');
                var payloadHash = crypto.createHash({ algorithm: crypto.HashAlg.SHA256 });
                payloadHash.update({ input: file_contents });
                var amzContentSha256 = payloadHash.digest().toLowerCase();

                // ************* TASK 1: CREATE A CANONICAL REQUEST *************
                /** (AWS example)
                    PUT
                    /test%24file.text

                    date:Fri, 24 May 2013 00:00:00 GMT
                    host:examplebucket.s3.amazonaws.com
                    x-amz-content-sha256:44ce7dd67c959e0d3524ffac1771dfbba87d2b6b4b4e99e42034a8b803f8b072
                    x-amz-date:20130524T000000Z

                    date;host;x-amz-content-sha256;x-amz-date;x-amz-storage-class
                    44ce7dd67c959e0d3524ffac1771dfbba87d2b6b4b4e99e42034a8b803f8b072
                */
                var canonicalUri = 'https://' + awsData.s3bucket_URI + str_encoded_filename;
                var str_date = '';
                // var canonicalHeaders = 'content-type:application/json\n';
                var canonicalHeaders = 'date:' + str_date + '\n';
                canonicalHeaders += 'host:' + awsData.host + '\n';
                canonicalHeaders += 'x-amz-content-sha256:' + amzContentSha256 + '\n';
                canonicalHeaders += 'x-amz-date:' + amzDate + '\n';
                var signedHeaders = 'date;host;x-amz-content-sha256;x-amz-date';
                var canonicalRequest = 'PUT\n';
                canonicalRequest += awsData.s3bucketFolder + str_encoded_filename + '\n\n';
                canonicalRequest += canonicalHeaders + '\n';
                canonicalRequest += signedHeaders + '\n';
                canonicalRequest += amzContentSha256;

                log.debug({title: 'GF_SL_File_Upload.js: TASK 1', details: 'canonical request: ' + JSON.stringify(canonicalRequest)});

                // ************* TASK 2: CREATE THE STRING TO SIGN*************
                /** (AWS example)
                    AWS4-HMAC-SHA256
                    20130524T000000Z
                    20130524/us-east-1/s3/aws4_request
                    9e0e90d9c76de8fa5b200d8c849cd5b8dc7a3be3951ddb7f6a76b4158342019d
                */
                var algorithm = 'AWS4-HMAC-SHA256';
                var credentialScope = datestamp + '/' + awsData.s3bucketRegion + '/s3/aws4_request';
                var canonicalHash = crypto.createHash({ algorithm: crypto.HashAlg.SHA256 });
                canonicalHash.update({ input: canonicalRequest });
                var stringToSign = algorithm + '\n';
                stringToSign += amzDate + '\n';
                stringToSign += credentialScope + '\n';
                stringToSign += canonicalHash.digest().toLowerCase();

                log.debug({title: 'GF_SL_File_Upload.js: TASK 2', details: 'string to sign: ' + stringToSign});

                // ************* TASK 3: CALCULATE THE SIGNATURE *************
                var signature = CryptoJS.HmacSHA256(stringToSign, signingKey);
                var authorizationHeader = algorithm + ' ';
                authorizationHeader += 'Credential=' + awsData.awsAccessKeyId + '/' + credentialScope + ', ';
                authorizationHeader += 'SignedHeaders=' + signedHeaders + ', ';
                authorizationHeader +=  'Signature=' + signature.toString();
                log.debug({title: 'GF_SL_File_Upload.js: TASK 3', details: 'authorizationHeader: ' + JSON.stringify(authorizationHeader)});

                var s3_headers = {
                    'x-amz-content-sha256': amzContentSha256,
                    'x-amz-date': amzDate,
                    'Authorization': authorizationHeader
                };
                log.debug({title: 'GF_SL_File_Upload.js: TASK 3', details: 's3_headers: ' + JSON.stringify(s3_headers)});

                var response = https.put({
                    url: canonicalUri,
                    body: file_contents,
                    headers: s3_headers
                });

                log.debug({title: 'GF_SL_File_Upload.js: TASK 3', details: 'response: ' + JSON.stringify(response)});

                var str_success = 'Failed';
                if (response.code == 200) {
                    // show successful message
                    var str_html_page = '<!DOCTYPE html><html><head><title>Upload Successful</title></head>';
                    str_html_page += '<body><div style="margin:auto;width:90%;padding-top:2vh;"><h3 onclick="window.history.go(-1); return false;" style="Tahoma,Geneva,sans-serif; font-size: 30px; font-weight:bold; color:#5C7499;">Upload Successful</h3>';
                    str_html_page += '<p><a href="#" onclick="window.history.go(-1); return false;">BACK</a></p></div></body></html>';
                    log.audit({title: 'GF_SL_File_Upload.js:Upload Successful', details: 'sent file name: ' + str_filename});
                    str_success = 'Successful';
                }else{
                    // show failure message - try again
                    var str_html_page = '<!DOCTYPE html><html><head><title>Upload Failed</title></head>';
                    str_html_page += '<body><div style="margin:auto;width:90%;padding-top:2vh;"><h3 onclick="window.history.go(-1); return false;" style="Tahoma,Geneva,sans-serif; font-size: 30px; font-weight:bold; color:#995c74;">Upload Failed</h3>';
                    // show error messages
                    str_html_page += '<p>Response Code: ' + response.code + '<br>';
                    str_html_page += '<br>Response Body:<br>&nbsp;&nbsp;&nbsp;<textarea rows="6" style="border:none;width:100%;max-width:100%;">' + response.body + '</textarea></p>';
                    str_html_page += '<br>String To Sign:<br>&nbsp;&nbsp;&nbsp;<textarea rows="6" style="border:none;width:100%;max-width:100%;">' + stringToSign + '</textarea></p>';
                    str_html_page += '<br>Canonical Request:<br>&nbsp;&nbsp;&nbsp;<textarea rows="12" style="border:none;width:100%;max-width:100%;">' + canonicalRequest + '</textarea></p>';
                    str_html_page += '<br>Response Headers:<br>';
                    for(var key in response.headers){
                        str_html_page += '&nbsp;&nbsp;&nbsp;' + key + ': ' + response.headers[key] + '<br>'
                    }
                    str_html_page += '<p><a href="#" onclick="window.history.go(-1); return false;">BACK</a></p></div></body>';
                    str_html_page += '</html>';
                }
                log.error({title: 'GF_SL_File_Upload.js:Upload '+str_success+'-code', details: 'response.code: ' + response.code});
                log.error({title: 'GF_SL_File_Upload.js:Upload '+str_success+'-body', details: 'response.body: ' + JSON.stringify(response.body)});
                log.error({title: 'GF_SL_File_Upload.js:Upload '+str_success+'-sign', details: 'stringToSign: ' + stringToSign});
                log.error({title: 'GF_SL_File_Upload.js:Upload '+str_success+'-sent', details: 'canonicalRequest: ' + canonicalRequest});
                context.response.write(str_html_page);
                file.delete({ id: f_id });

                return null;

            }catch(ex){
                // display an error message to the user prompting them to refresh and try again
                log.error({title: 'GF_SL_File_Upload.js:onRequest', details: 'Error: ' +  ex.toString() + ' : ' + ex.stack});
            }
        }
    }

    return {
        onRequest: onRequest
    };

    // =========================== extra functions
    /**
     * This function returns an AMZ acceptable formatted date as a string
     * @returns string
     */
    function getAmzDate() {
        var amzDate = new Date().toISOString().split('.')[0];
        amzDate = amzDate.replace(/-/g, '').replace(/:/g, '');
        amzDate += 'Z';
        return amzDate;
    }

    /**
     * This function will change the html special characters of a string to meet the rfc3986 standard
     * @param {*} str 
     * @returns 
     */
    function rfc3986EncodeURIComponent(str) {
        return encodeURIComponent(str).replace(/[!'()*]/g, function(c){'%'+c.charCodeAt(0).toString(16)});
    }

    /**
     * This function uses the parameters to create a valid AWS signature
     * @param {*} key 
     * @param {*} dateStamp 
     * @param {*} regionName 
     * @param {*} serviceName 
     * @returns string
     */
    function getSignatureKey(key, dateStamp, regionName, serviceName) {
        log.audit({title: 'GF_SL_File_Upload.js: getSignatureKey', details: 'params - key: ' + key + ', dateStamp: ' + dateStamp + ', regionName: ' + regionName + ', serviceName: ' + serviceName});
        var kDate = CryptoJS.HmacSHA256(dateStamp, 'AWS4'+key);
        var kRegion = CryptoJS.HmacSHA256(regionName, kDate);
        var kService = CryptoJS.HmacSHA256(serviceName, kRegion);
        log.audit({title: 'GF_SL_File_Upload.js: getSignatureKey', details: 'kService: ' + kService});
        return CryptoJS.HmacSHA256('aws4_request', kService);
    }

    /**
     * This function is used for debugging to determine all of the context coming from the request
     * @param {*} _context 
     */
    function logContextFileData(_context){
        log.audit({title: 'GF_SL_File_Upload.js: send POST', details: 'context: ' + JSON.stringify(_context)});
        for(var i in _context){
            log.debug({title: 'GF_SL_File_Upload.js: detail', details: 'key: ' + i + ', data: ' + _context[i]});
            if(i == 'request'){
                for(var req in _context.request){
                    if(JSON.stringify(_context.request[req]) != undefined){
                        log.debug({title: 'GF_SL_File_Upload.js: detail request', details: 'key: ' + req + ', data: ' + JSON.stringify(_context.request[req])});
                    }
                }
            }
            if(i == 'response'){
                for(var res in _context.response){
                    if(JSON.stringify(_context.response[res]) != undefined){
                        log.debug({title: 'GF_SL_File_Upload.js: detail response', details: 'key: ' + res + ', data: ' + JSON.stringify(_context.response[res])});
                    }
                }
            }
        }
        log.audit({title: 'GF_SL_File_Upload.js: send POST', details: 'file: ' + _context.request.files.custpage_file});
    }

});
