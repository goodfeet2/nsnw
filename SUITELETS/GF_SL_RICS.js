/**
 *@NApiVersion 2.x
 *@NScriptType Suitelet

Script Name:    GF_SL_RICS.js
Author:         Mark Robinson
Version:        1.0.0
 */
define(['N/record', 'N/log', 'N/runtime', 'N/format'],
function(record, log, runtime, format) {

    function runForm(context) {
        try{
            var Request = context.request;
            var Response = context.response;
            log.debug({title: 'GF_SL_RICS.js:Suitelet starting'});
            log.debug({title: 'GF_SL_RICS.js:runForm', details: 'Request: ' + JSON.stringify(Request)});

            //Determine User
            var userData = runtime.getCurrentUser();
            var userId = userData.id;
            var roleId = userData.role;
            log.debug({title: 'GF_SL_RICS.js:user data', details: 'userId: ' + userId + ', roleId: ' + roleId + ', userData: ' + JSON.stringify(userData)});

            //Get URL
            var url = pnvl(Request.parameters.custpage_url);

            //Record Entry
            try {
                // could create and save a record here
            } catch (err) {
                // log.error({title: 'Unable to save record', details: 'Error: ' + errText(err)});
            }

            // Build JSON
            var json = { "success":true };

            // //Return and Finalize
            log.debug({title: 'GF_SL_RICS.js:runForm', details: 'json: ' + JSON.stringify(json)});
            Response.write(JSON.stringify(json));
        }catch(ex){
            log.error({title: 'Error: GF_SL_RICS.js:runForm', details: 'Error: ' + ex.toString() + ' : ' + ex.stack});
        }
    }

    return {
        onRequest : runForm
    };

    // =========================== extra functions

    //Ensure proper value type
    function pnvl(value, number) {
        if (number) {
            if (isNaN(parseFloat(value))) return 0;
            return parseFloat(value);
        }
        if (value === null || value === undefined || value === 'null') return '';
        return '' + value;
    }
    
    function errText(_e){
        var txt = '';
        if(_e instanceof nlobjError){
            //this is netsuite specific error
            txt = 'SYM_MobileApp_Restlet.js:NLAPI Error: ' + _e.getCode() + ' :: ' + _e.getDetails() + ' :: ' + _e.getStackTrace().join(', ');
        }else{
            //this is generic javascript error
            txt = 'SYM_MobileApp_Restlet.js:JavaScript/Other Error: ' + _e.toString() + ' : ' + _e.stack;
        }
        return txt;
    }
});
