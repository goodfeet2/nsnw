/**
 *@NApiVersion 2.1
 *@NScriptType Suitelet

Script Name:    GF_SL_Variance.js
Author:         Mark Robinson
 */
define(['N/log', 'N/search', 'N/runtime', 'N/https', 'N/ui/serverWidget', 'N/ui/dialog'], function(log, search, runtime, https, serverWidget, dialog) {

    /**
     * This function serves up the variance form when the request type is GET and processes the form when it's not
     * @param {*} context - During the GET method context is only tested once and used to serve the form,
     *                      during the POST method it will contain a list of IDs that we will use to send data to the lambda function that will query the DataWarehouse for the RICS On Hand information
     * @returns void
     */
    function onRequest(context) {
        // load params
        let currentScript = runtime.getCurrentScript();
        let location_data = getLocationData();
        let data = {};
        data.param_vend = context.request.parameters['custscript_param_vend'];
        data.param_sku = context.request.parameters['custscript_param_sku'];
        data.param_row = context.request.parameters['custscript_param_row'];
        data.param_col = context.request.parameters['custscript_param_col'];
        data.param_loc = context.request.parameters['custscript_param_loc'];

        if (context.request.method === 'GET') {
            // ============================================== PRESENT FORM
            let form = serverWidget.createForm({ title: 'Find Inventory Variance' });
            form.clientScriptFileId = get_GF_CS_Variance_id(); // GF_CS_Variance.js
            // top of form buttons ========================== BUTTONS
            form.addButton({
                id: 'custpage_button_get_items',
                label: 'Get Items',
                functionName: 'getItems'
            });
            form.addButton({
                id: 'custpage_button_create_variance',
                label: 'Create Inventory Adjustments',
                functionName: 'createInventoryAdjustments'
            });
            // add filter fields ========================== FIELDS
            let field_flg_process_adjustments = form.addField({
                id: 'custpage_flg_process_adjustments',
                type: serverWidget.FieldType.CHECKBOX,
                label : 'PROCESS'
            });
            // totals group ========================== GROUP
            let field_group_totals = form.addFieldGroup({
                id : 'custpage_fieldgroup_totals',
                label : 'Totals'
            });
            // Number of items shown
            let field_num_items_shown = form.addField({
                id: 'custpage_num_items_shown',
                type: serverWidget.FieldType.INTEGER,
                label: 'Items Shown',
                container : 'custpage_fieldgroup_totals'
            });
            // Number of selected items
            let field_num_items_selected = form.addField({
                id: 'custpage_num_items_selected',
                type: serverWidget.FieldType.INTEGER,
                label: 'Items Selected',
                container : 'custpage_fieldgroup_totals'
            });
            // Quantity of items shown
            let field_qty_items_shown = form.addField({
                id: 'custpage_qty_items_shown',
                type: serverWidget.FieldType.INTEGER,
                label: 'Qty Shown',
                container : 'custpage_fieldgroup_totals'
            });
            // Quantity of selected items
            let field_qty_items_selected = form.addField({
                id: 'custpage_qty_items_selected',
                type: serverWidget.FieldType.INTEGER,
                label: 'Qty Selected',
                container : 'custpage_fieldgroup_totals'
            });
            // Variance Total
            let field_qty_variance_shown = form.addField({
                id: 'custpage_qty_variance_shown',
                type: serverWidget.FieldType.INTEGER,
                label: 'Variance Qty Shown',
                container : 'custpage_fieldgroup_totals'
            });
            // Selected Variance Total
            let field_qty_variance_selected = form.addField({
                id: 'custpage_qty_variance_selected',
                type: serverWidget.FieldType.INTEGER,
                label: 'Variance Qty Selected',
                container : 'custpage_fieldgroup_totals'
            });
            // Selected Variance Cost
            let field_cost_variance_selected = form.addField({
                id: 'custpage_cost_variance_selected',
                type: serverWidget.FieldType.CURRENCY,
                label: 'Variance Cost Selected',
                container : 'custpage_fieldgroup_totals'
            });

            field_flg_process_adjustments.updateDisplayType({ displayType : serverWidget.FieldDisplayType.NODISPLAY });
            field_num_items_shown.setHelpText({help : "This is the number of items that are listed based on the filter criteria."});
            field_num_items_shown.updateBreakType({breakType : serverWidget.FieldBreakType.STARTCOL});
            field_num_items_shown.defaultValue = 0;
            field_num_items_shown.updateDisplayType({ displayType : serverWidget.FieldDisplayType.INLINE });
            field_num_items_shown.updateLayoutType({ layoutType : serverWidget.FieldLayoutType.STARTROW });
            field_num_items_selected.setHelpText({help : "This is the number of items that are selected from the list."});
            field_num_items_selected.defaultValue = 0;
            field_num_items_selected.updateDisplayType({ displayType : serverWidget.FieldDisplayType.INLINE });
            field_num_items_selected.updateLayoutType({ layoutType : serverWidget.FieldLayoutType.ENDROW });
            field_qty_items_shown.setHelpText({help : "This is the current NetSuite quantity of items that are listed based on the filter criteria."});
            field_qty_items_shown.updateBreakType({breakType : serverWidget.FieldBreakType.STARTCOL});
            field_qty_items_shown.defaultValue = 0;
            field_qty_items_shown.updateDisplayType({ displayType : serverWidget.FieldDisplayType.INLINE });
            field_qty_items_shown.updateLayoutType({ layoutType : serverWidget.FieldLayoutType.STARTROW });
            field_qty_items_selected.setHelpText({help : "This is the current NetSuite quantity of items that are selected from the list."});
            field_qty_items_selected.defaultValue = 0;
            field_qty_items_selected.updateDisplayType({ displayType : serverWidget.FieldDisplayType.INLINE });
            field_qty_items_selected.updateLayoutType({ layoutType : serverWidget.FieldLayoutType.ENDROW });
            field_qty_variance_shown.setHelpText({help : "This is the difference between the RICS quantity and the current NetSuite quantity of items that are listed based on the filter criteria."});
            field_qty_variance_shown.updateBreakType({breakType : serverWidget.FieldBreakType.STARTCOL});
            field_qty_variance_shown.defaultValue = 0;
            field_qty_variance_shown.updateDisplayType({ displayType : serverWidget.FieldDisplayType.INLINE });
            field_qty_variance_shown.updateLayoutType({ layoutType : serverWidget.FieldLayoutType.STARTROW });
            field_qty_variance_selected.setHelpText({help : "This is the difference between the RICS quantity and the current NetSuite quantity of items that are selected from the list."});
            field_qty_variance_selected.defaultValue = 0;
            field_qty_variance_selected.updateDisplayType({ displayType : serverWidget.FieldDisplayType.INLINE });
            field_qty_variance_selected.updateLayoutType({ layoutType : serverWidget.FieldLayoutType.ENDROW });
            field_cost_variance_selected.setHelpText({help : "This is the Cost difference between the RICS quantity and the current NetSuite quantity of items that are selected from the list."});
            field_cost_variance_selected.updateBreakType({breakType : serverWidget.FieldBreakType.STARTCOL});
            field_cost_variance_selected.defaultValue = 0.00;
            field_cost_variance_selected.updateDisplayType({ displayType : serverWidget.FieldDisplayType.INLINE });

            // filter group ========================== GROUP
            let field_group_filter = form.addFieldGroup({
                id : 'custpage_fieldgroup_filters',
                label : 'Filters'
            });
            let field_vend = form.addField({
                id: 'custpage_vend',
                type: serverWidget.FieldType.TEXT,
                label: 'VENDOR',
                container : 'custpage_fieldgroup_filters'
            });
            let field_sku = form.addField({
                id: 'custpage_sku',
                type: serverWidget.FieldType.TEXT,
                label: 'SKU',
                container : 'custpage_fieldgroup_filters'
            });
            let field_row = form.addField({
                id: 'custpage_row',
                type: serverWidget.FieldType.TEXT,
                label: 'ROW',
                container : 'custpage_fieldgroup_filters'
            });
            let field_col = form.addField({
                id: 'custpage_col',
                type: serverWidget.FieldType.TEXT,
                label: 'COL',
                container : 'custpage_fieldgroup_filters'
            });
            let field_loc = form.addField({
                id: 'custpage_location',
                type: serverWidget.FieldType.SELECT,
                label: 'LOCATION',
                // source: -103,
                container : 'custpage_fieldgroup_filters'
            });
            
            field_vend.setHelpText({help : "Enter the VENDOR Code to filter the list by."});
            field_vend.updateBreakType({breakType : serverWidget.FieldBreakType.STARTCOL});
            field_sku.setHelpText({help : "Enter the SKU to filter the list by."});
            field_sku.updateBreakType({breakType : serverWidget.FieldBreakType.STARTCOL});
            field_row.setHelpText({help : "Enter the ROW to filter the list by."});
            field_row.updateBreakType({breakType : serverWidget.FieldBreakType.STARTCOL});
            field_col.setHelpText({help : "Enter the COL to filter the list by."});
            field_col.updateBreakType({breakType : serverWidget.FieldBreakType.STARTCOL});
            field_loc.setHelpText({help : "Enter the Location to filter the list by."});
            field_loc.updateBreakType({breakType : serverWidget.FieldBreakType.STARTCOL});
            setLocationOptions(field_loc);

            // hidden group ========================== GROUP
            let field_json = form.addField({
                id: 'custpage_json',
                type: serverWidget.FieldType.LONGTEXT,
                label: 'JSON'
            });
            field_json.updateDisplayType({ displayType : serverWidget.FieldDisplayType.HIDDEN });

            // list of inventory items  =================== SUBLIST
            let select_items_sublist = createSublist(form, 'items_sublist');

            let flg_filters_set = false;
            for(let p in data){
                log.debug({title: 'GF_SL_Variance.js:onRequest-data[p]', details: 'data[p]: ' +  data[p] + ', p: ' + p});
                if(data[p] != null && data[p] != ''){
                    flg_filters_set = true;
                    if(p == 'param_vend'){ field_vend.defaultValue = data.param_vend; }
                    if(p == 'param_sku'){ field_sku.defaultValue = data.param_sku; }
                    if(p == 'param_row'){ field_row.defaultValue = data.param_row; }
                    if(p == 'param_col'){ field_col.defaultValue = data.param_col; }
                    if(p == 'param_loc'){ field_loc.defaultValue = data.param_loc; }
                }
            }
            if(flg_filters_set){
                populateItemSublist(form, data, location_data);
            }

            context.response.writePage(form);

        }else{
            // ============================================== PROCESS DATA
            try{

            }catch(ex){
                // display an error message to the user prompting them to refresh and try again
                log.error({title: 'GF_SL_Variance.js:onRequest', details: 'Error: ' +  ex.toString() + ' : ' + ex.stack});
            }
        }
    }

    return {
        onRequest: onRequest
    };

    // =========================== extra functions
    /**
     * 
     * @param {*} _form
     * @param {*} _sublistName
     * @returns 
     */
     function createSublist(_form, _sublistName) {
        try {
            let sublistTitle = 'Items';
            let sublist = _form.addSublist({
                id: 'custpage_items_sublist_variance_list',
                type: serverWidget.SublistType.LIST,
                label: sublistTitle
            });

            // columns
            sublist.addField({
                id: 'custpage_items_sublist_flgprocess',
                label: 'Process',
                type: serverWidget.FieldType.CHECKBOX
            });
            sublist.addField({
                id: 'custpage_items_sublist_internal_item_id',
                label: 'Internal ID',
                type: serverWidget.FieldType.TEXT
            });
            let sublistField_Subsidiary = sublist.addField({
                id: 'custpage_items_sublist_subsidiary',
                label: 'SUBSIDIARY',
                type: serverWidget.FieldType.SELECT,
                source: -117
            });
            let sublistField_Location = sublist.addField({
                id: 'custpage_items_sublist_item_loc',
                label: 'LOCATION',
                type: serverWidget.FieldType.SELECT,
                source: -103
            });
            sublist.addField({
                id: 'custpage_items_sublist_vendor_code',
                label: 'Vendor Code',
                type: serverWidget.FieldType.TEXT,
            });
            let sublistField_Class = sublist.addField({
                id: 'custpage_items_sublist_item_class',
                label: 'CLASS',
                type: serverWidget.FieldType.SELECT,
                source: -101
            });
            sublist.addField({
                id: 'custpage_items_sublist_item_sku',
                label: 'SKU',
                type: serverWidget.FieldType.TEXT
            });
            sublist.addField({
                id: 'custpage_items_sublist_item_row',
                label: 'ROW',
                type: serverWidget.FieldType.TEXT
            });
            sublist.addField({
                id: 'custpage_items_sublist_item_col',
                label: 'COL',
                type: serverWidget.FieldType.TEXT
            });
            sublist.addField({
                id: 'custpage_items_sublist_item_upc',
                label: 'UPC',
                type: serverWidget.FieldType.TEXT
            });
            sublist.addField({
                id: 'custpage_items_sublist_ns_item_qty',
                label: 'NS Item Quantity',
                type: serverWidget.FieldType.INTEGER
            });
            sublist.addField({
                id: 'custpage_items_sublist_rics_item_qty',
                label: 'RICS Item Quantity',
                type: serverWidget.FieldType.INTEGER
            });
            sublist.addField({
                id: 'custpage_items_sublist_ns_item_cost',
                label: 'NetSuite Cost',
                type: serverWidget.FieldType.CURRENCY
            });
            sublist.addField({
                id: 'custpage_items_sublist_variance_cost',
                label: 'Variance Cost',
                type: serverWidget.FieldType.CURRENCY
            });
            sublistField_Subsidiary.updateDisplayType({displayType : serverWidget.FieldDisplayType.INLINE});
            sublistField_Location.updateDisplayType({displayType : serverWidget.FieldDisplayType.INLINE});
            sublistField_Class.updateDisplayType({displayType : serverWidget.FieldDisplayType.INLINE});
            sublist.addMarkAllButtons();

            return sublist;
        } catch (ex) {
            log.error({title: 'GF_SL_Variance.js:createSublist', details: 'Error: ' +  ex.toString() + ' : ' + ex.stack});
        }
    };

    /**
     * 
     */
     function populateItemSublist(_form, _data, _loc_data){
        try{
            let _sublistId = 'custpage_items_sublist_variance_list';
            let sublistObj = _form.getSublist({id: _sublistId});
            // get color/row
            let arr_prod_colors = getListValues('customlist_psgss_product_color');
            let row_id = getListId(arr_prod_colors, _data.param_row);
            // get size/column
            let arr_prod_sizes = getListValues('customlist_psgss_product_size');
            let col_id = getListId(arr_prod_sizes, _data.param_col);
            // build filters array
            let searchFilters = [];
            // must be matrix item
            searchFilters.push(["isinactive","is","F"]);
            searchFilters.push("AND");
            searchFilters.push(["parent","noneof","@NONE@"]);
            searchFilters.push("AND");
            searchFilters.push(["upccode","isnotempty",""]);
            searchFilters.push("AND");
            searchFilters.push(["custrecord_gf_vii_item.internalid","anyof","@NONE@"]);
            searchFilters.push("AND");
            searchFilters.push(["inventorylocation.custrecord_gf_location_type","anyof","1"]);
            searchFilters.push("AND");
            searchFilters.push(["formulanumeric: CASE WHEN {inventorylocation} = {custrecord_rics_ioh_ns_matrix_subitem.custrecord_rics_ioh_ns_location} AND {locationquantityonhand} != {custrecord_rics_ioh_ns_matrix_subitem.custrecord_rics_ioh_onhand} THEN 1 ELSE 0 END","greaterthan","0"]);
            // add params
            if(_data.param_vend != null && _data.param_vend != ''){
                searchFilters.push("AND");
                searchFilters.push(["vendor.entityid","is",_data.param_vend]);
            }
            if(_data.param_sku != null && _data.param_sku != ''){
                searchFilters.push("AND");
                searchFilters.push(["vendorname","is",_data.param_sku]);
            }
            if(row_id != null && row_id != '' && row_id != 0){
                searchFilters.push("AND");
                searchFilters.push(["custitem_psgss_product_color","anyof",row_id]);
            }
            if(col_id != null && col_id != '' && col_id != 0){
                searchFilters.push("AND");
                searchFilters.push(["custitem_psgss_product_size","anyof",col_id]);
            }
            if(_data.param_loc != null && _data.param_loc != ''){
                searchFilters.push("AND");
                searchFilters.push(["inventorylocation","anyof",_data.param_loc]);
            }
            let itemSearchObj = search.create({
                type: "item",
                filters: searchFilters,
                columns: [
                    search.createColumn({ name: "itemid", sort: search.Sort.ASC }),
                    // "name",
                    search.createColumn({ name: "entityid", join: "vendor" }),
                    "vendorname",
                    "class",
                    "custitem_psgss_product_color",
                    "custitem_psgss_product_size",
                    "upccode",
                    "inventorylocation",
                    "locationquantityonhand",
                    search.createColumn({
                       name: "custrecord_rics_ioh_onhand",
                       join: "CUSTRECORD_RICS_IOH_NS_MATRIX_SUBITEM"
                    }),
                    "cost",
                    search.createColumn({
                       name: "formulacurrency",
                       formula: "({custrecord_rics_ioh_ns_matrix_subitem.custrecord_rics_ioh_onhand} - {locationquantityonhand}) * {cost}"
                    })
                ]
            });
            let lineNum = 0;
            let total_num_items = 0;
            let total_qty_items = 0;
            let total_variance_items = 0;
            let resultSet = itemSearchObj.run().getRange({start: 0, end: 1000});
            for(let res in resultSet){
                let result = resultSet[res];
                if(lineNum == 0){ log.audit({title: 'GF_SL_Variance.js:populateSublist-params', details: 'result: ' +  JSON.stringify(result.toJSON())}); }
                let ns_internal_id = result.id;
                // let str_item_name = result.getValue('name');
                let class_id = result.getValue('class');
                let vendor_name = result.getValue({ name: "entityid", join: "vendor" });
                let str_sku = result.getValue('vendorname');
                let str_row = result.getText('custitem_psgss_product_color');
                let str_col = result.getText('custitem_psgss_product_size');
                let str_upc = result.getValue('upccode');
                let location_id = result.getValue('inventorylocation');
                let subsidiary_id = _loc_data[location_id];
                let location_qty = result.getValue('locationquantityonhand');
                let rics_onhand = result.getValue({ name: "custrecord_rics_ioh_onhand", join: "CUSTRECORD_RICS_IOH_NS_MATRIX_SUBITEM" });
                let float_item_cost = result.getValue('cost');
                let float_variance_cost = result.getValue({ name: "formulacurrency", formula: "({custrecord_rics_ioh_ns_matrix_subitem.custrecord_rics_ioh_onhand} - {locationquantityonhand}) * {cost}" });
                total_num_items += 1;
                total_qty_items += (location_qty != '') ? parseInt(location_qty) : 0;
                total_variance_items += (((rics_onhand != '') ? parseInt(rics_onhand):0) - ((location_qty != '') ? parseInt(location_qty):0)); // ricsQty - nsQty
                // add row
                // _internal_item_id
                sublistObj.setSublistValue({
                    id: 'custpage_items_sublist_internal_item_id',
                    line: lineNum,
                    value: ((ns_internal_id != undefined && ns_internal_id != '')  ? ns_internal_id : '-')
                });
                // _subsidiary  custpage_items_sublist_subsidiary
                sublistObj.setSublistValue({
                    id: 'custpage_items_sublist_subsidiary',
                    line: lineNum,
                    value: ((subsidiary_id != undefined && subsidiary_id != '') ? parseInt(subsidiary_id) : 1)
                });
                // _item_loc
                sublistObj.setSublistValue({
                    id: 'custpage_items_sublist_item_loc',
                    line: lineNum,
                    value: ((location_id != undefined && location_id != '') ? parseInt(location_id) : 1)
                });
                // _vendor_code
                sublistObj.setSublistValue({
                    id: 'custpage_items_sublist_vendor_code',
                    line: lineNum,
                    value: ((vendor_name != undefined && vendor_name != '') ? vendor_name : '-')
                });
                // _class - custpage_items_sublist_item_class
                sublistObj.setSublistValue({
                    id: 'custpage_items_sublist_item_class',
                    line: lineNum,
                    value: ((class_id != undefined && class_id != '') ? parseInt(class_id) : 1)
                });
                // _item_sku
                sublistObj.setSublistValue({
                    id: 'custpage_items_sublist_item_sku',
                    line: lineNum,
                    value: ((str_sku != undefined && str_sku != '') ? str_sku : '-')
                });
                // _item_row
                sublistObj.setSublistValue({
                    id: 'custpage_items_sublist_item_row',
                    line: lineNum,
                    value: ((str_row != undefined && str_row != '') ? str_row : '-')
                });
                // _item_col
                sublistObj.setSublistValue({
                    id: 'custpage_items_sublist_item_col',
                    line: lineNum,
                    value: ((str_col != undefined && str_col != '') ? str_col : '-')
                });
                // _item_upc
                sublistObj.setSublistValue({
                    id: 'custpage_items_sublist_item_upc',
                    line: lineNum,
                    value: ((str_upc != undefined && str_upc != '') ? str_upc : '-')
                });
                // _ns_item_qty
                sublistObj.setSublistValue({
                    id: 'custpage_items_sublist_ns_item_qty',
                    line: lineNum,
                    value: ((location_qty != undefined && location_qty != '') ? location_qty : '0')
                });
                // rics_onhand
                sublistObj.setSublistValue({
                    id: 'custpage_items_sublist_rics_item_qty',
                    line: lineNum,
                    value: ((rics_onhand != undefined && rics_onhand != '') ? rics_onhand : '0')
                });
                // rics_onhand
                sublistObj.setSublistValue({
                    id: 'custpage_items_sublist_ns_item_cost',
                    line: lineNum,
                    value: ((float_item_cost != undefined && float_item_cost != '') ? parseFloat(float_item_cost) : parseFloat('0.00'))
                });
                // rics_onhand
                sublistObj.setSublistValue({
                    id: 'custpage_items_sublist_variance_cost',
                    line: lineNum,
                    value: ((float_variance_cost != undefined && float_variance_cost != '') ? parseFloat(float_variance_cost) : parseFloat('0.00'))
                });
                lineNum++;
            }
            let field_num_items_shown = _form.getField({id: 'custpage_num_items_shown'});
            let field_qty_items_shown = _form.getField({id: 'custpage_qty_items_shown'});
            let field_qty_variance_shown = _form.getField({id: 'custpage_qty_variance_shown'});
            // update totals defaults
            field_num_items_shown.defaultValue = total_num_items;
            field_qty_items_shown.defaultValue = total_qty_items;
            field_qty_variance_shown.defaultValue = total_variance_items;
        } catch (ex) {
            log.error({title: 'GF_SL_Variance.js:populateSublist', details: 'Error: ' +  ex.toString() + ' : ' + ex.stack});
        }
    }


    function get_GF_CS_Variance_id(){
        let clientScriptFileId = 0;
        let fileSearchObj = search.create({
            type: "file",
            filters: [
                ["name","is","GF_CS_Variance.js"]
            ],
            columns: [
                "internalid"
            ]
        });
        fileSearchObj.run().each(function(result){
            // .run().each has a limit of 4,000 results
            if(clientScriptFileId == 0){
                clientScriptFileId = result.id;
            }
            return true;
        });
        return clientScriptFileId;
    }

    /**
     * 
     * @param {*} _field 
     */
    function setLocationOptions(_field){
        _field.addSelectOption({
            value : '',
            text : ''
        });
        let locationSearchObj = search.create({
            type: "location",
            filters: [
                ["custrecord_gf_location_type","anyof","1"], "AND",
                ["isinactive","is","F"]
            ],
            columns: [
                search.createColumn({
                    name: "name",
                    sort: search.Sort.ASC
                })
            ]
        });
        locationSearchObj.run().each(function(result){
            // .run().each has a limit of 4,000 results
            _field.addSelectOption({
                value : result.id,
                text : result.getValue('name')
            });
            return true;
        });
    }

    /**
     * This function is used to get an array containing the ids and name values of a list in NetSuite.
     * @param {*} _listScriptId - string - the internal id of the list to return.
     * @returns - array - containing all of the ids and values.
     */
     function getListValues(_listScriptId) {
        let searchColumn = search.createColumn({ name : 'name' });
        let listSearch = search.create({ type : _listScriptId, columns : searchColumn });
        let listArray = [];
        listSearch.run().each(function(searchResult) {
            listArray[searchResult.getValue(searchColumn)] = searchResult.id;
            return true;
        });
        return listArray;
    }

    /**
     * 
     * @param {*} _list 
     * @param {*} _value 
     * @returns 
     */
    function getListId(_list, _value){
        if(_list[_value] != undefined && _list.indexOf(_value) == -1){
            return _list[_value];
        }else{
            return 0;
        }
    }

    /**
     *
     * @returns
     */
    function getLocationData(){
		let arr_results = [];
		let arr_sub_ids = [];
		let subsidiarySearchObj = search.create({
			type: "subsidiary",
			filters: [
				["isinactive","is","F"]
			],
			columns: [
				"namenohierarchy"
			]
		});
		subsidiarySearchObj.run().each(function(result){
			// .run().each has a limit of 4,000 results
			let str_sub_name = result.getValue('namenohierarchy');
			arr_sub_ids[str_sub_name] = result.id;
			return true;
		});

		let locationSearchObj = search.create({
			type: "location",
			filters: [
				["isinactive","is","F"]
			],
			columns: [
				'subsidiary'
			]
		});
		locationSearchObj.run().each(function(result){
			// .run().each has a limit of 4,000 results
			let loc_id = result.id;
			if(arr_results[loc_id] == undefined){
				arr_results[loc_id] = parseInt(arr_sub_ids[result.getValue('subsidiary')]);
			}
			return true;
		});
		return arr_results;
	}

});