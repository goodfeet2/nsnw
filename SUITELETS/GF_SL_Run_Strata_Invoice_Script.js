/**
 *@NApiVersion 2.x
 *@NScriptType Suitelet
 *
Script Name:   GF_SL_Run_Strata_Invoice_Script.js
Author:        Mark Robinson
 */
define(["N/task"], function(task) {
    function onRequest(context) {
        executeScheduled();
        var Response = context.response;
        var url = 'https://4942365.app.netsuite.com/app/common/search/searchresults.nl?searchid=1967&saverun=T&whence=';
        var html = '<html><head><title>Redirecting...</title></head>';
        html+= '<body><script>top.location.href = "' + url + '";</script>';
        html+= 'If you are not redirected automatically, please <a href="' + url + '">click here</a>.';
        html+= '</body></html>';
        log.debug({title: 'Redirecting to URL', details: url});
        Response.write(html);
    }

    function executeScheduled() {
        var scriptTask = task.create({
            taskType: task.TaskType.MAP_REDUCE,
            scriptId: "customscript_mr_strata_getinvoicedata",
            deploymentId: "customdeploy_mr_strata_getinvoicedata_od",
            params: {
                custscript_strata_auth_appkey: 'iiUlK+qtf5xTjdj7FHvkwi2XhA9cHfl1d4m4i125epXUtOtgRLi2Hb2cY6qHNgaRxGq3pI+nKlvGNV5c7tuFA5kgBqJjli9IZuZRRQx5XIA5ymetfTsUYzoA7yy2BXVjowJIl1u5gb3hyc43agI1A6NceIBHtLha9BD6wgx7dbE=',
                custscript_strata_subkey: '542f559c80b847eb967dca88b3c313eb'
            }
        });

        var scriptTaskId = scriptTask.submit();

        log.debug("scriptTaskId", scriptTaskId);
    }

    return {
        onRequest: onRequest
    };
});