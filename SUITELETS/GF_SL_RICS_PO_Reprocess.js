/**
 *@NApiVersion 2.1
 *@NScriptType Suitelet
 *
Script Name:   GF_SL_RICS_PO_Reprocess.js
Author:        Mark Robinson
 */
define(['N/log', 'N/task', 'N/search', 'N/ui/serverWidget'], function(log, task, search, serverWidget) {

    /**
     * This function takes a RICS PO Number and pulls the data from RICS, then also processes the scripts that build the NetSuite Purchase Order transaction and Item Receipt
     * @param {*} context - During the GET method we get the PO Number,
     *                      during the POST method we use the PO Number to call the processing scripts
     * @returns void
     */
    function onRequest(context) {
        var request = context.request;
        var data = {};
        data.param_ponum = context.request.parameters['custscript_param_ponum'];
        if (context.request.method === 'GET') {
            // ============================================== PRESENT FORM
            var form = serverWidget.createForm({
                title: 'Process RICS Purchase Order'
            });

            var processing_warning = form.addField({
                id : 'custpage_processing_warning',
                type : serverWidget.FieldType.RICHTEXT,
                label : ' '
            });
            processing_warning.defaultValue = '<h4>When processing Purchase Orders with more than 40 lines, please allow for additional time to process.</h4>';
            processing_warning.updateDisplayType({displayType : serverWidget.FieldDisplayType.INLINE});
            var po_number = form.addField({
                id: 'custpage_ponumber',
                type: serverWidget.FieldType.TEXT,
                label: 'RICS Purchase Order Number'
            });
            po_number.setHelpText({help : "Enter the RICS PURCHASE ORDER NUMBER to get the latest data from RICS and process any updates into NetSuite transactions."});
            po_number.updateLayoutType({ layoutType: serverWidget.FieldLayoutType.OUTSIDEABOVE });
            po_number.isMandatory = true;

            form.addSubmitButton({
                label: 'Process'
            });

            context.response.writePage(form);

        }else{
            // ============================================== PROCESS DATA
            try{
                // log.debug({title: 'GF_SL_RICS_PO_Reprocess.js:onRequest', details: 'context: ' +  JSON.stringify(context)});
                var str_rics_ponum = context.request.parameters.custpage_ponumber;
                log.debug({title: 'GF_SL_RICS_PO_Reprocess.js:onRequest', details: 'str_rics_ponum: ' +  JSON.stringify(str_rics_ponum)});
                var str_html_page = '<h1>Processing Complete</h1><br><p>If you are not redirected automatically, please ';
                str_html_page += '<a href="/app/site/hosting/scriptlet.nl?script=1740&deploy=1">click here</a></p>.';
                str_html_page += '<script>setTimeout(function(){ window.location.href = "/app/site/hosting/scriptlet.nl?script=1740&deploy=1"; }, 3000);</script>';
                context.response.write(str_html_page);

                var getpodata_scriptTask = task.create({
                    taskType: task.TaskType.MAP_REDUCE,
                    scriptId: "customscript_mr_rics_purchase_orders",
                    deploymentId: "customdeploy_mr_rics_po_reproc",
                    params: {
                        custscript_rics_po_number: str_rics_ponum
                    }
                });
                var pull_latest_po_data = getpodata_scriptTask.submit();

                var pull_latest_po_data_taskStatus = task.checkStatus({ taskId: pull_latest_po_data });
                var latest_po_data_complete = false;

                do {
                    pull_latest_po_data_taskStatus = task.checkStatus({ taskId: pull_latest_po_data });
                    if (pull_latest_po_data_taskStatus.status === task.TaskStatus.FAILED) {
                        // show error in logs
                        log.error({title: 'GF_SL_RICS_PO_Reprocess.js:onRequest', details: 'pull_latest_po_data_taskStatus FAILURE: ' +  JSON.stringify(pull_latest_po_data_taskStatus)});
                    } else if (pull_latest_po_data_taskStatus.status === task.TaskStatus.COMPLETE) {
                        // set flag to process next step
                        log.debug({title: 'GF_SL_RICS_PO_Reprocess.js:onRequest', details: 'pull_latest_po_data_taskStatus COMPLETE: ' +  JSON.stringify(pull_latest_po_data_taskStatus)});
                        latest_po_data_complete = true;
                    }

                }while(pull_latest_po_data_taskStatus.status !== task.TaskStatus.COMPLETE && pull_latest_po_data_taskStatus.status !== task.TaskStatus.FAILED);

                if(latest_po_data_complete){

                    var create_and_update_nspo_complete = false;
                    // find the custom record
                    var custom_record_id = get_custom_rec_id_by_po_num(str_rics_ponum);

                    var firstPurchasesRun_scriptTask = task.create({
                        taskType: task.TaskType.MAP_REDUCE,
                        scriptId: "customscript_mr_rics_purchases",
                        deploymentId: "customdeploy_mr_rics_purchases_reproc",
                        params: {
                            custscript_ns_rics_purchase_order_id: custom_record_id
                        }
                    });
                    var create_or_update_nspo = firstPurchasesRun_scriptTask.submit();

                    var create_or_update_nspo_taskStatus = task.checkStatus({ taskId: create_or_update_nspo });

                    do {
                        create_or_update_nspo_taskStatus = task.checkStatus({ taskId: create_or_update_nspo });
                        if (create_or_update_nspo_taskStatus.status === task.TaskStatus.FAILED) {
                            // show error in dialog
                            log.error({title: 'GF_SL_RICS_PO_Reprocess.js:onRequest', details: 'create_or_update_nspo_taskStatus FAILURE: ' +  JSON.stringify(create_or_update_nspo_taskStatus)});
                        }

                    }while(create_or_update_nspo_taskStatus.status !== task.TaskStatus.COMPLETE && create_or_update_nspo_taskStatus.status !== task.TaskStatus.FAILED);

                    // update the line item connections between the custom record and the po
                    var secondPurchasesRun_scriptTask = task.create({
                        taskType: task.TaskType.MAP_REDUCE,
                        scriptId: "customscript_mr_rics_purchases",
                        deploymentId: "customdeploy_mr_rics_purchases_reproc",
                        params: {
                            custscript_ns_rics_purchase_order_id: custom_record_id
                        }
                    });
                    var line_uniquekey_update_nspo = secondPurchasesRun_scriptTask.submit();

                    var line_uniquekey_update_nspo_taskStatus = task.checkStatus({ taskId: line_uniquekey_update_nspo });

                    do {
                        line_uniquekey_update_nspo_taskStatus = task.checkStatus({ taskId: line_uniquekey_update_nspo });
                        if (line_uniquekey_update_nspo_taskStatus.status === task.TaskStatus.FAILED) {
                            // show error in dialog
                            log.error({title: 'GF_SL_RICS_PO_Reprocess.js:onRequest', details: 'line_uniquekey_update_nspo_taskStatus FAILURE: ' +  JSON.stringify(line_uniquekey_update_nspo_taskStatus)});
                        } else if (line_uniquekey_update_nspo_taskStatus.status === task.TaskStatus.COMPLETE) {
                            // set flag to process next step
                            create_and_update_nspo_complete = true;
                            log.debug({title: 'GF_SL_RICS_PO_Reprocess.js:onRequest', details: 'line_uniquekey_update_nspo_taskStatus COMPLETE: ' +  JSON.stringify(line_uniquekey_update_nspo_taskStatus)});
                        }

                    }while(line_uniquekey_update_nspo_taskStatus.status !== task.TaskStatus.COMPLETE && line_uniquekey_update_nspo_taskStatus.status !== task.TaskStatus.FAILED);


                    if(create_and_update_nspo_complete){
                        // finish by updating any receipts
                        var updateporeceipts_scriptTask = task.create({
                            taskType: task.TaskType.MAP_REDUCE,
                            scriptId: "customscript_mr_rics_po_receipt",
                            deploymentId: "customdeploy_mr_rics_po_receipt_od"
                        });
                        var updateporeceipts_task = updateporeceipts_scriptTask.submit();
                    }else{
                        // create_and_update_nspo_complete = FALSE
                        log.audit({title: 'GF_SL_RICS_PO_Reprocess.js:onRequest', details: 'create_and_update_nspo_complete = FALSE: ' +  JSON.stringify(line_uniquekey_update_nspo_taskStatus)});
                    }
                }else{
                    // latest_po_data_complete = FALSE
                    log.audit({title: 'GF_SL_RICS_PO_Reprocess.js:onRequest', details: 'latest_po_data_complete = FALSE: ' +  JSON.stringify(pull_latest_po_data_taskStatus)});
                }

                return null;

            }catch(ex){
                // display an error message to the user prompting them to refresh and try again
                log.error({title: 'GF_SL_RICS_PO_Reprocess.js:onRequest', details: 'Error: ' +  ex.toString() + ' : ' + ex.stack});
            }
        }
    }

    return {
        onRequest: onRequest
    };

    // =========================== extra functions
    /**
     * This function finds the custom record internal id where the custom record has the matching PO Number
     * @param {*} _ponum 
     * @returns {int} inernal id of the custom record
     */
    function get_custom_rec_id_by_po_num(_ponum){
        var result_id = 0;
        var customrecord_rics_purchase_orderSearchObj = search.create({
            type: "customrecord_rics_purchase_order",
            filters: [
                ["custrecord_rics_po_purchaseordernumber","is",_ponum]
            ],
            columns: [
                search.createColumn({
                    name: "internalid",
                    sort: search.Sort.ASC
                })
            ]
        });
        customrecord_rics_purchase_orderSearchObj.run().each(function(result){
            // .run().each has a limit of 4,000 results
            if(result_id == 0){ result_id = result.id; }
            return true;
        });
        return result_id;
    }

});